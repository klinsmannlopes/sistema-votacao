<?php

use Illuminate\Database\Seeder;

class CategoriasFixasSemTroagemSeeder extends Seeder
{
    public function run()
    {
        //REMOVENDO TRIAGEM DAS CATEGORIAS FIXAS
        foreach(\App\Categoria::where('is_fixo', 1)->get() as $row)
        {
            $row->nome = mb_strtoupper($row->nome, 'utf-8');
            $row->is_triagem = 0;
            $row->save();
        }
    }
}
