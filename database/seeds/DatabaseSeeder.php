<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        \App\Edicao::create([
            'nome'=>'GP Verdes Mares',
            'ano' => date('Y'),
            'data_inicio'       => \Carbon\Carbon::now()->addDays('-30'),
            'data_encerramento' => \Carbon\Carbon::now()->addDays('120')
        ]);

        $this->call(UserTableSeeder::class);
        $this->call(CategoriasFixasSeeder::class);
        $this->call(InscricaoPeriodoParametrizacaoSeeder::class);

        if( is_local() )
        {
            $this->call(CategoriasExampleSeeder::class);
            $this->call(CriteriosExampleSeeder::class);
            $this->call(InscricaoExampleSeeder::class);
            $this->call(TriagemExampleSeeder::class);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Model::reguard();
    }
}
