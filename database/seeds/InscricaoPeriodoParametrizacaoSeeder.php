<?php

use App\Parametrizacao as Model;
use Illuminate\Database\Seeder;

class InscricaoPeriodoParametrizacaoSeeder extends Seeder
{
    public function run()
    {
        Model::create(['key' => 'inscricoes_data_inicial', 'value' => date('Y-m-01')]);
        Model::create(['key' => 'inscricoes_data_final', 'value' => date('Y-m-t')]);
    }
}
