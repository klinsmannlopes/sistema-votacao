<?php

use App\Categoria as Model;
use Illuminate\Database\Seeder;

class CategoriasExampleSeeder extends Seeder
{
    public function run()
    {
        factory(Model::class, 10)->create()->each(function($r)
        {
            $range = range(1, 10);

            foreach(array_rand($range, 3) as $index)
                $r->criterios()->attach($range[$index]);
        });
    }
}
