<?php

use Illuminate\Database\Seeder;

class InscricaoExampleSeeder extends Seeder
{
    public function run()
    {
        factory(App\Inscricao::class, 100)->create()->each(function($inscricao)
        {
            if(in_array($inscricao->categoria->abreviatura, ['MID', 'PLA']))
            {
                $lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';

                if($inscricao->categoria->abreviatura == 'PLA')
                {
                    $metas = [
                        'objetivo' => $lorem,
                        'veiculos' => $lorem,
                        'defesa_mix' => $lorem,
                        'contribuicao_especifica_do_trabalho' => $lorem,
                        'grau_de_dificuldade_e_interdepencia' => $lorem,
                        'resultados_obtidos' => $lorem,
                    ];
                }

                if($inscricao->categoria->abreviatura == 'MID')
                {
                    $metas = [
                        'titulo_do_case' => $lorem,
                        'problema_ou_oportunidade' => $lorem,
                        'veiculos_utilizados' => $lorem,
                        'mix_de_veiculos' => $lorem,
                        'publico_alvo' => $lorem,
                        'resumo_da_midia' => $lorem,
                        'total_de_insercoes_domiciliar' => $lorem,
                        'total_de_grp' => $lorem,
                        'alcance_domiciliar' => $lorem,
                        'frequencia_media_domiciliar' => $lorem,
                        'universo_domiciliar' => $lorem,
                        'total_de_insercoes_publico_alvo' => $lorem,
                        'total_tarp' => $lorem,
                        'alcance_public_alvo' => $lorem,
                        'frequencia_media_publico_alvo' => $lorem,
                        'total_de_impactos' => $lorem,
                        'cmp_impactos' => $lorem,
                        'universo_do_target' => $lorem,
                    ];
                }

                foreach($metas as $key => $value)
                {
                    \App\InscricaoMeta::create([
                        'inscricao_id' => $inscricao->id,
                        'key' => $key,
                        'value' => $value,
                    ]);
                }
            }
            else
            {
                $inscricaoPeca = factory(App\InscricaoPeca::class)->create();
                $inscricaoPeca->inscricao_id = $inscricao->id;
                $inscricaoPeca->save();
            }
        });
    }
}