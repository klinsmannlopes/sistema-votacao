<?php

use App\Triagem as Model;

use Illuminate\Database\Seeder;

class TriagemExampleSeeder extends Seeder
{
    public function run()
    {
        $users = \App\User::whereIn('role', [\App\Role::JURADO, \App\Role::JUIZ])->lists('id')->toArray();

        //TRIAGEM - ALEATÓRIA
        $categorias = \App\Categoria::whereNotIn('id', [ 3, 4 ])->get();

        foreach($users as $user_id)
        {
            foreach($categorias as $categoria)
            {
                $categoria_id = $categoria->id;

                $inscricoes = $categoria->inscricoes->lists('id')->toArray();

                $inscricoesAleatorias = array_rand($inscricoes, count($inscricoes) > 10 ? 10 : count($inscricoes));

                foreach($inscricoesAleatorias as $inscricao)
                {
                    if($inscricao_id = $inscricoes[$inscricao])
                        \App\Triagem::create(compact('categoria_id', 'inscricao_id', 'user_id'));
                }
            }
        }
    }
}
