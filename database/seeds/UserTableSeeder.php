<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        \App\User::create([
            'role' => 'master_of_universe',
            'name' => 'Master of Universe',
            'login' => 'marcos',
            'email' => 'teste@gmail.com',
            'password' => \Hash::make('12345'),
            'activated' => 1,
        ]);

        \App\User::create([
            'role' => \App\Role::ADMINISTRADOR,
            'name' => 'Admin',
            'login' => 'admin',
            'email' => 'lopesklismann@gmail.com',
            'password' => \Hash::make('12345'),
            'activated' => 1,
        ]);

        if(is_local())
        {
            factory(App\User::class, 50)->create()->each(function($row)
            {
                if($row->is_participante())
                {
                    $row->agency = $row->name;
                    $row->save();
                }
            });
        }
    }
}
