<?php

use \App\Criterio as Model;
use Illuminate\Database\Seeder;

class CriteriosExampleSeeder extends Seeder
{
    public function run()
    {
        factory(Model::class, 10)->create();
    }
}
