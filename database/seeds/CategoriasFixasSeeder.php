<?php

use App\Categoria as Model;
use Illuminate\Database\Seeder;

class CategoriasFixasSeeder extends Seeder
{
    public function run()
    {
        $midia = Model::create([
            'nome' => 'Mídia',
            'abreviatura' => 'MID',
            'qtd_pecas' => 0,
            'qtd_pecas_tipo' => '',
            'tipos_de_anexo' => '',
            'is_triagem' => 1,
        ]);

        $midia->is_fixo = 1;
        $midia->save();

        $midia = Model::create([
            'nome' => 'Planejamento',
            'abreviatura' => 'PLA',
            'qtd_pecas' => 0,
            'qtd_pecas_tipo' => '',
            'tipos_de_anexo' => '',
            'is_triagem' => 1,
        ]);

        $midia->is_fixo = 1;
        $midia->save();
    }
}
