<?php

use App\Triagem as Model;

use Illuminate\Database\Seeder;

class JulgamentoExampleSeeder extends Seeder
{
    public function run()
    {
        $users = \App\User::whereIn('role', [\App\Role::JURADO, \App\Role::JUIZ])->lists('id')->toArray();
        $categorias = \App\Categoria::whereIn('id', [ 5,6,7 ])->get();

        $notaMinima = ($row = \App\Parametrizacao::where(['key' => 'nota_minima'])->first()) ? $row->value : 0;
        $notaMaxima = ($row = \App\Parametrizacao::where(['key' => 'nota_maxima'])->first()) ? $row->value : 10;

        foreach($users as $user_id)
        {
            foreach($categorias as $categoria)
            {
                $categoria_id = $categoria->id;

                $criterios = $categoria->criterios;
                $inscricoes = $categoria->inscricoes;

                foreach($inscricoes as $inscricao)
                {
                    $inscricao_id = $inscricao->id;

                    $julgamento = \App\Julgamento::create(compact('categoria_id', 'inscricao_id', 'user_id'));

                    $julgamento_id = $julgamento->id;

                    foreach($criterios as $criterio)
                    {
                        $criterio_id = $criterio->id;

                        $nota = rand((int)$notaMinima, (int)$notaMaxima);

                        //DEFINE UMA NOTA FIXA PARA TENTAR SIMULAR UM EMPATE (NEM SEMPRE DÁ)
                        if($categoria->id == 5)
                            $nota = 5;

                        \App\JulgamentoNota::create(compact('julgamento_id', 'criterio_id', 'nota'));
                    }
                }
            }
        }
    }
}
