<?php

$factory->define(App\User::class, function(Faker\Generator $faker)
{
    return [
        'role' => array_rand(\App\Role::all()),
        'name' => $faker->name,
        'email' => $faker->email,
        'address' => $faker->sentence(),
        'phone' => '(85) 988451324',
        'password' => \Hash::make('123123123'),
        'remember_token' => str_random(10),
        'login' => $faker->name,
        'activated' => 1,
    ];
});

$factory->define(App\Criterio::class, function(Faker\Generator $faker)
{
    return [
        'nome' => $faker->name,
    ];
});

$factory->define(App\Categoria::class, function(Faker\Generator $faker)
{
    return [
        'nome' => $name = $faker->firstName,
        'abreviatura' => strtoupper(substr($name, 0, 3)),
        'qtd_pecas' => rand(1,5),
        'qtd_pecas_tipo' => array_rand(\App\Categoria::qtdPecasTipo(), 1),
        'tipos_de_anexo' => serialize([array_rand(\App\Categoria::tiposDeAnexo(), 1)]),
        'is_triagem' => 1,
        'is_multiplo_opec'=>0,
        'qtd_opecs'=>0
    ];
});

$factory->define(App\Inscricao::class, function(Faker\Generator $faker)
{
    $user = App\User::where('role', \App\Role::PARTICIPANTE)->orderBy(\DB::raw('RAND()'))->first();

    return [
        'user_id' => $user->id,
        'categoria_id' => rand(1, 12),
        'responsavel' => $faker->name,
        'cargo' => $faker->firstName,
        'email' => $faker->email,
        'telefone' => '(85) 988451324',
        'anunciante' => $faker->firstName,
        'produto_ou_servico' => $faker->firstName,
        'veiculacao_ini' => date('Y-m-d', $date_ini = mt_rand(1262055681, 1262055681)),
        'veiculacao_fim' => date('Y-m-d', mt_rand($date_ini, 1262055681)),
        'criacao' => $faker->firstName,
        'midia' => $faker->firstName,
        'atendimento' => $faker->firstName,
        'aprovacao' => $faker->firstName,
        'campanha' => $faker->firstName,
    ];
});

$factory->define(App\InscricaoPeca::class, function(Faker\Generator $faker)
{
    $all = \App\Veiculo::all();
    $tipos = \App\Veiculo::getTipos();
    $formatos = \App\Veiculo::getFormatos();

    $tipo = array_rand($tipos, 1);

    $veiculo = $all[$tipo][array_rand($all[$tipo], 1)];
    $formato = $formatos[$tipo][array_rand($formatos[$tipo], 1)];

    $fields = [
        'tipo' => $tipo,
        'titulo' => $faker->name,
        'veiculo' => $veiculo,
        'formato' => $formato,
        'produtora' => $faker->firstName,
        'arquivo' => 'arquivo_nao_existente',
        'pdf' => 'arquivo_nao_existente',
        'pi_numero' => rand(100000, 9999999),
    ];

    if($formato === \App\Veiculo::FORMATO_JORNAL_DIFERENCIADO)
        $fields += ['formato_diferenciado' => $faker->sentence(1)];

    return $fields;
});

