<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscricoesProfissionaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscricoes_profissionais', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nome');
            $table->integer('user_id')->index();
            $table->integer('categoria_id')->index();
            $table->integer('edicao_id')->index();
            $table->string('foto');
            $table->string('curriculo');
            $table->longText('defesa');
            $table->string('status')->default(\App\Inscricao::STATUS_AGUARDANDO_VALIDACAO);
            $table->text('justificativa')->nullable();
            $table->timestamps();

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inscricoes_profissionais');
    }
}
