<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteOnTables extends Migration
{
    public function up()
    {
        if(! Schema::hasColumn($table = 'categorias', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }

        if(! Schema::hasColumn($table = 'criterios', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }

        if(! Schema::hasColumn($table = 'inscricoes', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }

        if(! Schema::hasColumn($table = 'inscricoes_meta', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }

        if(! Schema::hasColumn($table = 'inscricoes_peca', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }

        if(! Schema::hasColumn($table = 'julgamentos', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }

        if(! Schema::hasColumn($table = 'julgamento_notas', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }

        if(! Schema::hasColumn($table = 'triagems', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }

        if(! Schema::hasColumn($table = 'users', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        if(Schema::hasColumn($table = 'categorias', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }

        if(Schema::hasColumn($table = 'criterios', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }

        if(Schema::hasColumn($table = 'inscricoes', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }

        if(Schema::hasColumn($table = 'inscricoes_meta', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }

        if(Schema::hasColumn($table = 'inscricoes_peca', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }

        if(Schema::hasColumn($table = 'julgamentos', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }

        if(Schema::hasColumn($table = 'julgamento_notas', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }

        if(Schema::hasColumn($table = 'triagems', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }

        if(Schema::hasColumn($table = 'users', 'deleted_at'))
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropSoftDeletes();
            });
        }
    }
}