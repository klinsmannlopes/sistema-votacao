<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueIndexs extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->dropUnique('users_email_unique');
            $table->dropUnique('users_login_unique');
        });

        Schema::table('categorias', function (Blueprint $table)
        {
            $table->dropUnique('categorias_abreviatura_unique');
        });

        Schema::table('categoria_criterio', function (Blueprint $table)
        {
            $table->dropUnique('categoria_criterio_categoria_id_criterio_id_unique');
        });

        Schema::table('inscricoes_meta', function (Blueprint $table)
        {
            $table->dropUnique('inscricoes_meta_inscricao_id_key_unique');
        });

        Schema::table('triagems', function (Blueprint $table)
        {
            $table->dropUnique('triagems_categoria_id_inscricao_id_user_id_unique');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->unique('email');
            $table->unique('login');
        });

        Schema::table('categorias', function (Blueprint $table)
        {
            $table->unique('abreviatura');
        });

        Schema::table('categoria_criterio', function (Blueprint $table)
        {
            $table->unique(['categoria_id', 'criterio_id']);
        });

        Schema::table('inscricoes_meta', function (Blueprint $table)
        {
            $table->unique(['inscricao_id', 'key']);
        });

        Schema::table('triagems', function (Blueprint $table)
        {
            $table->unique(['categoria_id', 'inscricao_id', 'user_id']);
        });
    }
}
