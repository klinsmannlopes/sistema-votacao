<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Categoria;

class CreateTableCategorias extends Migration
{
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('nome');
            $table->string('abreviatura', 5)->unique();

            $table->string('qtd_pecas');
            $table->string('qtd_pecas_tipo')->default(Categoria::QTD_PECAS_TIPO_EXATAMENTE);
            $table->string('tipos_de_anexo');

            $table->boolean('is_triagem')->default(1);
            $table->boolean('is_fixo')->default(0);
            $table->boolean('is_ativo')->default(1);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('categorias');
    }
}
