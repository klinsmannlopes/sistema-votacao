<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogPecasTable extends Migration
{
    public function up()
    {
        Schema::create('log_pecas', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->integer('inscricao_id')->unsigned()->index();

            $table->string('acao');
            $table->string('agencia')->nullable();
            $table->string('numero_peca')->nullable();
            $table->string('arquivo');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('log_pecas');
    }
}
