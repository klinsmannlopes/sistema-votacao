<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEdicoesFieldOnTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inscricoes', function (Blueprint $table) {
            $table->unsignedInteger('edicao_id')->after('id')->nullable();
            $table->foreign('edicao_id')->references('id')->on('edicoes');
        });

        Schema::table('julgamentos', function (Blueprint $table) {
            $table->unsignedInteger('edicao_id')->after('id')->nullable();
            $table->foreign('edicao_id')->references('id')->on('edicoes');
        });

        Schema::table('parametrizacao', function (Blueprint $table) {
            $table->unsignedInteger('edicao_id')->after('id')->nullable();
            $table->foreign('edicao_id')->references('id')->on('edicoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inscricoes', function (Blueprint $table) {
            $table->dropForeign(['edicao_id']);
            $table->dropColumn('edicao_id');
        });

        Schema::table('julgamentos', function (Blueprint $table) {
            $table->dropForeign(['edicao_id']);
            $table->dropColumn('edicao_id');
        });

        Schema::table('parametrizacao', function (Blueprint $table) {
            $table->dropForeign(['edicao_id']);
            $table->dropColumn('edicao_id');
        });
    }
}
