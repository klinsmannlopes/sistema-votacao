<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueOnParametrizacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parametrizacao', function (Blueprint $table) {
            $table->dropUnique('parametrizacao_key_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parametrizacao', function (Blueprint $table) {
            $table->unique(['key']);
        });
    }
}
