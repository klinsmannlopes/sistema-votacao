<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscricaoPecasTable extends Migration
{
    public function up()
    {
        Schema::create('inscricoes_peca', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('inscricao_id')->unsigned()->index();
            $table->string('tipo');
            $table->string('titulo');
            $table->string('veiculo');
            $table->string('formato');
            $table->string('formato_diferenciado')->nullable();
            $table->string('produtora');
            $table->string('arquivo');
            $table->string('pdf');

            $table->boolean('pi_rejeitada')->default(0);
            $table->boolean('peca_rejeitada')->default(0);

            $table->string('pi_numero')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('inscricoes_peca');
    }
}