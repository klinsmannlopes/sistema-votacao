<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNumeroInscricaoOnTableLogPecas extends Migration
{
    public function up()
    {
        Schema::table('log_pecas', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->string('numero_inscricao');
        });
    }

    public function down()
    {
        Schema::table('log_pecas', function (Blueprint $table)
        {
            $table->dropColumn('numero_inscricao');
        });
    }
}
