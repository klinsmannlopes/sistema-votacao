<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeleteCascade extends Migration
{
    public function up()
    {
//        Schema::table('inscricoes', function (Blueprint $table)
//        {
//            $table->foreign('user_id')
//                ->references('id')->on('users')
//                ->onDelete('cascade');
//
//            $table->foreign('categoria_id')
//                ->references('id')->on('categorias')
//                ->onDelete('cascade');
//        });
//
//        Schema::table('inscricoes_peca', function (Blueprint $table)
//        {
//            $table->foreign('inscricao_id')
//                ->references('id')->on('inscricoes')
//                ->onDelete('cascade');
//        });
//
//        Schema::table('julgamentos', function (Blueprint $table)
//        {
//            $table->dropIndex('julgamentos_categoria_id_index');
//            $table->dropIndex('julgamentos_inscricao_id_index');
//            $table->dropIndex('julgamentos_user_id_index');
//
//            $table->foreign('categoria_id')
//                ->references('id')->on('categorias')
//                ->onDelete('cascade');
//
//            $table->foreign('inscricao_id')
//                ->references('id')->on('inscricoes')
//                ->onDelete('cascade');
//
//            $table->foreign('user_id')
//                ->references('id')->on('users')
//                ->onDelete('cascade');
//        });
//
//        Schema::table('triagems', function (Blueprint $table)
//        {
//            $table->dropIndex('triagems_categoria_id_index');
//            $table->dropIndex('triagems_inscricao_id_index');
//            $table->dropIndex('triagems_user_id_index');
//
//            $table->foreign('categoria_id')
//                ->references('id')->on('categorias')
//                ->onDelete('cascade');
//
//            $table->foreign('inscricao_id')
//                ->references('id')->on('inscricoes')
//                ->onDelete('cascade');
//
//            $table->foreign('user_id')
//                ->references('id')->on('users')
//                ->onDelete('cascade');
//        });
    }

    public function down()
    {
//        Schema::table('inscricoes', function (Blueprint $table)
//        {
//            $table->dropForeign('inscricoes_user_id_foreign');
//            $table->index('user_id');
//        });
//
//        Schema::table('inscricoes_peca', function (Blueprint $table)
//        {
//            $table->dropForeign('inscricoes_peca_inscricao_id_foreign');
//            $table->index('inscricao_id');
//        });
//
//        Schema::table('julgamentos', function (Blueprint $table)
//        {
//            $table->dropForeign('julgamentos_categoria_id_foreign');
//            $table->dropForeign('julgamentos_inscricao_id_foreign');
//            $table->dropForeign('julgamentos_user_id_foreign');
//
//            $table->index('categoria_id');
//            $table->index('inscricao_id');
//            $table->index('user_id');
//        });
//
//        Schema::table('triagems', function (Blueprint $table)
//        {
//            $table->dropForeign('triagems_categoria_id_foreign');
//            $table->dropForeign('triagems_inscricao_id_foreign');
//            $table->dropForeign('triagems_user_id_foreign');
//
//            $table->index('categoria_id');
//            $table->index('inscricao_id');
//            $table->index('user_id');
//        });
    }
}
