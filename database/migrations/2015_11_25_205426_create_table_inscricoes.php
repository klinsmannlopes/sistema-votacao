<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInscricoes extends Migration
{
    public function up()
    {
        Schema::create('inscricoes', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();
            $table->integer('user_opec_id')->unsigned()->index()->nullable();
            $table->integer('categoria_id')->unsigned()->index();

            $table->string('status')->default(\App\Inscricao::STATUS_AGUARDANDO_VALIDACAO);
            $table->text('justificativa')->nullable();

            $table->string('responsavel');
            $table->string('cargo');
            $table->string('email');
            $table->string('telefone');
            $table->string('anunciante');
            $table->string('produto_ou_servico');

            $table->date('veiculacao_ini');
            $table->date('veiculacao_fim');

            $table->string('criacao');
            $table->string('midia');
            $table->string('atendimento');
            $table->string('aprovacao');
            $table->string('campanha');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('inscricoes');
    }
}
