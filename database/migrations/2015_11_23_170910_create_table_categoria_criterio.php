<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategoriaCriterio extends Migration
{
    public function up()
    {
        Schema::create('categoria_criterio', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('categoria_id')->unsigned()->index();
            $table->integer('criterio_id')->unsigned()->index();

            $table->unique(['categoria_id', 'criterio_id']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('categoria_criterio');
    }
}