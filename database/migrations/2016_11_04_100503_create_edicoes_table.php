<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdicoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edicoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('ano');
            $table->timestamp('data_inicio')->nullable();
            $table->timestamp('data_encerramento')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('edicoes');
    }
}
