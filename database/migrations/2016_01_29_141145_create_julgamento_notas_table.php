<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJulgamentoNotasTable extends Migration
{
    public function up()
    {
        Schema::create('julgamento_notas', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('julgamento_id')->unsigned()->index();
            $table->integer('criterio_id')->unsigned()->index();

            $table->float('nota');
        });
    }

    public function down()
    {
        Schema::drop('julgamento_notas');
    }
}
