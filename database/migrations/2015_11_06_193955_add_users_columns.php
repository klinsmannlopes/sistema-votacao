<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->boolean('activated', 1);

            $table->string('role')->after('id');
            $table->string('agency')->nullable();
            $table->string('login')->unique()->after('role');
            $table->string('cnpj')->nullable()->after('activated');
            $table->string('address')->nullable()->after('cnpj');
            $table->string('phone')->nullable()->after('address');

            $table->text('director_partner')->nullable()->after('phone');

            $table->datetime('logged_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
