<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJulgamentosTable extends Migration
{
    public function up()
    {
        Schema::create('julgamentos', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('categoria_id')->unsigned()->index();
            $table->integer('inscricao_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();

            $table->integer('ordem')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('julgamentos');
    }
}
