<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableParametrizacao extends Migration
{
    public function up()
    {
        Schema::create('parametrizacao', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('key')->unique();
            $table->string('value');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('parametrizacao');
    }
}