<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInscricoesMeta extends Migration
{
    public function up()
    {
        Schema::create('inscricoes_meta', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('inscricao_id')->unsigned()->index();

            $table->string('key');
            $table->text('value');

            $table->unique(['inscricao_id', 'key']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('inscricoes_meta');
    }
}
