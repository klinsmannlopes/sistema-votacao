<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriagemsTable extends Migration
{
    public function up()
    {
        Schema::create('triagems', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('categoria_id')->unsigned()->index();
            $table->integer('inscricao_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();

            $table->unique(['categoria_id', 'inscricao_id', 'user_id']);

            $table->integer('ordem')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('triagems');
    }
}
