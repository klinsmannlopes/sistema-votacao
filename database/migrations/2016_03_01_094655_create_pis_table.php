<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pis', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('owner');
            $table->integer('index')->nullable();
            $table->string('pdf')->nullable();
            $table->string('pi_numero')->nullable();
            $table->boolean('pi_rejeitada')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pis');
    }
}
