<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoggableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loggable', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->morphs('loggable');
            $table->string('action')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->longText('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loggable');
    }
}
