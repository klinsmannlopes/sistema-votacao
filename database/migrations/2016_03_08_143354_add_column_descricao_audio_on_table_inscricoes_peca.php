<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDescricaoAudioOnTableInscricoesPeca extends Migration
{
    public function up()
    {
        Schema::table('inscricoes_peca', function (Blueprint $table)
        {
            $table->text('descricao_audio')->after('pi_numero')->nullable();
        });
    }

    public function down()
    {
        Schema::table('inscricoes_peca', function (Blueprint $table)
        {
            $table->dropColumn('descricao_audio');
        });
    }
}
