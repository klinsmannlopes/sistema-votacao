<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutTable extends Migration
{
    public function up()
    {
        Schema::create('layout', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('key')->unique();
            $table->string('value');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('layout');
    }
}
