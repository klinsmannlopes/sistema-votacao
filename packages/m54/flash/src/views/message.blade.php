@if(Session::has('flash_notifications'))
    @foreach(Session::get('flash_notifications') as $level => $messages)
        @foreach($messages as $message)
            <div class="alert alert-{{ $level }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! $message !!}
            </div>
        @endforeach
    @endforeach
    <?php Session::forget('flash_notifications'); ?>
@endif
