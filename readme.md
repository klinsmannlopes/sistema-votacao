# **Configurar o ambiente**
1. Localize o arquivo no projeto *.env.example*, faça uma cópia e renomeio para *.env*
2. Adicione as configurações do ambiente no arquivo *.env*
3. Adicione os Tokens para o Captcha. Leia mais: [Google reCAPTCHA](https://www.google.com/recaptcha/intro/index.html)
4. Adicione as configurações de envio de email. Solicite ao seu servidor de hospedagem. 


# **Iniciando a instalação**

Execute o comando via console

```
composer install
```


> pressione `enter` se exigir token github
## **Criando banco de dados**

Execute o comando via console

```
php artisan migrate --seed
```

> caso ocorra algum problema de “**not found**”, executar os seguintes procedimentos:

```
composer dump-autoload
php artisan migrate:rollback
php artisan migrate --seed
```

Execute o comando via console para publicar as configurações necessárias

```
php artisan vendor:publish
```

Crie um chave de autenticação para o projeto laravel. Necessário permissões de administrador. Utilize o comando `sudo`

```
sudo php artisan key:generate
```

```
Acessar a URL http://localhost/sistema-votacao/public
Acessos de usuário.
Usuário: admin
Senha: 12345
```