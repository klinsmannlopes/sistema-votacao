<?php

namespace App\ViewComposers;
use App\Edicao;
use Illuminate\View\View;

/**
 * StatusBarComposer.php
 *
 * @author Anderson Andrade <anderson.andrade@geq.com.br>
 */
class StatusBarComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $edicoes = array();

        foreach (Edicao::ativas()->all() as $edicao){
            $edicoes[$edicao->id] = sprintf('%s (%s)', $edicao->nome, $edicao->ano);
        }

        $view->with('edicoes', $edicoes);

        if( $edicaoAtual = getEdicaoAtual() ){
            $view->with('edicaoAtual', $edicaoAtual);
        }else{
            $view->with('edicaoAtual', null);
        }
    }
}