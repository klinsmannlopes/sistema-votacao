<?php

namespace App\ViewComposers;
use App\Edicao;
use App\Parametrizacao;
use Illuminate\View\View;

/**
 * PeriodoInscricaoComposer.php
 *
 * @author Anderson Andrade <anderson.andrade@geq.com.br>
 */
class PeriodoInscricaoComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(\Schema::hasTable('parametrizacao')) {
            $view->with('periodoDeInscricao', app('periodoDeInscricao'));
        }
    }
}