<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PI
 *
 * @package   App
 * @author    Anderson Andrade <anderson.andrade@geq.com.br>
 * @copyright Grupo Edson Queiroz
 */
class PI extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'pis';

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'pi_rejeitada'=>'boolean'
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'index',
        'pdf',
        'pi_numero',
        'pi_rejeitada'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function owner()
    {
        return $this->morphTo();
    }
}