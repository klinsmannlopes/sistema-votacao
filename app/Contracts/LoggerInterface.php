<?php 
namespace App\Contracts;

interface LoggerInterface {

		public function getLoggableTitle();
		public function getLoggableValuesUpdate();
		public function getLoggableDescription($action = null);
		public function getLoggableData();
}