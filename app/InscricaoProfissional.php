<?php
/**
 * Created by PhpStorm.
 * User: 750371426
 * Date: 11/01/2017
 * Time: 16:26
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EdicaoContexTrait;

use App\Presenters\InscricaoPresenter;
use Laracasts\Presenter\PresentableTrait;

class InscricaoProfissional extends  Model
{
    use EdicaoContexTrait;

    protected $table = "inscricoes_profissionais";

    use PresentableTrait;
    protected $presenter = InscricaoPresenter::class;

    protected $fillable = [
        'nome',
        'user_id',
        'categoria_id',
        'edicao_id',
        'defesa',
        'status',
        'foto',
        'curriculo'
    ];

    const STATUS_AGUARDANDO_VALIDACAO = 'Aguardando Validação';
    const STATUS_APROVADA = 'Aprovada';
    const STATUS_CANCELADA = 'Cancelada pelo Administrador';
    const STATUS_CANCELADA_PELO_PARTICIPANTE = 'Cancelada pelo Participante';


    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function limitCategoria()
    {
       return \DB::table($this->table)
              ->select('categoria_id')
              ->where('user_id', \Auth::id())
              ->where('status',  '<>',self::STATUS_CANCELADA)
              ->where('status','<>',self::STATUS_CANCELADA_PELO_PARTICIPANTE)
              ->get();
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function is_aguardando_validacao()
    {
        return $this->status === self::STATUS_AGUARDANDO_VALIDACAO;
    }

    public function is_cancelada()
    {
        return $this->status === self::STATUS_CANCELADA;
    }

    public function is_cancelada_pelo_participante()
    {
        return $this->status === self::STATUS_CANCELADA_PELO_PARTICIPANTE;
    }
}