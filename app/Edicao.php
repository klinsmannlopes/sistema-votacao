<?php 
namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Edicao
 *
 * @package   App
 * @author    Anderson Andrade <anderson.andrade@geq.com.br>
 * @copyright Grupo Edson Queiroz
 */
class Edicao extends Model
{
    const FOREIGN_KEY = 'edicao_id';

    protected $table = 'edicoes';

    protected $fillable = [
        'nome',
        'ano',
        'tipo',
        'data_inicio',
        'data_encerramento'
    ];

    protected $casts = [
    	'ano'=>'integer',
    	'data_inicio'=>'date',
    	'data_encerramento'=>'date',
    ];
    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */


    public function users()
    {
        return $this->belongsToMany('App\User', 'users_edicoes', 'edicao_id', 'user_id');
    }

    public function categorias()
    {
        return $this->belongsToMany('App\Categoria', 'categorias_edicoes', 'edicao_id', 'categoria_id');
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAtivas($query)
    {
        $dt = Carbon::now();

        return $query->where('data_inicio','<=',$dt)
            ->where('data_encerramento','>=',$dt)
            ->get();
    }
}