<?php

namespace App\Providers;

use App\Categoria;
use App\Inscricao;
use App\InscricaoPeca;
use App\Julgamento;
use App\LogPeca;
use App\User;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Listeners\LoggerListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CreatedUserEvent' => [
            'App\Handlers\Events\SendMailResetPasswordHandle',
        ],
        'App\Events\ActivatedUserEvent' => [
            'App\Handlers\Events\SendMailResetPasswordHandle',
        ],
        'App\Events\CreatedInscricaoEvent' => [
            'App\Handlers\Events\RenameFilesInscricaoHandle',
        ],
        'App\Events\UpdatedInscricaoEvent' => [
            'App\Handlers\Events\RenameFilesInscricaoHandle',
        ],
        'App\Events\RenameInscricaoProfissionalEvent'=>[
            'App\Handlers\Events\RenameFileInscricaoProfissionalHandle',
        ],
        'App\Events\CreatedInscricaoProfissionalPDFEvent'=>[
            'App\Handlers\Events\InscricaoProfissionalPDFHandle',
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        $events->subscribe(LoggerListener::class);

        User::deleted(function($row)
        {
            $row->inscricoes->each(function($inscricao)
            {
                $inscricao->delete();
            });
        });

        Inscricao::deleted(function($row)
        {
            $row->metas->each(function($meta)
            {
                $meta->delete();
            });

            $row->pecas->each(function($peca)
            {
                $peca->delete();
            });

            $row->triagems->each(function($triagem)
            {
                $triagem->delete();
            });

            $row->julgamentos->each(function($julgamento)
            {
                $julgamento->delete();
            });
        });

        Julgamento::deleted(function($row)
        {
            $row->notas->each(function($nota)
            {
                $nota->delete();
            });
        });

        InscricaoPeca::deleted(function($row)
        {
            $inscricao = $row->inscricao;
            $usuario = $inscricao->user;

            LogPeca::create([
                'user_id' => \Auth::user()->id,
                'inscricao_id' => $inscricao->id,
                'acao' => LogPeca::EXCLUIR,
                'agencia' => $usuario->agency,
                'numero_peca' => $row->id,
                'numero_inscricao' => $inscricao->present()->numeroInscricao(),
                'arquivo' => $row->arquivo,
            ]);
        });

        Categoria::deleted(function($row)
        {
            $row->inscricoes->each(function($inscricao)
            {
                $inscricao->delete();
            });
        });
    }
}