<?php

namespace App\Providers;

use App\Parametrizacao;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        \View::composer(['layouts.panel','auth.partials.login-form'], 'App\ViewComposers\StatusBarComposer');
        \View::composer(['*'], 'App\ViewComposers\PeriodoInscricaoComposer');

        \Validator::extend('actual_password', function($attribute, $value, $parameters)
        {
            return \Hash::check($value, \Auth::user()->password);
        });

        \Validator::extend('extensions', function($attribute, $value, $parameters)
        {
            foreach($parameters as $k => $v)
                $parameters[$k] = strtolower($v);

            if($value instanceof UploadedFile)
                return in_array(strtolower($value->getClientOriginalExtension()), $parameters);

            return false;
        });

        \Validator::extend('arraySize', function($attribute, $value, $parameters)
        {
            return is_array($value) && count($value) == $parameters[0];
        });

        \Validator::extend('arrayWithoutEmptyValues', function($attribute, $value, $parameters)
        {
            return arrayWithoutEmptyValues($value);
        });

        \Validator::extend('arrayWithoutRepeatedValues', function($attribute, $value, $parameters)
        {
            return arrayWithoutRepeatedValues($value);
        });

        \Validator::extend('checkNotaMinima', function($attribute, $value, $parameters)
        {
            return checkNotaMinima($value, $parameters[0]);
        });

        \Validator::extend('checkNotaMaxima', function($attribute, $value, $parameters)
        {
            return checkNotaMaxima($value, $parameters[0]);
        });

        \Validator::extend('checkNotaEmpty', function($attribute, $value, $parameters)
        {
            return checkNotaEmpty($value);
        });

        \Validator::extend('before_equal', function($attribute, $value, $parameters)
        {
            return strtotime( str2date(\Input::get($parameters[0])) ) >= strtotime($value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
