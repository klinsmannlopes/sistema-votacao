<?php

namespace App\Providers;

use App\Inscricao;
use App\Policies\InscriaoPolicy;
use App\Role;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Inscricao::class => InscriaoPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        foreach(Role::getPermissions() as $role => $permissions)
        {
            foreach($permissions as $permission)
            {
                $gate->define($permission, function($user) use ($role, $permission)
                {
                    if($user->role === 'master_of_universe' || in_array($permission, Role::getPermissions($user->role)))
                        return true;
                });
            }
        }
    }
}
