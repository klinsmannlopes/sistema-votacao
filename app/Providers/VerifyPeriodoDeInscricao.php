<?php

namespace App\Providers;

use App\Parametrizacao;
use Illuminate\Support\ServiceProvider;

class VerifyPeriodoDeInscricao extends ServiceProvider
{
    public function boot()
    {
        \App::singleton('periodoDeInscricao', function()
        {
            try{
                $ini = Parametrizacao::where('key', 'inscricoes_data_inicial')->first();
                $ini = !empty($ini->value) ? $ini->value : false;

                $fim = Parametrizacao::where('key', 'inscricoes_data_final')->first();
                $fim = !empty($fim->value) ? $fim->value : false;

                $now = date('Y-m-d H:i:s');

                $periodoDeInscricao = $now >= $ini && $now <= $fim;

                return $periodoDeInscricao;
            }
            catch (\Exception $e)
            {
                logger()->error($e->getMessage());
                logger()->error($e);
                return false;
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
