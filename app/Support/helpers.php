<?php

if (! function_exists('app_path_uploads'))
{
    function app_path_uploads($path = '')
    {
        return public_path() .'/uploads/'. $path;
    }
}

if (! function_exists('url_uploads'))
{
    function url_uploads($path = '')
    {
        return url('/uploads/'. $path);
    }
}

if (! function_exists('path_uploads'))
{
    function path_uploads($path = '')
    {
        return '/uploads/'. $path;
    }
}

if (! function_exists('generate_file_name'))
{
    function generate_file_name($extension, $title = false)
    {
        $name = ( ($title) ? $title : strtoupper(uniqid(date('Ydm_His_'))) );
        return "{$name}.{$extension}";
    }
}

if (! function_exists('str2date'))
{
    function str2date($s)
    {
        $s = explode('/', $s);

        if(count($s) !== 3)
            return false;

        return implode('-', [ $s[2], $s[1], $s[0] ]);
    }
}

if (! function_exists('str2datetime'))
{
    function str2datetime($s)
    {
        $s = explode(' ', $s);

        $d = explode('/', $s[0]);

        if(count($d) !== 3)
            return false;

        $t = explode(':', $s[1]);

        if(count($t) !== 3)
            return false;

        return implode( '-', [ $d[2], $d[1], $d[0] ] ) .' '. $s[1];
    }
}

if (! function_exists('is_local'))
{
    function is_local()
    {
        return env('APP_ENV', 'local') === 'local';
    }
}

if (! function_exists('is_production'))
{
    function is_production()
    {
        return ! is_local();
    }
}

if (! function_exists('float2money'))
{
    function float2money($p)
    {
        return number_format($p, 2, ',', '.');
    }
}

if (! function_exists('money2float'))
{
    function money2float($p)
    {
        $p = str_replace('.', '', $p);
        $p = str_replace(',', '.', $p);

        return number_format($p, 2, '.', '');
    }
}

if (! function_exists('arrayWithoutEmptyValues'))
{
    function arrayWithoutEmptyValues($array)
    {
        $count = count($array);
        $countAfterFilter = count(array_filter($array));

        return is_array($array) && !empty($array) && $count === $countAfterFilter;
    }
}

if (! function_exists('arrayWithoutRepeatedValues'))
{
    function arrayWithoutRepeatedValues($array)
    {
        foreach(array_count_values($array) as $qty)
            if($qty > 1)
                return false;

        return true;
    }
}

if (! function_exists('checkNotaMinima'))
{
    function checkNotaMinima($array, $min)
    {
        $flag = true;

        foreach($array as $array2)
        {
            foreach($array2 as $value)
            {
                if($value < $min)
                {
                    $flag = false;
                    break;
                }
            }
        }

        return $flag;
    }
}

if (! function_exists('checkNotaMaxima'))
{
    function checkNotaMaxima($array, $max)
    {
        $flag = true;

        foreach($array as $array2)
        {
            foreach($array2 as $value)
            {
                if($value > $max)
                {
                    $flag = false;
                    break;
                }
            }
        }

        return $flag;
    }
}

if (! function_exists('checkNotaEmpty'))
{
    function checkNotaEmpty($array)
    {
        $flag = true;

        foreach($array as $array2)
        {
            foreach($array2 as $value)
            {
                if(empty($value))
                {
                    $flag = false;
                    break;
                }
            }
        }

        return $flag;
    }
}

if (! function_exists('notasToDB'))
{
    function notasToDB($array)
    {
        foreach($array as $key => $value)
            $array[$key] = str_replace(',', '.', $value);

        return $array;
    }
}

if (! function_exists('array_avg'))
{
    function array_avg(array $array)
    {
        $total = 0;

        foreach($array as $value)
            $total = bcadd($total, $value, 2);

        $qtd = count($array);

        if(!$qtd)
            $retorno = 0;
        else
            $retorno = (float)bcdiv($total, $qtd, 2);

        return (float)number_format($retorno, 2, '.', '');
    }
}

if (! function_exists('nota_presenter'))
{
    function nota_presenter($nota)
    {
        return str_pad(number_format($nota, 2, ',', ''), 5, '0', STR_PAD_LEFT);
    }
}

if (! function_exists('found_combobox_ordenacao'))
{
    function found_combobox_ordenacao($array, $value)
    {
        $flag = false;

        foreach($array as $index => $values)
            if(in_array($value, $values))
                $flag = $index;

        return $flag === false ? false : $flag;
    }
}

if (! function_exists('extractPis'))
{
    /**
     * @param       $row
     * @param       $key
     * @param array $extra
     *
     * @return array
     */
    function extractPis($row, $key, array $extra = [])
    {
        $pis = isset($row[$key]) ? $row[$key] : [];
        $pis = ($pis instanceof \Illuminate\Contracts\Support\Arrayable) ? $pis->toArray() : $pis;

        $pis = array_map(function($item) {
            $item['canUpload']  = true;
            $item['pdf']        = isset($item['pdf-old']) ? $item['pdf-old'] : $item['pdf'];
            return $item;
        }, $pis);

        $pis = array_merge($pis, $extra);

        return $pis;
    }
}

if(! function_exists('is_between'))
{
    function is_between($value, $min, $max)
    {
        $value = (float)str_replace(',', '.', $value);
        $min = (float)str_replace(',', '.', $min);
        $max = (float)str_replace(',', '.', $max);

        return $value >= $min && $value <= $max;
    }
}

if(! function_exists('is_serialized'))
{
    function is_serialized($data)
    {
        $formatos = "";
        if(@unserialize($data) == TRUE)
        {
            $data = unserialize($data);
             foreach ($data as $key => $value) {
                $formatos .= $value."|";
             }
             return $formatos;
        }
       return $data;
    }
}

if(! function_exists('arrayRemove'))
{
    function arrayRemove($arr)
    {
        $arryRemove =['created_at','updated_at','remember_token','logged_at','director_partner','is_triagem','is_fixo','user_opec_id'];
        foreach ($arryRemove as $key => $value)
        {
            if(array_key_exists($value,$arr))
            {
                unset($arr[$value]);
            }
        }
        return $arr;
    }
}


if (! function_exists('getEdicaoAtual'))
{
    /**
     * @return integer
     */
    function getEdicaoAtualId()
    {
        return \Session::get('edicaoAtual');
    }
}

if (! function_exists('getEdicaoAtual'))
{
    /**
     * @return \App\Edicao|null
     */
    function getEdicaoAtual()
    {
        $id = getEdicaoAtualId();

        if( !is_null($id) && !empty($id) )
            return \App\Edicao::find(getEdicaoAtualId());

        return null;
    }
}

if (! function_exists('getNomeEdicao'))
{
    function getNomeEdicao($id)
    {
       $edicao =  \App\Edicao::find($id);
        return $edicao->nome;
    }
}

if (! function_exists('getAgencia'))
{
    function getAgencia($id)
    {
        $users = DB::table('users')
            ->join('inscricoes', function ($join) use($id) {
                $join->on('users.id', '=', 'inscricoes.user_id')
                    ->where('inscricoes.id','=', $id );
            })
            ->get();

        return $users[0]->name;
    }
}

if(! function_exists('getCategoria'))
{
    function getCategoria($id)
    {
        $categoria = App\Categoria::find($id);
        if (!is_edicao_profissional() && isset($categoria->peso) && $categoria->peso > 0) {
            return array('nome' => $categoria->nome, 'has_pis' => $categoria->has_pis, 'peso' => $categoria->peso);
        }

        return array('nome' => $categoria->nome, 'peso' => 0, 'has_pis' => 1);
    }
}

if(! function_exists('orderArrayByField'))
{
    function orderArrayByField($array, $field)
    {
        $sortArray=[];

        foreach($array as $item){
            foreach($item as $key=>$value){
                if(!isset($sortArray[$key])){
                    $sortArray[$key] = array();
                }
                $sortArray[$key][] = $value;
            }
        }
        array_multisort($sortArray[$field],SORT_DESC, $array);

        return $array;
    }
}


if (! function_exists('getSelectCategoria'))
{
    /**
     * @param string $name
     * @param null   $selected
     * @param array  $attrs
     *
     * @return mixed
     */
    function getSelectCategoria($name = "categoria_id", $selected = null, array $attrs = array(), $viewCategories = false)
    {

        if($viewCategories){
            $query = \App\Categoria::where('is_ativo', 1)->orWhere('parent_id', '0');
        }
        else{
            $idsPai = with(new \App\Categoria())->getCategoriasPais();
            $query = \App\Categoria::where('is_ativo', 1)->whereNotIn('id', $idsPai)->orWhere('parent_id', '0');
        }


        $options    = array();
        $categorias = $query->get();
        $options    = getCategoriaNested($options, $categorias);

        ?>
        <select class="form-control cartegoria_select" name="<?php echo $name; ?>" id="<?php echo $name; ?>" <?php echo \Html::attributes($attrs); ?> >
            <option value="">Selecione uma opção</option>
            <?php foreach ($options as $category_id => $option): $isSelected = ($option['id'] == $selected) ? 'selected="selected"' : ''; ?>
                <option value="<?php echo $option['id']; ?>" <?php echo $isSelected; ?> data-tipos_de_anexo='<?php echo $option['tipos_de_anexo']; ?>' data-has_pis='<?php echo $option['has_pis']; ?>' data-abreviatura="<?php echo $option['abreviatura']; ?>" <?php echo old($name, $selected ? $selected : '') == $option['id'] ? 'selected' : ''?> >
                    <?php echo $option['nome']; ?>
                </option>
            <?php endforeach;?>
        </select>
<?php
    }
}

if (! function_exists('getCategoriaNested'))
{

    /**
     * @param array  $options
     * @param        $categorias
     *
     * @return array
     */
    function getCategoriaNested(array &$options, $categorias)
    {
        foreach ($categorias as $categoria)
        {
            $options[$categoria->id] = array(
                'id'             => $categoria->id,
                'nome'           => $categoria->nome,
                'abreviatura'    => $categoria->abreviatura,
                'tipos_de_anexo' => json_encode(unserialize($categoria->tipos_de_anexo)),
                'has_pis'        => $categoria->has_pis,
            );

            if( isset($categoria->childs) && $categoria->childs ){
                getCategoriaNested($options, $categoria->childs);
            }
        }

        return $options;
    }
}

if(! function_exists('is_categoria_solidaria')){

    function is_categoria_solidaria($cat)
    {
        if(!empty($cat) && isset($cat))
        {
            if($cat === 'CAMPA'){
                return true;
            }
            return false;
        }
        return false;
    }
}

if(! function_exists('is_edicao_profissional'))
{
    function is_edicao_profissional()
    {
        $edicao = getEdicaoAtual();
        if($edicao->tipo == "profissional")
        {
            return true;
        }

        return false;
    }
}

if(! function_exists('removerAcentos'))
{
    function removerAcentos($text)
    {
        $text = iconv( "UTF-8" , "ASCII//TRANSLIT//IGNORE" , $text );
        $text = preg_replace( array( '/[ ]/' , '/[^A-Za-z0-9\-]/' ) , array( '' , '' ) , $text );
        $text = str_replace(' ', '-', $text);
        $text = strtolower($text);

        return $text;
    }
}

if(! function_exists('exibirPdf'))
{
    function exibirPdf($nome, $numeroInscricao, $path = false)
    {
        $extension = 'pdf';
        $nome      = removerAcentos($nome);
        $file      = generate_file_name($extension, $numeroInscricao.'-'.$nome);

        if($path){
            return app_path_uploads('inscricao-profissional'.DIRECTORY_SEPARATOR.$file);
        }

        return url_uploads('inscricao-profissional'.DIRECTORY_SEPARATOR.$file);
    }
}

if(! function_exists('fileGetContents'))
{
    function fileGetContents($path, $disk = 'local')
    {
        return Storage::disk($disk)->get($path);
    }
}