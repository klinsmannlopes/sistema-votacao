<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    //TIPOS
    const TIPO_TV = 'TV';
    const TIPO_RADIO = 'Rádio';
    const TIPO_JORNAL = 'Jornal';
    const TIPO_INTERNET = 'Internet';

    //VEÍCULOS
    const TV_DIARIO = 'TV Diário';
    const TV_VERDES_MARES = 'TV Verdes Mares';
    const TV_VERDES_MARES_CARIRI = 'TV Verdes Mares Cariri';

    const RADIO_FM_93 = 'FM 93';
    const RADIO_VERDES_MARES = 'Rádio Verdes Mares';

    const JORNAL_DIARIO_DO_NORDESTE = 'Diário do Nordeste';

    const INTERNET_DIARIO_DO_NORDESTE = self::JORNAL_DIARIO_DO_NORDESTE;
    const INTERNET_FM_93 = self::RADIO_FM_93;
    const INTERNET_RADIO_VERDES_MARES = self::RADIO_VERDES_MARES;
    const INTERNET_G1 = 'G1';
    const INTERNET_GE = 'GE';
    const INTERNET_TV_DIARIO = self::TV_DIARIO;
    const INTERNET_TV_VERDES_MARES = self::TV_VERDES_MARES;
    const INTERNET_TV_VERDES_MARES_CARIRI = self::TV_VERDES_MARES_CARIRI;

    //FORMATOS
    const FORMATO_TV_15 = '15"';
    const FORMATO_TV_30 = '30"';
    const FORMATO_TV_60 = '60"';
    const FORMATO_TV_OUTRO = 'Outro';

    const FORMATO_RADIO_15 = '15"';
    const FORMATO_RADIO_30 = '30"';
    const FORMATO_RADIO_60 = '60"';
    const FORMATO_RADIO_OUTRO = 'Outro';

    const FORMATO_JORNAL_UM_QUARTO_DE_PAGINA = '1/4 página';
    const FORMATO_JORNAL_METDADE_DE_PAGINA = '1/2 página';
    const FORMATO_JORNAL_UMA_PAGINA = '1 página';
    const FORMATO_JORNAL_DIFERENCIADO = 'Diferenciado';

    const FORMATO_INTERNET_SLIM_BANNER = 'Slim Banner';
    const FORMATO_INTERNET_SLIM_BANNER_EXPANSIVEL = 'Slim Banner Expansível';
    const FORMATO_INTERNET_SKYSCRAPER = 'Skyscraper';
    const FORMATO_INTERNET_SKYSCRAPER_EXPANSIVEL = 'Skyscraper Expansível';
    const FORMATO_INTERNET_QUADRADO = 'Quadrado';
    const FORMATO_INTERNET_QUADRADO_EXPANSIVEL = 'Quadrado Expansível';
    const FORMATO_INTERNET_RETANGULO = 'Retângulo';
    const FORMATO_INTERNET_SUPER_BANNER = 'Super Banner';
    const FORMATO_INTERNET_SUPER_BANNER_EXPANSIVEL = 'Super Banner Expansível';
    const FORMATO_INTERNET_DHTML = 'DHTML';
    const FORMATO_INTERNET_INTERVENCAO = 'Intervenção';
    const FORMATO_INTERNET_OUTRO = 'Outro';

    static public function all($tipo = null)
    {
        $a = [
            self::TIPO_TV => [
                self::TV_DIARIO,
                self::TV_VERDES_MARES,
                self::TV_VERDES_MARES_CARIRI,
            ],
            self::TIPO_RADIO => [
                self::RADIO_FM_93,
                self::RADIO_VERDES_MARES,
            ],
            self::TIPO_JORNAL => [
                self::JORNAL_DIARIO_DO_NORDESTE
            ],
            self::TIPO_INTERNET => [
                self::INTERNET_DIARIO_DO_NORDESTE,
                self::INTERNET_FM_93,
                self::INTERNET_RADIO_VERDES_MARES,
                self::INTERNET_G1,
                self::INTERNET_GE,
                self::INTERNET_TV_DIARIO,
                self::INTERNET_TV_VERDES_MARES,
                self::INTERNET_TV_VERDES_MARES_CARIRI,
            ],
        ];

        if($tipo !== null)
            return !empty($a[$tipo]) ? $a[$tipo] : null;

        return $a;
    }

    static public function getTipos($key = null)
    {
        $a = [
            self::TIPO_TV => self::TIPO_TV,
            self::TIPO_RADIO => self::TIPO_RADIO,
            self::TIPO_JORNAL => self::TIPO_JORNAL,
            self::TIPO_INTERNET => self::TIPO_INTERNET,
        ];

        if($key !== null)
            return !empty($a[$key]) ? $a[$key] : null;

        return $a;
    }

    static public function getFormatos($tipo = null)
    {
        $a = [
            self::TIPO_TV => [
                self::FORMATO_TV_15,
                self::FORMATO_TV_30,
                self::FORMATO_TV_60,
                self::FORMATO_TV_OUTRO,
            ],
            self::TIPO_RADIO => [
                self::FORMATO_RADIO_15,
                self::FORMATO_RADIO_30,
                self::FORMATO_RADIO_60,
                self::FORMATO_RADIO_OUTRO,
            ],
            self::TIPO_JORNAL => [
                self::FORMATO_JORNAL_UM_QUARTO_DE_PAGINA,
                self::FORMATO_JORNAL_METDADE_DE_PAGINA,
                self::FORMATO_JORNAL_UMA_PAGINA,
                self::FORMATO_JORNAL_DIFERENCIADO,
            ],
            self::TIPO_INTERNET => [
                self::FORMATO_INTERNET_SLIM_BANNER,
                self::FORMATO_INTERNET_SLIM_BANNER_EXPANSIVEL,
                self::FORMATO_INTERNET_SKYSCRAPER,
                self::FORMATO_INTERNET_SKYSCRAPER_EXPANSIVEL,
                self::FORMATO_INTERNET_QUADRADO,
                self::FORMATO_INTERNET_QUADRADO_EXPANSIVEL,
                self::FORMATO_INTERNET_RETANGULO,
                self::FORMATO_INTERNET_SUPER_BANNER,
                self::FORMATO_INTERNET_SUPER_BANNER_EXPANSIVEL,
                self::FORMATO_INTERNET_DHTML,
                self::FORMATO_INTERNET_INTERVENCAO,
                self::FORMATO_INTERNET_OUTRO,
            ],
        ];

        if($tipo !== null)
            return !empty($a[$tipo]) ? $a[$tipo] : null;

        return $a;
    }
}
