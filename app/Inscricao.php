<?php

namespace App;

use App\Contracts\LoggerInterface;
use App\Scopes\EdicaoScope;
use App\Traits\EdicaoContexTrait;
use App\Traits\LoggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

use App\Presenters\InscricaoPresenter;

class Inscricao extends Model implements LoggerInterface
{
    use SoftDeletes, LoggableTrait, EdicaoContexTrait;

    protected $dates = ['deleted_at'];

    use PresentableTrait;
    protected $presenter = InscricaoPresenter::class;

    protected $table = 'inscricoes';

    protected $fillable = [
        'user_id',
        'categoria_id',
        'user_opec_id',
        'responsavel',
        'cargo',
        'email',
        'telefone',
        'anunciante',
        'produto_ou_servico',
        'veiculacao_ini',
        'veiculacao_fim',
        'criacao',
        'midia',
        'atendimento',
        'aprovacao',
        'campanha',
    ];

    protected $with = ['pis'];

    const STATUS_AGUARDANDO_VALIDACAO = 'Aguardando Validação';
    const STATUS_AGUARDANDO_VALIDACAO_OPEC = 'Aguardando Validação OPEC';
    const STATUS_APROVADA_PELA_OPEC = 'Aprovada pela OPEC';

    const STATUS_REJEITA_PELA_OPEC = 'Rejeitada pela OPEC';
    const STATUS_PENDENTE_DE_CORRECAO = 'Pendente de Correção';

    const STATUS_PENDENTE_DE_CORRECAO_OPEC = 'Pendente OPEC';

    const STATUS_VALIDA = 'Válida';
    const STATUS_CANCELADA = 'Cancelada pelo Administrador';
    const STATUS_CANCELADA_PELO_PARTICIPANTE = 'Cancelada pelo Participante';
    const STATUS_CORRIGIDO_PELO_PARTICIPANTE = 'Corrigida pelo Participante';

    const STATUS_CORRIGIDO_PELO_PARTICIPANTE_OPEC = 'Corrigida OPEC';
    const STATUS_PEDENTE_PARA_REVISAO = 'Pendente Para Revisão';


    public function metas()
    {
        return $this->hasMany(InscricaoMeta::class);
    }

    public function meta($key)
    {
        $row = $this->metas()->where('key', $key)->first();

        return $row ? $row->value : null;
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function pecas()
    {
        return $this->hasMany(InscricaoPeca::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function user_opec()
    {
        return $this->belongsTo(User::class, 'user_opec_id');
    }

    public function triagems()
    {
        return $this->hasMany(Triagem::class);
    }

    public function julgamentos($categoria_id = null, $user_id = null)
    {
        $query = $this->hasMany(Julgamento::class);

        if($categoria_id)
            $query = $query->where('categoria_id', $categoria_id);

        if($user_id)
            $query = $query->where('user_id', $user_id);

        return $query;
    }

    public function notas($categoria_id = null, $user_id = null)
    {
        $query = $this->julgamentos($categoria_id, $user_id)->first()->notas()->get()->sortBy(function($row, $key)
        {
            return $row->criterio->nome;
        });

        return $query;
    }

    public function mediaFinal()
    {
        return $this->categoria->notaDaMediaDasInscricoes()[$this->id];
    }

    public function is_aguardando_validacao()
    {
        return $this->status === self::STATUS_AGUARDANDO_VALIDACAO;
    }

    public function is_cancelada()
    {
        return $this->status === self::STATUS_CANCELADA;
    }

    public function is_cancelada_pelo_participante()
    {
        return $this->status === self::STATUS_CANCELADA_PELO_PARTICIPANTE;
    }

    public function is_pendente_de_correcao()
    {
        return $this->status === self::STATUS_PENDENTE_DE_CORRECAO;
    }

    public function is_pendente_de_correcao_opec()
    {
        return $this->status_opec === self::STATUS_PENDENTE_DE_CORRECAO_OPEC;
    }

    public function  is_pendente_para_revisao()
    {
        return $this->status === self::STATUS_PEDENTE_PARA_REVISAO;
    }



    public function is_midia()
    {
        return $this->categoria->abreviatura === 'MID';
    }

    public function is_planejamento()
    {
        return $this->categoria->abreviatura === 'PLA';
    }

    public function juradosQueTriaram()
    {
        $jurados = User::whereIn('id', $this->triagems()->lists('user_id'))->where('role', Role::JURADO);
        return $jurados;
    }

    public function juizesQueTriaram()
    {
        $jurados = User::whereIn('id', $this->triagems()->lists('user_id'))->where('role', Role::JUIZ);
        return $jurados;
    }

    public function juradosQueJulgaram()
    {
        $jurados = User::whereIn('id', $this->julgamentos()->lists('user_id'))->where('role', Role::JURADO);
        return $jurados;
    }

    public function juizSolidarioJulgaram()
    {
        $jurados_solidario = User::whereIn('id', $this->julgamentos()->lists('user_id'))->where('role', Role::JUIZ_SOLIDARIO);
        return $jurados_solidario;
    }

    public function juizesQueJulgaram()
    {
        $jurados = User::whereIn('id', $this->julgamentos()->lists('user_id'))->where('role', Role::JUIZ);
        return $jurados;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function pis()
    {
        return $this->morphMany('App\PI', 'owner');
    }
}