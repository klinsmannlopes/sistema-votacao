<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InscricaoMeta extends Model
{
    //SOFT DELETE
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'inscricoes_meta';

    protected $fillable = [
        'inscricao_id',
        'key',
        'value',
    ];

    public function inscricao()
    {
        return $this->belongsTo(Inscricao::class);
    }
}
