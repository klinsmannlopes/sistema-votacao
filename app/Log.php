<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    protected $table = 'loggable';

    protected $fillable = [
        'user_id',
        'title',
        'action',
        'description',
        'data',
    ];

    protected $casts = [
    	'data'=>'array'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function loggable()
    {
        return $this->morphTo('loggable');
    }
}