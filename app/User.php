<?php

namespace App;

use App\Contracts\LoggerInterface;
use App\Traits\LoggableTrait;
use App\Http\Requests\UsuariosCRUDRequest;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Collections\CellCollection;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract,
                                    LoggerInterface
{
    use Authenticatable, Authorizable, CanResetPassword;
    use LoggableTrait;

    //SOFT DELETE
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'users';

    protected $fillable = [
        'name',
        'login',
        'email',
        'password',
        'role',
        'activated',
        'cnpj',
        'address',
        'phone',
        'director_partner',
    ];

    protected $hidden = ['password', 'remember_token'];

    public function permissions()
    {
        return Role::getPermissions($this->role);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function edicoes()
    {
        return $this->belongsToMany('App\Edicao', 'users_edicoes', 'user_id', 'edicao_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getEdicoesIdAttribute()
    {
        return $this->edicoes()->get()->pluck('id')->toArray();
    }

    public static function createFromExcel(CellCollection $cellCollection)
    {
        if($cellCollection->login)
        {
            $request = new UsuariosCRUDRequest;
            $edicao = getEdicaoAtualId();

            $dataRow = [
                'role' => Role::PARTICIPANTE,
                'login' => $cellCollection->login,
                'name' => $cellCollection->agencia,
                'email' => $cellCollection->e_mail,
                'cnpj' => $cellCollection->cnpj,
                'address' => $cellCollection->endereco,
                'phone' => $cellCollection->telefone,
                'director_partner' => $cellCollection->diretoriasocios,
                'activated' => 1,
            ];

            $validator = validator($dataRow, $request->rules(), $request->messages() );
            $validatorMessages = $validator->getMessageBag();

            if(!$validator->fails())
            {
                $row = self::create($dataRow);

                if($row->is_participante())
                {
                    $row->agency = $dataRow['name'];
                    $row->edicoes()->attach($edicao);
                    $row->save();
                }
                return $row;
            }else{
                if( $validatorMessages->has('email') || $validatorMessages->has('login') ){
                    throw new \Exception("Contato duplicado ou com erro.", 23000);
                }
            }
        }

        return false;
    }

    public function is_administrator()
    {
        return $this->role === Role::ADMINISTRADOR;
    }

    public function is_participante()
    {
        return $this->role === Role::PARTICIPANTE;
    }

    public function is_jurado()
    {
        return $this->role === Role::JURADO;
    }

    public function is_opec()
    {
        return $this->role === Role::OPEC;
    }

    public function is_juiz()
    {
        return $this->role === Role::JUIZ;
    }

    public function is_juiz_solidario()
    {
        return $this->role === Role::JUIZ_SOLIDARIO;
    }

    public function is_jurado_e_solidario() {

        if($this->role === Role::JURADO){
            return $this->role === Role::JURADO;
        } elseif ($this->role === Role::JUIZ_SOLIDARIO) {
            return $this->role === Role::JUIZ_SOLIDARIO;
        } else {
            return false;
        }
    }

    public function triagems()
    {
        return $this->hasMany(Triagem::class);
    }

    public function julgamentos()
    {
        return $this->hasMany(Julgamento::class);
    }

    public function inscricoes()
    {
        return $this->hasMany(Inscricao::class);
    }

    public function inscricoesProfissionais()
    {
        return $this->hasMany(InscricaoProfissional::class);
    }

    public function getJurados()
    {
        return $this->where('role', Role::JURADO)->whereHas('edicoes',function ($query) {
            $query->where('edicao_id', getEdicaoAtualId() );
        })->get();
    }

    public function getJuizSolidarioEJurado()
    {
        return $this->where('role', Role::JUIZ_SOLIDARIO)
                    ->orWhere('role', Role::JURADO)
            ->whereHas('edicoes',function ($query) {
            $query->where('edicao_id', getEdicaoAtualId() );
        })->get();
    }

    public function getJuizSolidario()
    {
        return $this->where('role', Role::JUIZ_SOLIDARIO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })->get();
    }

    public function getJuizes()
    {
        return $this->where('role', Role::JUIZ)->whereHas('edicoes',function ($query) {
            $query->where('edicao_id', getEdicaoAtualId() );
        })->get();
    }

    public function getOpec()
    {
        return $this->where('role', Role::OPEC)->whereHas('edicoes', function($query){
                $query->where('edicao_id', getEdicaoAtualId());
        })->get();
    }
}