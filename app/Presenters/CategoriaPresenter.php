<?php namespace App\Presenters;

use App\Categoria;
use Laracasts\Presenter\Presenter;

class CategoriaPresenter extends Presenter
{
    public function qtdPecas()
    {
        $qtdPecasTipo = Categoria::qtdPecasTipo($this->qtd_pecas_tipo);

        return $this->qtd_pecas ? "{$qtdPecasTipo}: <b>{$this->qtd_pecas}</b>" : '-';
    }

    public function criterios()
    {
        $criterios = [];

        foreach($this->entity->criterios as $criterio)
            $criterios[] = $criterio->nome;

        return implode(', ', $criterios) ? : '-';
    }

    public function tiposDeArquivo()
    {
        $types = [];

        if($this->tipos_de_anexo)
            foreach(unserialize($this->tipos_de_anexo) as $key)
                $types[] = Categoria::tiposDeAnexo($key);

        return implode(', ', $types) ? : '-';
    }

    public function triagem()
    {
        return $this->is_triagem ? 'Sim' : 'Não';
    }
}