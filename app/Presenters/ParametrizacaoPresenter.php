<?php namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class ParametrizacaoPresenter extends Presenter
{
    public function nota()
    {
        return number_format($this->value, 2, ',', '');
    }
}