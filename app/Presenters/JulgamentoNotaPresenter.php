<?php namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class JulgamentoNotaPresenter extends Presenter
{
    public function numberFormat()
    {
        return str_pad(number_format($this->nota, 2, ',', ''), 5, '0', STR_PAD_LEFT);
    }
}