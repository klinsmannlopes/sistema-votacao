<?php namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class InscricaoPresenter extends Presenter
{
    public function numeroInscricao()
    {
        return $this->categoria->abreviatura . str_pad($this->id, 5, 0, STR_PAD_LEFT);
    }



    public function veiculacaoIni()
    {
        if ($this->veiculacao_ini == "0000-00-00") {
            return null;
        } else {
            return date('d/m/Y', strtotime($this->veiculacao_ini));
        }
    }

    public function veiculacaoFim()
    {
        if ($this->veiculacao_fim == "0000-00-00") {
            return null;
        } else {
            return date('d/m/Y', strtotime($this->veiculacao_fim));
        }
    }

    public function inscricaoStatus()
    {
        return "Inscrição {$this->status}";
    }

    public function inscricaoStatusOpec()
    {
        return $this->status_opec;
    }

    public function statusOpec()
    {
        return "teste {$this->status_opec}";
    }

    public function inscricaoStatusColored()
    {
        switch($this->status)
        {
            case \App\Inscricao::STATUS_CORRIGIDO_PELO_PARTICIPANTE:
                $color = '#5B9BD5'; //AZUL
                break;

            case \App\Inscricao::STATUS_PEDENTE_PARA_REVISAO:
                $color = '#8053BA'; //roxo
                break;

            case \App\Inscricao::STATUS_CANCELADA_PELO_PARTICIPANTE:
                $color = '#E36868'; //VERMELHO
                break;

            case \App\Inscricao::STATUS_PENDENTE_DE_CORRECAO:
                $color = '#FFCB00'; //AMARELO
                break;

            case \App\Inscricao::STATUS_CANCELADA:
                $color = '#D2D2D2'; //CINZA
                break;

            case \App\InscricaoProfissional::STATUS_APROVADA:
                $color = '#5B9BD5'; //AZUL
                break;

            case \App\Inscricao::STATUS_VALIDA:
                $color = '#71BF63'; //VERDE
                break;

            default:
                $color = '';
        }

        return "<span id='sta'style='color: {$color}'>". $this->inscricaoStatus() ."</span>";
    }

    public function coresStatus($opec)
    {

        if($this->status == 'Cancelada pelo Administrador' || $this->status == 'Cancelada pelo Participante'){
            switch($this->status)
            {
                case \App\Inscricao::STATUS_CANCELADA_PELO_PARTICIPANTE:
                    return "<span  style='color: #E36868;' >Cancelada pelo participante - ". $opec ."</span>";
                    break;

                case \App\Inscricao::STATUS_CANCELADA:
                    return "<span  style='color: #D2D2D2;' >Cancelada - ". $opec ."</span>";
                    break;
            }
        } else {
            switch($this->status_opec)
            {
                case \App\Inscricao::STATUS_CORRIGIDO_PELO_PARTICIPANTE_OPEC:
                    return "<span  style='color: #5B9BD5;' >Corigida - ". $opec ."</span>";
                    break;

                case \App\Inscricao::STATUS_APROVADA_PELA_OPEC:
                    return "<span  style='color: #71BF63;' >Aprovada - ". $opec ."</span>";
                    break;

                case \App\Inscricao::STATUS_PENDENTE_DE_CORRECAO_OPEC:
                    return "<span  style='color: #FFCB00;' >Pendente de correção - ". $opec ."</span>";
                    break;

                case \App\Inscricao::STATUS_AGUARDANDO_VALIDACAO_OPEC:
                    return "<span>recebido/ ". $opec ."</span>";
                    break;

                default:
                    return "<span></span>";
            }
        }


    }
}