<?php

namespace App;

use App\Traits\EdicaoContexTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Julgamento extends Model
{
    //SOFT DELETE
    use SoftDeletes, EdicaoContexTrait;
    protected $dates = ['deleted_at'];

    protected $table = 'julgamentos';

    protected $fillable = [
        'categoria_id',
        'inscricao_id',
        'user_id'
    ];

    public function notas()
    {
        return $this->hasMany(JulgamentoNota::class);
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function inscricao()
    {
        return $this->belongsTo(Inscricao::class);
    }

    public function criterio()
    {
        return $this->belongsTo(Criterio::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}