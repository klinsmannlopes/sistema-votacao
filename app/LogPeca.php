<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogPeca extends Model
{
    const INSERIR = 'Inserir';
    const EXCLUIR = 'Excluir';

    protected $table = 'log_pecas';

    protected $fillable = [
        'user_id',
        'inscricao_id',
        'acao',
        'agencia',
        'numero_inscricao',
        'numero_peca',
        'arquivo',
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function inscricao()
    {
        return $this->belongsTo(Inscricao::class)->withTrashed();
    }
}