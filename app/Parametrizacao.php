<?php

namespace App;

use App\Contracts\LoggerInterface;
use App\Traits\EdicaoContexTrait;
use App\Traits\LoggableTrait;
use App\Presenters\ParametrizacaoPresenter;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class Parametrizacao extends Model implements LoggerInterface
{
	use LoggableTrait, PresentableTrait, EdicaoContexTrait;

    protected $presenter = ParametrizacaoPresenter::class;

    protected $table = 'parametrizacao';

    protected $fillable = [
        'key',
        'value',
    ];
}
