<?php 
namespace App\Traits;
use App\Edicao;
use App\Scopes\EdicaoHasScope;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EdicaoHasTrait
 *
 * @package   App\Traits
 * @author    Anderson Andrade <anderson.andrade@geq.com.br>
 * @copyright Grupo Edson Queiroz
 */
trait EdicaoHasTrait
{
	 public function scopeWithAllEditions($query){
        return self::withoutGlobalScope(EdicaoHasScope::class);
     }

	/**
	 * @return void
	 */
	public static function bootEdicaoHasTrait()
	{
		static::addGlobalScope( new EdicaoHasScope() );
	}
}