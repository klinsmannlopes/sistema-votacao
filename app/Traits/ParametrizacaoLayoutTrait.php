<?php namespace App\Traits;

use App\Parametrizacao;

trait ParametrizacaoLayoutTrait
{
    public function shareView()
    {
        if(\Schema::hasTable('parametrizacao'))
        {
            $logo = ($row = Parametrizacao::where('key', 'logo')->first()) ? $row->value : '';
            $envelopagem = ($row = Parametrizacao::where('key', 'envelopagem')->first()) ? $row->value : '';

            view()->share('logo', $logo);
            view()->share('envelopagem', $envelopagem);
        }
    }
}