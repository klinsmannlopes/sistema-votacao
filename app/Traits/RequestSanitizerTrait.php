<?php namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Lukzgois\Sanitizer\Sanitizer;
use Illuminate\Foundation\Validation\ValidatesRequests;

trait RequestSanitizerTrait
{
    use ValidatesRequests;

    protected $sanitizer;

    public function setSanitizer()
    {
    }

    protected function sanitize()
    {
        if($this->all() && $this->sanitizer instanceof Sanitizer)
        {
            $this->originalValues = $this->all();
            $this->replace($this->sanitizer->sanitize($this->all(), $this->sanitizer->rules()));
        }
    }

    protected function getValidatorInstance()
    {
        $this->setSanitizer();
        $this->sanitize();

        return parent::getValidatorInstance();
    }

    public function validate()
    {
        return parent::validate();
    }

    protected function beforeFireFailException()
    {
        if(!empty($this->originalValues))
            $this->replace($this->originalValues);
    }

    protected function failedAuthorization()
    {
        $this->beforeFireFailException();
        return parent::failedAuthorization();
    }

    protected function failedValidation(Validator $validator)
    {
        $this->beforeFireFailException();
        return parent::failedValidation($validator);
    }
}