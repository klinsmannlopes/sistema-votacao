<?php namespace App\Traits;

trait ActivatedTrait
{
    public function active($id)
    {
        $row = $this->model->WithAllEditions()->find($id);

        $row->is_ativo = 1;
        $row->save();

        \Flash::success(\Lang::get('crud.activated'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function unactive($id)
    {
        $row = $this->model->WithAllEditions()->find($id);

        $row->is_ativo = 0;
        $row->save();

        \Flash::success(\Lang::get('crud.unactivated'));

        return redirect(route("{$this->namespace}.index"));
    }
}