<?php 
namespace App\Traits;

use Illuminate\Contracts\Support\Arrayable;

trait LoggableTrait 
{
	public function getLoggableTitle()
	{
		return $this->getLoggableClassName();
	}

	public function getLoggableDescription($action = null)
	{
		$className = $this->getLoggableClassName();
		$line = sprintf('logger.%s.%s', strtolower($className), $action);

		return trans($line, $this->getLoggableData() );
	}

	public function getLoggableData()
	{
		return $this->getAttributes();
	}

	public function getLoggableValuesUpdate(){

		return $this->getDirty();
	}

	public function getLoggableClassName()
	{
		$class = get_class($this);
		return str_replace('App\\','',$class);
	}

    public function logs()
    {
        return $this->morphMany('App\Log', 'loggable');
    }
}