<?php 
namespace App\Traits;
use App\Edicao;
use App\Scopes\EdicaoScope;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EdicaoContexTrait
 *
 * @package   App\Traits
 * @author    Anderson Andrade <anderson.andrade@geq.com.br>
 * @copyright Grupo Edson Queiroz
 */
trait EdicaoContexTrait
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function edicao(){
		return $this->belongsTo('App\Edicao',Edicao::FOREIGN_KEY,'id');
	}
	
	/**
	 * @return void
	 */
	public static function bootEdicaoContexTrait()
	{
		static::addGlobalScope( new EdicaoScope() );
		parent::saving(function(Model $model){
			$model->setAttribute(Edicao::FOREIGN_KEY, getEdicaoAtualId());
		});
	}
}