<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InscricaoPeca extends Model
{
    //SOFT DELETE
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'inscricoes_peca';

    protected $fillable = [
        'inscricao_id',
        'tipo',
        'titulo',
        'veiculo',
        'formato',
        'formato_diferenciado',
        'produtora',
        'arquivo',
        'pdf',
        'pi_numero',
        'descricao_audio'
    ];

    protected $with = ['pis'];

    public function inscricao()
    {
        return $this->belongsTo(Inscricao::class)->withTrashed();
    }

    public function is_rejeitada()
    {
        return $this->pi_rejeitada || $this->peca_rejeitada;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function pis()
    {
        return $this->morphMany('App\PI', 'owner');
    }
}
