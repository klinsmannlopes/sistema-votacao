<?php namespace App\Sanitizers;

use Lukzgois\Sanitizer\Sanitizer;

class CategoriasSanitizer extends Sanitizer {

    public function rules()
    {
        return [
            'abreviatura' => 'strtoupper',
            'tipos_de_anexo' => 'serialize',
        ];
    }

}