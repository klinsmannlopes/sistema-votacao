<?php namespace App\Sanitizers;

use Lukzgois\Sanitizer\Sanitizer;

class InscricoesSanitizer extends Sanitizer {

    public function rules()
    {
        return [
            'veiculacao_ini' => 'trim|str2date',
            'veiculacao_fim' => 'trim|str2date',
        ];
    }

}