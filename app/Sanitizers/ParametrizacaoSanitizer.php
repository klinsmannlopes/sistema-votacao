<?php namespace App\Sanitizers;

use Lukzgois\Sanitizer\Sanitizer;

class ParametrizacaoSanitizer extends Sanitizer
{
    public function rules()
    {
        return [
            'avaliacoes' => 'notasToDB'
        ];
    }
}