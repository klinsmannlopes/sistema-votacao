<?php namespace App\Sanitizers;

use Lukzgois\Sanitizer\Sanitizer;

class PeriodoDeInscricaoSanitizer extends Sanitizer
{
    public function rules()
    {
        return [
            'inscricoes_data_inicial' => 'str2datetime|trim',
            'inscricoes_data_final' => 'str2datetime|trim',
        ];
    }
}