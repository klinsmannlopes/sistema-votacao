<?php namespace App\Sanitizers;

use Lukzgois\Sanitizer\Sanitizer;

class JulgamentoParametrizacaoSanitizer extends Sanitizer
{
    public function rules()
    {
        return [
            'nota_minima' => 'trim|money2float',
            'nota_maxima' => 'trim|money2float',
        ];
    }
}