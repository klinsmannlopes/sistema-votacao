<?php

namespace App;
use App\Contracts\LoggerInterface;
use App\Traits\LoggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Criterio extends Model implements LoggerInterface
{
    use LoggableTrait;
    
    //SOFT DELETE
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'criterios';

    protected $fillable = [
        'nome',
        'is_ativo',
    ];

    public function categorias()
    {
        return $this->belongsToMany(Categoria::class)->withTimestamps();
    }
}
