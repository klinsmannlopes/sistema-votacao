<?php

Route::get('/', function()
{
    return redirect(route('getLogin'));
});

Route::controller('auth', 'Auth\AuthController',
[
    'getLogin'  => 'getLogin',
    'getLogout' => 'logout',
    'postLogin' => 'postLogin',
]);

Route::controller('password', 'Auth\PasswordController',
[
    'getEmail'  => 'getEmail',
    'postEmail' => 'postEmail',
    'getReset'  => 'getReset',
    'postReset'  => 'postReset',
]);

Route::group([ 'middleware' => 'auth' ], function()
{
    Route::group(['middleware' => 'edition'], function(){
        
        //DASHBOARD
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        //LAYOUT
        Route::controller('layout', 'LayoutController',
        [
            'getIndex' => 'layout',
            'postSave' => 'layout.save',
            'getDestroy' => 'layout.destroy',
        ]);

        //PERIODO DE INSCRIÇÃO
        Route::controller('periodo-de-inscricoes', 'PeriodoDeInscricaoController',
        [
            'getIndex' => 'periodo-de-inscricoes',
            'postSave' => 'periodo-de-inscricoes.save',
        ]);

        //USUÁRIOS
        Route::controller('usuarios', 'UsuariosController',
        [
            'getIndex' => 'usuarios',
            'getCreate' => 'usuario.create',
            'getEdit' => 'usuario.edit',
            'postStore' => 'usuario.store',
            'getShow' => 'usuario.show',
            'getActive' => 'usuario.active',
            'getUnactive' => 'usuario.unactive',
        ]);

        Route::put('usuario/{row}', 'UsuariosController@postUpdate')->name('usuario.update');

        Route::delete('usuario/{row}', 'UsuariosController@postDestroy')->name('usuario.destroy');

        //PROFILE
        Route::get('profile', 'ProfileController@getProfile')->name('profile');
        Route::put('profile/save', 'ProfileController@postProfile')->name('profile.save');

        //CADASTRO DE PARTICIPANTES
        Route::controller('cadastro-de-participantes', 'CadastroDeParticipantesController',
        [
            'getIndex' => 'cadastro-de-participantes',
            'postStore' => 'cadastro-de-participantes.store',
        ]);

        //INSCRIÇÕES
        Route::group(['middleware' => 'permissions:inscricoes,inscricoes-opec,inscricoes-participante'], function()
        {
            Route::resource('inscricoes', 'InscricoesController');

            Route::put('inscricoes/{ids}/{user_id}/associar-opec', 'InscricoesController@associarOPEC')->name('inscricoes.associar-opec');
            Route::put('inscricoes/{id}/aprovar', 'InscricoesController@aprovar')->name('inscricoes.aprovar');
            Route::put('inscricoes/{id}/enviar-para-correcao', 'InscricoesController@enviarParaCorrecao')->name('inscricoes.enviar-para-correcao');

            Route::put('inscricoes/{id}/pendente-para-revisao', 'InscricoesController@pendenteParaRevisao')->name('inscricoes.pendente-para-revisao');

            Route::put('inscricoes/{id}/cancelar', 'InscricoesController@cancelar')->name('inscricoes.cancelar');
            Route::put('inscricoes/{id}/validar', 'InscricoesController@validar')->name('inscricoes.validar');
            Route::put('inscricoes/{id}/cancelada-pelo-participante', 'InscricoesController@canceladaPeloParticipante')->name('inscricoes.cancelada-pelo-participante');
            Route::get('inscricoes/{id}/pdf', 'InscricoesController@pdf')->name('inscricoes.pdf');

            Route::get('inscricoes-xls', 'InscricoesController@xls')->name('inscricoes.xls');
            Route::get('inscricoes-csv', 'InscricoesController@csv')->name('inscricoes.csv');
            Route::get('inscricoes/{id}/continuar', 'InscricoesController@continuar')->name('inscricoes.continuar');

            Route::post('inscricoes/upload-peca', 'InscricoesController@uploadPeca')->name('inscricoes.upload-peca');
            Route::post('inscricoes/upload-pi', 'InscricoesController@uploadPI')->name('inscricoes.upload-pi');

            Route::get('inscricoes-imprimir', 'InscricoesController@imprimir')->name('inscricoes.imprimir');

            //Inscrição Profissional
            Route::resource('inscricao-profissional', 'InscricaoProfissionalController');
            Route::post('upload-curriculo','InscricaoProfissionalController@uploadCurriculo')->name('inscricao-profissional.upload-curriculo');
            Route::post('upload-foto','InscricaoProfissionalController@uploadFoto')->name('inscricao-profissional.upload-foto');

            Route::get('inscricoes-profissional-xls', 'InscricaoProfissionalController@xls')->name('inscricoes.profissional.xls');
            Route::get('inscricoes-profissional-csv', 'InscricaoProfissionalController@csv')->name('inscricoes.profissional.csv');
            Route::get('inscricoes-profissional-imprimir', 'InscricaoProfissionalController@imprimir')->name('inscricoes.profissional.imprimir');

            Route::post('inscricao-profissional/{id}/aprovar', 'InscricaoProfissionalController@aprovar')->name('inscricao-profissional.aprovar');
            Route::put('inscricao-profissional/{id}/cancelar','InscricaoProfissionalController@cancelar')->name('inscricao-profissional.cancelar');
            Route::put('inscricao-profissional/{id}/cancelada-pelo-participante', 'InscricaoProfissionalController@canceladaPeloParticipante')->name('inscricao-profissional.cancelada-pelo-participante');

            Route::get('inscricao-profissional/{id}/pdf/{view}', 'InscricaoProfissionalController@pdf')->name('inscricao-profissional.pdf');

            Route::resource('profissional-resultados','ProfissionalResultadoController');
            Route::get('profissional-resultados/{id}/zipper', 'ProfissionalResultadoController@zipper')->name('profissional-resultados.zipper');


        });

        //INSCRIÇÕES DETALHES
        Route::group(['middleware' => 'permissions:log,detalhes-inscricao'], function()
        {
            Route::get('inscricoes/{id}/log', 'InscricoesController@log')->name('inscricoes.log');
            Route::get('inscricoes/{id}/detalhes', 'InscricoesController@log')->name('inscricoes.detalhes');
        });


        //INSCRIÇÕES CATEGORIAS
        Route::resource('categorias', 'CategoriasController');
        Route::get('categorias/{id}/active', 'CategoriasController@active')->name('categorias.active');
        Route::get('categorias/{id}/unactive', 'CategoriasController@unactive')->name('categorias.unactive');

        //INSCRIÇÕES CRITÉRIOS
        Route::resource('criterios', 'CriteriosController');
        Route::get('criterios/{id}/active', 'CriteriosController@active')->name('criterios.active');
        Route::get('criterios/{id}/unactive', 'CriteriosController@unactive')->name('criterios.unactive');

        //INSCRIÇÕES EDICOES
        Route::resource('edicoes', 'EdicoesController');

        Route::get('logs', 'LogsController@index')->name('logs.index');

        //LOG DAS PEÇAS
        Route::get('log-pecas', 'LogPecasController@index')->name('log-pecas.index');

        //JULGAMENTO - Parametrização
        Route::controller('julgamento-parametrizacao', 'JulgamentoParametrizacaoController',
        [
            'getIndex' => 'julgamento-parametrizacao',
            'postSave' => 'julgamento-parametrizacao.save',
        ]);

        //JULGAMENTO TRIAR
        Route::get('julgamento-triar/categorias', 'JulgamentoTriarController@categorias')->name('julgamento-triar.categorias');
        Route::get('julgamento-triar/{id}',       'JulgamentoTriarController@index')->name('julgamento-triar');
        Route::post('julgamento-triar/{id}/save', 'JulgamentoTriarController@save')->name('julgamento-triar.save');

        //JULGAMENTO TRIAGEM
        Route::get('julgamento-triagem/categorias', 'JulgamentoTriagemController@categorias')->name('julgamento-triagem.categorias');
        Route::get('julgamento-triagem/{id}', 'JulgamentoTriagemController@index')->name('julgamento-triagem');

        //DESEMPATAR TRIAGEM
        Route::get('desempatar-triagem/categorias', 'DesempatarTriagemController@categorias')->name('desempatar-triagem.categorias');
        Route::get('desempatar-triagem/{id}', 'DesempatarTriagemController@index')->name('desempatar-triagem');

        Route::post('desempatar-triagem/{id}/confirm-ordenacao', 'DesempatarTriagemController@confirmarOrdenacao')->name('desempatar-triagem.confirm-ordenacao');
        Route::post('desempatar-triagem/{id}/ordernar', 'DesempatarTriagemController@ordenar')->name('desempatar-triagem.ordenar');

        Route::get('desempatar-triagem/{id}/top10', 'DesempatarTriagemController@top10')->name('desempatar-triagem.top10');

        //VER PEÇAS
        Route::get('inscricoes/{id}/ver-pecas', 'VerPecasController@index')->name('inscricoes.pecas');

        //JULGAR - Jurados e Juiz
        Route::get('julgar/categorias', 'JulgarController@categorias')->name('julgar.categorias');
        Route::get('julgar/{id}', 'JulgarController@index')->name('julgar.index');
        Route::post('julgar/{id}/finalizar', 'JulgarController@finalizar')->name('julgar.finalizar');

        //JULGAMENTO - Administrador
        Route::get('julgamento/categorias', 'JulgamentoController@categorias')->name('julgamento.categorias');
        Route::get('julgamento/{id}', 'JulgamentoController@index')->name('julgamento.index');
        Route::post('julgamento/{id}/finalizar', 'JulgamentoController@finalizar')->name('julgamento.finalizar');

        Route::get('julgamento/{id}/top5/{format}', 'DesempatarJulgamentoController@top5')->name('julgamento.top5');
        Route::get('julgamento/resultadogeral/{format}', 'DesempatarJulgamentoController@resultadoGeral')->name('julgamento.resultadogeral');

        Route::get('resultado-edicao', 'EdicoesController@getAgenciasGanhadoras')->name('resultado-edicao');
        Route::get('export', 'EdicoesController@export')->name('edicoes.export');

        //DESEMPATAR JULGAMENTO
        Route::get('desempatar-julgamento/categorias', 'DesempatarJulgamentoController@categorias')->name('desempatar-julgamento.categorias');
        Route::get('desempatar-julgamento/{id}', 'DesempatarJulgamentoController@index')->name('desempatar-julgamento.index');

        Route::post('desempatar-julgamento/{id}/confirm-ordenacao', 'DesempatarJulgamentoController@confirmarOrdenacao')->name('desempatar-julgamento.confirm-ordenacao');
        Route::post('desempatar-julgamento/{id}/ordernar', 'DesempatarJulgamentoController@ordenar')->name('desempatar-julgamento.ordenar');

        Route::get('desempatar-julgamento/{id}/lista-desempate', 'DesempatarJulgamentoController@listaDesempate')->name('desempatar-julgamento.lista-desempate');
    });

});
