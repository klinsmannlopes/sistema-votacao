<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Http\Request;

class VerifyEdition
{
    use AuthorizesRequests;

    public function __construct()
    {
         $this->redirectAfterLogout = route('getLogin');
    }

    public function handle(Request $request, Closure $next)
    {

        $user   = \Auth::user();
        $edicoes  = $user->edicoes()->get()->toArray();
        $edicoesHabilitadas = array_pluck($edicoes, 'id');

        $edicaoAtual = empty((int)getEdicaoAtualId()) ? $edicoesHabilitadas[0] : (int)getEdicaoAtualId();

        if(empty((int)getEdicaoAtualId())){
            \Session::set('edicaoAtual', $edicaoAtual);
        }

        if($user->role !== 'administrador')
        {
            if(!in_array($edicaoAtual, $edicoesHabilitadas))
            {
                \Auth::logout();
                return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/')
                    ->withErrors('Seu usuário não está habilitado para essa edição.');
            }
        }
        return $next($request);
    }
}
