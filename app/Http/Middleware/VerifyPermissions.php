<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Testing\HttpException;
use Illuminate\Http\Request;

class VerifyPermissions
{
    use AuthorizesRequests;

    public function handle(Request $request, Closure $next)
    {
        $permissions = array_slice(func_get_args(), 2, func_num_args() - 2);

        foreach($permissions as $permission)
        {
            if(\Gate::check($permission))
            {
                $authorized = true;
                break;
            }
        }

        if(empty($authorized))
        {
            $user = \Auth::user();
            foreach($permissions as $permission)
                $this->authorizeForUser($user, $permission);
        }

        return $next($request);
    }
}
