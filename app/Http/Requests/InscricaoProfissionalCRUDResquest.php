<?php
/**
 * Created by PhpStorm.
 * User: 750371426
 * Date: 12/01/2017
 * Time: 11:05
 */

namespace App\Http\Requests;


class InscricaoProfissionalCRUDResquest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'        => 'required',
            'categoria_id'=> 'required',
            'curriculo'   => 'required_without:curriculo_old|max:20480|mimes:pdf',
            'foto'        => "required_without:foto_old|max:20480|mimes:jpeg,jpg",
            'defesa'      => "required|max:1200",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'curriculo.required_without' => 'O campo <b> "currículo" </b> deve ser preenchido',
            'foto.required_without'      => 'O campo <b> "foto"      </b> deve ser preenchido',
            'categoria_id.required'      => 'O campo <b> "categoria" </b> deve ser preenchido',
        ];
    }
}