<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CancelarInscricaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'justificativa' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'justificativa.required' => \Lang::get('inscricoes.cancelar.justificativa'),
        ];
    }
}
