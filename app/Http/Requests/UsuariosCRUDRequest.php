<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\User;

class UsuariosCRUDRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required',
             //'cnpj' => ['regex:/^\d{2}.\d{3}.\d{3}\/\d{4}-\d{2}$/'],
            'address' => '',
            'role' => 'required',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
             //'phone' => ['regex:/^\(\d{2}\) \d{8,9}$/'],
            'login' => 'required|unique:users,login,NULL,id,deleted_at,NULL',
            //'agency' => 'required_if:role,participante',
            'director_partner' => 'required_if:role,participante',
        ];

        switch($this->method())
        {
            case 'PUT':
            case 'PATCH':
            {
                $row = User::find($this->row_id);

                $rules = array_merge($rules, [
                    'email' => "required|email|unique:users,email,{$row->email},email,deleted_at,NULL",
                    'login' => "required|unique:users,login,{$row->login},login,deleted_at,NULL",
                ]);

                if(!empty($this->input('is_profile')))
                {
                    if(!empty($this->input('is_profile')))
                    {
                        $rules = array_merge($rules, [
                            'password' => 'actual_password',
                            'new_password' => [ 'required_with:password' ,'confirmed', 'min:8', 'regex:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!?]).*$/' ],
                        ]);
                    }

                    unset($rules['role']);
                }

                if(\Gate::check('usuarios.edit-password'))
                {
                    $rules = array_merge($rules, [
                        'new_password' => [ 'required_with:password' ,'confirmed', 'min:8', 'regex:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!?]).*$/' ],
                    ]);
                }
            }
            break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required'  => \Lang::get('validation.required', ['attribute' => 'nome']),
            'role.required'  => \Lang::get('validation.required', ['attribute' => 'perfil']),
            'email.email' => \Lang::get('validation.email', ['attribute' => 'E-mail']),
            'email.required' => \Lang::get('validation.required', ['attribute' => 'e-mail']),
            'email.unique' => \Lang::get('validation.unique', ['attribute' => 'e-mail']),
            'login.required' => \Lang::get('validation.required', ['attribute' => 'login']),
            'login.unique' => \Lang::get('validation.unique', ['attribute' => 'login']),
            'phone.regex' => \Lang::get('validation.regex', ['attribute' => 'telefone']),
            'password.actual_password' => 'A senha atual não está correta.',
            'new_password.required_with' => \Lang::get('validation.required_with', ['attribute' => 'nova senha', 'values' => 'senha atual']),
            'new_password.regex' => \Lang::get('validation.regex', ['attribute' => 'nova senha']),
            'new_password.confirmed' => \Lang::get('validation.confirmed', ['attribute' => 'nova senha']),
            'agency.required_if' => \Lang::get('validation.required_if', ['attribute' => 'Agência', 'other' => 'Perfil', 'value' => 'Participante']),
            'director_partner.required_if' => \Lang::get('validation.required_if', ['attribute' => 'Diretoria/Sócios', 'other' => 'Perfil', 'value' => 'Participante']),
        ];
    }
}