<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SaveProfileRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user = \Auth::user();

        return [
            'name' => 'required',
            'cnpj' => ['regex:/^\d{2}.\d{3}.\d{3}\/\d{4}-\d{2}$/'],
            'email' => "required|email|unique:users,email,{$user->email},email,deleted_at,NULL",
            'phone' => ['regex:/^\(\d{2}\) \d{8,9}$/'],
            'login' => "required|unique:users,login,{$user->login},login,deleted_at,NULL",
            'new_password' => ['required_with:password', 'confirmed', 'regex:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!?]).*$/']
        ];
    }

    public function messages()
    {
        return [
            'name.required' => \Lang::get('validation.required', [ 'attribute' => 'Nome' ]),
            'email.required' => \Lang::get('validation.required', [ 'attribute' => 'E-mail' ]),
            'email.email' => \Lang::get('validation.email', [ 'attribute' => 'E-mail' ]),
            'phone.regex' => \Lang::get('validation.regex', [ 'attribute' => 'Telefone' ]),
            'login.required' => \Lang::get('validation.required', [ 'attribute' => 'Login' ]),
            'login.unique' => \Lang::get('validation.unique', [ 'attribute' => 'Login' ]),
            'new_password.unique' => \Lang::get('validation.unique', [ 'attribute' => 'Login' ]),
            'new_password.required_with' => \Lang::get('validation.required_with', [ 'attribute' => 'Nova senha', 'values' => 'Senha Atual' ]),
            'new_password.regex' => 'Formato Inválido. Você deve escolher uma senha com no mínimo de 8 caracteres, entre maiúsculas e minúsculas, números e caracteres especiais.',
            'new_password.confirmed' => \Lang::get('validation.confirmed', [ 'attribute' => 'Nova senha' ]),
        ];
    }
}
