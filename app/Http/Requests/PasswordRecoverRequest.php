<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PasswordRecoverRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function getRulesResetPassword()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required', 'confirmed', 'regex:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!?]).*$/']
        ];
    }

    public function getRulesSendEmailForReset()
    {
        return [
            'email' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => \Lang::get('validation.required', [ 'attribute' => 'E-mail' ]),
            'email.email' => \Lang::get('validation.email', [ 'attribute' => 'E-mail' ]),
            'password.required' => \Lang::get('validation.required', [ 'attribute' => 'Nova Senha' ]),
            'password.min' => \Lang::get('validation.min.string', [ 'attribute' => 'Nova Senha' ]),
            'password.confirmed' => \Lang::get('validation.confirmed', [ 'attribute' => 'Confirme sua nova senha' ]),
            'password.regex' => 'Você deve escolher uma senha com o mínimo de 8 caracteres, entre maiúsculas e minúsculas, números e caracteres especiais.',
        ];
    }
}
