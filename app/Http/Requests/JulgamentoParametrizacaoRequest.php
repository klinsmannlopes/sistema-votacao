<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Sanitizers\JulgamentoParametrizacaoSanitizer as Sanitizer;
use App\Traits\RequestSanitizerTrait;

class JulgamentoParametrizacaoRequest extends Request
{
    use RequestSanitizerTrait;

    public function authorize()
    {
        return true;
    }

    public function setSanitizer()
    {
        return $this->sanitizer = new Sanitizer;
    }

    public function rules()
    {
        return [
            'nota_minima' => 'required|numeric',
            'nota_maxima' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'nota_minima.required' => \Lang::get('validation.required', [ 'attribute' => 'Nota mínima' ]),
            'nota_minima.numeric' => \Lang::get('validation.numeric', [ 'attribute' => 'Nota mínima' ]),

            'nota_maxima.required' => \Lang::get('validation.required', [ 'attribute' => 'Nota máxima' ]),
            'nota_maxima.numeric' => \Lang::get('validation.numeric', [ 'attribute' => 'Nota máxima' ]),
        ];
    }
}
