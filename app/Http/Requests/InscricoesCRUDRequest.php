<?php

namespace App\Http\Requests;

use App\Categoria;
use App\Http\Requests\Request;

use App\Inscricao;
use App\Traits\RequestSanitizerTrait;
use App\Sanitizers\InscricoesSanitizer as Sanitizer;
use App\Veiculo;

class InscricoesCRUDRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    use RequestSanitizerTrait;
    public function setSanitizer()
    {
        return $this->sanitizer = new Sanitizer;
    }

    public function rules()
    {

       return is_categoria_solidaria($this->categoria_abrev ) ? $this->rulesSolidaria() : $this->rulesDefaut();

    }

    public function rulesSolidaria()
    {
        $rules = [
            'responsavel' => 'required',
            'cargo' => 'required',
            'email' => 'required|email',
            'telefone' => ['required', 'regex:/^\(\d{2}\) \d{8,9}$/'],
            'categoria_id' => 'required',
            'criacao' => 'required',
            'campanha' => 'required',

        ];

        return $rules;
    }


    public function rulesDefaut()
    {
        $rules = [
            'responsavel' => 'required',
            'cargo' => 'required',
            'email' => 'required|email',
            'telefone' => ['required', 'regex:/^\(\d{2}\) \d{8,9}$/'],
            'categoria_id' => 'required',
            'anunciante' => 'required',
            'produto_ou_servico' => 'required',
            'veiculacao_ini' => ['required', 'date', 'date_format:Y-m-d', 'before_equal:veiculacao_fim'],
            'veiculacao_fim' => ['required', 'date', 'date_format:Y-m-d'],
            'criacao' => 'required',
            'midia' => 'required',
            'atendimento' => 'required',
            'aprovacao' => 'required',
            'campanha' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'responsavel.required' => \Lang::get('validation.required', ['attribute' => 'Responsável']),
            'cargo.required' => \Lang::get('validation.required', ['attribute' => 'Cargo']),
            'email.required' => \Lang::get('validation.required', ['attribute' => 'E-mail']),
            'email.email' => \Lang::get('validation.email', ['attribute' => 'E-mail']),
            'telefone.required' => \Lang::get('validation.required', ['attribute' => 'Telefone']),
            'telefone.regex' => \Lang::get('validation.regex', ['attribute' => 'Telefone']),
            'categoria_id.required' => \Lang::get('validation.required', ['attribute' => 'Categoria']),
            'anunciante.required' => \Lang::get('validation.required', ['attribute' => 'Anunciante']),
            'produto_ou_servico.required' => \Lang::get('validation.required', ['attribute' => 'Produto/Serviço']),
            'veiculacao_ini.required' => \Lang::get('validation.required', ['attribute' => 'Início da Veiculação']),
            'veiculacao_ini.date' => \Lang::get('validation.date', ['attribute' => 'Início da Veiculação']),
            'veiculacao_ini.date_format' => \Lang::get('validation.date', ['attribute' => 'Início da Veiculação']),
            'veiculacao_ini.before_equal' => \Lang::get('validation.before_equal', ['attribute' => 'Início da Veiculação', 'date' => ($fimVeiculacao = 'Fim da Veiculação')]),
            'veiculacao_fim.required' => \Lang::get('validation.required', ['attribute' => $fimVeiculacao]),
            'veiculacao_fim.date' => \Lang::get('validation.date', ['attribute' => $fimVeiculacao]),
            'veiculacao_fim.date_format' => \Lang::get('validation.date', ['attribute' => $fimVeiculacao]),
            'criacao.required' => \Lang::get('validation.required', ['attribute' => 'Criação']),
            'midia.required' => \Lang::get('validation.required', ['attribute' => 'Mídia']),
            'atendimento.required' => \Lang::get('validation.required', ['attribute' => 'Atendimento']),
            'aprovacao.required' => \Lang::get('validation.required', ['attribute' => 'Aprovação']),
            'campanha.required' => \Lang::get('validation.required', ['attribute' => 'Campanha']),
        ];
    }

    /**
     * @param $rules
     *
     * @return array
     */
    protected function getCatFixasPIRules($rules)
    {
        if( isset($this->pis) && is_array($this->pis) ){
            foreach($this->pis as $pi_index => $pi ){
                $rules += [
                    "pis.{$pi_index}.pi_numero"=> 'required|numeric'
                ];
            }
        }

        return $rules;
    }

    /**
     * @param $messages
     *
     * @return array
     */
    protected function getCatFixasPIMessages($messages)
    {
        if( isset($this->pis) && is_array($this->pis) ){
            foreach($this->pis as $pi_index => $pi ){
                $messages += [
                    "pis.{$pi_index}.pi_numero.required"=> \Lang::get('validation.required', [ 'attribute' => "PI {$pi_index}- Número"]),
                    "pis.{$pi_index}.pi_numero.numeric"=> \Lang::get('validation.numeric', [ 'attribute' => "PI {$pi_index} - Número"]),
                    "pis.{$pi_index}.pdf-old.required" => \Lang::get('validation.required', [ 'attribute' => "PI {$pi_index} - Peça"])
                ];
            }
        }

        return $messages;
    }

    public function midiaRules()
    {
        $rules = [
            'titulo_do_case' => 'required',
            'problema_ou_oportunidade' => 'required',
            'veiculos_utilizados' => 'required',
            'mix_de_veiculos' => 'required',
            'publico_alvo' => 'required',
            'resumo_da_midia' => 'required',
            'total_de_insercoes_domiciliar' => 'required',
            'total_de_grp' => 'required',
            'alcance_domiciliar' => 'required',
            'frequencia_media_domiciliar' => 'required',
            'universo_domiciliar' => 'required',
            'total_de_insercoes_publico_alvo' => 'required',
            'total_tarp' => 'required',
            'alcance_public_alvo' => 'required',
            'frequencia_media_publico_alvo' => 'required',
            'total_de_impactos' => 'required',
            'cmp_impactos' => 'required',
            'universo_do_target' => 'required',
            'defesa_geral' => 'required',
            'pis' => 'required|array',
        ];

        return $this->getCatFixasPIRules($rules);
    }

    public function midiaMessages()
    {
        $messages = [
            'titulo_do_case.required' => \Lang::get('validation.required', ['attribute' => 'Título do Case']),
            'problema_ou_oportunidade.required' => \Lang::get('validation.required', ['attribute' => 'Problema/Oportunidade']),
            'veiculos_utilizados.required' => \Lang::get('validation.required', ['attribute' => 'Veículos Utilizados']),
            'mix_de_veiculos.required' => \Lang::get('validation.required', ['attribute' => 'Def. Mix de Veículos']),
            'publico_alvo.required' => \Lang::get('validation.required', ['attribute' => 'Público Alvo']),
            'resumo_da_midia.required' => \Lang::get('validation.required', ['attribute' => 'Resumo da Mídia']),
            'total_de_insercoes_domiciliar.required' => \Lang::get('validation.required', ['attribute' => 'Total de Inserções']),
            'total_de_grp.required' => \Lang::get('validation.required', ['attribute' => 'Total de GRP']),
            'alcance_domiciliar.required' => \Lang::get('validation.required', ['attribute' => 'Resultado Domiciliar - Alcance %']),
            'frequencia_media_domiciliar.required' => \Lang::get('validation.required', ['attribute' => 'Resultado Domiciliar - Frequência Média']),
            'universo_domiciliar.required' => \Lang::get('validation.required', ['attribute' => 'Universo Domiciliar']),
            'total_de_insercoes_publico_alvo.required' => \Lang::get('validation.required', ['attribute' => 'Total de Inserções']),
            'total_tarp.required' => \Lang::get('validation.required', ['attribute' => 'Total TARP']),
            'alcance_public_alvo.required' => \Lang::get('validation.required', ['attribute' => 'Público - Alcance %']),
            'frequencia_media_publico_alvo.required' => \Lang::get('validation.required', ['attribute' => 'Resultado Domiciliar - Frequência Média']),
            'total_de_impactos.required' => \Lang::get('validation.required', ['attribute' => 'Total de Impactos']),
            'cmp_impactos.required' => \Lang::get('validation.required', ['attribute' => 'CPM Impactos']),
            'universo_do_target.required' => \Lang::get('validation.required', ['attribute' => 'Universo do Target']),
            'defesa_geral.required' => \Lang::get('validation.required', ['attribute' => 'Defesa Geral']),
            'pis.array' => \Lang::get('validation.array', ['attribute' => 'PI']),
            'pis.required' => \Lang::get('validation.required', ['attribute' => 'PI']),
        ];

        return $this->getCatFixasPIMessages($messages);
    }

    public function planejamentoRules()
    {
        $rules = [
            'objetivo' => 'required',
            'veiculos' => 'required',
            'defesa_mix' => 'required',
            'contribuicao_especifica_do_trabalho' => 'required',
            'grau_de_dificuldade_e_interdepencia' => 'required',
            'resultados_obtidos' => 'required',
            'pis' => 'required|array',
        ];

        return $this->getCatFixasPIRules($rules);
    }

    public function planejamentoMessages()
    {
        $messages = [
            'objetivo.required' => \Lang::get('validation.required', ['attribute' => 'Objetivo']),
            'veiculos.required' => \Lang::get('validation.required', ['attribute' => 'Veículos']),
            'defesa_mix.required' => \Lang::get('validation.required', ['attribute' => 'Defesa Mix']),
            'contribuicao_especifica_do_trabalho.required' => \Lang::get('validation.required', ['attribute' => 'Contribuição Específica do Trabalho']),
            'grau_de_dificuldade_e_interdepencia.required' => \Lang::get('validation.required', ['attribute' => 'Grau de Dificuldade e Interdepência']),
            'resultados_obtidos.required' => \Lang::get('validation.required', ['attribute' => 'Resultados Obtidos']),
            'pis.array' => \Lang::get('validation.array', ['attribute' => 'PI']),
            'pis.required' => \Lang::get('validation.required', ['attribute' => 'PI']),
        ];

        return $this->getCatFixasPIMessages($messages);
    }

    public function categoriaNaoFixaRules($method = null)
    {
        $categoria = Categoria::find($this->categoria_id);

        $tiposDeAnexo = unserialize($categoria->tipos_de_anexo);

        $mimes = $this->mimesType();

        $rules = [];

        foreach($this->pecas as $index => $peca)
        {
            $rules += [
                "{$index}.tipo" => 'required',
                "{$index}.titulo" => 'required',
                "{$index}.formato" => 'required',
                "{$index}.formato_diferenciado" => "required_if:{$index}.formato,". Veiculo::FORMATO_JORNAL_DIFERENCIADO,
                "{$index}.produtora" => "required_unless:{$index}.tipo,"."Jornal",
            ];



            //CASO A CATEGORIA ACEITE TIPO_DE_ANEXO_AUDIO, O CAMPO descricao_audio DEVE ESTAR PREENCHIDO
            if(in_array(Categoria::TIPO_DE_ANEXO_AUDIO, $tiposDeAnexo))
                $rules += [ "{$index}.descricao_audio" => "required_if:{$index}.tipo,"."Rádio"];

            if($this->method() === 'POST' && empty($this->pecas[$index]['arquivo-old']))
            {
                $rules += [
                    "{$index}.arquivo"  => "required|max:20480|extensions:{$mimes}"
                ];
            }

            if(!is_categoria_solidaria($this->categoria_id))
            {
                $rules += [
                    "{$index}.veiculo" => 'required',
                ];

                if(isset($categoria->has_pis) && $categoria->has_pis==1){
                    $rules += [
                        "{$index}.pis"      => 'required',
                    ];
                }
                
                if(isset($categoria->has_pis) && $categoria->has_pis==1){
                    if( isset($peca['pis']) && is_array($peca['pis']) )
                        {
                            foreach($peca['pis'] as $pi_index => $pi ){
                                $rules += [
                                    "{$index}.pis.{$pi_index}.pi_numero"=> 'required|numeric'
                                ];
                            }
                        }
                }
            }
        }

        return $rules;
    }

    public function categoriaNaoFixaMessages()
    {
        $mimes = $this->mimesType();

        $messages = [];
        foreach($this->pecas as $index => $peca)
        {
            $messages += [
                "{$index}.tipo.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - Tipo"]),
                "{$index}.titulo.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - Título"]),
                "{$index}.veiculo.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - Veículo"]),
                "{$index}.formato.required" => \Lang::get('validation.required', [ 'attribute' => ($formatoIndex = "Peça {$index} - Formato")]),
                "{$index}.formato_diferenciado.required_if" => \Lang::get('validation.required_if', [ 'attribute' => "Peça {$index} - Formato Diferenciado", 'other' => $formatoIndex]),
                "{$index}.produtora.required_unless" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - Produtora"]),
                "{$index}.descricao_audio.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - Descrição de áudio"]),
                "{$index}.pis.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - PI"]),
            ];

            if($this->method() === 'POST' && empty($this->pecas[$index]['arquivo-old']))
            {
                $messages += [
                    "{$index}.arquivo.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - Peça"]),
                    "{$index}.arquivo.max" => \Lang::get('validation.max.file', [ 'attribute' => "Peça {$index} - Peça", 'max' => '20mb']),
                    "{$index}.arquivo.extensions" => \Lang::get('validation.extensions', [ 'attribute' => "Peça {$index} - Peça", 'values' => $mimes]),

//                    "{$index}.pdf.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - P.I. da peça "]),
//                    "{$index}.pdf.max" => \Lang::get('validation.max.file', [ 'attribute' => "Peça {$index} - P.I. da peça ", 'max' => '5mb']),
//                    "{$index}.pdf.extensions" => \Lang::get('validation.extensions', [ 'attribute' => "Peça {$index} - P.I. da peça ", 'values' => 'pdf']),
                ];
            }

            if( isset($peca['pis']) && is_array($peca['pis']) )
            {
                foreach($peca['pis'] as $pi_index => $pi ){
                    $messages += [
                        "{$index}.pis.{$pi_index}.pi_numero.required"=> \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - PI Número"]),
                        "{$index}.pis.{$pi_index}.pi_numero.numeric"=> \Lang::get('validation.numeric', [ 'attribute' => "Peça {$index} - PI Número"]),
                        "{$index}.pis.{$pi_index}.pdf-old.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - PI Peça"])
                    ];
                }
            }
        }

        return $messages;
    }

    public function getMetasValidatorInstance()
    {
        $categoria = Categoria::find($this->categoria_id);

        switch($categoria->abreviatura)
        {
            case 'MID':
                $validator = \Validator::make($this->metas + ['pis'=>$this->get('pis')], $this->midiaRules(), $this->midiaMessages());
            break;
            case 'PLA':
                $validator = \Validator::make($this->metas + ['pis'=>$this->get('pis')], $this->planejamentoRules(), $this->planejamentoMessages());
            break;
            default:
                $validator = \Validator::make($this->pecas, $this->categoriaNaoFixaRules($this->method()), $this->categoriaNaoFixaMessages());
        }

        return $validator;
    }

    protected function mimesType()
    {
        $categoria = Categoria::find($this->categoria_id);

        $tiposDeAnexo = unserialize($categoria->tipos_de_anexo);

        $mimes = [];
        if(in_array(Categoria::TIPO_DE_ANEXO_AUDIO, $tiposDeAnexo))
            $mimes[] = 'mp3';

        if(in_array(Categoria::TIPO_DE_ANEXO_VIDEO, $tiposDeAnexo))
            $mimes[] = 'flv';

        if(in_array(Categoria::TIPO_DE_ANEXO_IMAGEM, $tiposDeAnexo))
            $mimes[] = 'pdf';

        if(in_array(Categoria::TIPO_DE_ANEXO_IMAGEM_VIDEO, $tiposDeAnexo))
            $mimes[] = 'gif,swf';

        $mimes = implode(',', $mimes);

        return $mimes;
    }
}