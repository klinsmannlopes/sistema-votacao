<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Parametrizacao;
use App\Traits\RequestSanitizerTrait;

use App\Sanitizers\ParametrizacaoSanitizer as Sanitizer;

class JulgamentoRequest extends Request
{
    use RequestSanitizerTrait;
    public function setSanitizer()
    {
        return $this->sanitizer = new Sanitizer;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $notaMinima = (($row = Parametrizacao::where('key', 'nota_minima')->first()) ? $row->present()->nota() : 0);
        $notaMaxima = (($row = Parametrizacao::where('key', 'nota_maxima')->first()) ? $row->present()->nota() : 10);

        return [
            'avaliacoes' => [ "checkNotaMinima:{$notaMinima}", "checkNotaMaxima:{$notaMaxima}", 'checkNotaEmpty' ],
        ];
    }

    public function messages()
    {
        $notaMinima = ($row = Parametrizacao::where('key', 'nota_minima')->first()) ? $row->present()->nota() : '1,00';
        $notaMaxima = ($row = Parametrizacao::where('key', 'nota_maxima')->first()) ? $row->present()->nota() : '10,00';

        return [
            'avaliacoes.check_nota_minima' => "Você não pode dar nota menor que <b>\"{$notaMinima}\"</b>",
            'avaliacoes.check_nota_maxima' => "Você não pode dar nota maior que <b>\"{$notaMaxima}\"</b>",
            'avaliacoes.check_nota_empty' => 'Você deve avaliar todos os critérios e inscrições',
        ];
    }
}
