<?php

namespace App\Http\Requests;

use App\Categoria;
use App\Http\Requests\Request;

use App\Sanitizers\CategoriasSanitizer as Sanitizer;
use App\Traits\RequestSanitizerTrait;

class CategoriasCRUDRequest extends Request
{
    use RequestSanitizerTrait;

    public function authorize()
    {
        return true;
    }

    public function setSanitizer()
    {
        return $this->sanitizer = new Sanitizer;
    }

    public function rules()
    {

        if(is_edicao_profissional())
        {
            $rules = [
                'nome'        => 'required',
                'abreviatura' => 'required|min:3|max:5|alpha|unique:categorias,abreviatura,NULL,id,deleted_at,NULL',
            ];

        }
        else
        {
            $rules = [
                'nome'           => 'required',
                'abreviatura'    => 'required|min:3|max:5|alpha|unique:categorias,abreviatura,NULL,id,deleted_at,NULL',
                'qtd_pecas'      => 'required|integer|min:1',
                'qtd_pecas_tipo' => 'required',
                'tipos_de_anexo' => 'required',
                'criterios'      => 'required',
                'peso'           => 'required',
            ];
        }

        switch($this->method()) {
            case 'PUT':
            case 'PATCH':
            {
                $row = Categoria::WithAllEditions()->find($this->row_id);

                if(!$row->is_fixo)
                {
                    $rules = array_merge($rules, [
                        'abreviatura' => "required|min:3|max:5|alpha|unique:categorias,abreviatura,{$row->id},id,deleted_at,NULL",
                    ]);
                }
                else
                {
                    $rules = [
                        'criterios' => 'required'
                    ];
                }
            }
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'nome.required' => \Lang::get('validation.required', [ 'attribute' => 'Nome' ]),
            'abreviatura.required' => \Lang::get('validation.required', [ 'attribute' => 'Abreviatura' ]),
            'abreviatura.alpha' => \Lang::get('validation.alpha', [ 'attribute' => 'Abreviatura' ]),
            'abreviatura.min' => \Lang::get('validation.min.string', [ 'attribute' => 'Abreviatura' ]),
            'abreviatura.max' => \Lang::get('validation.max.string', [ 'attribute' => 'Abreviatura' ]),
            'qtd_pecas.required' => \Lang::get('validation.required', [ 'attribute' => 'Quantidade de peças' ]),
            'qtd_pecas.min' => \Lang::get('validation.min.numeric', [ 'attribute' => 'Quantidade de peças' ]),
            'qtd_pecas_tipo.required' => 'Você deve selecionar se a <b>"quantidade de peças"</b> é: <b>mínima</b>, <b>máxima</b>, <b>exatamente</b>.',
            'tipos_de_anexo.required' => \Lang::get('validation.required', [ 'attribute' => 'Tipo de arquivo' ]),
            'criterios.required' => \Lang::get('validation.required', [ 'attribute' => 'Critérios' ]),
            'peso.required' => \Lang::get('validation.required', [ 'attribute' => 'Peso' ]),
        ];
    }
}
