<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CadastroDeParticipantesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'participantes' => 'required|max:5120'
        ];
    }

    public function messages()
    {
        return [
            'participantes.required' => \Lang::get('validation.required', ['attribute' => 'Lista de participantes']),
            'participantes.mimes' => \Lang::get('validation.mimes', ['attribute' => 'Lista de participantes', 'values' => 'xls, xlsx']),
        ];
    }
}
