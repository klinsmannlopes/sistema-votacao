<?php
/**
 * Created by PhpStorm.
 * User: 750371426
 * Date: 08/12/2016
 * Time: 08:53
 */

namespace App\Http\Requests;

use App\Http\Requests\Request;


class DesempatarJugamentoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ordenacao' => 'arrayWithoutEmptyValues|arrayWithoutRepeatedValues'
        ];
    }

    public function messages()
    {
        return [
            'ordenacao.array_without_empty_values' => 'Você deve ordenar todas as inscrições',
            'ordenacao.array_without_repeated_values' => 'Você não pode repetir ordenação',
        ];
    }
}

