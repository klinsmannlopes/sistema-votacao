<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EnviarInscricaoParaCorrecaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'justificativa' => 'required',
            'pecas_rejeitadas' => 'required_without:pis_rejeitadas',
            'pis_rejeitadas' => 'required_without:pecas_rejeitadas',
        ];

        if(!isset($this->pecas_rejeitadas))
            unset($rules['pecas_rejeitadas']);

        if(!isset($this->pis_rejeitadas))
            unset($rules['pis_rejeitadas']);

        return $rules;
    }

    public function messages()
    {
        return [
            'justificativa.required' => \Lang::get('inscricoes.enviar-para-correcao.justificativa'),
            'pecas_rejeitadas.required_without' => \Lang::get('inscricoes.pecas_rejeitadas.required_without'),
            'pis_rejeitadas.required_without' => \Lang::get('inscricoes.pis_rejeitadas.required_without'),
        ];
    }
}