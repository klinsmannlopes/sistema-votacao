<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DesempatarTriagemRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ordenacao' => 'min:10|max:10'
        ];
    }
    public function messages()
    {
        return [
             'ordenacao.min' => 'O total de inscrições é menor que <b>:min</b>',
             'ordenacao.max' => 'O total de inscrições é maior que o permitido. É esperado <b>:max</b>',
        ];
    }
}
