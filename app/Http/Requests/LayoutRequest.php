<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LayoutRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'envelopagem' => 'required_without:logo|image|max:5120',
            'logo' => 'required_without:envelopagem|image|max:5120'
        ];
    }
}
