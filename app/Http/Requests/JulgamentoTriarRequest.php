<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JulgamentoTriarRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'inscricoes'  => 'required|arraySize:10',
        ];
    }

    public function messages()
    {
        return [
            'inscricoes.required'   => 'Você deve escolher 10 inscrições para a próxima fase.',
            'inscricoes.array_size'  => 'Você deve escolher 10 inscrições para a próxima fase.',
        ];
    }
}
