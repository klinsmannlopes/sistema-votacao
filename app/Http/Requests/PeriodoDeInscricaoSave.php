<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Sanitizers\PeriodoDeInscricaoSanitizer as Sanitizer;
use App\Traits\RequestSanitizerTrait;

class PeriodoDeInscricaoSave extends Request
{
    use RequestSanitizerTrait;

    public function authorize()
    {
        return true;
    }

    public function setSanitizer()
    {
        return $this->sanitizer = new Sanitizer;
    }

    public function rules()
    {
        return [
            'inscricoes_data_inicial' => 'required|date_format:Y-m-d H:i:s',
            'inscricoes_data_final' => 'required|date_format:Y-m-d H:i:s',
        ];
    }

    public function messages()
    {
        return [
            'inscricoes_data_inicial.required' => \Lang::get('validation.required', [ 'attribute' => 'Início das inscrições de peças' ]),
            'inscricoes_data_final.required' => \Lang::get('validation.required', [ 'attribute' => 'Fim das inscrições de peças' ]),
            'inscricoes_data_inicial.date_format' => \Lang::get('validation.date_format', [ 'attribute' => 'Início das inscrições de peças' ]),
            'inscricoes_data_final.date_format' => \Lang::get('validation.date_format', [ 'attribute' => 'Início das inscrições de peças' ]),
        ];
    }
}
