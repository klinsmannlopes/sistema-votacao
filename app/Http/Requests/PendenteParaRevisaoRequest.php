<?php
/**
 * Created by PhpStorm.
 * User: 750371871
 * Date: 14/12/2018
 * Time: 15:13
 */

namespace App\Http\Requests;


class PendenteParaRevisaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'justificativa' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'justificativa.required' => \Lang::get('inscricoes.pendente-para-revisao.justificativa'),
        ];
    }
}