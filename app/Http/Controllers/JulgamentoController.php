<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class JulgamentoController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'julgamento');
        \View::share('comboboxCategorias', (new Categoria)->getWithJulgamentosRealizados());

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:{$this->namespace}");
    }

    public function categorias()
    {
        $rows = with(new Categoria())->getCategoriesWithInscriptions();
        return view("{$this->namespace}.categorias", compact('rows'));
    }

    public function index($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        if( ! $categoria->is_triagem ) {
            $rows = $categoria->inscricoesValidas();
        }else{
            $rows = $categoria->topTriagem();
        }

        $tamanhoTop = 5;

        $julgamentoEmpatado = $categoria->isJulgamentoEmpatado($tamanhoTop) && !$categoria->isJulgamentoDesempatado();

        $top5 = $categoria->top($tamanhoTop);

        if(! $julgamentoEmpatado)
            $top5 = $top5->slice(0, $tamanhoTop);

        $julgamentoDesempatado  = $categoria->isJulgamentoDesempatado();
        $inscricoesDuplicadas   = $categoria->getInscricoesDuplicadas();

        return view("{$this->namespace}.index", compact('categoria', 'rows', 'julgamentoDesempatado', 'top5', 'julgamentoEmpatado','inscricoesDuplicadas'));
    }
}