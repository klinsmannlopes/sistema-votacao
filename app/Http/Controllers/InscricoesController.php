<?php

namespace App\Http\Controllers;

use App\Events\CreatedInscricaoEvent;
use App\Events\UpdatedInscricaoEvent;
use App\Http\Requests\CancelarInscricaoRequest;
use App\Http\Requests\PendenteParaRevisaoRequest;
use App\Inscricao as Model;
use App\Categoria;
use App\Http\Requests\InscricoesCRUDRequest as Request;
use App\Inscricao;
use App\InscricaoMeta;
use App\InscricaoPeca;
use App\LogPeca;
use App\Parametrizacao;
use App\PI;
use App\Role;
use App\User;
use App\Veiculo;

use App\Mailers\AssociarOPECMailer;
use App\Mailers\ValidacaoInscricao;

use App\Http\Requests\EnviarInscricaoParaCorrecaoRequest;

use FontLib\Header;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use JavaScript;

class InscricoesController extends Controller
{

    public function __construct(Model $model)
    {
        parent::__construct();

        $this->model = $model;

        \View::share('namespace', $this->namespace = 'inscricoes');
    }

    public function index()
    {

        if( \Request::ajax() ) {
            $inscricao_id = \Input::get('inscricoes');
            if( $inscricao_id ){
                $result = $this->isMultiplaOPECByInscricao( $inscricao_id );    
                print $result;
            }
            exit;
        }

        $rows = $this->getFilteredList();

        if(\Gate::check('log'))
            $rows = $rows->withTrashed();

        $rows = $rows->paginate(env('ROWS_PER_PAGE'));

        return view("{$this->namespace}.index", $this->getIndexSupplies() + compact('rows', 'search', 'status', 'categoria_id'));
    }

    public function show($id)
    {
        $readonly = true;
        JavaScript::put(compact('readonly'));

        $idForm = '';
        $route = '';

        if(\Gate::check($this->namespace))
        {
            $idForm = 'validar';
            $route = "{$this->namespace}.validar";
        }

        if(\Gate::check("{$this->namespace}-opec"))
        {
            $idForm = 'aprovar';
            $route = "{$this->namespace}.aprovar";
        }

        $row = $this->model->find($id);
        
        return view("{$this->namespace}.show", $this->getFormSupplies($row) + compact('row', 'readonly', 'route', 'idForm'));
    }

    public function log($id)
    {
        $readonly = true;

        $row = $this->model->withTrashed()->find($id);

        return view("{$this->namespace}.log", $this->getFormSupplies($row) + compact('row', 'readonly'));
    }

    public function create()
    {
        $uploadPecaRoute = route("{$this->namespace}.upload-peca");
        $uploadPIRoute = route("{$this->namespace}.upload-pi");
        JavaScript::put(compact('uploadPecaRoute', 'uploadPIRoute'));

        $pecaHTML = $this->getHTMLPeca([ 'index' => '%INDEX%' ]);
        JavaScript::put(compact('pecaHTML'));

        return view("{$this->namespace}.create", $this->getFormSupplies());
    }

    public function store(Request $request)
    {
        $categoria_id = $request->get('categoria_id');

        if(! app('periodoDeInscricao'))
        {
            \Flash::info(\Lang::get("{$this->namespace}.encerradas"));
            return redirect(route("{$this->namespace}.index"));
        }

        $validator = $request->getMetasValidatorInstance();

        if(isset($categoria_id) && !empty($categoria_id)){
            $categoria = Categoria::find($categoria_id);
        }

        if(isset($categoria->has_pis) && $categoria->has_pis>0){
            $this->validate($request, array(
                'pecas.*.pis.*.pi_numero'=>'required|numeric'
            ));
        }else{
            $this->validate($request, array(
                'pecas.*'=>'required'
            ));
        }
        
        if($validator->fails())
            $this->throwValidationException($request, $validator);

        $all = $request->all();

        $all['user_id'] = \Auth::getUser()->id;

        \DB::transaction(function() use ($all)
        {
            $this->created_row = $row = Inscricao::create($all);
            $this->saveInscricaoMetas($row, $all);
        });
        
        \Event::fire(new CreatedInscricaoEvent($this->created_row));

        return redirect(route("{$this->namespace}.continuar", [ $this->created_row->id ]));
    }

    public function edit($id)
    {
        $row = $this->model->find($id);

        if(\Gate::denies('owner', $row))
            abort(403);

        $uploadPecaRoute = route("{$this->namespace}.upload-peca");
        $uploadPIRoute = route("{$this->namespace}.upload-pi");
        JavaScript::put(compact('uploadPecaRoute', 'uploadPIRoute'));
        $readonly = null;
        $buttonsDisabled = null;
        $status_incricao = null;

        if(in_array($row->status, [ Model::STATUS_APROVADA_PELA_OPEC, Model::STATUS_VALIDA, Model::STATUS_CANCELADA, Model::STATUS_CANCELADA_PELO_PARTICIPANTE ]))
        {
            $readonly = true;
            $buttonsDisabled = 'disabled';
        }



        if ($row->status === Model::STATUS_CANCELADA && \Auth::user()->role === 'participante')
        {
            $status_incricao = true;
        }

        if ($row->status === Model::STATUS_PEDENTE_PARA_REVISAO)
        {
            $buttonsDisabled = 'disabled';
        }

        $pendenteDeRevisao = $row->is_pendente_para_revisao();
        $pendenteDeCorrecao = $row->is_pendente_de_correcao();
        $pecaHTML = $this->getHTMLPeca([ 'index' => '%INDEX%' ]);
        JavaScript::put(compact('pecaHTML', 'pendenteDeCorrecao', 'readonly', 'pendenteDeRevisao'));

        return view("{$this->namespace}.edit", $this->getFormSupplies($row) + compact('row', 'readonly', 'pendenteDeCorrecao', 'pendenteDeRevisao', 'buttonsDisabled', 'status_incricao'));
    }

    public function update(Request $request, $id)
    {
        $validator = $request->getMetasValidatorInstance();

        $categoria_id = $request->get('categoria_id');
        $categoria = [];

        if(isset($categoria_id) && !empty($categoria_id)){
            $categoria = Categoria::find($categoria_id);
        }
        
        if(isset($categoria->has_pis) && $categoria->has_pis>0){
            $this->validate($request, array(
                'pecas.*.pis.*.pi_numero'=>'required|numeric'
            ));
        }else{
            $this->validate($request, array(
                'pecas.*'=>'required'
            ));
        }

        if($validator->fails()){
            $this->throwValidationException($request, $validator);
        }

        $model = $this->model;

        \DB::transaction(function() use ($model, $request, $id)
        {
            $row = $model->find($id);

            $status = Model::STATUS_AGUARDANDO_VALIDACAO;
            $status_opec = $row->status_opec;

            if($row->status == Model::STATUS_PENDENTE_DE_CORRECAO) {
                $status = Model::STATUS_CORRIGIDO_PELO_PARTICIPANTE;
            }
            if($row->status_opec == Model::STATUS_PENDENTE_DE_CORRECAO_OPEC) {
                $status = $row->status;
                $status_opec = Model::STATUS_CORRIGIDO_PELO_PARTICIPANTE_OPEC;
            }

            $row->fill($all = $request->all());
            $row->status = $status;
            $row->status_opec = $status_opec;
            $row->save();

            $row->metas()->delete();
            $row->pecas()->delete();

            $this->saveInscricaoMetas($row, $all);

            \Event::fire(new UpdatedInscricaoEvent($row));

            \Flash::success(\Lang::get("{$this->namespace}.atualizada"));
        });

        return redirect(route("{$this->namespace}.index"));
    }

    public function destroy($id)
    {
        $ids = explode(',', $id);

        foreach($ids as $id)
            if(is_numeric($id))
                $this->model->find($id)->delete();

        \Flash::success(\Lang::get('crud.destroyed'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function associarOPEC($ids, $user_id)
    {
        $associados = [];
        foreach(explode(',', $ids) as $id)
        {
            if(is_numeric($id))
            {
                $inscricao = Inscricao::find($id);
                $inscricao->user_opec_id = $user_id;
                $inscricao->status_opec  = Model::STATUS_AGUARDANDO_VALIDACAO_OPEC;
                $inscricao->save();
                $associados[] = $inscricao->present()->numeroInscricao();
            }
        }
        if($associados)
            AssociarOPECMailer::send(compact('associados', 'user_id'));

        \Flash::success('Inscrições <b>"associadas"</b> com sucesso!');

        return redirect(route("{$this->namespace}.index"));
    }

    public function aprovar($id)
    {
        $row = $this->model->find($id);
        //$row->status = Model::STATUS_APROVADA_PELA_OPEC;
        $row->status_opec = Model::STATUS_APROVADA_PELA_OPEC;
        $row->justificativa = null;
        $row->save();

        $this->unRejectPIs($row->pis);

        foreach($row->pecas as $peca)
        {
            $peca->pi_rejeitada = 0;
            $peca->peca_rejeitada = 0;
            $this->unRejectPIs($peca->pis);
            $peca->save();
        }

        \Flash::success(\Lang::get("{$this->namespace}.aprovada"));

        return redirect(route("{$this->namespace}.index"));
    }

    /**
     * @param $collection
     *
     * @return void
     */
    protected function unRejectPIs($collection)
    {
        if( !is_null($collection) && ( is_array($collection) || $collection instanceof Arrayable ) )
        {
            foreach($collection as $pi)
            {
                if( $pi instanceof PI)
                {
                    $pi->pi_rejeitada = 0;
                    $pi->save();
                }
            }
        }
    }

    /**
     * @param $collection
     *
     * @return void
     */
    protected function updatePis(array $collection)
    {
        foreach($collection as $pi_data)
        {
            if( isset($pi_data['id']) && !empty($pi_data['id']) ){
                $pi = PI::find($pi_data['id']);

                if( $pi )
                {
                    $pi_data = array_merge(['pi_rejeitada'=>false], $pi_data);
                    $pi->fill($pi_data);
                    $pi->save();
                }
            }
        }
    }

    public function enviarParaCorrecao(EnviarInscricaoParaCorrecaoRequest $request, $id)
    {
        $this->middleware("permissions:{$this->namespace}.enviar-para-correcao");

        if ( \Auth::user()->role === 'opec')
        {
            $row = $this->model->find($id);
            $row->status_opec = Model::STATUS_PENDENTE_DE_CORRECAO_OPEC;
            $row->justificativa = $request->justificativa;
            $row->save();

            $pis_rejeitadas = explode(',', $request->pis_rejeitadas);
            $pecas_rejeitadas = explode(',', $request->pecas_rejeitadas);

            if( $pis_rejeitadas && is_array($pis_rejeitadas) ){
                foreach($pis_rejeitadas as $pi_id)
                {
                    $pi = PI::find($pi_id);

                    if( $pi ) {
                        $pi->pi_rejeitada = 1;
                        $pi->save();
                    }
                }
            }

            foreach($row->pecas as $peca)
            {
                //$peca->pi_rejeitada = in_array($peca->id, $pis_rejeitadas);
                $peca->peca_rejeitada = in_array($peca->id, $pecas_rejeitadas);
                $peca->save();
            }

            ValidacaoInscricao::sendRejected([ 'inscricao' => $row ]);

            \Flash::success(\Lang::get("{$this->namespace}.enviada-para-correcao"));

            return redirect(route("{$this->namespace}.index"));
            }
            else {
            $row = $this->model->find($id);
            $row->status = Model::STATUS_PENDENTE_DE_CORRECAO;
            $row->justificativa = $request->justificativa;

            $row->save();

            $pis_rejeitadas = explode(',', $request->pis_rejeitadas);
            $pecas_rejeitadas = explode(',', $request->pecas_rejeitadas);

            if( $pis_rejeitadas && is_array($pis_rejeitadas) ){
                foreach($pis_rejeitadas as $pi_id)
                {
                    $pi = PI::find($pi_id);

                    if( $pi ) {
                        $pi->pi_rejeitada = 1;
                        $pi->save();
                    }
                }
            }

            foreach($row->pecas as $peca)
            {
                //$peca->pi_rejeitada = in_array($peca->id, $pis_rejeitadas);
                $peca->peca_rejeitada = in_array($peca->id, $pecas_rejeitadas);
                $peca->save();
            }

            ValidacaoInscricao::sendRejected([ 'inscricao' => $row ]);

            \Flash::success(\Lang::get("{$this->namespace}.enviada-para-correcao"));

            return redirect(route("{$this->namespace}.index"));
        }




    }

    //ALTERANDO O STATUS DA INSCRIÇÃO PARA PENDENTE PARA REVISAO
    public function pendenteParaRevisao(PendenteParaRevisaoRequest $request, $id)
    {
        $this->middleware("permissions:{$this->namespace}.pendente-para-revisao");

        $row = $this->model->find($id);
        $row->status = Model::STATUS_PEDENTE_PARA_REVISAO;
        $row->justificativa = $request->justificativa;
        $row->save();

        ValidacaoInscricao::sendReview([ 'inscricao' => $row ]);

        \Flash::success(\Lang::get("{$this->namespace}.pendente-para-revisao"));

        return redirect(route("{$this->namespace}.index"));
    }


    //ALTERANDO O STATUS DA INSCRIÇÃO PARA VÁLIDA
    public function validar($id)
    {
        $this->middleware("permissions:{$this->namespace}.validar");

        $row = $this->model->find($id);
        $row->status = Model::STATUS_VALIDA;
        $row->justificativa = null;
        $row->save();

        ValidacaoInscricao::send([ 'inscricao' => $row ]);

        \Flash::success(\Lang::get("{$this->namespace}.validada"));

        return redirect(route("{$this->namespace}.index"));
    }

    //ALTERANDO O STATUS DA INSCRIÇÃO PARA CANCELADA
    public function cancelar(CancelarInscricaoRequest $request, $id)
    {
        $row = $this->model->find($id);
        $row->status = Model::STATUS_CANCELADA;
        $row->justificativa = $request->justificativa;
        $row->save();
        
        ValidacaoInscricao::sendCancel([ 'inscricao' => $row ]);

        \Flash::success(\Lang::get("{$this->namespace}.cancelada"));

        return redirect(route("{$this->namespace}.index"));
    }

    public function canceladaPeloParticipante($id)
    {
        $row = $this->model->find($id);
        $row->status = Model::STATUS_CANCELADA_PELO_PARTICIPANTE;
        $row->save();

        ValidacaoInscricao::sendUserCancel([ 'inscricao' => $row ]);

        \Flash::success(\Lang::get("{$this->namespace}.cancelada"));

        return redirect(route("{$this->namespace}.index"));
    }

    public function imprimir()
    {
        $print = true;
        $rows = $this->getFilteredList()->get();

        return view("{$this->namespace}.imprimir", compact('rows', 'print'));
    }

    public function pdf($id)
    {
        $viewPdf = true;
        $readonly = true;
        $row = $this->model->find($id);

        return view("{$this->namespace}.pdf", $this->getFormSupplies($row) + compact('row', 'readonly', 'viewPdf'));
    }

    public function xls()
    {
        $rows = $this->getFilteredList()->get();
        $qtdPecas = $this->getMaxQtdPecas();

        return \Excel::create(uniqid('inscricoes_'), function($excel) use ($rows, $qtdPecas)
        {
            $excel->sheet('Inscrições', function($sheet) use ($rows, $qtdPecas)
            {
                $sheet->loadView("{$this->namespace}.partials.table-xls", compact('rows', 'qtdPecas'));
            });
        })
        ->export('xls');
    }

    public function csv()
    {
        $is_csv = true;
        $rows = $this->getFilteredList()->get();
        $qtdPecas = $this->getMaxQtdPecas();

        \Excel::create($fileName = uniqid('inscricoes_'), function($excel) use ($is_csv, $rows, $qtdPecas)
        {
            $excel->sheet('Inscrições', function($sheet) use ($is_csv, $rows, $qtdPecas)
            {
                $sheet->loadView("{$this->namespace}.partials.table-csv", compact('rows', 'qtdPecas'));
            });
        })->store('csv', storage_path('csv/exports'));

        $fileName .= '.csv';

        $filePath = storage_path("csv/exports/{$fileName}");

        ob_start();

        readfile($filePath);
        $fileContent = ob_get_contents();

        ob_end_clean();

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");

        //BUGFIX - Adiciona o BOM
        echo chr(255) . chr(254);

        $output = iconv('UTF-8', 'UTF-16LE//IGNORE', $fileContent);
        @unlink($filePath);

        die($output);
    }

    public function continuar($id)
    {
        $row = $this->model->find($id);

        return view("{$this->namespace}.continuar", compact('row'));
    }

    public function uploadPeca()
    {
        $newArquivo = false;
        $all = \Input::all();

        if($pecas = $all['pecas'])
        {
            foreach($pecas as $index => $peca)
            {
                $mimes = $this->getArquivoMimeTypes($all['categoria_id']);

                $validator = \Validator::make(
                    [ "arquivo" => $peca['arquivo']],
                    [ "arquivo" => "required|max:20480|extensions:{$mimes}"],
                    [
                        "arquivo.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$index} - Peça"]),
                        "arquivo.max" => \Lang::get('validation.max.file', [ 'attribute' => "Peça {$index} - Peça", 'max' => '20mb']),
                        "arquivo.extensions" => \Lang::get('validation.extensions', [ 'attribute' => "Peça {$index} - Peça", 'values' => $mimes]),
                    ]
                );
                
                if($validator->fails())
                    return $validator->errors();

                $peca['arquivo']->move(
                    app_path_uploads('pecas'),
                    $newArquivo = generate_file_name($peca['arquivo']->getClientOriginalExtension())
                );

                $newFileURL = url_uploads("pecas/{$newArquivo}");

                $key = 'arquivo-old';

                return view("{$this->namespace}.partials.uploaded-file", compact('index', 'newArquivo', 'newFileURL', 'key'))->render();
            }
        }

        return url_uploads("pecas/{$newArquivo}");
    }

    public function uploadPi()
    {
        $newArquivo = false;
        $all = \Input::all();

        if( isset($all['pecas']) && $pecas = $all['pecas'])
        {
            foreach($pecas as $index => $peca)
            {
                foreach($peca['pis'] as $pi_index => $pi)
                {
                    return $this->piUploadProcess($pi, $pi_index, 'peca', $index);
                }
            }
        }else if( isset($all['pis']) && $pis = $all['pis'])
        {
            foreach($pis as $index => $pi)
            {
                return $this->piUploadProcess($pi, $index, 'inscricao');
            }
        }

        return url_uploads("pecas/{$newArquivo}");
    }

    /**
     * @param array $pi
     *
     * @return \Illuminate\Support\MessageBag|mixed|null|string
     */
    protected function piUploadProcess(array $pi, $pi_index, $owner_type, $owner_index = null)
    {
        $validator = \Validator::make(
            [ "pdf" => $pi['pdf']],
            [ "pdf" => 'required|max:5120|extensions:pdf'],
            [
                "pdf.required" => \Lang::get('validation.required', [ 'attribute' => "Peça {$owner_index} - P.I. da peça "]),
                "pdf.max" => \Lang::get('validation.max.file', [ 'attribute' => "Peça {$owner_index} - P.I. da peça ", 'max' => '5mb']),
                "pdf.extensions" => \Lang::get('validation.extensions', [ 'attribute' => "Peça {$owner_index} - P.I. da peça ", 'values' => 'pdf']),
            ]
        );

        if($validator->fails())
            return $validator->errors();

        $pi['pdf']->move(
            app_path_uploads('pecas-pdf'),
            $newArquivo = generate_file_name($pi['pdf']->getClientOriginalExtension())
        );

        $newFileURL = url_uploads("pecas-pdf/{$newArquivo}");

        $key    = 'pdf-old';
        $index  = $owner_index;

        return view("{$this->namespace}.partials.uploaded-pi-file", compact('index', 'newArquivo', 'newFileURL', 'key', 'pi_index','owner_type', 'owner_index'))->render();
    }

    protected function getArquivoMimeTypes($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        $tiposDeAnexo = unserialize($categoria->tipos_de_anexo);

        $mimes = [];
        if(in_array(Categoria::TIPO_DE_ANEXO_AUDIO, $tiposDeAnexo))
            $mimes[] = 'mp3';

        if(in_array(Categoria::TIPO_DE_ANEXO_VIDEO, $tiposDeAnexo))
            $mimes[] = 'flv';

        if(in_array(Categoria::TIPO_DE_ANEXO_IMAGEM, $tiposDeAnexo))
            $mimes[] = 'pdf';

        if(in_array(Categoria::TIPO_DE_ANEXO_IMAGEM_VIDEO, $tiposDeAnexo))
            $mimes[] = 'gif,swf';

        $mimes = implode(',', $mimes);

        return $mimes;
    }

    protected function PeriodoDeInscricaoAtivo()
    {
        $now = strtotime(date('Y-m-d H:i:s'));

        $ini = Parametrizacao::where('key', 'inscricoes_data_inicial')->first();
        $fim = Parametrizacao::where('key', 'inscricoes_data_final')->first();

        return $ini->value && $fim->value && $now >= strtotime($ini->value) && $now <= strtotime($fim->value);
    }

    protected function getMaxQtdPecas()
    {
        $sql = "SELECT MAX(counted) as qtd FROM
                    (
                        SELECT COUNT(*) AS counted
                        FROM inscricoes_peca
                        WHERE inscricao_id and deleted_at is null
                        GROUP BY inscricao_id
                    ) AS counts";

        $qtdPecas = \DB::select($sql)[0]->qtd ? : 0;

        return $qtdPecas;
    }

    protected function getFilteredList()
    {
        \DB::enableQueryLog();

        $search = \Input::get('search');
        $status = \Input::get('status');
        $categoria_id = \Input::get('categoria_id');
        //
       // $status_opec = \Input::get('statusOpec');
        //
        view()->share(compact('search', 'status', 'categoria_id'));
        
        $query = $this->model;

        if($search)
        {
            $query = $query->where(function($q) use ($search)
            {
                if(\Gate::check('inscricoes'))
                {
                    $q = $q->whereHas('user', function($q2) use ($search)
                    {
                        $q2->where('agency', 'LIKE', "%{$search}%");
                    });
                }

                $q
                    ->orWhere('anunciante', 'LIKE', "%{$search}%")
                    ->orWhere(function($q1) use ($search)
                    {
                        $q1
                            ->whereHas('categoria', function($q2) use ($search)
                            {
                                $q2->where(\DB::raw("CONCAT(categorias.abreviatura,LPAD(inscricoes.id,5,'0'))"), $search);
                            });
                    });
            });
        }

        if($status)
            $query = $query->where('status', $status)
                            ->orWhere('status_opec', $status);
            ;

        if($categoria_id)
            $query = $query->where('categoria_id', $categoria_id);

        //
       // if($status){
         //   $query = $query->where('status_opec', $status);
        //}
        //

        $user = \Auth::user();

        if($user->role === Role::PARTICIPANTE)
            $query = $query->where('user_id', $user->id);

        if($user->role === Role::OPEC)
            $query = $query->where('user_opec_id', $user->id);

        //SOMENTE INSCRIÇÕES ONDE AS CATEGORIAS ESTEJAM ATIVAS
        $query = $query->whereHas('categoria', function($q) use ($search)
        {
            $q->where('is_ativo', 1);
        });

        $rows = $query->orderby('created_at', 'DESC');

        return $rows;
    }

    protected function getIndexSupplies()
    {
        $categorias   = Categoria::orderby('nome')->get();
        $usuariosOpec = with(new User())->getOpec();
        $statusList   = Inscricao::groupby('status')->orderby('status')->lists('status')->toArray();
        $statusOpec   = Inscricao::groupby('status_opec')->orderby('status_opec')->lists('status_opec')->toArray();

        return compact('categorias', 'usuariosOpec', 'statusList', 'statusOpec');
    }

    protected function getFormSupplies($row = null)
    {        
        $agencia = \Auth::user()->agency;
        $endereco = \Auth::user()->address;
        $has_pis = 1;

        $veiculosPorTipo = Veiculo::all();
        $veiculosFormatos = Veiculo::getFormatos();
        $tiposVeiculos = Veiculo::getTipos();
        $categorias = Categoria::where('is_ativo', 1)->orderby('nome')->get();

        JavaScript::put([ 'categorias' => $categorias->keyBy('id')->toArray() ]);

        $midiaClass = $planejamentoClass = $naoFixaClass = $piClass = 'hidden';

        if($row)
        {
            $agencia = $row->user->agency;
            $endereco = $row->user->address;

            $abreviatura = $row->categoria->abreviatura;
            switch($abreviatura)
            {
                case 'MID':
                    $midiaClass = '';
                    $piClass = '';
                break;
                case 'PLA':
                    $planejamentoClass = '';
                    $piClass = '';
                break;
                default:
                    $naoFixaClass = '';
            }

            //EXIBIÇÃO DOS BOTÕES DE ACORDO COM A ROLE E STATUS DA INSCRIÇÃO
            if(\Gate::check($this->namespace))
            {
                $validarDisabled = '';
                $enviarParaCorrecaoDisabled = '';
                $cancelarDisabled = '';
                $status = '';

                if($row->status === Model::STATUS_VALIDA)
                    $validarDisabled = 'disabled';

                if($row->status === Model::STATUS_PENDENTE_DE_CORRECAO || $row->status_opec === Model::STATUS_PENDENTE_DE_CORRECAO_OPEC)
                    $enviarParaCorrecaoDisabled = 'disabled';
                    $status = $row->status;


                if( in_array( $row->status, [ Model::STATUS_CANCELADA, Model::STATUS_CANCELADA_PELO_PARTICIPANTE, Model::STATUS_PEDENTE_PARA_REVISAO ] ) )
                {
                    $validarDisabled = 'disabled';
                    $enviarParaCorrecaoDisabled = 'disabled';
                    $cancelarDisabled = 'disabled';
                }
            }

            if(\Gate::check("{$this->namespace}-opec"))
            {
                $aprovarDisabled = !empty($row->status) && $row->status === Model::STATUS_APROVADA_PELA_OPEC ? 'disabled' : '';
                $enviarParaCorrecaoDisabled = !empty($row->status) && $row->status === Model::STATUS_PENDENTE_DE_CORRECAO || !empty($row->status_opec) && $row->status_opec == Model::STATUS_PENDENTE_DE_CORRECAO_OPEC ? 'disabled' : '';


                if(!empty($row->status) && in_array($row->status, [  Model::STATUS_CANCELADA, Model::STATUS_PEDENTE_PARA_REVISAO, Model::STATUS_CANCELADA_PELO_PARTICIPANTE ]))
                {
                    $aprovarDisabled = 'disabled';
                    $enviarParaCorrecaoDisabled = 'disabled';
                }
            }

            //ESCOLHENDO A COR DA MENSAGEM DE ACORDO COM O STATUS
            switch($row->status)
            {
                case Model::STATUS_AGUARDANDO_VALIDACAO:
                    $color = '#FFD75C'; //amarelo
                break;

                case Model::STATUS_CORRIGIDO_PELO_PARTICIPANTE:
                    $color = '#5B9BD5'; //AZUL
                    break;

                case Model::STATUS_PEDENTE_PARA_REVISAO:
                    $color = '#8053BA'; //roxo
                break;

                case Model::STATUS_CANCELADA_PELO_PARTICIPANTE:
                    $color = '#E36868'; //VERMELHO
                break;

                case Model::STATUS_PENDENTE_DE_CORRECAO:
                    $color = '#FFCB00'; //AMARELO
                break;

                case Model::STATUS_CANCELADA:
                    $color = '#D2D2D2'; //CINZA
                break;

                case Model::STATUS_APROVADA_PELA_OPEC:
                    $color = '#5B9BD5'; //AZUL
                break;

                case Model::STATUS_VALIDA:
                    $color = '#71BF63'; //VERDE
                break;

                default:
                    $color = '';
            }
        }

        $authUser = \Auth::getUser();

        return compact(

            'agencia',
            'endereco',

            //CLASSES
            'midiaClass',
            'planejamentoClass',
            'naoFixaClass',
            'piClass',

            //SUPPLIES
            'veiculosPorTipo',
            'veiculosFormatos',
            'categorias',
            'tiposVeiculos',

            //BUTTONS
            'aprovarDisabled',
            'enviarParaCorrecaoDisabled',
            'validarDisabled',
            'cancelarDisabled',
            'status',

            //MESSAGE COLOR
            'color',

            //USER
            'authUser'
        );
    }

    protected function getHTMLPeca($params)
    {
        return str_replace(PHP_EOL, '', \View::make("{$this->namespace}.partials.form-peca", $params + $this->getFormSupplies())->render());
    }

    protected function upload_peca(&$peca)
    {
        if(!empty($peca['arquivo']))
        {
            $peca['arquivo']->move(
                app_path_uploads('pecas'),
                $newArquivo = generate_file_name($peca['arquivo']->getClientOriginalExtension())
            );

            $peca['arquivo'] = $newArquivo;
        }
        else if(!empty($peca['arquivo-old']))
        {
            $peca['arquivo'] = $peca['arquivo-old'];
        }

        if( isset($peca['pis']) && !empty($peca['pis']))
        {
            foreach($peca['pis'] as $pi_index => $pi)
            {
                if(!empty($pi['pdf']))
                {
                    $pi['pdf']->move(
                        app_path_uploads('pecas-pdf'),
                        $newPDF = generate_file_name($pi['pdf']->getClientOriginalExtension())
                    );

                    $peca['pis'][$pi_index]['pdf'] = $newPDF;
                }
                else if(!empty($pi['pdf-old']))
                {
                    $peca['pis'][$pi_index]['pdf'] = $pi['pdf-old'];
                }
            }
        }
    }


    protected function saveInscricaoMetas($inscricao, $all)
    {
        $inscricao_id = $inscricao->id;

        if(in_array($inscricao->categoria->abreviatura, [ 'MID', 'PLA' ]))
        {
            foreach($all['metas'] as $key => $value)
            {
                if($value)
                    InscricaoMeta::create(compact('inscricao_id', 'key', 'value'));
            }
        }
        else
        {
            foreach($all['pecas'] as $key => $peca)
            {
                $this->upload_peca($peca);

                $newRow = InscricaoPeca::create(compact('inscricao_id') + $peca);

                if(!empty($peca['peca_rejeitada']))
                {
                    $newRow->peca_rejeitada = true;
                    $newRow->save();
                }

                if( isset($peca['pis']) )
                {
                    $pis_ids = collect();

                    foreach($peca['pis'] as $index => $pi)
                    {
                        if( $pis_model = $this->savePI($pi,$newRow, $index) ){
                            $pis_ids->push($pis_model->id);
                        }
                    }

                    if( $newRow->pis instanceof Collection )
                    {
                        $pis       = $newRow->pis->lists('id')->toArray();
                        $pis_ids   = $pis_ids->toArray();
                        $deletePis = array_diff($pis, $pis_ids);

                        PI::destroy($deletePis);
                    }
                }

                LogPeca::create([
                    'user_id' => \Auth::user()->id,
                    'inscricao_id' => $inscricao_id,
                    'acao' => LogPeca::INSERIR,
                    'agencia' => !empty($inscricao->user->agency) ? $inscricao->user->agency : '-',
                    'numero_peca' => $newRow->id,
                    'numero_inscricao' => $inscricao->present()->numeroInscricao(),
                    'arquivo' => $newRow->arquivo,
                ]);
            }


        }

        if( isset($all['pis']) && is_array($all['pis']) )
        {
            $pis_ids = collect();

            foreach($all['pis'] as $index => $pi)
            {
                if(!empty($pi['pdf']))
                {
                    $pi['pdf']->move(
                        app_path_uploads('pecas-pdf'),
                        $newPDF = generate_file_name($pi['pdf']->getClientOriginalExtension())
                    );

                    $pi['pdf'] = $newPDF;
                }
                else if(!empty($pi['pdf-old']))
                {
                    $pi['pdf'] = $pi['pdf-old'];
                }

                if( $pis_model = $this->savePI($pi,$inscricao, $index) ){
                    $pis_ids->push($pis_model->id);
                }

            }

            if( $inscricao->pis instanceof Collection )
            {
                $pis       = $inscricao->pis->lists('id')->toArray();
                $pis_ids   = $pis_ids->toArray();
                $deletePis = array_diff($pis, $pis_ids);

                PI::destroy($deletePis);
            }
        }
    }

    /**
     * @param array $pi
     * @param Model|InscricaoPeca|Inscricao $owner
     * @param integer $index
     *
     * @return PI|null
     */
    protected function savePI(array $pi, $owner, $index = null)
    {
        if( !empty($pi) && method_exists($owner, 'pis') )
        {
            $pi_id   = isset($pi['id']) ? $pi['id'] : null;
            $piModel = PI::findOrNew($pi_id);

            $piModel->fill(array(
                'index'         =>$index,
                'pdf'           =>$pi['pdf'],
                'pi_numero'     =>$pi['pi_numero'],
                'pi_rejeitada'  =>(bool) isset($pi['pi_rejeitada']) ? $pi['pi_rejeitada'] : 0,
            ));

            $piModel->save();
            $owner->pis()->save($piModel);

            return $piModel;
        }

        return null;
    }
}