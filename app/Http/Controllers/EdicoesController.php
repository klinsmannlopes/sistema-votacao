<?php

namespace App\Http\Controllers;

use App\Http\Requests\CriteriosCRUDRequest as Request;

use App\Edicao as Model;
use App\Categoria;
use App\Traits\ActivatedTrait;
use Carbon\Carbon;

/**
 * Class EdicoesController
 *
 * @package   App\Http\Controllers
 * @author    Anderson Andrade <anderson.andrade@geq.com.br>
 * @copyright Grupo Edson Queiroz
 */
class EdicoesController extends Controller
{
    use ActivatedTrait;

    public function __construct(Model $model)
    {
        parent::__construct();

        $this->model = $model;
        \View::share('namespace', $this->namespace = 'edicoes');
        $this->middleware("permissions:{$this->namespace}");
    }

    public function index()
    {
        $rows = $this->model->orderby('ano', 'DESC')->paginate(env('ROWS_PER_PAGE'));

        return view("{$this->namespace}.index", compact('rows'));
    }

    public function create()
    {
        return view("{$this->namespace}.create");
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if( isset($data['data_inicio']) && !empty($data['data_inicio']) )
            $data['data_inicio'] = Carbon::createFromFormat('d/m/Y H:i:s', $data['data_inicio']);

        if( isset($data['data_encerramento']) && !empty($data['data_encerramento']) )
            $data['data_encerramento'] = Carbon::createFromFormat('d/m/Y H:i:s', $data['data_encerramento']);

        if(empty($data['tipo']) && !isset($data['tipo']))
            $data['tipo'] = null;

        $this->model->create($data);

        \Flash::success(\Lang::get('crud.stored'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function edit($id)
    {
        $row = $this->model->find($id);
        return view("{$this->namespace}.edit", compact('row'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        if( isset($data['data_inicio']) && !empty($data['data_inicio']) )
            $data['data_inicio'] = Carbon::createFromFormat('d/m/Y H:i:s', $data['data_inicio']);

        if( isset($data['data_encerramento']) && !empty($data['data_encerramento']) )
            $data['data_encerramento'] = Carbon::createFromFormat('d/m/Y H:i:s', $data['data_encerramento']);

        if(empty($data['tipo']) && !isset($data['tipo']))
            $data['tipo'] = null;

        $this->model
            ->find($id)
            ->fill($data)
            ->save();

        \Flash::success(\Lang::get('crud.updated'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function destroy($id)
    {
        $this->model->find($id)->delete();

        \Flash::success(\Lang::get('crud.destroyed'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function getAgenciasGanhadoras($export = false){

        $categorias = Categoria::all();
        $agencias = [];
        foreach($categorias as $categoria) {
            $cat = Categoria::find($categoria->id);
            $rows[$categoria->id] = $cat->top($tamanhoTop = 1)->slice(0, $tamanhoTop);
        }

        if(!is_edicao_profissional()) {
            foreach ($rows as $id => $row) {
                foreach ($row as $r) {
                    $agencias[$r->user_id]['pesoTotal'] = 0;
                }
            }
        }

        foreach($rows as $id => $row){
            foreach($row as $r){
                $agencias[$r->user_id]['agencia']        = getAgencia($r['id']);
                $agencias[$r->user_id]['edicao']         = getNomeEdicao($r['edicao_id']);
                $agencias[$r->user_id]['categorias'][]   = getCategoria($id);

                if(!is_edicao_profissional()) {
                    $agencias[$r->user_id]['pesoTotal'] += getCategoria($id)['peso'];
                }

            }
        }

        // Linha responsável pela ordenação por coluna
        if(!is_edicao_profissional()){
            $agencias = orderArrayByField($agencias, 'pesoTotal');
        }

        if($export)
            return $agencias;

        return view("{$this->namespace}.relatorio", compact('agencias'));
    }

    function cmp($a, $b)
    {
        return strcmp($a["pesoTotal"], $b["pesoTotal"]);
    }

    public function order($a, $b)
    {
        return strcmp($a["title"], $b["title"]);
    }

    public function export(){
        $agencias = $this->getAgenciasGanhadoras(true);

        $edicao = getEdicaoAtual();

        $edicao = strtolower(str_replace(' ','-', $edicao->nome));

        $file = \Excel::create($fileName = uniqid('Agencias-vencedoras-'.$edicao), function($excel) use ($agencias)
        {
            $excel->sheet('Agencias-vencedoras', function($sheet) use ($agencias)
            {
                $sheet->setSize('A1', 50);
                $sheet->setSize('B1', 30);
                $sheet->setSize('C1', 25);
                $sheet->setSize('D1', 40);

                $sheet->loadView("{$this->namespace}.partials.table-export", compact('agencias'));
            });
        });

        return $file->export('xls');
    }
}