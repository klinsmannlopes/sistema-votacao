<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\DesempatarJugamentoRequest as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Julgamento;

class DesempatarJulgamentoController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'desempatar-julgamento');

        \View::share('authUser', \Auth::getUser());

        \View::share('comboboxCategorias', (new Categoria)->getWithJulgamentoEmpatado());

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:julgamento.desempatar");
    }

    public function categorias()
    {
        $rows = with(new Categoria())->getCategoriesWithInscriptions();
        return view("{$this->namespace}.categorias", compact('rows'));
    }

    public function index($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        $rows = $categoria->top($tamanhoTop = 5);

        $comboboxOrdenacao = $categoria->getComboboxDesempateJulgamento($rows);

        return view("{$this->namespace}.index", compact('categoria', 'rows', 'comboboxOrdenacao'));
    }

    public function confirmarOrdenacao(Request $request, $categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        $ordenacao = $request->ordenacao;

        $rows = $categoria->top($tamanhoTop = 5)->sortBy(function($row, $key) use ($ordenacao)
        {
            return $ordenacao[$row->id];
        });

        return view("{$this->namespace}.confirm-ordenacao", compact('categoria', 'rows'));
    }

    public function ordenar(Request $request, $categoria_id)
    {
        $ordenacao = $request->ordernacao;

        foreach($ordenacao as $inscricao_id => $ordem)
        {
            $julgamento = Julgamento::where('inscricao_id', $inscricao_id)->where('categoria_id', $categoria_id)->first();

            if($julgamento->categoria->id == $categoria_id)
            {
                $julgamento->ordem = $ordem;
                $julgamento->save();
            }
        }

        \Flash::success('Desempate da Triagem realizado com sucesso!');

        return redirect(route("{$this->namespace}.categorias"));
    }

    public function listaDesempate($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        $rows = $categoria->top($tamanhoTop = 5)->slice(0, $tamanhoTop);

        return view("{$this->namespace}.lista-desempate", compact('categoria', 'rows'));
    }

    public function top5($categoria_id, $format)
    {
        $categoria = Categoria::find($categoria_id);

        $rows = $categoria->top($tamanhoTop = 5)->slice(0, $tamanhoTop);

        $file = \Excel::create($fileName = uniqid('categoria_top5_'), function($excel) use ($rows)
        {
            $excel->sheet('top5', function($sheet) use ($rows)
            {
                $sheet->loadView("{$this->namespace}.partials.table-export", compact('rows'));
            });
        });

        if($format == 'xls')
            return $file->export('xls');

        if($format == 'csv')
        {
            $file->store('csv', storage_path('csv/exports'));

            $fileName .= '.csv';

            $filePath = storage_path("csv/exports/{$fileName}");

            ob_start();

            readfile($filePath);
            $fileContent = ob_get_contents();

            ob_end_clean();

            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename={$fileName}");

            //BUGFIX - Adiciona o BOM
            echo chr(255) . chr(254);

            $output = iconv('UTF-8', 'UTF-16LE//IGNORE', $fileContent);
            @unlink($filePath);

            die($output);
        }
    }

    public function resultadoGeral($format)
    {
        $rows = [];
        $categorias =  with(new Categoria())->getCategoriesWithInscriptions();

        foreach($categorias as $categoria)
        {
            $rows[$categoria->nome]= $top5 = Categoria::find($categoria->id)->top(5);
        }


        $file = \Excel::create($fileName = uniqid('categoria_resultado_geral_'), function($excel) use ($rows)
        {
            $excel->sheet('resultado_geral', function($sheet) use ($rows)
            {
                $sheet->loadView("{$this->namespace}.partials.table-geral-export", compact('rows'));
            });
        });

        if($format == 'xls')
            return $file->export('xls');

        if($format == 'csv')
        {
            $file->store('csv', storage_path('csv/exports'));

            $fileName .= '.csv';

            $filePath = storage_path("csv/exports/{$fileName}");

            ob_start();

            readfile($filePath);
            $fileContent = ob_get_contents();

            ob_end_clean();

            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename={$fileName}");

            //BUGFIX - Adiciona o BOM
            echo chr(255) . chr(254);

            $output = iconv('UTF-8', 'UTF-16LE//IGNORE', $fileContent);
            @unlink($filePath);

            die($output);
        }

    }

}