<?php

namespace App\Http\Controllers;

use App\Inscricao;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class VerPecasController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'ver-pecas');

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:ver-pecas");
    }

    public function index($inscricao_id)
    {
        $inscricao = Inscricao::find($inscricao_id);

        if(in_array($inscricao->categoria->abreviatura, ['PLA', 'MID']))
            abort(503, 'A categoria dessa inscrição não permite cadastro de peças.');

        return view("{$this->namespace}.index", compact('inscricao'));
    }
}