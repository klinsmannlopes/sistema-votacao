<?php

namespace App\Http\Controllers;

use App\Log as Model;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LogsController extends Controller
{
    public function __construct(Model $model)
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'logs');

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware('permissions:log');

        $this->model = $model;
    }

    public function index()
    {
        $query = $this->model;

        if($search = \Input::get('search'))
        {
            $query = $query->orWhere('action', 'LIKE', "%{$search}%")
                ->orWhere('title', 'LIKE', "%{$search}%")
                ->orWhere('description', 'LIKE', "%{$search}%");
        }

        $rows = $query->orderby('id', 'DESC')->paginate(env('ROWS_PER_PAGE'));

        return view("{$this->namespace}.index", compact('rows', 'search'));
    }
}
