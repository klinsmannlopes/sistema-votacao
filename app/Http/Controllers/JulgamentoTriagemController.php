<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Categoria;

class JulgamentoTriagemController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'julgamento-triagem');

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:julgamento.triagem");
    }

    public function categorias()
    {
        $rows = with(new Categoria())->getCategoriesWithInscriptions();

        return view("{$this->namespace}.categorias", compact('rows'));
    }

    public function index($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        if(! $categoria->is_triagem)
        {
            $view = "{$this->namespace}.index";
        }
        else if($categoria->isTriagemRealizada())
        {
            if($categoria->isAguardandoDesempate())
            {
                $view = "{$this->namespace}.empatadas";
                $combobox = (new Categoria)->getAguardandoDesempate();
            }
            else
            {
                $title = "Triagem Realizada";
                $view = "{$this->namespace}.index";
                $combobox = (new Categoria)->getTriagemRealizada();
            }
        }
        else if($categoria->isTriagemEmAndamento())
        {

        }

        $rows = [];
        if($empatadas = $categoria->inscricoesAteEmpatadas())
        {
            $rows = $empatadas->sortBy(function($row, $key)
            {
                return $row->triagems()->count();
            })->reverse();
        }

        return view($view, compact('categoria', 'rows', 'combobox', 'title'));
    }
}