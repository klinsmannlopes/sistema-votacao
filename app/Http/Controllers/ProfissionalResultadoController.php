<?php

namespace App\Http\Controllers;


use App\Categoria;
use Chumper\Zipper\Facades\Zipper;


class ProfissionalResultadoController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        \View::share('namespace', $this->namespace = 'inscricao-profissional');
        $this->middleware("permissions:{$this->namespace}");
    }

    public function index()
    {
        $rows = with(new Categoria())->getCategoriesWithInscriptionsProfissinais();
        return view('inscricao-profissional.resultado.index', compact('rows'));
    }

    public function show($id)
    {
        $rows  = Categoria::find($id)->inscricoesProfissionaisAprovadas();
        $return = true;

        return view('inscricao-profissional.resultado.show', compact('rows','return'));
    }

    public function zipper($id)
    {
        $rows  = Categoria::find($id)->inscricoesProfissionaisAprovadas();
        $files = [];
        foreach($rows as $row){
            $nomeCategoria = $row->categoria->nome;
            $files[] = exibirPdf($row->nome, $row->present()->numeroInscricao(), true);
            $files[] = app_path_uploads('curriculos'.DIRECTORY_SEPARATOR.$row->curriculo);
            $files[] = app_path_uploads('fotos'.DIRECTORY_SEPARATOR.$row->foto);
        }

        $categoria =  removerAcentos($nomeCategoria);
        $path = app_path_uploads(generate_file_name('zip', $categoria));

        Zipper::make($path)->add($files)->close();
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="'.$categoria.'.zip"');
        header('Content-Length: '.filesize($path) );
        readfile($path);

        if(file_exists($path)){
            unlink($path);
        }

        exit();
    }
}