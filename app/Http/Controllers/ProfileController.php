<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\SaveProfileRequest as Request;

class ProfileController extends Controller
{
    public function getProfile()
    {
        $row = \Auth::user();
        return view('usuarios.profile', compact('row'));
    }

    public function postProfile(Request $request)
    {
        $user = \Auth::user();
        $all = $request->all();

        if(!empty($all['password']))
        {
            if(! \Hash::check($all['password'], $user->password))
            {
                \Flash::error('A <b>"Senha atual"</b> informada não está correta.');
                return redirect(route('profile'))->withInput($request->all());
            }

            $all['password'] = \Hash::make($all['new_password']);
        }
        else
            unset($all['password']);

        $user->fill($all);
        $user->save();

        \Flash::success('Informações de perfil salvas com sucesso!');

        return redirect(route('profile'));
    }
}
