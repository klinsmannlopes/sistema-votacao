<?php
/**
 * Created by PhpStorm.
 * User: 750371426
 * Date: 11/01/2017
 * Time: 16:25
 */

namespace App\Http\Controllers;

use App\Categoria;
use App\Events\CreatedInscricaoProfissionalPDFEvent;
use App\Inscricao;
use App\InscricaoProfissional as Model;
use App\Http\Requests\CancelarInscricaoRequest;
use App\Http\Requests\InscricaoProfissionalCRUDResquest as Request;
use App\InscricaoProfissional;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Events\RenameInscricaoProfissionalEvent;
use App\Mailers\ValidacaoInscricaoProfissional;
use JavaScript;


class InscricaoProfissionalController extends Controller
{

    public function __construct(Model $model)
    {
        parent::__construct();

        $this->model = $model;
        \View::share('namespace', $this->namespace = 'inscricao-profissional');
    }

    public function index()
    {
        $rows = $this->getFilteredList();

        $rows = $rows->paginate(env('ROWS_PER_PAGE'));

        return view("{$this->namespace}.index", $this->getIndexSupplies() + compact('rows','search','categoria_id'));
    }

    public function show($id)
    {
        $row = $this->model->find($id);
        $idForm = 'aprovar';
        $route = "{$this->namespace}.aprovar";
        $readonly = true;
        $buttonsDisabled = null;

        if(in_array($row->status, [ Model::STATUS_APROVADA, Model::STATUS_CANCELADA, Model::STATUS_CANCELADA_PELO_PARTICIPANTE ]))
        {
            $buttonsDisabled = 'disabled';
        }

        return view("{$this->namespace}.show", $this->getFormSupplies($row) + compact('row','idForm','route','readonly','buttonsDisabled'));
    }

    public function create()
    {
        $uploadCurriculo = route("{$this->namespace}.upload-curriculo");
        $uploadFoto = route("{$this->namespace}.upload-foto");
        JavaScript::put(compact('uploadCurriculo','uploadFoto'));

        return view("{$this->namespace}.create", $this->getFormSupplies());
    }

    public function store(Request $request)
    {
        $inscricao = $request->all();

        if(!empty($inscricao['curriculo']) && isset($inscricao['curriculo'])){
            $inscricao['curriculo'] = $request->file('curriculo')->getClientOriginalName();
        }else{
            $inscricao['curriculo'] = $inscricao['curriculo_old'];
            unset($inscricao['curriculo_old']);
        }

        if(!empty($inscricao['foto']) && isset($inscricao['foto']) ){
            $inscricao['foto'] = $request->file('foto')->getClientOriginalName();
        }else{
            $inscricao['foto'] = $inscricao['foto_old'];
            unset($inscricao['foto_old']);
        }

        $inscricao['user_id'] = \Auth::getUser()->id;

        if($this->verificarCategoriaCadastrada($inscricao['categoria_id'])){
            return back()->withInput()
                ->withErrors('Agência já cadastrada nessa categoria');
        }

        $inscricao = $this->model->create($inscricao);

        \Event::fire(new RenameInscricaoProfissionalEvent($inscricao));
        \Event::fire(new CreatedInscricaoProfissionalPDFEvent($inscricao));

        \Flash::success(\Lang::get('crud.stored'));
        return redirect(route("{$this->namespace}.index"));
    }

    protected function getIndexSupplies()
    {
        $categorias   = Categoria::orderby('nome')->get();
        $usuariosOpec = with(new User())->getOpec();
        $statusList   = InscricaoProfissional::groupby('status')->orderby('status')->lists('status')->toArray();

        return compact('categorias', 'usuariosOpec', 'statusList');
    }

    protected function getFormSupplies($row = null)
    {
        $agencia = \Auth::user()->agency;
        $endereco = \Auth::user()->address;

        if($row)
        {
            $agencia = $row->user->agency;
            $endereco = $row->user->address;
            $abreviatura = $row->categoria->abreviatura;
        }

        return compact(
            'agencia',
            'endereco',
            'abreviatura'
        );
    }

    protected function getFilteredList()
    {
        \DB::enableQueryLog();

        $search = \Input::get('search');
        $status = \Input::get('status');
        $categoria_id = \Input::get('categoria_id');

        view()->share(compact('search', 'status', 'categoria_id'));

        $query = $this->model;

        if($search)
        {
            $query = $query->where(function($q) use ($search)
            {
                if(\Gate::check('inscricoes'))
                {
                    $q = $q->whereHas('user', function($q2) use ($search)
                    {
                        $q2->where('agency', 'LIKE', "%{$search}%");
                    });
                }

                $q->orWhere(function($q1) use ($search)
                    {
                        $q1
                            ->whereHas('categoria', function($q2) use ($search)
                            {
                                $q2->where(\DB::raw("CONCAT(categorias.abreviatura,LPAD(inscricoes_profissionais.id,5,'0'))"), $search);
                            });
                    });
            });
        }

        if($status)
            $query = $query->where('status', $status);

        if($categoria_id)
            $query = $query->where('categoria_id', $categoria_id);


        $user = \Auth::user();

        if($user->role === Role::PARTICIPANTE)
            $query = $query->where('user_id', $user->id);

        if($user->role === Role::OPEC)
            $query = $query->where('user_opec_id', $user->id);

        //SOMENTE INSCRIÇÕES ONDE AS CATEGORIAS ESTEJAM ATIVAS
        $query = $query->whereHas('categoria', function($q) use ($search)
        {
            $q->where('is_ativo', 1);
        });

        $rows = $query->orderby('created_at', 'DESC');

        return $rows;
    }


    public function uploadCurriculo()
    {
        $arquivo = Input::all();

        $validator = Validator::make(
            [ "curriculo" => $arquivo['curriculo']],
            [ "curriculo" => "required|max:20480|mimes:pdf"]
        );

        if($validator->fails())
            return $validator->errors();

        $curriculo = $arquivo['curriculo']->move(public_path('uploads'.DIRECTORY_SEPARATOR.'curriculos'), $arquivo['curriculo']->getClientOriginalName());

        $newFileUrl = url_uploads('curriculos'.DIRECTORY_SEPARATOR.$curriculo->getFileName());
        $newArquivo = $curriculo->getFileName();

        return view("{$this->namespace}.partials.uploaded-file", compact('newFileUrl','newArquivo'))->render();
    }

    public function uploadFoto()
    {
        $arquivo = Input::all();

        $validator = Validator::make(
            [ "foto" => $arquivo['foto']],
            [ "foto" => "required|max:20480|mimes:jpeg"]
        );

        if($validator->fails())
            return $validator->errors();

        $foto = $arquivo['foto']->move(public_path('uploads'.DIRECTORY_SEPARATOR.'fotos'), $arquivo['foto']->getClientOriginalName());

        $newFileUrl = url_uploads('foto'.DIRECTORY_SEPARATOR.$foto->getFileName());
        $newArquivo = $foto->getFileName();

        return view("{$this->namespace}.partials.uploaded-file-foto", compact('newFileUrl','newArquivo'))->render();
    }

    public function edit($id)
    {
        $row = $this->model->find($id);

        $uploadCurriculo = route("{$this->namespace}.upload-curriculo");
        $uploadFoto = route("{$this->namespace}.upload-foto");

        $readonly = null;
        $buttonsDisabled = null;

        if(in_array($row->status, [ Model::STATUS_APROVADA, Model::STATUS_CANCELADA, Model::STATUS_CANCELADA_PELO_PARTICIPANTE ]))
        {
            $readonly = true;
            $buttonsDisabled = 'disabled';
        }

        JavaScript::put(compact('uploadCurriculo','uploadFoto'));

        return view("{$this->namespace}.edit", $this->getFormSupplies($row) + compact('row','readonly','buttonsDisabled'));
    }

    public function update(Request $request, $id)
    {
        $inscricao = $this->model->find($id);
        $data = $request->all();

        if(!empty($data['curriculo']) && isset($data['curriculo'])){
            $data['curriculo'] = $request->file('curriculo')->getClientOriginalName();
        }else{
            $data['curriculo'] = $data['curriculo_old'];
            unset($data['curriculo_old']);
        }

        if(!empty($data['foto']) && isset($data['foto']) ){
            $data['foto'] = $request->file('foto')->getClientOriginalName();
        }else{
            $data['foto'] = $data['foto_old'];
            unset($data['foto_old']);
        }

        if($this->verificarCategoriaCadastrada($data['categoria_id']) && $inscricao->categoria_id != $data['categoria_id']){
            return back()->withInput()
                ->withErrors('Agência já cadastrada nessa categoria');
        }


        $inscricao->fill($data);
        $inscricao->save();

        \Event::fire(new RenameInscricaoProfissionalEvent($inscricao));
        \Event::fire(new CreatedInscricaoProfissionalPDFEvent($inscricao));

        \Flash::success(\Lang::get('crud.stored'));
        return redirect(route("{$this->namespace}.index"));
    }

    public function destroy($id)
    {
        $this->model->find($id)->delete();

        \Flash::success(\Lang::get('crud.destroyed'));

        return redirect(route("{$this->namespace}.index"));
    }


    public function verificarCategoriaCadastrada($categoria)
    {
        $categoriasJaCadastradas = with(new InscricaoProfissional)->limitCategoria();
        $categoriasJaCadastradas = array_pluck($categoriasJaCadastradas, 'categoria_id');

        // Bloco responsável por verificar se já existe inscrição na categoria
        // selecionada, aumentando o limite de inscrições para 3.

        if(in_array($categoria, $categoriasJaCadastradas)){
            $countArray = array_count_values($categoriasJaCadastradas);
            $keyArray = array_search($categoria, $countArray);

            foreach($categoriasJaCadastradas as $item){
                if($item == $categoria && $countArray[$item] < 100){
                    return false;
                }else if($item == $categoria && $countArray[$item] >= 100){
                    return true;
                }
            }
        }
        return false;
    }

    public function aprovar($id)
    {
        $row = $this->model->find($id);
        $row->status = Model::STATUS_APROVADA;
        $row->justificativa = null;
        $row->save();

        ValidacaoInscricaoProfissional::send([ 'inscricao' => $row ]);

        \Flash::success(\Lang::get("inscricoes.aprovada"));

        return redirect(route("{$this->namespace}.index"));
    }

    public function cancelar(CancelarInscricaoRequest $request, $id)
    {
        $row = $this->model->find($id);
        $row->status = Model::STATUS_CANCELADA;
        $row->justificativa = $request->justificativa;
        $row->save();

        ValidacaoInscricaoProfissional::sendCancel([ 'inscricao' => $row ]);

        \Flash::success(\Lang::get("inscricoes.cancelada"));

        return redirect(route("{$this->namespace}.index"));
    }

    public function canceladaPeloParticipante($id)
    {
        $row = $this->model->find($id);
        $row->status = Model::STATUS_CANCELADA_PELO_PARTICIPANTE;
        $row->save();

        ValidacaoInscricaoProfissional::sendUserCancel([ 'inscricao' => $row ]);

        \Flash::success(\Lang::get("inscricoes.cancelada"));

        return redirect(route("{$this->namespace}.index"));
    }

    public function pdf($id, $view = 1)
    {
        $row = $this->model->find($id);
        $urlFoto    =  url_uploads('fotos/'.$row->foto);
        $pathFoto   =  path_uploads('fotos'.DIRECTORY_SEPARATOR.$row->foto);
        $pdf = \PDF::loadView("{$this->namespace}.pdf",compact('row','urlFoto','pathFoto'));

        if($view) {
            return view("{$this->namespace}.pdf", $this->getFormSupplies($row) + compact('row','urlFoto','pathFoto'));
        }
        else {
            return $pdf->stream();
        }
    }

    public function xls()
    {
        $rows = $this->getFilteredList()->get();       
        
        return \Excel::create(uniqid('inscricoes_'), function($excel) use ($rows)
        {
            $excel->sheet('Inscrições', function($sheet) use ($rows)
            {
                $sheet->loadView("{$this->namespace}.partials.table-xls", compact('rows'));
            });
        })
        ->export('xls');
    }

    public function csv()
    {
        $is_csv = true;
        $rows = $this->getFilteredList()->get();

        \Excel::create($fileName = uniqid('inscricoes_'), function($excel) use ($is_csv, $rows)
        {
            $excel->sheet('Inscrições', function($sheet) use ($is_csv, $rows)
            {
                $sheet->loadView("{$this->namespace}.partials.table-csv", compact('rows'));
            });
        })->store('csv', storage_path('csv/exports'));

        $fileName .= '.csv';

        $filePath = storage_path("csv/exports/{$fileName}");

        ob_start();

        readfile($filePath);
        $fileContent = ob_get_contents();

        ob_end_clean();

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");

        //BUGFIX - Adiciona o BOM
        echo chr(255) . chr(254);

        $output = iconv('UTF-8', 'UTF-16LE//IGNORE', $fileContent);
        @unlink($filePath);

        die($output);
    }

    public function imprimir()
    {
        $print = true;
        $rows = $this->getFilteredList()->get();

        return view("{$this->namespace}.imprimir", compact('rows', 'print'));
    }
}