<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;

use App\Parametrizacao;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
    }

    protected function throwValidationException(Request $request, $validator)
    {
        if(!empty($request->originalValues))
            $request->replace($request->originalValues);

        $request->flash();

        throw new HttpResponseException($this->buildFailedValidationResponse(
            $request, $this->formatValidationErrors($validator)
        ));
    }
}