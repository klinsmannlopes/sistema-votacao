<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Inscricao;
use App\Julgamento;
use App\JulgamentoNota;

use App\Http\Requests\JulgamentoRequest as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Parametrizacao;

class JulgarController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'julgar');

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:{$this->namespace}");
    }

    public function index($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        if( ! $categoria->is_triagem )
            $rows = $categoria->inscricoesValidas();
        else
            $rows = $categoria->topTriagem();

        $notaMinima = Parametrizacao::where('key', 'nota_minima')->first();
        $notaMinima = empty($notaMinima) ? '1,00' : $notaMinima->present()->nota();

        $notaMaxima = Parametrizacao::where('key', 'nota_maxima')->first();
        $notaMaxima = empty($notaMaxima) ? '10,00' : $notaMaxima->present()->nota();

        return view("{$this->namespace}.index", compact('categoria', 'rows', 'notaMinima', 'notaMaxima'));
    }

    public function categorias()
    {

        if(\Auth::user()->role === \App\Role::JUIZ_SOLIDARIO) {
            $rows = with(new Categoria())->getCategoriesWithSolidaria();
        } else {
            $rows = with(new Categoria())->getCategoriesWithInscriptions();
        }
        return view("{$this->namespace}.categorias", compact('rows'));
    }

    public function finalizar(Request $request, $id)
    {
        $categoria = Categoria::find($id);

        foreach($request->avaliacoes as $inscricao_id => $criterios)
        {
            $julgamento = Julgamento::create([
                'categoria_id' => $categoria->id,
                'inscricao_id' => $inscricao_id,
                'user_id' => \Auth::getUser()->id,
            ]);

            foreach($criterios as $criterio_id => $nota)
            {
                JulgamentoNota::create([
                    'julgamento_id' => $julgamento->id,
                    'criterio_id' => $criterio_id,
                    'nota' => $nota,
                ]);
            }
        }

        \Flash::success('Julgamento realizado com sucesso!');

        return redirect(route("{$this->namespace}.categorias"));
    }
}