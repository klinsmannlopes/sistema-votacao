<?php

namespace App\Http\Controllers;

use App\Events\CreatedUserEvent;
use App\Http\Requests\CadastroDeParticipantesRequest as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;

class CadastroDeParticipantesController extends Controller
{
    public function getIndex()
    {
        return view('cadastro-de-participantes.index');
    }

    public function postStore(Request $request)
    {
        if($uploadedFile = $this->uploadFile($request))
        {
            \Excel::load($uploadedFile, function($reader) use ($request)
            {
                $createdCount = 0;
                $duplicatedCount = [];

                foreach($reader->all() as $row)
                {
                    try
                    {
                        if($user = User::createFromExcel($row))
                        {
                            $createdCount++;

                            \Event::fire(new CreatedUserEvent($user));
                        }
                    }
                    catch(\Exception $e)
                    {
                        //login ou email duplicados
                        if($e->getCode() == 23000)
                        {
                            $duplicatedCount[] = [
                                'login' => $row->login,
                                'email' => $row->email,
                            ];
                        }else{
                            \Flash::error($e->getMessage());
                        }
                    }
                }

                if($createdCount)
                    \Flash::success(\Lang::get('cadastro-de-participantes.store.success', ['qtd' => $createdCount]));

                if($duplicatedCount)
                {
                    $qtd = count($duplicatedCount);
                    $html = view('cadastro-de-participantes.partials.error-list', ['rows' => $duplicatedCount])->render();

                    \Flash::error( \Lang::get( 'cadastro-de-participantes.store.error', compact('qtd', 'html') ) );
                }
            });
        }

        return redirect(route('cadastro-de-participantes'));
    }

    protected function uploadFile($request)
    {
        $file = $request->file('participantes');

        $file->move(
            $pathfile = app_path_uploads('cadastro-de-participantes/'),
            $uploadedFile = generate_file_name($file->getClientOriginalExtension())
        );

        return $pathfile . $uploadedFile;
    }
}
