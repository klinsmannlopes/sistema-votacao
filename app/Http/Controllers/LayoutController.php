<?php

namespace App\Http\Controllers;

use App\Parametrizacao as Model;
use Illuminate\Http\Request;

use App\Http\Requests\LayoutRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LayoutController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware('permissions:layout');
    }

    public function getIndex()
    {
        $logo = Model::firstOrNew(['key' => 'logo']);
        $envelopagem = Model::firstOrNew(['key' => 'envelopagem']);

        return view('layout.index', compact('logo', 'envelopagem'));
    }

    public function postSave(LayoutRequest $request)
    {
        $uploads = [
            'envelopagem',
            'logo',
        ];

        try
        {
            $uploaded = false;

            foreach($uploads as $key)
            {
                if($request->hasFile($key))
                {
                    $$key = $request->file($key);

                    $$key->move(
                        app_path_uploads('layout'),
                        $newFileName = generate_file_name($$key->getClientOriginalExtension())
                    );

                    $row = Model::firstOrNew(compact('key'));
                    $row->value = $newFileName;
                    $row->save();

                    $uploaded = true;
                }
            }

            if($uploaded)
                \Flash::success('Imagens salvas com successo!');
        }
        catch(\Exception $e)
        {
            \Flash::error($e->getMessage());
        }

        return redirect(route('layout'));
    }

    public function getDestroy($key)
    {
        if(in_array($key, ['logo', 'envelopagem']))
        {
            $query = Model::where('key', $key);

            if($query->count())
            {
                $query->delete();
                \Flash::success('Imagem removida com sucesso.');
            }
        }

        return redirect(route('layout'));
    }
}
