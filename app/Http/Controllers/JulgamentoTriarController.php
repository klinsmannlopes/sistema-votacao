<?php

namespace App\Http\Controllers;

use App\Inscricao;

use App\Triagem as Model;
use App\Http\Requests\JulgamentoTriarRequest as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Categoria;

class JulgamentoTriarController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'julgamento-triar');

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:julgamento.triar");
    }

    public function categorias()
    {
        $rows = with(new Categoria())->getCategoriesWithInscriptions();

        return view("{$this->namespace}.categorias", compact('rows'));
    }

    public function index($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        $rows = $categoria->inscricoes()->where('status', Inscricao::STATUS_VALIDA)->get();

        return view("{$this->namespace}.index", compact('categoria', 'rows'));
    }

    public function save(Request $request, $categoria_id)
    {
        $user_id = \Auth::getUser()->id;

        foreach($request->inscricoes as $inscricao_id)
            Model::create(compact('categoria_id', 'inscricao_id', 'user_id'));

        \Flash::success('Triagem realizada com sucesso!');

        return redirect(route("{$this->namespace}.categorias"));
    }
}
