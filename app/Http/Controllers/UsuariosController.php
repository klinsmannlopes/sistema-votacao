<?php

namespace App\Http\Controllers;

use App\Edicao;
use App\Events\ActivatedUserEvent;
use App\Events\CreatedUserEvent;
use App\Role;
use App\User as Model;
use App\Http\Requests\UsuariosCRUDRequest as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsuariosController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware('permissions:usuarios');
    }

    public function getIndex()
    {
        $query = Model::where('role', '<>', 'master_of_universe');

        if(!empty($search = \Input::get('search')))
        {
            $query = $query->where(function($q) use ($search)
            {
                $q->orWhere('name', 'LIKE', "%{$search}%")
                ->orWhere('login', 'LIKE', "%{$search}%")
                ->orWhere('email', 'LIKE', "%{$search}%")
                ->orWhere('role', 'LIKE', "%{$search}%")
                ;
            });
        }

        $rows = $query->orderby('id', 'DESC')->paginate(env('ROWS_PER_PAGE'));

        return view('usuarios.index', compact('rows', 'search'));
    }

    public function getCreate()
    {
        $roles      = Role::all();
        $edicoes    = Edicao::all();

        return view('usuarios.create', compact('roles','edicoes'));
    }

    public function postStore(Request $request)
    {
        try
        {
            \DB::transaction(function() use ($request)
            {
                $row = Model::create($request->all());

                if($row->is_participante())
                {
                    $row->agency = $request->agency;
                    $row->director_partner = $request->director_partner;
                }
                else
                {
                    $row->agency = null;
                    $row->director_partner = null;
                }

                $row->save();

                try{
                    $edicoes = $request->has('edicoes') ? $request->get('edicoes') : array();
                    $row->edicoes()->sync($edicoes);
                }
                catch (\Exception $e)
                {
                    logger($e->getMessage());
                    logger($e);
                }

                \Event::fire(new CreatedUserEvent($row));

                \Flash::success('Usuário criado com sucesso. Foi enviado um e-mail para o usuário contendo as informações necessárias.');
            });
        }
        catch(\Exception $e)
        {
            \Flash::error($e->getMessage());
            return redirect()->back()->withInput($request->all());
        }

        return redirect(route('usuarios'));
    }


    public function getEdit($id)
    {
        $row = Model::find($id);
        $roles = Role::all();
        $edicoes    = Edicao::all();

        return view('usuarios.edit', compact('row', 'roles','edicoes'));
    }

    public function postUpdate(Request $request, $id)
    {
        $all = $request->all();
        $all['activated'] = $request->activated ? 1 : 0;

        if($all['new_password'])
            $all['password'] = \Hash::make($all['new_password']);

        $row = Model::find($id);
        $row->fill($all);

        if($row->is_participante())
        {
            $row->agency = $request->agency;
            $row->director_partner = $request->director_partner;
        }
        else
        {
            $row->agency = null;
            $row->director_partner = null;
        }

        $row->save();

        try{
            $edicoes = $request->has('edicoes') ? $request->get('edicoes') : array();
            $row->edicoes()->sync($edicoes);
        }
        catch (\Exception $e)
        {
            logger($e->getMessage());
            logger($e);
        }

        \Flash::success('Usuário editado com sucesso.');

        return redirect(route('usuarios'));
    }

    public function postDestroy($id)
    {
        Model::destroy($id);
        \Flash::success('Usuário excluido com sucesso.');

        return redirect(route('usuarios'));
    }

    public function getShow($id)
    {
        $row = Model::find($id);
        $roles = Role::all();

        return view('usuarios.show', compact('row', 'roles'));
    }

    public function getActive($id)
    {
        $row = Model::find($id);

        $row->activated = 1;
        $row->save();

        \Event::fire(new ActivatedUserEvent($row));

        \Flash::success('Usuário ativado com sucesso! Instruções para criar senha de acesso foram enviadas para o e-mail cadastrado.');

        return redirect(route('usuarios'));
    }

    public function getUnactive($id)
    {
        $row = Model::find($id);

        $row->activated = 0;
        $row->save();

        \Flash::success(\Lang::get('crud.unactivated'));

        return redirect(route('usuarios'));
    }
}