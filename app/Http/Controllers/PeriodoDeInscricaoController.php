<?php

namespace App\Http\Controllers;

use App\Parametrizacao as Model;

use App\Http\Requests\PeriodoDeInscricaoSave as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PeriodoDeInscricaoController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware('permissions:inscricoes.parametrizacao');
    }

    public function getIndex()
    {
        $inscricoesDataInicial = Model::firstOrNew(['key' => 'inscricoes_data_inicial'])->value;
        $inscricoesDataFinal= Model::firstOrNew(['key' => 'inscricoes_data_final'])->value;

        return view('periodo-de-inscricoes.index', compact('inscricoesDataInicial', 'inscricoesDataFinal'));
    }

    public function postSave(Request $request)
    {
        $dataInicial = Model::firstOrNew(['key' => 'inscricoes_data_inicial']);
        $dataInicial->value = $request->input('inscricoes_data_inicial');
        $dataInicial->save();

        $dataFinal = Model::firstOrNew(['key' => 'inscricoes_data_final']);
        $dataFinal->value = $request->input('inscricoes_data_final');
        $dataFinal->save();

        \Flash::success('Período para inscrições de peça salvo com sucesso!');

        return redirect(route('periodo-de-inscricoes'));
    }
}