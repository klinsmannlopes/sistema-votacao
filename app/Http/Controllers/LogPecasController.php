<?php

namespace App\Http\Controllers;

use App\LogPeca as Model;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LogPecasController extends Controller
{
    public function __construct(Model $model)
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'log-pecas');

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware('permissions:log');

        $this->model = $model;
    }

    public function index()
    {
        $query = $this->model;

        if($search = \Input::get('search'))
        {
            $query = $query->orWhere('numero_inscricao', 'LIKE', "%{$search}%")
                ->orWhere('arquivo', 'LIKE', "%{$search}%")
                ->orWhere('agencia', 'LIKE', "%{$search}%")
                ->orWhere('acao', 'LIKE', "%{$search}%")
                ->orWhereHas('user', function ($q) use ($search)
                {
                    $q->where('name', 'LIKE', "%{$search}%");
                });
        }

        $rows = $query->orderby('id', 'DESC')->paginate(env('ROWS_PER_PAGE'));

        return view("{$this->namespace}.index", compact('rows', 'search'));
    }
}
