<?php

namespace App\Http\Controllers;

use App\Categoria as Model;
use App\Criterio;
use App\Edicao;

use App\CategoriaEdicao;

use App\Http\Requests\CategoriasCRUDRequest as Request;

use App\Http\Controllers\Controller;
use App\Traits\ActivatedTrait;

class CategoriasController extends Controller
{
    use ActivatedTrait;

    public function __construct(Model $model)
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'categorias');
        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:{$this->namespace}");

        $this->model = $model;
    }

    public function index()
    {
        $categoriasDisponiveis = getEdicaoAtual()->categorias;

        $rows = $this->model->WithAllEditions()->orderby('is_fixo', 'ASC')->orderby('nome', 'ASC')->paginate(env('ROWS_PER_PAGE'));
        
        return view("{$this->namespace}.index", compact('rows','categoriasDisponiveis'));
    }

    public function create()
    {
        $edicoes = Edicao::all();
        return view("{$this->namespace}.create", $this->getFormSupplies() + compact('edicoes'));
    }

    public function store(Request $request)
    {

        $row =  $this->model->create($request->all());

        try{
            $edicoes = $request->has('edicoes') ? $request->get('edicoes') : array();
            $row->edicoes()->sync($edicoes);
        }

        catch (\Exception $e)
        {
            logger($e->getMessage());
            logger($e);
        }

        $row->criterios()->attach($request->criterios);

        \Flash::success(\Lang::get('crud.stored'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function edit($id)
    {
        $row =  $this->model->WithAllEditions()->find($id);

        $edicoes = Edicao::all();

        $rowCriterios = $row->criterios()->lists('criterio_id')->toArray();

        return view("{$this->namespace}.edit", $this->getFormSupplies() + compact('row', 'rowCriterios', 'edicoes'));
    }

    public function update(Request $request, $id)
    {
        $all = $request->all();

        $row =  $this->model->WithAllEditions()->find($id);
        $row->fill($all);
        $row->save();

        try{
            $edicoes = $request->has('edicoes') ? $request->get('edicoes') : array();
            $row->edicoes()->sync($edicoes);
        }
        
        catch (\Exception $e)
        {
            logger($e->getMessage());
            logger($e);
        }

        if(!is_edicao_profissional())
            $row->criterios()->sync($request->criterios);

        \Flash::success(\Lang::get('crud.updated'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function destroy($id)
    {
        $row = $this->model->WithAllEditions()->find($id);

        if(!$row->is_fixo)
        {
            $row->delete();
            \Flash::success(\Lang::get('crud.destroyed'));
        }

        return redirect(route("{$this->namespace}.index"));
    }

    protected function getFormSupplies()
    {
        $criterios = Criterio::where('is_ativo', 1)->get();
        $tiposDeAnexo = $this->model->tiposDeAnexo();
        $qtdPecasTipo = $this->model->qtdPecasTipo();

        return compact('criterios', 'tiposDeAnexo', 'qtdPecasTipo');
    }
}
