<?php

namespace App\Http\Controllers\Auth;

use App\Traits\ParametrizacaoLayoutTrait;
use App\User;
use App\Http\Requests\AuthenticationRequest as Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins, ParametrizacaoLayoutTrait;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest',   ['except' => 'getLogout']);

        $this->redirectAfterLogout = route('getLogin');

        $this->shareView();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|confirmed|min:6',
            'edicao'    => 'required|integer',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    private function lockoutTime()
    {
        return 60 * 30;
    }

    protected function maxLoginAttempts()
    {
        return 4;
    }

    public function getLogin()
    {
//        var_dump(\Session::get('retriesLeft'));

        if (view()->exists('auth.authenticate')) {
            return view('auth.authenticate');
        }

        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
//        $this->clearLoginAttempts($request);

        $rules = [
            $this->loginUsername() => 'required',
            'password'  => 'required',
            'edicao'    => 'required',
        ];

        \Session::set('retriesLeft', $this->retriesLeft($request));

        if(!empty(\Session::get('retriesLeft')) && \Session::get('retriesLeft') <= 1)
            $rules += ['g-recaptcha-response' => 'required|recaptcha'];

        $messages = [
            'g-recaptcha-response.required' => 'O recaptcha deve ser preenchido.',
            'g-recaptcha-response.recaptcha' => 'O recaptcha não está correto.',
            'email.required'    => \Lang::get('validation.required', [ 'attribute' => 'Login ou E-mail' ]),
            'password.required' => \Lang::get('validation.required', [ 'attribute' => 'Senha' ]),
            'edicao.required'   => 'Por favor, selecione uma <strong>Edição</strong>.',
        ];

        $this->validate($request, $rules, $messages);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (\Auth::attempt($credentials, $request->has('remember'))
            || \Auth::attempt(['login' => $request->input('email'), 'password' => $request->input('password')], $request->has('remember'))) {

            \Session::forget('retriesLeft');

            $user = \Auth::user();

            if(empty($user->activated))
            {
                \Auth::logout();

                return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/')
                    ->withErrors('Seu usuário não está ativo.');
            }

            $user->logged_at = date('Y-m-d H:i:s');
            $user->save();

            \Session::set('edicaoAtual', $request->get('edicao') );

            $this->redirectPath = route('dashboard');

            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles)
            $this->incrementLoginAttempts($request);

        return redirect( action('Auth\AuthController@getLogin') )
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    protected function getLockoutErrorMessage($seconds)
    {
        return \Lang::has('auth.throttle')
            ? \Lang::get('auth.throttle', ['minutes' => ceil($seconds / 60)])
            : 'Too many login attempts. Please try again in '.$seconds.' seconds.';
    }
}
