<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\ParametrizacaoLayoutTrait;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests\PasswordRecoverRequest as Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Password;

class PasswordController extends Controller
{
    use ResetsPasswords, ParametrizacaoLayoutTrait;

    public $subject = '[GP] Dados de acesso';

    public function __construct()
    {
        $this->middleware('guest');

        $this->redirectTo = route('dashboard');

        $this->shareView();
    }

    public function postEmail(Request $request)
    {
        $this->validate($request, $request->getRulesSendEmailForReset(), array_merge($request->messages(), [ 'email.required' => \Lang::get('validation.required', [ 'attribute' => 'Informe seu e-mail' ]) ]));

        $response = Password::sendResetLink($request->only('email'), function (Message $message)
        {
            $message->subject($this->getEmailSubject());
        });

        switch ($response)
        {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('status', Lang::has('passwords.sent') ? Lang::get('passwords.sent', ['email' => $request->get('email')]) : trans($response));

            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }

    public function postReset(Request $request)
    {
        $this->validate($request, $request->getRulesResetPassword(), $request->messages());

        $credentials = $request->only('email', 'password', 'password_confirmation', 'token');

        global $User;
        $response = Password::reset($credentials, function ($user, $password)
        {
            $User = $user;

            if(empty($User->activated))
            {
                $User->password = bcrypt($password);
                $User->save();
            }
            else
                $this->resetPassword($user, $password);
        });

        switch ($response)
        {
            case Password::PASSWORD_RESET:
                if(empty($User->activated))
                    return redirect(route('getLogin'))->withErrors('Seu usuário não está ativo.');
                else
                    return redirect($this->redirectPath())->with('status', trans($response));
            default:
                return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => trans($response)]);
        }
    }
}