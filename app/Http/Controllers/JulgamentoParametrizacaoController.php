<?php

namespace App\Http\Controllers;

use App\Parametrizacao as Model;
use App\Http\Requests\JulgamentoParametrizacaoRequest as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class JulgamentoParametrizacaoController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'julgamento-parametrizacao');

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:julgamento.parametrizacao");

    }

    public function getIndex()
    {
        $notaMinima = Model::where(['key' => 'nota_minima']);
        $notaMaxima = Model::where(['key' => 'nota_maxima']);

        $notaMinima = $notaMinima->count() ? $notaMinima->first()->value : '1,00';
        $notaMaxima = $notaMaxima->count() ? $notaMaxima->first()->value : '10,00';

        return view("{$this->namespace}.index", compact('notaMinima', 'notaMaxima'));
    }

    public function postSave(Request $request)
    {
        $notaMinima = Model::firstOrNew(['key' => 'nota_minima']);
        $notaMinima->value = $request->get('nota_minima');
        $notaMinima->save();

        $notaMaxima = Model::firstOrNew(['key' => 'nota_maxima']);
        $notaMaxima->value = str_pad($request->get('nota_maxima'), 5, 0, STR_PAD_LEFT);
        $notaMaxima->save();

        \Flash::success('Notas cadastradas com sucesso!');

        return redirect(route($this->namespace));
    }

}