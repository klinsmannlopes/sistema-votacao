<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\DesempatarTriagemRequest as Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Inscricao;
use App\Triagem;

class DesempatarTriagemController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        \View::share('namespace', $this->namespace = 'desempatar-triagem');

        //ACL - VERIFICA SE A ROLE DO USUÁRIO PODE EXECUTAR A AÇÃO
        $this->middleware("permissions:julgamento.triagem.desempatar");
    }

    public function index($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        $somenteInscricoesEmpatadas = $categoria->somenteInscricoesEmpatadas()->lists('id')->toArray();

        $rows = [];
        if($empatadas = $categoria->inscricoesAteEmpatadas())
        {
            $rows = $empatadas->sortBy(function($row, $key)
            {
                return $row->triagems()->count();
            })->reverse();
        }

        return view("{$this->namespace}.index", compact('categoria', 'rows', 'somenteInscricoesEmpatadas'));
    }

    public function categorias()
    {
        $rows = with(new Categoria())->getCategoriesWithInscriptions();
        return view("{$this->namespace}.categorias", compact('rows'));
    }

    public function confirmarOrdenacao(Request $request, $categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        $ordenacao = $request->ordenacao;

        $rows = $categoria->inscricoesSelecionadas($ordenacao);

        return view("{$this->namespace}.confirm-ordenacao", compact('categoria', 'rows'));
    }

    public function ordenar(Request $request, $categoria_id)
    {
        $ordenacao = $request->ordernacao;

        foreach($ordenacao as $inscricao_id => $ordem)
        {
            $triagem = Triagem::where(['inscricao_id'=> $inscricao_id, 'categoria_id'=> $categoria_id])->first();

            if($triagem->categoria->id == $categoria_id)
            {
                $triagem->ordem = $ordem;
                $triagem->save();
            }
        }

        \Flash::success('Desempate da Triagem realizado com sucesso!');

        return redirect(route("{$this->namespace}.categorias"));
    }

    public function top10($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);

        $rows = $categoria->topTriagem();

        return view("{$this->namespace}.top10", compact('categoria', 'rows'));
    }
}