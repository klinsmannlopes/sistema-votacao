<?php

namespace App\Http\Controllers;

use App\Http\Requests\CriteriosCRUDRequest as Request;

use App\Http\Controllers\Controller;

use App\Criterio as Model;
use App\Traits\ActivatedCriteriosTrait;

class CriteriosController extends Controller
{
    use ActivatedCriteriosTrait;

    public function __construct(Model $model)
    {
        parent::__construct();

        $this->model = $model;
        \View::share('namespace', $this->namespace = 'criterios');
    }

    public function index()
    {
        $rows = $this->model->orderby('nome', 'ASC')->paginate(env('ROWS_PER_PAGE'));

        return view("{$this->namespace}.index", compact('rows'));
    }

    public function create()
    {
        return view("{$this->namespace}.create");
    }

    public function store(Request $request)
    {
        $this->model->create($request->all());

        \Flash::success(\Lang::get('crud.stored'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function edit($id)
    {
        $row = $this->model->find($id);
        return view("{$this->namespace}.edit", compact('row'));
    }

    public function update(Request $request, $id)
    {
        $this->model
            ->find($id)
            ->fill($request->all())
            ->save();

        \Flash::success(\Lang::get('crud.updated'));

        return redirect(route("{$this->namespace}.index"));
    }

    public function destroy($id)
    {
        $this->model->find($id)->delete();

        \Flash::success(\Lang::get('crud.destroyed'));

        return redirect(route("{$this->namespace}.index"));
    }
}