<?php

namespace App;

use App\Presenters\JulgamentoNotaPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class JulgamentoNota extends Model
{
    //SOFT DELETE
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    use PresentableTrait;
    protected $presenter = JulgamentoNotaPresenter::class;

    protected $table = 'julgamento_notas';

    protected $fillable = [
        'julgamento_id',
        'inscricao_id',
        'criterio_id',
        'nota',
    ];

    public $timestamps = false;

    public function julgamento()
    {
        return $this->belongsTo(Julgamento::class);
    }

    public function criterio()
    {
        return $this->belongsTo(Criterio::class);
    }
}
