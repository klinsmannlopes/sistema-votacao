<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Triagem extends Model
{
    //SOFT DELETE
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $name = 'triagems';

    protected $fillable = [
        'categoria_id',
        'inscricao_id',
        'user_id'
    ];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function inscricao()
    {
        return $this->belongsTo(Inscricao::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('edicao', function(Builder $builder) {
            $builder->whereHas('inscricao', function (Builder $query){
                $query->where('edicao_id', getEdicaoAtualId());
            });
        });
    }
}