<?php
/**
 * Created by PhpStorm.
 * User: 750371426
 * Date: 01/02/2017
 * Time: 17:30
 */

namespace App\Events;
use App\InscricaoProfissional;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class CreatedInscricaoProfissionalPDFEvent extends Event
{
    use SerializesModels;

    public function __construct(InscricaoProfissional $inscricao)
    {
        $this->inscricaoProfissional = $inscricao;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}