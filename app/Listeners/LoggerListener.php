<?php

namespace App\Listeners;

use App\Contracts\LoggerInterface;
use Illuminate\Events\Dispatcher;
use App\Categoria;
use App\Criterio;
use App\Inscricao;
use App\InscricaoPeca;
use App\Parametrizacao;
use App\PI;
use App\Role;
use App\User;
use App\Veiculo;
use App\Log;

class LoggerListener
{
	public $models = [
		Categoria::class,
		Criterio::class,
		Parametrizacao::class,
		User::class,
	];

	public function subscribe(Dispatcher $event)
	{
		foreach ($this->models as $model) {
			$event->listen('eloquent.created: '.$model, [$this, 'onCreated']);
			$event->listen('eloquent.updated: '.$model, [$this, 'onUpdated']);
			$event->listen('eloquent.deleted: '.$model, [$this, 'onDelete']);
		}

		$event->listen('eloquent.updated: '.Inscricao::class, [$this, 'onInscricoes']);
	}

	public function onCreated(LoggerInterface $loggable){
		$this->storeLogger($loggable, 'created');
	}

	public function onUpdated(LoggerInterface $loggable){
		$this->storeLogger($loggable, 'updated');
	}

	public function onDelete(LoggerInterface $loggable){
		$this->storeLogger($loggable, 'deleted');
	}

	public function onInscricoes(Inscricao $inscricao)
	{
		$numInscricao = $inscricao->present()->numeroInscricao();
		$alterados = $inscricao->getDirty();
		unset($alterados['updated_at']);
		$description = "";

		foreach ($alterados as $key => $value)
		{
			$description .= "O campo ".$key." foi alterado para ".$value.";";
		}
		Log::create([
            'user_id'=>\Auth::getUser()->id,
	        'title'  =>'Inscricao: '.$numInscricao,
	        'action' =>'updated',
	        'description'=>$description,
        ]);
	}

	public function storeLogger(LoggerInterface $loggable, $action)
	{
		$alterados = arrayRemove($loggable->getLoggableValuesUpdate());
		$line = $loggable->getLoggableData();
		$description = "";
		$nome = "";

		foreach ($alterados as $key => $value)
		{
			if($loggable->getLoggableTitle() ==  "User")
			{
				if(!empty($value))
					$description  .= "Campo ".$key." alterado para ".($key == "password" ? "******" : $value)."; ";

				$nome = $line['name'];
			}
			if($loggable->getLoggableTitle() ==  "Parametrizacao")
			{
				if(!empty($value))
					$description .="Campo ".$line['key']." alterado para ".$value."; ";
			}
			if($loggable->getLoggableTitle() ==  "Categoria" || $loggable->getLoggableTitle() == "Criterio")
			{
				$nome = $line['nome'];
				if($key == "is_ativo")
				{
					$description .= "Campo status alterado para ".($value == 0 ? "desativada; " : "ativada; ");
				}else
				{
					if(!empty($value))
						$description .= "Campo ".$key." alterado para ".is_serialized($value)."; ";
				}
			}
		}
		if(!empty($description))
		{
			$user = \Auth::getUser();

	         Log::create([
	            'user_id'=>$user && !isset($user->id) ? $user->id : '0',
		        'title'  =>$loggable->getLoggableTitle().":".$nome,
		        'action' =>$action,
		        'description'=>($action == "updated" ? $description : $loggable->getLoggableDescription($action)),
	        ]);
     	}

	}
	public function getUserId()
	{
		if( auth()->check() ){
			$user = auth()->user();
			return $user->id;
		}
		return null;
	}
}
