<?php namespace App\Mailers;

use App\User;

class AssociarOPECMailer implements Mailer
{
    static function send($params)
    {
        $user = User::find($params['user_id']);
        $associados = $params['associados'];

        \Mail::send('emails.associar-inscricao-opec', compact('associados'), function ($message) use ($user)
        {
            $email = is_local() ? env('MAIL_TEST') : $user->email;
            $message->to($email, $user->name)->subject('[GP] Associamento de inscrições a OPEC');
        });
    }
}