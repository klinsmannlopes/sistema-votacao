<?php namespace App\Mailers;

use App\User;

class ValidacaoInscricao implements Mailer
{
    static function send($params)
    {
        $inscricao = $params['inscricao'];

        $user = $inscricao->user;
        $numero = $inscricao->present()->numeroInscricao();

        \Mail::send('emails.validar-inscricao', compact('numero'), function ($message) use ($user, $numero)
        {
            $email = is_local() ? env('MAIL_TEST') : $user->email;
            $message->to($email, $user->name)->subject("[GP] Sua inscrição de Nº {$numero} foi validada!");
        });
    }

    static function sendCancel($params)
    {
        $inscricao = $params['inscricao'];

        $user = $inscricao->user;
        $justificativa = $inscricao->justificativa;
        $numero = $inscricao->present()->numeroInscricao();

        \Mail::send('emails.validar-inscricao-cancelada', compact('numero', 'justificativa'), function ($message) use ($user, $numero, $justificativa)
        {
            $email = is_local() ? env('MAIL_TEST') : $user->email;
            $message->to($email, $user->name)->subject("[GP] Sua inscrição de Nº {$numero} foi cancelada!");
        });
    }

    static function sendUserCancel($params)
    {
        $inscricao = $params['inscricao'];

        $user = $inscricao->user;
        $numero = $inscricao->present()->numeroInscricao();

        \Mail::send('emails.validar-inscricao-cancelada-usuario', compact('numero'), function ($message) use ($user, $numero)
        {
            $email = is_local() ? env('MAIL_TEST') : $user->email;
            $message->to($email, $user->name)->subject("[GP] Sua inscrição de Nº {$numero} foi cancelada!");
        });
    }

    static function sendRejected($params)
    {
        $inscricao = $params['inscricao'];

        $user = $inscricao->user;
        $numero = $inscricao->present()->numeroInscricao();

        \Mail::send('emails.validar-inscricao-rejeitada', compact('numero'), function ($message) use ($user, $numero)
        {
            $email = is_local() ? env('MAIL_TEST') : $user->email;
            $message->to($email, $user->name)->subject("[GP] Sua inscrição de Nº {$numero} está pendente de correção!");
        });
    }

    static function sendReview($params)
    {
        $inscricao = $params['inscricao'];

        $user = $inscricao->user;
        $numero = $inscricao->present()->numeroInscricao();

        \Mail::send('emails.validar-pendente-revisao', compact('numero'), function ($message) use ($user, $numero)
        {
            $email = is_local() ? env('MAIL_TEST') : $user->email;
            $message->to($email, $user->name)->subject("[GP] Sua inscrição de Nº {$numero} foi solicitada a revisão de status cancelado!");
        });
    }
}