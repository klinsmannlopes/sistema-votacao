<?php namespace App\Mailers;

interface Mailer
{
    static function send($params);
}