<?php namespace App\Mailers;

class ResettingPasswordMailer implements Mailer
{
    static function send($params)
    {
        $user = $params['user'];
        $token = $params['token'];

        \Mail::send('emails.password', compact('user', 'token'), function ($message) use ($user)
        {
            $email = is_local() ? env('MAIL_TEST') : $user->email;
            $message->to($email, $user->name)->subject('[GP] Dados de acesso');
        });
    }
}