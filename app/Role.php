<?php

namespace App;

class Role
{
    const ADMINISTRADOR = 'administrador';
    const JUIZ = 'juiz';
    const JUIZ_SOLIDARIO = 'juiz solidário';
    const JURADO = 'jurado';
    const OPEC = 'opec';
    const PARTICIPANTE = 'participante';

    static public function all()
    {
        return [
            self::ADMINISTRADOR => ucfirst(self::ADMINISTRADOR),
            self::JUIZ => 'Juiz Minerva',
            self::JUIZ_SOLIDARIO => ucfirst(self::JUIZ_SOLIDARIO),
            self::JURADO => ucfirst(self::JURADO),
            self::OPEC => strtoupper(self::OPEC),
            self::PARTICIPANTE => ucfirst(self::PARTICIPANTE),
        ];
    }

    static public function getRole($key)
    {
        return !empty(self::all()[$key]) ? self::all()[$key] : false;
    }

    static public function getPermissions($key = null)
    {
        $permissions = [
            self::ADMINISTRADOR => [
                'layout',
                'usuarios',
                'usuarios.edit-password',
                'cadastro-de-participantes',
                'inscricoes',
                'inscricao-profissional',
                'inscricoes.validar',
                'inscricoes.enviar-para-correcao',
                'inscricoes.parametrizacao',
                'julgamento.parametrizacao',
                'categorias',
                'criterios',
                'edicoes',
                'julgamento.triagem',
                'ver-pecas',
                'julgamento',
                'julgamento.desempatar',
                'log',
                'detalhes-inscricao',
                'resultado-edicao',
                'edicoes'
            ],
            self::JUIZ => [
                'julgamento.triagem.desempatar',
                'ver-pecas',
                'julgar',
                'julgamento.desempatar',
                'detalhes-inscricao'
            ],
            self::JUIZ_SOLIDARIO => [
                'julgamento.triar',
                'julgar',
                'ver-pecas',
                'detalhes-inscricao'
            ],
            self::JURADO => [
                'julgamento.triar',
                'julgar',
                'ver-pecas',
                'detalhes-inscricao'
            ],
            self::OPEC => [
                'inscricoes-opec',
                'inscricoes.enviar-para-correcao',
                'detalhes-inscricao'
            ],
            self::PARTICIPANTE => [
                //'inscricoes.pedente-para-revisao',
                'inscricoes-participante',
            ],
        ];

        return !empty($key) ? $permissions[$key] : $permissions;
    }
}