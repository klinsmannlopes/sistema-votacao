<?php

namespace App\Policies;

use App\Inscricao;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InscriaoPolicy
{
    use HandlesAuthorization;

    public function owner(User $user, Inscricao $inscricao)
    {
        return $user->id === $inscricao->user_id;
    }
}
