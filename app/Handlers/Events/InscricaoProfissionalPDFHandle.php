<?php
/**
 * Created by PhpStorm.
 * User: 750371426
 * Date: 01/02/2017
 * Time: 17:33
 */

namespace App\Handlers\Events;

use App\Events\Event;

class InscricaoProfissionalPDFHandle
{

    private $nomeFicha;

    public function setNomeFicha($numeroInscricao, $nome)
    {
        if(!empty($nome))
        {
            $extension = 'pdf';
            $nome = removerAcentos($nome);
            $newName   = generate_file_name($extension, $numeroInscricao.'-'.$nome);
            $this->nomeFicha =  $newName;
        }
        return false;
    }

    public function getNomeFicha()
    {
        return $this->nomeFicha;
    }


    public function urlCurriculo($curriculo)
    {
        return app_path_uploads('curriculos'.DIRECTORY_SEPARATOR.$curriculo);
    }

    public function urlFichaInscricao()
    {
        return app_path_uploads('inscricao-profissional'.DIRECTORY_SEPARATOR.$this->getNomeFicha());
    }


    public function gerarPdfInscricao($row)
    {
        $urlFoto =    url_uploads('fotos'.DIRECTORY_SEPARATOR.$row->foto);
        $pathFoto   = path_uploads('fotos'.DIRECTORY_SEPARATOR.$row->foto);

        $view = view("inscricao-profissional.pdf", compact('row','urlFoto','pathFoto'))->render();
        $pdf  = \PDF::loadHTML($view)->setPaper('legal', 'potrait')->setWarnings(false)->save(app_path_uploads('inscricao-profissional'.DIRECTORY_SEPARATOR.$this->getNomeFicha()));

        return $pdf->stream();
    }


    public function setPermissions()
    {
        return chmod($this->urlFichaInscricao(), 0777);
    }

    public function handle(Event $event)
    {
            $row = $event->inscricaoProfissional;
            $this->setNomeFicha($row->present()->numeroInscricao(), $row->nome);
            $this->gerarPdfInscricao($row);
            $this->setPermissions();
    }
}