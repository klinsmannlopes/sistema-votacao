<?php

namespace App\Handlers\Events;

use App\Events\Event;
use App\Mailers\ResettingPasswordMailer;

class SendMailResetPasswordHandle
{
    public function handle(Event $event)
    {
        $user = $event->user;
        $token = \Password::getRepository()->create($user);
        ResettingPasswordMailer::send(compact('user', 'token'));
    }
}
