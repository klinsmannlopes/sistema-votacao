<?php
/**
 * Created by PhpStorm.
 * User: 750371426
 * Date: 23/01/2017
 * Time: 08:06
 */

namespace App\Handlers\Events;

use App\Events\Event;
use App\Categoria;
use App\InscricaoProfissional;
use App\Events\RenameInscricaoProfissionalEvent;


class RenameFileInscricaoProfissionalHandle
{

    public function setNewName($numeroInscricao, $nome, $arquivo)
    {
        if(!empty($nome) && !empty($arquivo) )
        {
            $extension = pathinfo($arquivo, PATHINFO_EXTENSION);
            $extension = strtolower($extension);
            $nome      = removerAcentos($nome);
            $newName   = $extension == "pdf" ? generate_file_name($extension, $numeroInscricao.'-curriculo'.'-'.$nome) : generate_file_name($extension,$numeroInscricao.'-foto'.'-'.$nome);
            return $newName;
        }

        return false;
    }

    public function renameFile($path, $fileOld, $fileNew )
    {
        try{
            $filePath = $path.DIRECTORY_SEPARATOR.$fileOld;

            if( !empty($fileOld) && file_exists($filePath)){
                return rename( $filePath, $path . '/' . $fileNew );
            }
        }
        catch (\Exception $e)
        {
            logger()->error($e);
            logger()->error($e->getMessage());
        }
    }


    public function handle(Event $event)
    {
        $inscricao =  $event->inscricaoProfissional;

        $newNameCurriculo = $this->setNewName($inscricao->present()->numeroInscricao(),$inscricao->nome, $inscricao->curriculo);
        $newNameFoto      = $this->setNewName($inscricao->present()->numeroInscricao(),$inscricao->nome, $inscricao->foto);

        $oldNameCurriculo = $inscricao->curriculo;
        $oldNameFoto      = $inscricao->foto;

        $inscricao->curriculo = $newNameCurriculo;
        $inscricao->foto = $newNameFoto;

        if($inscricao->save()){
            $this->renameFile(
                app_path_uploads('curriculos'),
                $oldNameCurriculo,
                $newNameCurriculo
            );
            $this->renameFile(
                app_path_uploads('fotos'),
                $oldNameFoto,
                $newNameFoto
            );
        }
    }
}