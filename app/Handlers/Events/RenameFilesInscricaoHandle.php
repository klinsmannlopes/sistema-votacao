<?php

namespace App\Handlers\Events;

use App\Inscricao;
use App\InscricaoPeca;
use App\PI;
use App\Categoria;
use App\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Storage;

class RenameFilesInscricaoHandle
{

	protected $namePecaOld = "";
	protected $namePecaNew = "";
	protected $pecaExtension   = "";
	protected $namePisOld = "";
	protected $namePisNew = "";
	protected $pisExtension   = "";
	
	public function setPecaExtension( $pecaExtension ){
		$this->pecaExtension = $pecaExtension;
	}

	public function getPecaExtension(){
		return $this->pecaExtension;
	}

	public function setNamePecaOld( $namePecaOld ){
		$this->namePecaOld = $namePecaOld;
	}

	public function getNamePecaOld(){
		return $this->namePecaOld;
	}

	public function setNamePecaNew( $namePecaNew ){
		$this->namePecaNew = $namePecaNew;
	}

	public function getNamePecaNew(){
		return $this->namePecaNew;
	}

	public function setPisExtension( $pisExtension ){
		$this->pisExtension = $pisExtension;
	}

	public function getPisExtension(){
		return $this->pisExtension;
	}

	public function setNamePisOld( $namePisOld ){
		$this->namePisOld = $namePisOld;
	}

	public function getNamePisOld(){
		return $this->namePisOld;
	}

	public function setNamePisNew( $namePisNew ){
		$this->namePisNew = $namePisNew;
	}

	public function getNamePisNew(){
		return $this->namePisNew;
	}

    public function handle(Event $event)
    {
        $inscricao = $event->inscricao;
        $numero_inscricao = $inscricao->present()->numeroInscricao();
        $pecas_pis = $inscricao->pecas()->get();

        $this->setChangesFiles($inscricao->id, $pecas_pis, $numero_inscricao);
    }

    public function setChangesFiles( $id, $pecas, $numero_inscricao ){

    	$cont_pis = 0;

    	foreach($pecas as $index => $peca) {
            	
            	$inscricaoPeca = InscricaoPeca::find( $peca->id );
            	$this->getNewPecaPis( $peca['arquivo'], $numero_inscricao, ($index+1), 'peca' );
            	$inscricaoPeca->arquivo = $this->getNamePecaNew();

				foreach ( $peca['pis'] as $index2 => $pis ) {	
					$cont_pis++;
					$inscricaoPis = PI::find( $pis->id );
					$this->getNewPecaPis( $pis['pdf'], $numero_inscricao, $cont_pis, 'pis' );            		
            		$inscricaoPis->pdf= $this->getNamePisNew();

            		if( $inscricaoPis->save() ) {
            			$this->renameFile(
		            		app_path_uploads('pecas-pdf'), //caminho dos arquivos
		            		$this->getNamePisOld(), //nome do arquivo antigo
		            		$this->getNamePisNew() //nome do arquivo novo
		            	);		
            		}

				}

				if( $inscricaoPeca->save() ) {
            		$this->renameFile(
		        		app_path_uploads('pecas'), //caminho dos arquivos
		        		$this->getNamePecaOld(), //nome do arquivo antigo
		        		$this->getNamePecaNew() //nome do arquivo novo
		        	);			
            	}
        }
    }

    public function getNewPecaPis( $peca_pis, $numero_inscricao, $sequencia, $tipo = 'peca' ){

    	if ( $tipo == "peca" ) {
    		$this->setPecaExtension( pathinfo( $peca_pis, PATHINFO_EXTENSION ) );
	    	$this->setNamePecaNew( generate_file_name( $this->getPecaExtension(), $numero_inscricao."_PECA0".$sequencia) );
	    	$this->setNamePecaOld( $peca_pis );	
    	} else if ( $tipo == "pis" ) {
    		$this->setPisExtension( pathinfo( $peca_pis, PATHINFO_EXTENSION ) );
	    	$this->setNamePisNew( generate_file_name( $this->getPisExtension(), $numero_inscricao."_PIS0".$sequencia) );
	    	$this->setNamePisOld( $peca_pis );	
    	}
    	
    }

    public function renameFile( $path, $fileOld, $fileNew ){

		try{
			$filePath = $path.DIRECTORY_SEPARATOR.$fileOld;

			if( !empty($fileOld) && file_exists($filePath)){
				return rename( $filePath, $path . '/' . $fileNew );
			}
		}
		catch (\Exception $e)
		{
			logger()->error($e);
			logger()->error($e->getMessage());
		}

    }

}