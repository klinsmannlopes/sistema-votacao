<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaEdicao extends Model
{
    protected $table = "categorias_edicoes";

    protected $fillable = [
    	'categoria_id',
    	'edicao_id'
    ];
}
