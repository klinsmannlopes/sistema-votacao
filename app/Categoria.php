<?php

namespace App;

use App\Contracts\LoggerInterface;
use App\Traits\LoggableTrait;
use App\Traits\EdicaoHasTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

use App\Presenters\CategoriaPresenter;

class Categoria extends Model implements LoggerInterface
{
    use LoggableTrait, EdicaoHasTrait;
    
    //SOFT DELETE
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    //PRESENTER
    use PresentableTrait;
    protected $presenter = CategoriaPresenter::class;

    protected $exibirMinerva;
    protected $inscricoesDuplicadas = [];

    protected $table = 'categorias';

    protected $fillable = [
        'nome',
        'abreviatura',
        'parent_id',
        'qtd_pecas',
        'qtd_pecas_tipo',
        'tipos_de_anexo',
        'is_triagem',
        'is_ativo',
        'is_multiplo_opec',
        'qtd_opecs',
        'peso',
        'has_pis'
    ];

    protected $guarded = [
        'is_fixo'
    ];

    const QTD_PECAS_TIPO_MIN        = 'minimo';
    const QTD_PECAS_TIPO_MAX        = 'maximo';
    const QTD_PECAS_TIPO_EXATAMENTE = 'exatamente';

    const TIPO_DE_ANEXO_IMAGEM = 'imagem';
    const TIPO_DE_ANEXO_AUDIO = 'audio';
    const TIPO_DE_ANEXO_VIDEO = 'video';
    const TIPO_DE_ANEXO_IMAGEM_VIDEO = 'imagem/video';


    public function edicoes()
    {
        return $this->belongsToMany('App\Edicao', 'categorias_edicoes', 'categoria_id', 'edicao_id');
    }

    public static function qtdPecasTipo($key = null)
    {
        $a = [
            self::QTD_PECAS_TIPO_MIN => 'Mínimo',
            self::QTD_PECAS_TIPO_MAX => 'Máximo',
            self::QTD_PECAS_TIPO_EXATAMENTE => ucfirst(self::QTD_PECAS_TIPO_EXATAMENTE),
        ];

        if($key !== null)
            return isset($a[$key]) ? $a[$key] : false;

        return $a;
    }

    public static function tiposDeAnexo($key = null)
    {
        $a = [
            self::TIPO_DE_ANEXO_IMAGEM => 'Imagem (PDF)',
            self::TIPO_DE_ANEXO_AUDIO => 'Áudio (MP3)',
            self::TIPO_DE_ANEXO_VIDEO => 'Vídeo (FLV)',
            self::TIPO_DE_ANEXO_IMAGEM_VIDEO => 'Imagem/Vídeo (SWF,GIF)',
        ];

        if($key !== null)
            return isset($a[$key]) ? $a[$key] : false;

        return $a;
    }

    /**
     * @param $value
     *
     * @return mixed|string
     */
    public function getNomeAttribute()
    {
        if( isset($this->parent) && $this->parent )
            return sprintf('%s &rsaquo; %s', $this->parent->getAttribute('nome'), $this->attributes['nome']);

        return $this->attributes['nome'];
    }

    /**
     * @param $value
     *
     * @return mixed|string
     */
    public function getNomeRawAttribute()
    {
        return $this->attributes['nome'];
    }

    public function setParentIdAttribute($value)
    {
        $this->attributes['parent_id'] = empty($value) || $value == 0 ?  NULL : $value;
    }

    public function criterios()
    {
        return $this->belongsToMany(Criterio::class)->withTimestamps();
    }

    public function inscricoes()
    {
        return $this->hasMany(Inscricao::class);
    }

    public function inscricoesProfissionais()
    {
        return $this->hasMany(InscricaoProfissional::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany(Categoria::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parent()
    {
        return $this->belongsTo(Categoria::class, 'parent_id', 'id');
    }

    public function triagems()
    {
        return $this->hasMany(Triagem::class);
    }

    public function triagem($inscricao_id = null, $user_id = null)
    {
        $query = $this->hasMany(Triagem::class);

        if($inscricao_id)
            $query = $query->where('inscricao_id', $inscricao_id);

        if($user_id)
            $query = $query->where('user_id', $user_id);

        return $query;
    }

    public function julgamentos()
    {
        return $this->hasMany(Julgamento::class);
    }

    public function is_fixa()
    {
        return $this->is_fixo ? true : false;
    }

    public function isAguardandoDesempate()
    {
        if( $this->isTriagemRealizada() )
        {
            if( $this->somenteInscricoesEmpatadas()->count() )
            {
                return ! $this->triagems()->whereNotNull('ordem')->count();
            }
        }

        return false;
    }

    public function isTriagemRealizada()
    {
        if($this->abreviatura === "CAMPA") {
            return ( ($this->juradosQueTriaram()->count() && $this->juradosQueNaoTriaram()->count() === 0 ) && ($this->juizSolidarioQueTriaram()->count() && $this->juizSolidarioQueNaoTriaram()->count() === 0 )) || ! $this->is_triagem;
        } else {
            return ( $this->juradosQueTriaram()->count() && $this->juradosQueNaoTriaram()->count() === 0 ) || ! $this->is_triagem;
        }
    }

    public function isTriagemEmAndamento()
    {
        return ! $this->isTriagemRealizada();
    }

    public function isJulgamentoRealizado()
    {
        $julgamentoRealizado = true;

        $user = new User;

        $categoria_id = $this->id;

        if(! $this->is_triagem)
            $inscricoes = $this->inscricoesValidas();
        else
            $inscricoes = $this->topTriagem();

        if ($categoria_id == 97) {
            $judges = $user->getJurados()->merge($user->getJuizes())->merge($user->getJuizSolidario());
        }else{
            $judges = $user->getJurados()->merge($user->getJuizes());
        }

        foreach($judges as $judge)
        {
            foreach($inscricoes as $inscricao)
            {
                $julgamento = $judge->julgamentos()->where('categoria_id', $categoria_id)->where('inscricao_id', $inscricao->id);

                if( ! $julgamento->count() )
                {
                    $julgamentoRealizado = false;
                    break;
                }
            }
        }

        return $julgamentoRealizado && $this->juradosQueJulgaram()->count() && $this->juradosQueNaoJulgaram()->count() === 0;
    }

    public function isJulgamentoEmAndamento()
    {
        return $this->isTriagemRealizada() && ! $this->isJulgamentoRealizado();
    }

    public function isJulgamentoEmpatado($tamanhoTop = 5)
    {
        if($this->isJulgamentoRealizado())
        {
            $top5 = $this->top($tamanhoTop);
            $contagemDiferente = $top5->count() > $tamanhoTop && $top5->count() !== $tamanhoTop;

            if($contagemDiferente)
                return true;

            $inscricoesNotas = $this->notaDaMediaDasInscricoes();

            //VERIFICAÇÃO DA DUPLICIDADE DAS NOTAS
            $agrupamentoDeNotas = $this->agruparInscricoesPorMedia($inscricoesNotas);

            $agrupamentoDeNotas = array_slice($agrupamentoDeNotas, 0, $tamanhoTop);

            //SE EXISTIR NOTA REPETIDA
            $hasDuplicidade = $this->verificarNotaRepetida($agrupamentoDeNotas);

            return $hasDuplicidade;
        }

        return false;
    }

    public function isJulgamentoDesempatado()
    {
        return $this->julgamentos()->whereNotNull('ordem')->count() > 1;
    }

    //LISTA OS JURADOS QUE JÁ TRIARAM A CATEGORIA
    public function juradosQueTriaram()
    {
        $categoria_id = $this->id;

        $jurados = User::where('role', Role::JURADO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })
            ->whereHas('triagems', function($q) use ($categoria_id) {
                $q->where('categoria_id', $categoria_id);
            });

        return $jurados->get();
    }


    public function juizSolidarioQueTriaram()
    {
        $categoria_id = 97;

        $jurados = User::where('role', Role::JUIZ_SOLIDARIO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })
            ->whereHas('triagems', function($q) use ($categoria_id) {
                $q->where('categoria_id', $categoria_id);
            });

        return $jurados->get();
    }


    //LISTA OS JURADOS QUE AINDA NÃO TRIARAM A CATEGORIA
    public function juradosQueNaoTriaram()
    {
        $juradosQueTriaram = $this->juradosQueTriaram();
        $juradosQueNaoTriaram = User::where('role', Role::JURADO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })->get();

        $diff = $juradosQueNaoTriaram->diff($juradosQueTriaram);

        return $diff;
    }

    public function juizSolidarioQueNaoTriaram()
    {
        $juradosQueTriaram = $this->juizSolidarioQueTriaram();
        $juradosQueNaoTriaram = User::where('role', Role::JUIZ_SOLIDARIO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })->get();

        $diff = $juradosQueNaoTriaram->diff($juradosQueTriaram);

        return $diff;
    }


    public function juizesQueTriaram()
    {
        $categoria_id = $this->id;

        $juizes = User::where('role', Role::JUIZ)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })
            ->whereHas('triagems', function($q) use ($categoria_id) {
                $q->where('categoria_id', $categoria_id);
            });

        return $juizes->get();
    }

    public function juizesQueNaoTriaram()
    {
        $user = new User;

        $juizesQueTriaram = $this->juizesQueTriaram();
        $juizes = $user->getJuizes();

        $diff = $juizes->diff($juizesQueTriaram);

        return $diff;
    }

    public function juradosQueJulgaram()
    {
        $categoria_id = $this->id;

        $jurados = User::where('role', Role::JURADO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })
            ->whereHas('julgamentos', function($q) use ($categoria_id) {
                $q->where('categoria_id', $categoria_id);
            });

        return $jurados->get();
    }

    public function juizSolidarioJulgaram()
    {
        $categoria_id = 97;

        $jurados = User::where('role', Role::JUIZ_SOLIDARIO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })
            ->whereHas('julgamentos', function($q) use ($categoria_id) {
                $q->where('categoria_id', $categoria_id);
            });

        return $jurados->get();
    }

    public function juradosQueNaoJulgaram()
    {
        $user = new User;

        $jurados = $user->getJurados();
        $juradosQueJulgaram = $this->juradosQueJulgaram();


        $diff = $jurados->diff($juradosQueJulgaram);

        return $diff;
    }

    public function juizSolidarioQueJulgaram()
    {
        $categoria_id = $this->id;

        $juiz_solidario = User::where('role', Role::JUIZ_SOLIDARIO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })
            ->whereHas('julgamentos', function($q) use ($categoria_id) {
                $q->where('categoria_id', 97);
            })
            ->orWhere('role', Role::JURADO)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })
            ->whereHas('julgamentos', function($q) use ($categoria_id) {
                $q->where('categoria_id', $categoria_id);
            })
        ;

        return $juiz_solidario->get();
    }

    public function juizSolidarioQueNaoJulgaram()
    {
        $user = new User;

        $juiz_solidario = $user->getJuizSolidarioEJurado();
        $juizSolidarioQueJulgaram = $this->juizSolidarioQueJulgaram();
        $diff = $juiz_solidario->diff($juizSolidarioQueJulgaram);

        return $diff;
    }

    public function juizSolidarioNaoJulgaram()
    {
        $user = new User;

        $juiz_solidario = $user->getJuizSolidario();
        $juizSolidarioQueJulgaram = $this->juizSolidarioQueJulgaram();
        $diff = $juiz_solidario->diff($juizSolidarioQueJulgaram);

        return $diff;
    }

    public function juizesQueJulgaram()
    {
        $categoria_id = $this->id;

        $juizes = User::where('role', Role::JUIZ)
            ->whereHas('edicoes',function ($query) {
                $query->where('edicao_id', getEdicaoAtualId() );
            })
            ->whereHas('julgamentos', function($q) use ($categoria_id) {
                $q->where('categoria_id', $categoria_id);
            });

        return $juizes->get();
    }

    public function juizesQueNaoJulgaram()
    {
        $juizes = User::where('role', Role::JUIZ)->whereHas('edicoes',function ($query) {
            $query->where('edicao_id', getEdicaoAtualId() );
        })->get();
        $juizesQueJulgaram = $this->juizesQueJulgaram();

        $diff = $juizes->diff($juizesQueJulgaram);

        return $diff;
    }

    public function inscricoesTriadasPelosJurados()
    {
        $ids = \DB::table('triagems')
            ->groupBy('inscricao_id')
            ->lists('inscricao_id');

        $inscricoes = Inscricao::whereIn('id', $ids);

        return $inscricoes;
    }

    public function getAguardandoDesempate()
    {
        $ids = [];
        $all = $this->all();

        foreach($all as $row)
            if($row->isAguardandoDesempate())
                $ids[] = $row->id;

        if(!$ids)
            return $ids;

        return $this->whereIn('id', $ids);
    }

    public function getTriagemRealizada()
    {
        $ids = [];
        $all = $this->all();

        foreach($all as $row)
            if($row->isTriagemRealizada())
                $ids[] = $row->id;

        if(!$ids)
            return $ids;

        return $this->whereIn('id', $ids)->get();
    }

    protected function inscricoesEmpatadas($onlyEmpatadas)
    {
        if($this->inscricoes->count() > 10)
        {
            $inscricoesAgroup = [];
            foreach($this->inscricoes as $inscricao)
                $inscricoesAgroup[$inscricao->triagems->count()][] = $inscricao->id;

            unset($inscricoesAgroup[0]);

            krsort($inscricoesAgroup);

            $i = 0;
            $empatadas = [];

            foreach($inscricoesAgroup as $qtd => $inscricoes)
            {
                $i += count($inscricoes);

                if($onlyEmpatadas)
                {
                    if($i >= 10)
                    {
                        $empatadas = $inscricoes;
                        break;
                    }
                }
                else
                {
                    $empatadas = array_merge($empatadas, $inscricoes);

                    if($i >= 10)
                        break;
                }
            }

            //SOMENTE SE EXISTIR MAIS DE UM REGISTRO
            if(count($empatadas) > 1)
                return $this->inscricoes()->whereIn('id', $empatadas)->get();
        }

        return collect();
    }

    public function somenteInscricoesEmpatadas()
    {
        $empatadas = $this->inscricoesEmpatadas(false);
        return $empatadas->count() > 10 ? $this->inscricoesEmpatadas(true) : collect();
    }

    public function inscricoesAteEmpatadas()
    {
        return $this->inscricoesEmpatadas(false);
    }

    public function top($tamanho = 5)
    {
        if($this->isJulgamentoDesempatado())
        {
            $julgamentos = $this->julgamentos()->whereNotNull('ordem')->orderBy('ordem')->get();

            $items = [];
            foreach($julgamentos as $julgamento)
                $items[] = $julgamento->inscricao;

            return collect($items);
        }

        $inscricoesNotas = $this->notaDaMediaDasInscricoes();

        //VERIFICAÇÃO DA DUPLICIDADE DAS NOTAS
        $agrupamentoDeNotas = $this->agruparInscricoesPorMedia($inscricoesNotas);

        //SE EXISTIR NOTA REPETIDA
        if($hasDuplicidade = $this->verificarNotaRepetida($agrupamentoDeNotas))
        {
            $i = 0;
            $duplos = [];

            foreach($agrupamentoDeNotas as $nota => $inscricoes_ids)
            {
                $i += count($inscricoes_ids);

                foreach($inscricoes_ids as $id)
                    $duplos[$id] = (float)$nota;

                if($i >= $tamanho)
                    break;
            }

            $retornoInscricoesIds = array_keys($duplos);
        }

        if(! isset($retornoInscricoesIds))
            $retornoInscricoesIds = array_slice(array_keys($inscricoesNotas), 0, $tamanho);

        return $this->inscricoes()->whereIn('id', $retornoInscricoesIds)->get()->sortBy(function($row, $key) use ($inscricoesNotas)
        {
            return $inscricoesNotas[$row->id];
        })->reverse();
    }

    protected function agruparInscricoesPorMedia($inscricoesNotas)
    {
        $agrupamento = [];
        foreach($inscricoesNotas as $incricao_id => $nota)
            $agrupamento[(string)$nota][] = $incricao_id;

        return $agrupamento;
    }

    protected function verificarNotaRepetida($array, $returnValues = false)
    {
        $flag = false;
        $currentValues = [];

        foreach($array as $k => $values)
        {
            if(count($values) > 1)
            {
                $flag = true;
                $currentValues = array_merge($currentValues, $values);
            }
        }

        if( true === $returnValues ){
            return [$flag, $currentValues];
        }

        return $flag;
    }

    public function getInscricoesDuplicadas()
    {
        return array_unique($this->inscricoesDuplicadas);
    }

    protected function getMediaJugamentos($onlyJurados = true)
    {
        $medias = [];

        foreach($this->julgamentos as $julgamento)
        {
            $user = $julgamento->user;

            $inscricao = $julgamento->inscricao;
            $categoria = $julgamento->categoria;

            $notas = [];

            foreach($julgamento->notas as $nota){
                $notas[] = $nota->nota;
            }

            if($categoria->abreviatura === "CAMPA") {
                $medias[$inscricao->id][$julgamento->id] = [
                    'values'        =>  $notas,
                    'is_jurado'     =>  $user->is_jurado_e_solidario()
                ];
            } else {
                $medias[$inscricao->id][$julgamento->id] = [
                    'values'        =>  $notas,
                    'is_jurado'     =>  $user->is_jurado()
                ];
            }
//            SE SOMENTE JURADOS
//            if($onlyJurados){
//                SE A NOTA NÃO FOI DADA POR UM JURADO, ELA É REMOVIDA
//                if( ! $user->is_jurado()){
//                    unset($medias[$inscricao->id][$julgamento->id]);
//                }
//            }
        }

        return $medias;
    }

    protected function getMediaAvg(array $medias, $onlyJurados = true)
    {
        $inscricoesNotas = [];
        $inscricoesDuplicadas = $this->getInscricoesDuplicadas();

        foreach($medias as $inscricao_id => $julgamentos)
        {

            $aux = [];

            foreach($julgamentos as $julgamento_id => $notas)
            {

                $isJurado = isset($notas['is_jurado']) && $notas['is_jurado'] ? true : false;

                if( $isJurado ){
                    $aux[] = array_avg($notas['values']);
                }

                if( $onlyJurados === false && !$isJurado ){
                    if( in_array($inscricao_id, $inscricoesDuplicadas) ){
                        $aux[] = array_avg($notas['values']);
                    }
                }
            }

            $inscricoesNotas[$inscricao_id] = array_avg($aux);
        }

        arsort($inscricoesNotas);

        return $inscricoesNotas;
    }

    public function notaDaMediaDasInscricoes($onlyJurados = true, $tamanhoTop = 5)
    {
        $medias          = $this->getMediaJugamentos($onlyJurados);
        $inscricoesNotas = $this->getMediaAvg($medias, $onlyJurados);

        //VERIFICAÇÃO DA DUPLICIDADE DAS NOTAS
        $agrupamentoDeNotas = $this->agruparInscricoesPorMedia($inscricoesNotas);
        $agrupamentoDeNotas = array_slice($agrupamentoDeNotas, 0, $tamanhoTop);

        //SE EXISTIR NOTA REPETIDA
        list($hasDuplicidade, $inscDuplicadas) = $this->verificarNotaRepetida($agrupamentoDeNotas, true);

        //dd($medias, $inscricoesNotas, $hasDuplicidade, $inscDuplicadas);

        if( is_array($inscDuplicadas) && $inscDuplicadas ){
            //REGISTRA AS INSCRICOES DUPLICADAS
            $this->inscricoesDuplicadas = array_merge($this->inscricoesDuplicadas, $inscDuplicadas);
        }

        if($hasDuplicidade && $onlyJurados){
            return $this->notaDaMediaDasInscricoes(false);
        }
        else{
            return $inscricoesNotas;
        }
    }

    public function exibirVotoMinerva()
    {
        return $this->exibirMinerva;
    }

    public function getComboboxDesempateJulgamento($top)
    {
        $inscricoesNotas = [];
        foreach($top as $inscricao)
            $inscricoesNotas[$inscricao->id] = $inscricao->mediaFinal();

        //VERIFICAÇÃO DA DUPLICIDADE DAS NOTAS
        $agrupamentoDeNotas = $this->agruparInscricoesPorMedia($inscricoesNotas);

        $i = 1;
        $combobox = [];

        foreach($agrupamentoDeNotas as $nota => $ids)
        {
            if(($qtd = count($ids)) > 1)
            {
                $aux = [];
                foreach($ids as $id)
                    $aux[] = $i++;

                $combobox[] = $aux;
            }
            else
                $i += 1;
        }

        return $combobox;
    }

    function getWithJulgamentoEmpatado()
    {
        $categorias = [];

        foreach($this->all() as $row)
            if($row->isJulgamentoEmpatado())
                $categorias[] = $row;

        return $categorias ? collect($categorias) : $categorias;
    }

    function getWithJulgamentosRealizados()
    {
        $categorias = [];

        foreach($this->all() as $row)
            if($row->isJulgamentoRealizado())
                $categorias[] = $row;

        return $categorias ? collect($categorias) : $categorias;
    }

    public function topTriagem($tamanho = 10)
    {
        //CASO TENHA OCORRIDO UM DESEMPATE
        if($this->triagems()->whereNotNull('ordem')->count())
        {
            $triagems = $this->triagems()->whereNotNull('ordem')->orderby('ordem')->take($tamanho)->get();

            $inscricoes = [];
            foreach($triagems as $triagem)
                $inscricoes[] = $triagem->inscricao;

            return collect($inscricoes);
        }
        else
        {
            $inscricoesAgroup = [];
            foreach($this->inscricoesValidas() as $inscricao)
                $inscricoesAgroup[$inscricao->triagems->count()][] = $inscricao->id;

            krsort($inscricoesAgroup);

            $i = 0;
            $ids = [];
            foreach($inscricoesAgroup as $qtd => $group)
            {
                $i += count($group);

                $ids = array_merge($ids, $group);

                if($i >= 10)
                    break;
            }

            $inscricoes = Inscricao::whereIn('id', $ids)->get()->sortBy(function($row, $key) use($ids)
            {
                return array_search($row->id, $ids);
            });

            return $inscricoes;
        }
    }

    public function inscricoesSelecionadas($ids)
    {
        return $this->inscricoes()->whereIn('id', $ids)->get();
    }

    public function inscricoesValidas()
    {
        return $this->inscricoes()->where('status', Inscricao::STATUS_VALIDA)->get();
    }

    public function inscricoesProfissionaisAprovadas()
    {
        return $this->inscricoesProfissionais()->where('status', InscricaoProfissional::STATUS_APROVADA)->get();
    }

    public function getCategoriesWithInscriptionsProfissinais(){
        return $this->whereHas('inscricoesProfissionais', function($query){
            $query->where('status', \App\InscricaoProfissional::STATUS_APROVADA);
        })->get();
    }

    public function getCategoriasPais()
    {
        $query =  \DB::table($this->table)
            ->select('parent_id')
            ->where('parent_id', '<>', 0)
            ->where('is_ativo', 1)
            ->get();

        return array_pluck($query, 'parent_id');
    }


    public function getCategoriesWithInscriptions(){
        return $this->whereHas('inscricoes', function($query){
            $query->where('status', \App\Inscricao::STATUS_VALIDA);
        })->get();
    }

    public function getCategoriesWithSolidaria(){
        return $this->whereHas('inscricoes', function($query){
            $query->where('status', \App\Inscricao::STATUS_VALIDA)
            ->Where('categoria_id',  97);
        })->get();
    }
}