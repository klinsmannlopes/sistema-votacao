@extends('layouts.panel')

@section('page-title')
    Personalização de Layout
@endsection

@section('content')

    @include('errors')

    <form method="post" action="{{ route('layout.save') }}" class="form-horizontal" enctype="multipart/form-data">

        {!! csrf_field() !!}

        <div class="form-group">
            <label class="col-lg-2 control-label" for="envelopagem">Envelopagem</label>
            <div class="col-lg-10">
                <input type="file" class="form-control" name="envelopagem" id="envelopagem">
                <small><b>1920px</b> <i class="fa fa-close"></i> <b>1080px</b> (Largura x Altura).</small>
                @if(!empty($envelopagem->value))
                <div style="margin-top: 10px;">
                    <a class="btn btn-xs btn-white" target="_blank" href="{{ url_uploads("layout/{$envelopagem->value}") }}"><i class="fa fa-file-image-o"></i> <b>Arquivo atual:</b> Clique aqui para visualizá-lo.</a></small>
                    <a class="btn btn-xs btn-danger" href="{{ route('layout.destroy', [$envelopagem->key]) }}"><i class="fa fa-close"></i> Remover arquivo</a>
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="logo">Logo</label>
            <div class="col-lg-10">
                <input type="file" class="form-control" name="logo" id="logo">
                <small><b>128px</b> <i class="fa fa-close"></i> <b>128px</b> (Largura x Altura).</small>
                @if(!empty($logo->value))
                <div style="margin-top: 10px;">
                    <a class="btn btn-xs btn-white" target="_blank" href="{{ url_uploads("layout/{$logo->value}") }}"><i class="fa fa-file-image-o"></i> <b>Arquivo atual:</b> Clique aqui para visualizá-lo.</a></small>
                    <a class="btn btn-xs btn-danger" href="{{ route('layout.destroy', [$logo->key]) }}"><i class="fa fa-close"></i> Remover arquivo</a>
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route('dashboard') }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button class="btn btn-info" type="submit"><i class="fa fa-save"></i> Salvar</button>
            </div>
        </div>
    </form>
@endsection