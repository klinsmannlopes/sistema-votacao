@extends('layouts.panel')

@section('page-title')
    Desempatar Triagem "{{ $categoria->nome }}"
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

    @include("{$namespace}.partials.errors")
    @include("{$namespace}.partials.modal-confirm-limpar")

    @if(count($rows))

        <h1><i>"Visualize as inscrições através do link e defina a ordenação entre elas. Se você errar e quiser limpar sua ordenação para começar de novo, basta clicar no botão ‘Limpar’, ao final da tabela. Ao terminar, clique no botão ‘Salvar’, também ao final da tabela."</i></h1>
        <h3 class="text-danger text-center">Importante: Após salvar o Desempate da Triagem, não é possível alterá-lo.</h3>

        <form method="post" action="{{ route("{$namespace}.confirm-ordenacao", [ $categoria->id ]) }}">

            {!! csrf_field() !!}

            @include("{$namespace}.partials.table")

            <div class="text-center">
                <a class="btn btn-white" href="{{ route("{$namespace}.categorias") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button type="button" id="limpar-ordenacao" class="btn btn-danger"><i class="fa fa-eraser"></i> Limpar</button>
                <button class="btn btn-info"><i class="fa fa-check"></i>Enviar</button>
            </div>
        </form>
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection