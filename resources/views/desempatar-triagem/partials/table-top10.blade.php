<div class="table-responsive">
    <br/>
    <table class="table">
        <thead>
            <th>&nbsp;</th>
            <th class="text-center">Nº da Inscrição</th>
            <th>Campanha</th>
            <th>Anunciante</th>
            <th class="text-center">Nº de Indicações</th>
            <th>Jurado(s)</th>
            <th>Juíz(es)</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
                <?php isset($i) ? ++$i : ($i = 1); ?>
            <tr style="{{ $i > 10 ? 'background-color: #EFEFEF; color: #BBB; text-decoration: line-through;' : '' }}">
                <td>{{ $i }}</td>
                <td width="200" class="text-center"><a href="{{ route($row->categoria->is_fixa() ? 'inscricoes.detalhes' : 'inscricoes.pecas', [ $row->id ]) }}" target="_blank">{{ $row->present()->numeroInscricao() }}</a></td>
                <td>{{ $row->campanha }}</td>
                <td>{{ $row->anunciante }}</td>
                <td class="text-center">{{ $row->triagems->count() }}</td>
                <td>@include("{$namespace}.partials.users", ['users' => $row->juradosQueTriaram()->lists('name')])</td>
                <td>@include("{$namespace}.partials.users", ['users' => $row->juizesQueTriaram()->lists('name')])</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>