<div class="table-responsive">
    <table class="table">
        <thead>
            <th>Categoria</th>
            <th>Status</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ $row->nome }}</td>
                <td>
                    @if(! $row->is_triagem)
                        Categoria levada diretamente para julgamento
                    @elseif($row->isTriagemRealizada())
                        @if($row->isAguardandoDesempate())
                            <a href="{{ route($namespace, [$row->id]) }}">Aguardando Desempate</a>
                        @else
                            <a href="{{ route("{$namespace}.top10", [$row->id]) }}">Triagem Realizada</a>
                        @endif
                    @elseif($row->isTriagemEmAndamento())
                        <p>Triagem em andamento</p>

                        <ol>
                            @foreach($row->juradosQueNaoTriaram() as $jurado)
                            <li>{{ $jurado->name }}</li>
                            @endforeach
                        </ol>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
