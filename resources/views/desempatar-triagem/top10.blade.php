@extends('layouts.panel')

@section('page-title')
    Inscrições Triadas - "{{ $categoria->nome }}"
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

    @if(count($rows))
        @include("{$namespace}.partials.table-top10")
    @else
        <h2>{{ $error }}</h2>
    @endif

    <div class="text-center">
        <a class="btn btn-white" href="{{ route("{$namespace}.categorias") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
    </div>

@endsection