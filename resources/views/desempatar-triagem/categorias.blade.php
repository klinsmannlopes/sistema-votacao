@extends('layouts.panel')

@section('page-title')
    Julgamento - Triagem
@endsection

@section('content')

    @if(count($rows))
        @include("{$namespace}.partials.table-categorias")
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection