@if(count($errors))
    <div class="row messages">
        <div class="col-xs-10 col-xs-offset-2">
            <div class="alert alert-danger">
                <h4>Encontramos alguns erros:</h4>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif