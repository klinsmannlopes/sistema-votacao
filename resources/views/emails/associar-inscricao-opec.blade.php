<p>Segue lista das Inscrições que foram associadas ao seu usuário:</p>

<ul>
    @foreach($associados as $associado)
    <li>{{ $associado }}</li>
    @endforeach
</ul>

<p>Avalie-as assim que possível.</p>