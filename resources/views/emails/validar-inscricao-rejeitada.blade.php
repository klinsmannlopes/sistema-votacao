<p>A sua inscrição de <b>Nº {{ $numero }}</b> está Pendente de Correção.</p><br>
<p>Por favor, acessar nosso sistema, realizar as correções e reenviar sua inscrição para continuar no concurso.</p><br>
<p>Agradecemos desde já e aguardamos seu retorno.</p>
