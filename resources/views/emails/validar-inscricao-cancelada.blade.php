<p>A sua inscrição de <b>Nº {{ $numero }}</b> foi Cancelada.</p><br>
<p>Justificativa: {{ $justificativa }}</p>
<p>Mas você pode solicitar no sistema sua revisão de status de cancelado.</p>