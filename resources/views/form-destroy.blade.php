<form method="post" class="destroy pull-right" action="{{ route($route, [ $id ]) }}" data-row-id="{{ $id }}">

    {!! csrf_field() !!}

    <input type="hidden" name="_method" value="delete">

    @if(Gate::check("{$namespace}-participante"))
        @if($periodoDeInscricao)
            <button type="button" class="btn btn-danger show-modal-destroy" data-toggle="tooltip" data-placement="top" title="Excluir"><i class="fa fa-trash"></i></button>
        @endif
    @else
        <button type="button" class="btn btn-danger show-modal-destroy" data-toggle="tooltip" data-placement="top" title="Excluir"><i class="fa fa-trash"></i></button>
    @endif


</form>