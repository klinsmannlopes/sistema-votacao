@extends('layouts.panel')

@section('page-title')
    Parametrização - Critérios
@endsection

@section('content')

    @include('modal-confirm-destroy')

    @if(count($rows))
    <div class="table-responsive">
        <table class="table">
            <thead>
            <th>Nome</th>
            <th class="text-center" width="60">Ativo ?</th>
            <th width="130"><a href="{{ route("{$namespace}.create") }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Novo</a></th>
            </thead>
            <tbody>
            @foreach($rows as $row)
                <tr>
                    <td>{{ $row->nome }}</td>
                    <td class="text-center">
                        @if($row->is_ativo)
                            <a href="{{ route("{$namespace}.unactive", [$row->id]) }}"><span class="label label-primary" data-toggle="tooltip" data-placement="top" title="Clique para desativá-lo"><i class="fa fa-check"></i></span></a>
                        @else
                            <a href="{{ route("{$namespace}.active", [$row->id]) }}"><span class="label label-default" data-toggle="tooltip" data-placement="top" title="Clique para ativá-lo"><i class="fa fa-close"></i></span></a>
                        @endif
                    </td>
                    <td class="text-right">
                        <a class="btn btn-white" href="{{ route("{$namespace}.edit", ['id' => $row->id]) }}" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                        @if(!$row->categorias()->with('inscricoes')->count())
                            @include('form-destroy', [ 'route' => "{$namespace}.destroy", 'id' => $row->id ])
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="text-center">
        {!! $rows->appends(compact('search'))->render() !!}
    </div>
    @else
    <h2>Nenhum registro foi encontrado. <br/> Clique <a href="{{ route("{$namespace}.create") }}">aqui</a> para cadastrar um novo critério.</h2>
    @endif

@endsection