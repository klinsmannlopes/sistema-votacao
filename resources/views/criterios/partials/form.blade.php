@include('errors')

<div class="form-group">
    <label class="col-lg-2 control-label" for="nome">Nome <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" value="{{ old('name', !empty($row->nome) ? $row->nome : '') }}" >
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label" for="is_ativo">Habilitar critério <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <ul class="list-unstyled">
            <li><label><input type="radio" name="is_ativo" value="1" {{ !empty($row->is_ativo) || empty($row->is_ativo) ? 'checked' : '' }}> Sim</label></li>
            <li><label><input type="radio" name="is_ativo" value="0" {{ isset($row->is_ativo) && !$row->is_ativo ? 'checked' : '' }}> Não</label></li>
        </ul>
    </div>
</div>

@include('campos-obrigatorios')