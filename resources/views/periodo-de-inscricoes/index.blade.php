@extends('layouts.panel')

@section('page-title')
    Cadastro das Inscrições
@endsection

@section('content')
    @include('errors')

    <form method="post" action="{{ route('periodo-de-inscricoes.save') }}" class="form-horizontal">

        {!! csrf_field() !!}

        <div class="form-group">
            <label class="col-lg-2 control-label" for="inscricoes_data_inicial">Início das inscrições de peças <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" class="form-control datetimepicker" name="inscricoes_data_inicial" id="inscricoes_data_inicial" value="{{ old('inscricoes_data_inicial', !empty($inscricoesDataInicial) ? date('d/m/Y H:i:s', strtotime($inscricoesDataInicial)) : '' ) }}" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="inscricoes_data_final">Fim das inscrições de peças <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" class="form-control datetimepicker" name="inscricoes_data_final" id="inscricoes_data_final" value="{{ old('inscricoes_data_final', !empty($inscricoesDataFinal) ? date('d/m/Y H:i:s', strtotime($inscricoesDataFinal)) : '' ) }}" autocomplete="off">
                </div>
            </div>
        </div>

        @include('campos-obrigatorios')

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route('dashboard') }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button class="btn btn-info" type="submit"><i class="fa fa-save"></i> Salvar</button>
            </div>
        </div>
    </form>
@endsection