@extends('layouts.panel')

@section('page-title')
    Parametrização - Edições
@endsection

@section('content')

    @include('modal-confirm-destroy')

    @if(count($rows))
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Nome</th>
                <th class="text-center" width="60">Ano</th>
                <th width="130"><a href="{{ route("{$namespace}.create") }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Novo</a></th>
            </thead>
            <tbody>
            @foreach($rows as $row)
                <tr>
                    <td>{{ $row->nome }}</td>
                    <td class="text-center">
                        {{ $row->ano }}
                    </td>
                    <td class="text-right">
                        <a class="btn btn-white" href="{{ route("{$namespace}.edit", ['id' => $row->id]) }}" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="text-center">
        {!! $rows->appends(compact('search'))->render() !!}
    </div>
    @else
    <h2>Nenhum registro foi encontrado. <br/> Clique <a href="{{ route("{$namespace}.create") }}">aqui</a> para cadastrar uma nova edição.</h2>
    @endif

@endsection