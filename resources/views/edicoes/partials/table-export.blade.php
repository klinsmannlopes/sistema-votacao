<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <table class="table">
        <thead>
        <tr>
            <th> Agência   </th>
            <th> Edição    </th>
            <th> Número de Categorias</th>
            <th> Categoria </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($agencias as $agencia): ?>
        <tr>
            <td valign="middle" style="border-right: solid 1px #e7eaec;" rowspan="<?php echo count($agencia['categorias']) + 1; ?>">
                <?php echo $agencia['agencia']; ?>
            </td>
            <td style="border-right: solid 1px #e7eaec;" rowspan="<?php echo count($agencia['categorias']) + 1; ?>">
                @if( isset($agencia['pesoTotal']) && $agencia['pesoTotal'] > 0 )
                    <?php echo $agencia['edicao'] . ' - Peso total: '.$agencia['pesoTotal']; ?>
                @else
                    <?php echo $agencia['edicao']; ?>
                @endif
            </td>
            <td  valign="middle" style="border-right: solid 1px #e7eaec;" rowspan="<?php echo count($agencia['categorias']) + 1; ?>">
                <?php echo count($agencia['categorias']); ?>
            </td>
        </tr>
            <?php foreach($agencia['categorias'] as $categoria): ?>
            <tr>
                <td>
                    @if( isset($categoria['peso']) && $categoria['peso'] > 0 )
                        <?php echo $categoria['nome'] . ' - Peso: '.$categoria['peso']; ?>
                    @else
                        <?php echo $categoria['nome']; ?>
                    @endif
                </td>
            </tr>
            <?php endforeach ?>
        <?php endforeach ?>
        </tbody>
    </table>
</body>
</html>