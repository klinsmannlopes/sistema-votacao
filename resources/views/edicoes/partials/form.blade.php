@include('errors')

<div class="form-group">
    <label class="col-lg-2 control-label" for="nome">Nome <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" value="{{ old('nome', !empty($row->nome) ? $row->nome : '') }}" >
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="ano">Ano <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="number" min="2000" class="form-control" name="ano" id="ano" autocomplete="off" value="{{ old('ano', !empty($row->ano) ? $row->ano : '') }}" >
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="data_inicio">Início<b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control datetimepicker" name="data_inicio" id="data_inicio" value="{{ old('data_inicio', isset($row->data_inicio) && !empty($row->data_inicio) ? date('d/m/Y H:i:s', strtotime($row->data_inicio)) : '' ) }}" autocomplete="off">
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="data_encerramento">Encerramento<b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control datetimepicker" name="data_encerramento" id="data_encerramento" value="{{ old('data_encerramento', isset($row->data_encerramento) && !empty($row->data_encerramento) ? date('d/m/Y H:i:s', strtotime($row->data_encerramento)) : '' ) }}" autocomplete="off">
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="tipo">Edição Profissional</label>
    <div class="col-lg-10">
        <div class="input-group">
            <input {{empty($row->tipo) ? '' :'checked'}} type="checkbox" style="margin-top: 10px;" name="tipo" value="profissional">
        </div>
    </div>
</div>


@include('campos-obrigatorios')