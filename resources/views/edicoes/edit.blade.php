@extends('layouts.panel')

@section('page-title')
    Editar Edição
@endsection

@section('content')
    <form method="post" class="form-horizontal" action="{{ route("{$namespace}.update", [ $row->id ]) }}">
        {!! csrf_field() !!}

        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="row_id" value="{{ $row->id }}">

        @include("{$namespace}.partials.form")

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route("{$namespace}.index") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button class="btn btn-info" type="submit"><i class="fa fa-save"></i> Salvar</button>
            </div>
        </div>
    </form>
@endsection