@extends('layouts.panel')

@section('page-title')
   Relatoria Final
@endsection

@section('content')
    <div class="actions clearfix" style="margin-bottom: 20px;">
           <a target="_blank" href="{{ route("{$namespace}.export") }}" class="btn btn-white"><i class="fa fa-download"></i> XLS</a>
    </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th> Agência   </th>
                    <th> Edição    </th>
                    <th> Número de Categorias</th>
                    <th> Categoria </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($agencias as $agencia): ?>
                <tr>
                    <td style="border-right: solid 1px #e7eaec;" rowspan="<?php echo count($agencia['categorias']) + 1; ?>">
                        <?php echo $agencia['agencia']; ?>
                    </td>
                    <td style="border-right: solid 1px #e7eaec;" rowspan="<?php echo count($agencia['categorias']) + 1; ?>">
                        @if( isset($agencia['pesoTotal']) && $agencia['pesoTotal'] > 0 )
                            <?php echo $agencia['edicao'] . ' - Peso total: '.$agencia['pesoTotal']; ?>
                        @else
                            <?php echo $agencia['edicao']; ?>
                        @endif
                    </td>
                    <td style="text-align: center; border-right: solid 1px #e7eaec;" rowspan="<?php echo count($agencia['categorias']) + 1; ?>">
                        <?php echo count($agencia['categorias']); ?>
                    </td>
                </tr>
                <?php foreach($agencia['categorias'] as $categoria): ?>
                <tr>
                    <td>
                        @if( isset($categoria['peso']) && $categoria['peso'] > 0 )
                            <?php echo $categoria['nome'] . ' - Peso: '.$categoria['peso']; ?>
                        @else
                            <?php echo $categoria['nome']; ?>
                        @endif
                    </td>
                </tr>
                <?php endforeach ?>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
@endsection