<div class="table-responsive">
    <table class="table">
        <thead>
            <th>Ação</th>
            <th>Usuário</th>
            <th>Agência</th>
            <th class="text-center">Nº da Inscrição</th>
            <th>Peça</th>
            <th>Nome do Arquivo</th>
            <th>Data/Hora</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ $row->acao }}</td>
                <td>{{ ! empty($row->user->name) ? $row->user->name : '-' }}</td>
                <td>{{ $row->agencia }}</td>
                <td class="text-center"><a href="{{ route('inscricoes.log', [$row->inscricao_id]) }}">{{ $row->numero_inscricao }}</a></td>
                <td>{{ $row->numero_peca }}</td>
                <td><a target="_blank" href="{{ url_uploads("pecas/{$row->arquivo}") }}">{{ $row->arquivo }}</a></td>
                <td>{{ date('d/m/Y H:i', strtotime($row->created_at)) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
