<div class="modal fade" id="modal-destroy" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-danger">Confirmando exclusão do registro</h3>
            </div>
            <div class="modal-body">
                <h2 class="text-center">Você realmente deseja excluir o registro selecionado?</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Voltar</button>
                <button type="button" class="btn btn-danger confirm-row-destroy" data-loading-text="..."><i class="fa fa-trash"></i> Excluir</button>
            </div>
        </div>
    </div>
</div>