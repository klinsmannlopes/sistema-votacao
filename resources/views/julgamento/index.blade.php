@extends('layouts.panel')

@section('page-title')
    Julgamento - {{ $categoria->nome }}
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('content')

    @include("{$namespace}.partials.errors")
    @include("{$namespace}.partials.modal-confirm-limpar")
    @include("{$namespace}.partials.modal-confirm-finalizar")

    @include("{$namespace}.partials.combobox-categorias")

    @if(count($rows))

        @include("{$namespace}.partials.table")
        @include("{$namespace}.partials.table-top")

    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection