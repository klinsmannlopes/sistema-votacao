<?php if ($julgamentoDesempatado): ?>
    <div class="alert alert-warning text-center" role="alert"><b>Voto do juiz de minerva utilizado para desempate.</b></div>    
<?php endif ?>
@foreach($rows as $row)
    <?php global $notasFinais, $criteriosNotas, $notas; ?>
    <?php $notasFinais = $criteriosNotas = $notas = []; ?>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th class="text-center">Juri</th>
                <th class="text-center">Nº da Inscrição</th>
                <th class="text-center">Campanha</th>
                <th class="text-center">Anunciante</th>
                <th class="text-center">Nota Final</th>
                @foreach($categoria->criterios()->orderBy('nome')->get() as $criterio)
                <th class="text-center">{{ $criterio->nome }}</th>
                @endforeach
            </thead>
            <tbody>

                @foreach($row->juradosQueJulgaram()->get() as $user)
                    @include("{$namespace}.partials.jurado-juiz-row")
                @endforeach

                @foreach($row->juizSolidarioJulgaram()->get() as $user)
                    @include("{$namespace}.partials.jurado-juiz-row")
                @endforeach

                @if( in_array($row->id, $inscricoesDuplicadas))
                    @foreach($row->juizesQueJulgaram()->get() as $user)
                        @include("{$namespace}.partials.jurado-juiz-row")
                    @endforeach
                @endif
                
                <tr style="background-color: #EEE;">
                    <td  class="text-right" colspan="4" style="padding-right: 40px;">MÉDIA</td>
                    <td width="120" class="text-center" style="color: #333; font-weight: bold; border: 3px solid #666">{{ nota_presenter(array_avg($notasFinais)) }}</td>
                    @foreach($criteriosNotas as $criterio_id => $notas)
                    <td class="text-center">{{ nota_presenter(array_avg($notas)) }}</td>
                    @endforeach
                </tr>
            </tbody>
        </table>
    </div>
    <br/>
@endforeach

<script type="text/javascript">

</script>