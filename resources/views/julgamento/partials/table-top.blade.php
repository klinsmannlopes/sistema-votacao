<div class="table-responsive">
    <h1 class="text-center">TOP 5</h1>

    <div align="center" style="margin: 15px 0;">
        @if($julgamentoEmpatado)
        <h3 class="text-danger" style="margin: 20px 0">Categoria necessita de Desempate</h3>
        @else
        <div class="btn-group" role="group">
            <a target="_blank" href="{{ route("{$namespace}.top5", [ 'id' => $categoria->id, 'format' => 'xls' ]) }}" class="btn btn-white"><i class="fa fa-download"></i> XLS</a>
            <a target="_blank" href="{{ route("{$namespace}.top5", [ 'id' => $categoria->id, 'format' => 'csv' ]) }}" class="btn btn-white"><i class="fa fa-download"></i> CSV</a>
        </div>
        @endif
    </div>

    <table class="table">
        <thead>
            <th>&nbsp;</th>
            <th>Agência</th>
            <th>Categoria</th>
            <th>Campanha</th>
            <th>Anunciante</th>
            <th class="text-center">Nº da Inscrição</th>
            <th>Produto/Serviço</th>
            <th class="text-center">Nota Final</th>
        </thead>
        <tbody>
            @foreach($top5 as $top)
            <tr>
                <td>{{ isset($i) ? ++$i : ($i = 1) }}</td>
                <td>{{ $top->user->agency }}</td>
                <td>{{ $top->categoria->nome }}</td>
                <td>{{ $top->anunciante }}</td>
                <td>{{ $top->campanha }}</td>
                <td width="200" class="text-center"><a href="{{ route($categoria->is_fixa() ? 'inscricoes.detalhes' : 'inscricoes.pecas', [ $top->id ]) }}" target="_blank">{{ $top->present()->numeroInscricao() }}</a></td>
                <td>{{ $top->produto_ou_servico }}</td>
                <td class="text-center">{{ nota_presenter($top->mediaFinal()) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    @if($julgamentoEmpatado)
    <div align="center"><span class="text-danger">*</span> No caso de empate, consideraremos a nota(s) do(s) Juíz(es) de Minerva.</div>
    @endif
</div>