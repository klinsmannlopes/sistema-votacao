<?php global $notasFinais, $criteriosNotas, $notas; ?>
<tr>
    <td>{{ $user->name }}</td>
    <td width="200" class="text-center"><a href="{{ route($row->categoria->is_fixa() ? 'inscricoes.detalhes' : 'inscricoes.pecas', [ $row->id ]) }}" target="_blank">{{ $row->present()->numeroInscricao() }}</a></td>
    <td class="text-center">{{ $row->campanha }}</td>
    <td class="text-center">{{ $row->anunciante }}</td>

    <td class="text-center">{{ nota_presenter($notaFinal = array_avg($row->notas($categoria->id, $user->id)->lists('nota')->toArray())) }}</td>
    <?php $notasFinais[] = $notaFinal; ?>

    @foreach($row->notas($categoria->id, $user->id) as $nota) <?php $notas[] = $nota->nota; ?>
    <td class="text-center">{{ $nota->present()->numberFormat() }}</td>
    <?php $criteriosNotas[$nota->criterio_id][] = $nota->nota; ?>
    @endforeach
</tr>