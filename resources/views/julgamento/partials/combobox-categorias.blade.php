<div style="margin: 15px 0;">
    <h2>Veja o Julgamento de outras categorias:</h2>
    <select class="form-control" name="categoria_id" id="categoria_id" data-url="{{ route("{$namespace}.index", ['id']) }}">
        <option value="">Selecione uma categoria</option>
        @foreach($comboboxCategorias as $row)
            <option value="{{ $row->id }}" {{ $categoria->id == $row->id ? 'selected' : '' }}>{{ $row->nome }}</option>
        @endforeach
    </select>
</div>