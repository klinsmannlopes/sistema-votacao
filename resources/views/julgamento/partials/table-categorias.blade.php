<div class="table-responsive">
    <h1 class="text-center">Imprimir o resultado geral</h1>
    <div align="center" style="margin: 15px 0;">
        <div class="btn-group" role="group">
            <a target="_blank" href="{{ route("{$namespace}.resultadogeral", [ 'format' => 'xls' ]) }}" class="btn btn-white"><i class="fa fa-download"></i> XLS</a>
        </div>
    </div>
    <table class="table">
        <thead>
            <th>Categoria</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ $row->nome }}</td>
                <td>
                    @if($row->isTriagemEmAndamento())
                        <h5>Aguarde o término da Triagem</h5>
                        @include('julgamento-triagem.partials.jurados-juizes-nao-triaram')
                    
                    @elseif($row->isJulgamentoRealizado() && $row->isJulgamentoEmpatado() && !$row->isJulgamentoDesempatado())
                        <a href="{{ route("{$namespace}.index", [ $row->id ]) }}">Aguardando Desempate</a>
                    
                    @elseif($row->isJulgamentoRealizado())
                        <a href="{{ route("{$namespace}.index", [ $row->id ]) }}">Julgamento Realizado</a>
                    
                    @elseif($row->isJulgamentoEmAndamento())
                        <h5>Aguarde o término do Julgamento</h5>
                        @include("{$namespace}.partials.jurados-juizes-nao-julgaram")
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
