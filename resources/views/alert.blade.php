<div class="modal fade" id="alert" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h3 class="modal-title text-danger">Atenção</h3>
            </div>
            <div class="modal-body">
                <h2 class="msg"></h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-close text-danger"></i> Fechar</button>
            </div>
        </div>
    </div>
</div>