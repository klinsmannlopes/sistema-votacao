<form id="login" method="POST" class="m-t" action="{{ route('postLogin') }}">

    {!! csrf_field() !!}

    @include('remove-autofill')

    <div class="form-group">
        <input type="text" name="email" id="email" value="{{ old('email') }}" class="form-control" placeholder="Login ou E-mail" required autocomplete="off">
    </div>

    <div class="form-group">
        <input type="password" name="password" id="password" class="form-control" placeholder="Senha" required autocomplete="off">
    </div>

    <div class="form-group">
        {!! Form::select('edicao', $edicoes, null, ['placeholder' => ' -- Edição --','class'=>'form-control']) !!}
    </div>

    @if(!empty(\Session::get('retriesLeft')) && \Session::get('retriesLeft') <= 2)
    <div class="form-group">
        {!! recaptcha() !!}
    </div>
    @endif

    <br/>
    <button type="submit" class="btn btn-primary block full-width m-b">Entrar</button>

    <a href="{{ route('getEmail') }}"><small>Problema com senha?</small></a>
</form>