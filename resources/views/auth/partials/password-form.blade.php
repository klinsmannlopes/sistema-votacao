<form class="m-t" method="POST" action="{{ route('postEmail') }}">

    {!! csrf_field() !!}

    @include('remove-autofill')

    <div class="form-group">
        <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control" placeholder="Informe seu e-mail" required autocomplete="off">
        <small>Digite seu login e clique em "Recuperar senha"</small>
    </div>

    <div style="margin: 10px 0 15px 0"><a href="{{ route('getLogin') }}">Lembrei da minha senha</a></div>

    <button type="submit" class="btn btn-primary block full-width m-b">Recuperar senha</button>
</form>