<form class="m-t" method="POST" action="{{ route('postReset') }}">

    @include('remove-autofill')

    {!! csrf_field() !!}
    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group">
        <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control" placeholder="E-mail" required autocomplete="off">
    </div>

    <div class="form-group">
        <input type="password" name="password" id="password" class="form-control" placeholder="Nova senha" required autocomplete="off">
        <small><b class="text-danger">*</b> Você deve escolher uma senha com o mínimo de 8 caracteres, entre maiúsculas e minúsculas, números e caracteres especiais.</small>
    </div>

    <div class="form-group">
        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirme sua nova senha" required autocomplete="off">
    </div>

    <br/>
    <button type="submit" class="btn btn-primary block full-width m-b">Resetar senha</button>
</form>