@if(!empty($status = \Session::get('status')))
<div class="row messages">
    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
        <div class="alert alert-success text-center">{{ $status }}</div>
    </div>
</div>
@endif