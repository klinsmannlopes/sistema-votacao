@extends('layouts.auth')

@section('page-title')
    Recuperar Senha
@endsection

@section('content')
    @include('auth.partials.password-form')
@endsection