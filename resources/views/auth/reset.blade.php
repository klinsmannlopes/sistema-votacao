@extends('layouts.auth')

@section('page-title')
    Resetar Senha
@endsection

@section('content')
    @include('auth.partials.reset-form')
@endsection