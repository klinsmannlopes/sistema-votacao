@extends('layouts.auth')

@section('page-title')
    Login
@endsection

@section('content')
    @include('auth.partials.login-form')
@endsection