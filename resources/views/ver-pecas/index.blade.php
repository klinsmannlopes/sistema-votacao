@extends('layouts.panel')

@section('page-title')
    Peças da Inscrição
@endsection

@section('content')

    <script src="{{ url('assets/vendor/hdflvplayer/swfobject.js') }}"></script>

    <h1>Nº da Inscrição: <b>{{ $inscricao->present()->numeroInscricao() }}</b></h1>
    <h1 style="margin-bottom: 30px;">Campanha: <b>{{ $inscricao->campanha }}</b></h1>

    <div class="pecas">
        @foreach($inscricao->pecas as $peca) <?php $pathInfo = pathinfo($peca->arquivo); ?>
            <div class="well" style="background-color: #FFF;">
                <div>
                    <h2 style="margin-bottom: 20px;">{{ $peca->titulo }}</h2>
                    <ul class="list-group" style="background-color: #FFF">
                        <li class="list-group-item"><b>Tipo:</b> {{ $peca->tipo }}</li>
                        <li class="list-group-item"><b>Veículos:</b> {{ $peca->veiculo }}</li>
                        <li class="list-group-item"><b>Formato:</b> {{ $peca->formato }}</li>
                        @if($peca->formato_diferenciado)
                            <li class="list-group-item"><b>Formato Diferenciado:</b> {{ $peca->formato_diferenciado }}</li>
                        @endif
                    </ul>
                </div>

                <div>
                    <h3>Peça</h3>
                    
                    @if( strtolower( $pathInfo['extension'] ) == 'mp3')
                        <div>
                            <audio controls>
                                <source src="{{ url_uploads("pecas/{$peca->arquivo}") }}">
                            </audio>
                            <div><a href="{{ url_uploads("pecas/{$peca->arquivo}") }}" target="_blank">Baixar arquivo</a></div>

                            @if(! empty($peca->descricao_audio))
                            <br/>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <h4>Descrição do áudio</h4>
                                        <textarea class="form-control" rows="10"  disabled>{{ $peca->descricao_audio }}</textarea>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    @elseif ( strtolower( $pathInfo['extension'] ) == 'flv')

                        <script type="text/javascript">
                            swfobject.registerObject("myId", "9.0.0", "expressInstall.swf");
                        </script>

                        <object id="myId">
                            <param name="movie" />
                            <!--[if !IE]>-->
                            <object type="application/x-shockwave-flash" width="0" height="0">
                                <!--<![endif]-->
                                <div>
                                    <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
                                </div>
                                <!--[if !IE]>-->
                            </object>
                            <!--<![endif]-->
                        </object>

                        <div id="flv_peca_{{ $peca->id }}" >
                            <script type="text/javascript">
                                var s1 = new SWFObject('{{ url('assets/vendor/hdflvplayer/hdplayer.swf') }}', 'player', '600','338','9',"#333333");
                                s1.addParam('allowfullscreen','true');
                                s1.addParam('allowscriptaccess','always');
                                s1.addParam('WMode','direct');
                                s1.addVariable('file', '{{ url_uploads("pecas/{$peca->arquivo}") }}');
                                s1.addVariable('autoplay','false');
                                s1.write('flv_peca_{{ $peca->id }}');
                            </script>
                        </div>
                    @elseif ( strtolower( $pathInfo['extension'] ) == 'pdf' )
                        <div><embed src="{{ url_uploads("pecas/{$peca->arquivo}") }}" width="100%" height="600" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html"></div>
                    @elseif ( strtolower( $pathInfo['extension'] ) == 'swf' )
                        <object class="peca_swf" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="100%" height="600">
                            <param name="movie" value="{{ url_uploads("pecas/{$peca->arquivo}") }}" />
                            <param name="quality" value="high" />
                            <param NAME="SCALE" VALUE="noScale">
                            <param src="{{ url_uploads("pecas/{$peca->arquivo}") }}" quality="high" type="application/x-shockwave-flash" SCALE="noScale" pluginspage="http://www.macromedia.com/go/getflashplayer" />
                        </object>
                    @elseif ( strtolower( $pathInfo['extension'] ) == 'gif')
                        <div><img height="auto" src="{{ url_uploads("pecas/{$peca->arquivo}") }}" alt="" /></div>
                    @else
                        <div><a href="{{ url_uploads("pecas/{$peca->arquivo}") }}">{{ url_uploads("pecas/{$peca->arquivo}") }}</a></div>
                    @endif
                    <p><strong><a href='{{ url_uploads("pecas/{$peca->arquivo}") }}' target="_blank">Clique aqui caso a peça não esteja funcionando</a></strong></p>
                </div>
            </div>
        @endforeach
    </div>

@endsection