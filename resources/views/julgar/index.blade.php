@extends('layouts.panel')

@section('page-title')
    Julgar - {{ $categoria->nome }}
@endsection

@section('scripts')
    <script>
        NOTA_MINIMA = {{ (float)str_replace(',', '.', $notaMinima) }};
        NOTA_MAXIMA = {{ (float)str_replace(',', '.', $notaMaxima) }};
    </script>

    @include("{$namespace}.partials.scripts")
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('content')

    @include("{$namespace}.partials.errors")
    @include("{$namespace}.partials.modal-confirm-limpar")
    @include("{$namespace}.partials.modal-confirm-finalizar")

    @if(count($rows))

        <h2><i>“Visualize as inscrições através do link e informe sua nota para cada critério (<b>{{ $notaMinima }}</b> a <b>{{ $notaMaxima }}</b>). É possível utilizar duas casas decimais. Se você errar e quiser limpar todas as notas para começar de novo, basta clicar no botão ‘Limpar’, ao final da tabela. Ao terminar, clique no botão ‘Salvar’, também ao final da tabela.”</i></h2>
        <form id="finalizar" method="post" action="{{ route("{$namespace}.finalizar", [ $categoria->id ]) }}">

            {!! csrf_field() !!}

            @include("{$namespace}.partials.table")

        </form>

        <div class="text-center">
            <a class="btn btn-white" href="{{ route("{$namespace}.categorias") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
            <button type="button" id="limpar" class="btn btn-danger"><i class="fa fa-eraser"></i> Limpar</button>
            <button type="button" id="salvar" class="btn btn-info"><i class="fa fa-save"></i> Salvar</button>
        </div>
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection