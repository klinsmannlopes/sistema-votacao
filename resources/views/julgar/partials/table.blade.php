<div class="table-responsive">
    <br/>
    <table class="table">
        <thead>
            <th>&nbsp;</th>
            <th class="text-center">Nº da Inscrição</th>
            <th>Campanha</th>
            <th>Anunciante</th>

            @foreach($categoria->criterios as $criterio)
                <th class="text-center">{{ $criterio->nome }}</th>
            @endforeach
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ isset($i) ? ++$i : ($i = 1) }}</td>
                <td width="200" class="text-center"><a href="{{ route($row->categoria->is_fixa() ? 'inscricoes.detalhes' : 'inscricoes.pecas', [ $row->id ]) }}" target="_blank">{{ $row->present()->numeroInscricao() }}</a></td>
                <td>{{ $row->campanha }}</td>
                <td>{{ $row->anunciante }}</td>
                @foreach($categoria->criterios as $criterio)
                    <td style="vertical-align: middle">
                        <div class="form-group {{ ! is_between(old("avaliacoes.{$row->id}.{$criterio->id}"), $notaMinima, $notaMaxima) ? 'has-error' : '' }}" style="margin-top: 15px;">
                            <input type="text" class="form-control number" name="avaliacoes[{{ $row->id }}][{{ $criterio->id }}]" value="{{ old("avaliacoes.{$row->id}.{$criterio->id}") }}">
                        </div>
                    </td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div>