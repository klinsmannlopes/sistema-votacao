<div class="table-responsive">
    <table class="table">
        <thead>
            <th>Categoria</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ $row->nome }}</td>
                <td>
                    @if($row->isTriagemEmAndamento() )
                        <h5>Aguarde o término da Triagem</h5>

                        @include("julgamento-triagem.partials.jurados-juizes-nao-triaram")
                    @else

                        @if($row->isAguardandoDesempate())
                            Aguardando Desempate da Triagem
                        @else

                            @if($row->isJulgamentoRealizado() && $row->isJulgamentoEmpatado() && !$row->isJulgamentoDesempatado())
                                Aguardando Desempate
                            @else

                                @if($row->isJulgamentoRealizado())
                                    Julgamento Realizado
                                @else

                                    @if($row->julgamentos()->where('user_id', \Auth::getUser()->id)->count())
                                        <p>Julgamento em andamento</p>

                                        <ol>
                                            @if($row->abreviatura === "CAMPA")
                                                @foreach($row->juizSolidarioQueNaoJulgaram() as $juiz_solidario)
                                                    <li>{{$juiz_solidario->name}}</li>
                                                @endforeach
                                            @else
                                                @foreach($row->juradosQueNaoJulgaram() as $jurado)
                                                    <li>{{ $jurado->name }}</li>
                                                @endforeach
                                            @endif
                                        </ol>
                                    @else
                                        <a href="{{ route("{$namespace}.index", [ $row->id ]) }}">Realizar Julgamento</a>
                                    @endif

                                @endif
                                
                            @endif

                        @endif

                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
