<div class="modal fade" id="modal-confirm-finalizar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-danger">Confirmação que deseja finalizar o Julgamento</h3>
            </div>
            <div class="modal-body">
                <h1 class="text-center">Tem certeza que deseja finalizar o Julgamento?</h1>
                <h2 class="text-center text-danger">Depois de salvar, o Julgamento não pode ser alterado.</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Não</button>
                <button id="confirm-finalizar" type="button" class="btn btn-info"><i class="fa fa-check"></i> Sim</button>
            </div>
        </div>
    </div>
</div>