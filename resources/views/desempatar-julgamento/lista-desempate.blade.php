@extends('layouts.panel')

@section('page-title')
    Desempate Realizado "{{ $categoria->nome }}"
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

    @include("{$namespace}.partials.combobox-categorias")

    @if(count($rows))

        @include("{$namespace}.partials.table-lista-desempate")

        <div align="center" style="margin: 20px 0;">
            <h2>Download</h2>
            <div class="btn-group" role="group">
                <a target="_blank" href="{{ route("julgamento.top5", [ 'id' => $categoria->id, 'format' => 'xls' ]) }}" class="btn btn-white"><i class="fa fa-download"></i> XLS</a>
                <a target="_blank" href="{{ route("julgamento.top5", [ 'id' => $categoria->id, 'format' => 'csv' ]) }}" class="btn btn-white"><i class="fa fa-download"></i> CSV</a>
            </div>
        </div>

        <div class="text-center">
            <a href="{{ route("{$namespace}.categorias") }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Voltar Listagem de Categorias</a>
        </div>
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection