@extends('layouts.panel')

@section('page-title')
    Confirmar Desempatar Triagem "{{ $categoria->nome }}"
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

    @include("{$namespace}.partials.errors")
    @include("{$namespace}.partials.modal-confirm-desempatar")

    @if(count($rows))

        <h1><i>"Visualize as inscrições através do link e defina a ordenação entre elas. Se você errar e quiser limpar sua ordenação para começar de novo, basta clicar no botão ‘Limpar’, ao final da tabela. Ao terminar, clique no botão ‘Salvar’, também ao final da tabela."</i></h1>
        <h3 class="text-danger text-center">Importante: Após salvar o Desempate, não é possível alterá-lo.</h3>

        <form id="desempatar" method="post" action="{{ route("{$namespace}.ordenar", [ $categoria->id ]) }}">

            {!! csrf_field() !!}

            @include("{$namespace}.partials.table-confirm-ordenacao")
        </form>

        <div class="text-center">
            <a href="{{ route("{$namespace}.index", [$categoria->id]) }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Voltar para reordenar</a>
            <button id="show-modal-confirm-desempatar" class="btn btn-info" type="button"><i class="fa fa-check"></i> Confirmar ordenação</button>
        </div>
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection