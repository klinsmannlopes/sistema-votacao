@extends('layouts.panel')

@section('page-title')
    @if($authUser->is_juiz())
    Desempatar - {{ $categoria->nome }}
    @else
        Aguardando Desempate - {{ $categoria->nome }}
    @endif
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

    @include("{$namespace}.partials.errors")
    @include("{$namespace}.partials.modal-confirm-limpar")

    @include("{$namespace}.partials.combobox-categorias")

    @if(count($rows))

        @if($authUser->is_juiz())
        <h1><i>"Visualize as inscrições através do link e defina a ordenação entre elas. Se você errar e quiser limpar sua ordenação para começar de novo, basta clicar no botão ‘Limpar’, ao final da tabela. Ao terminar, clique no botão ‘Salvar’, também ao final da tabela."</i></h1>
        <h3 class="text-danger text-center">Importante: Após salvar o Desempate, não é possível alterá-lo.</h3>
        @endif

        @if($authUser->is_juiz())
        <form method="post" action="{{ route("{$namespace}.confirm-ordenacao", [ $categoria->id ]) }}">

            {!! csrf_field() !!}

            @include("{$namespace}.partials.table")

            <div class="text-center">
                <a href="{{ route("{$namespace}.categorias") }}" class="btn btn-white"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button type="button" id="limpar-ordenacao" class="btn btn-danger"><i class="fa fa-eraser"></i> Limpar</button>
                <button class="btn btn-info"><i class="fa fa-sort"></i> Ordenar</button>
            </div>
        </form>
        @else
            @include("{$namespace}.partials.table")
        @endif
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection