<div class="table-responsive">
    <br/>
    <table class="table">
        <thead>
            <th>&nbsp;</th>
            <th class="text-center">Nº da Inscrição</th>
            <th>Campanha</th>
            <th>Anunciante</th>
            <th class="text-center">Nota Juiz Minerva</th>
            <th class="text-center">Ordem</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ isset($i) ? ++$i : ($i = 1) }}</td>
                <td width="200" class="text-center"><a href="{{ route('inscricoes.pecas', [ $row->id ]) }}" target="_blank">{{ $row->present()->numeroInscricao() }}</a></td>
                <td>{{ $row->campanha }}</td>
                <td>{{ $row->anunciante }}</td>
                <td class="text-center">{{ nota_presenter($media = $row->mediaFinal()) }}</td>
                <td class="text-center">
                    @if($authUser->is_juiz())
                        @if(($index = found_combobox_ordenacao($comboboxOrdenacao, $i)) !== false)
                            @include("{$namespace}.partials.combobox-ordenacao", compact($index))
                        @else
                            {{ $i }} <input type="hidden" name="ordenacao[{{ $row->id }}]" value="{{ $i }}">
                        @endif
                    @else
                        -
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>