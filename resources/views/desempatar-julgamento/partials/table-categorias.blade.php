<div class="table-responsive">
    <table class="table">
        <thead>
            <th>Categoria</th>
            <th>Status</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ $row->nome }}</td>
                <td>
                    @if($row->isTriagemEmAndamento())
                        <p>Categoria na fase de Triagem</p>
                        @include('julgamento-triagem.partials.jurados-juizes-nao-triaram')
                    @elseif($row->isJulgamentoEmAndamento())
                        <p>Aguarde o término do Julgamento</p>
                        @include('julgamento.partials.jurados-juizes-nao-julgaram')
                    @elseif($row->isJulgamentoRealizado())
                        @if($row->isJulgamentoEmpatado())
                            @if($row->isJulgamentoDesempatado())
                                <p>Desempate realizado - <a href="{{ route("{$namespace}.lista-desempate", [ $row->id ]) }}">ver lista</a></p>
                            @else
                            <a href="{{ route('desempatar-julgamento.index', [ $row->id ]) }}">
                                @if($authUser->is_juiz())
                                Realizar Desempate
                                @else
                                Aguardando Desempate
                                @endif
                            </a>
                            @endif
                        @else
                        <p>Não foi necessário Desempate</p>
                        @endif
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
