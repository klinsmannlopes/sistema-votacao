<div style="margin: 15px 0;">
    <h2>Veja o Desempate de outras categorias:</h2>

    <select class="form-control" name="categoria_id" id="categoria_id" data-url-lista="{{ route("{$namespace}.lista-desempate", ['id']) }}" data-url-desempate="{{ route("{$namespace}.index", ['id'])  }}">
        <option value="">Selecione uma categoria</option>

        @foreach($comboboxCategorias as $row)
        <option
                @if(($relizado = $row->isJulgamentoRealizado()) && ($empatado = $row->isJulgamentoEmpatado()) && ! ($desempatado = $row->isJulgamentoDesempatado()))
                data-attr-url="data-url-desempate"
                @elseif($relizado && $empatado && $desempatado)
                data-attr-url="data-url-lista"
                @endif

                value="{{ $row->id }}"
                {{ $categoria->id == $row->id ? 'selected' : '' }}>{{ $row->nome }}</option>
        @endforeach
    </select>
</div>