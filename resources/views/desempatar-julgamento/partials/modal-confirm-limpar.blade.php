<div class="modal fade" id="modal-confirm-limpar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-danger">Confirmação que deseja limpar sua ordenação</h3>
            </div>
            <div class="modal-body">
                <h2 class="text-center">Tem certeza que deseja limpar sua ordenação?</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Não</button>
                <button id="confirm-limpar-ordenacao" type="button" class="btn btn-info"><i class="fa fa-check"></i> Sim</button>
            </div>
        </div>
    </div>
</div>