<div class="table-responsive">
    <br/>
    <table class="table">
        <thead>
            <th>&nbsp;</th>
            <th class="text-center">Nº da Inscrição</th>
            <th>Campanha</th>
            <th>Anunciante</th>
            <th class="text-center">Nota Juiz Minerva</th>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr id="{{ isset($i) ? ++$i : ($i = 1) }}" style="{{ $i > 5 ? 'background-color: #EFEFEF; color: #BBB; text-decoration: line-through;' : '' }}">
                <td>{{ $i }}<input type="hidden" name="ordernacao[{{ $row->id }}]" value="{{ $i }}"></td>
                <td width="200" class="text-center"><a href="{{ route('inscricoes.show', [ $row->id ]) }}" target="_blank">{{ $row->present()->numeroInscricao() }}</a></td>
                <td>{{ $row->campanha }}</td>
                <td>{{ $row->anunciante }}</td>
                <td class="text-center">{{ nota_presenter($row->mediaFinal()) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>