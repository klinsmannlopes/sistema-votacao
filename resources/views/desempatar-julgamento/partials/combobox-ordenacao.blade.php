<select name="ordenacao[{{ $row->id }}]" class="form-control text-center ordenacao" style="width: 70px; margin: 0 auto;">
    <option value="">-</option>
    @foreach($comboboxOrdenacao[$index] as $value)
    <option value="{{ $value }}" {{ old("ordenacao.{$row->id}") == $value ? 'selected' : '' }}>{{ $value }}</option>
    @endforeach
</select>