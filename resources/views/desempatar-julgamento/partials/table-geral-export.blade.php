<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<table class="table">
    <tr>
        <th align="center">Categoria</th>
        <th align="center">Ag�ncia</th>
        <th align="center">Campanha</th>
        <th align="center">Anunciante</th>
        <th align="center">N� da Inscri��o</th>
        <th align="center">Produto/Servi�o</th>
        <th align="center">Nota Final</th> teste
    </tr>

    @foreach($rows as $categoria => $row)
        <tr>
            <td valign="middle" rowspan="{{count($row) + 1}}">{{$categoria}}</td>
        </tr>
        @foreach($row as $r)
            <tr>
                <td></td>
                <td align="center">{{ $r->user->agency }}</td>
                <td align="center">{{ $r->campanha }}</td>
                <td align="center">{{ $r->anunciante }}</td>
                <td align="center">{{ $r->present()->numeroInscricao() }}</td>
                <td align="center">{{ $r->produto_ou_servico }}</td>
                <td align="center">{{ nota_presenter($r->mediaFinal()) }}</td>
            </tr>
        @endforeach
    @endforeach
</table>
</body>
</html>
