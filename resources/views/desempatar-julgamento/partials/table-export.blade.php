<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<table>
    <tr>
        <th align="center">Agência</th>
        <th align="center">Categoria</th>
        <th align="center">Campanha</th>
        <th align="center">Anunciante</th>
        <th align="center">Nº da Inscrição</th>
        <th align="center">Produto/Serviço</th>
        <th align="center">Nota Final</th>
    </tr>

    @foreach($rows as $row)
        <tr>
            <td align="center">{{ $row->user->agency }}</td>
            <td align="center">{{ $row->categoria->nome }}</td>
            <td align="center">{{ $row->anunciante }}</td>
            <td align="center">{{ $row->campanha }}</td>
            <td align="center">{{ $row->present()->numeroInscricao() }}</td>
            <td align="center">{{ $row->produto_ou_servico }}</td>
            <td align="center">{{ nota_presenter($row->mediaFinal()) }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>