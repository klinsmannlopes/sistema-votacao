<div class="modal fade" id="confirm" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close no"><i class="fa fa-close"></i></button>
                <h3 class="modal-title">Confirmação</h3>
            </div>
            <div class="modal-body">
                <h2 class="msg"></h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger no">Não</button>
                <button type="button" class="btn btn-info yes">Sim</button>
            </div>
        </div>
    </div>
</div>