@extends('layouts.panel')

@section('page-title')
    Parametrização - Julgamento
@endsection

@section('content')
    @include('errors')

    <form method="post" action="{{ route("{$namespace}.save") }}" class="form-horizontal">

        {!! csrf_field() !!}

        <div class="form-group">
            <label class="col-lg-2 control-label" for="nota_minima">Nota mínima <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control number" name="nota_minima" id="nota_minima" value="{{ old('nota_minima', $notaMinima) }}" autocomplete="off">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="nota_maxima">Nota máxima <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control number" name="nota_maxima" id="nota_maxima" value="{{ old('nota_maxima', $notaMaxima) }}" autocomplete="off">
            </div>
        </div>

        @include('campos-obrigatorios')

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route('dashboard') }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button class="btn btn-info" type="submit"><i class="fa fa-save"></i> Salvar</button>
            </div>
        </div>
    </form>
@endsection