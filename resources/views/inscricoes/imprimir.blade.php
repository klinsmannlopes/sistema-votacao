<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ url('assets/vendor/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/vendor/css/font-awesome.min.css') }}">

    <style>
        .hidden-print:not(.btn-print) { display: none; }
        table { font-size: 12px; }
        tr { page-break-inside: avoid; }

        .page {
            width: 29.7cm;
            min-height: 21cm;
            margin: 0 auto;
        }

    </style>

    <script>
        window.print();
    </script>
</head>

<body>
    <div class="page">
        <div class="text-center hidden-print btn-print" style="padding: 10px 0;">
            <button type="button" class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Imprimir</button>
        </div>
        @include("{$namespace}.partials.table")
    </div>
</body>
</html>
