@extends('layouts.panel')

@section('page-title')
    Detalhes
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('scripts')
    @include("inscricoes.partials.scripts")
@endsection

@section('content')

<div class="form-horizontal">

    @include("{$namespace}.partials.form")

    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10">
            <a class="btn btn-white" href="{{ URL::previous() === Request::url() ? route('inscricoes.index') : URL::previous() }}"><i class="fa fa-arrow-left"></i> Voltar</a>
        </div>
    </div>
</div>

@endsection