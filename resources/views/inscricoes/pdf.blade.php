<link rel="stylesheet" href="{{ url('assets/vendor/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/vendor/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/modules/css/inscricao.css') }}">

<html>
    <head>
        <style>
            .texts { border: 1px solid #DDD; padding: 10px; }

            legend,
            div.form-group,
            .pecas,
            .categoria-midia,
            .categoria-planejamento,
            .categoria-nao-fixa { page-break-inside: avoid; }

            hr { display: none; }
            h4 { padding-bottom: 10px; border-bottom: 1px solid #DDD; }
        </style>
        <script src="{{ url('assets/layouts/insipia/js/jquery-2.1.1.js') }}"></script>
        <script>
            jQuery(function()
            {
                $("textarea, input[type=text], input[type=email]").each(function ()
                {
                    var value = $(this).val();

                    $(this).after("<div class='texts'>" + value + "</div>");
                    $(this).remove();
                });

                $('legend').each(function()
                {
                    var html = $(this).html();
                    $(this).after('<h4>'+ html +'</h4>');
                    $(this).remove();
                });
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="form-horizontal">
                <div class="text-center hidden-print btn-print" style="padding: 10px 0;">
                    <button type="button" class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Imprimir</button>
                </div>
                @include("{$namespace}.partials.form", ['noPrint' => true])
            </div>
        </div>
    </body>
</html>

