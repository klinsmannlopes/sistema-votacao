@extends('layouts.panel')

@section('page-title')
    Cadastro realizado com sucesso!
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('content')
    <h2 class="text-center">Cadastro realizado com sucesso!</h2>
    <h2 class="text-center">Nº da Inscrição: <b>{{ $row->present()->numeroInscricao() }}</b></h2>
    <h1 class="text-center">O que você deseja fazer agora?</h1>
    <hr>
    <div class="row">
        <div class="col-xs-12 text-center">
            <a href="{{ route("{$namespace}.index") }}" class="btn btn-lg btn-white"><i class="fa fa-chevron-circle-left"></i> Ver minhas Inscrições</a>
            &nbsp;
            &nbsp;
            &nbsp;
            <a href="{{ route("{$namespace}.create") }}" class="btn btn-lg btn-white"><i class="fa fa-plus text-info"></i> Cadastrar outra Inscrição</a>
        </div>
    </div>
@endsection