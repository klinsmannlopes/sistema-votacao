@extends('layouts.panel')

@section('page-title')
    Listagem das inscrições
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('filtros')
    @include("{$namespace}.partials.filtros")
@endsection

@section('content')

    @include('modal-confirm-destroy')
    @include("{$namespace}.partials.modal-associar-opec")
    @include("{$namespace}.partials.modal-confirm-mass-destroy")

    @if(empty($periodoDeInscricao))
        <div class="alert alert-warning text-center" role="alert"><b>{{ \Lang::get("{$namespace}.encerradas") }}</b></div>
    @endif

    @if(count($rows))

        @if(Gate::check($namespace))
        <div class="actions clearfix">
            <form method="post" class="pull-left" action="{{ route("{$namespace}.associar-opec", [ 'ids', 'user_id' ]) }}" id="associar-opec-selecionados">
                &nbsp;
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="PUT">
                <button type="button" class="btn btn-info show-modal-associar-opec"><i class="fa fa-check-square-o"></i> Enviar para OPEC</button>
            </form>
            <form method="post" class="pull-left" action="{{ route("{$namespace}.destroy", [ 'ids' ]) }}" id="remover-selecionados">
                &nbsp;
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="delete">
                <button type="button" class="btn btn-danger show-modal-cofirm-mass-destroy"><i class="fa fa-check-square-o"></i> Excluir selecionados</button>
            </form>
        </div>

        <hr style="margin-top: 10px;">
        @endif

        @include("{$namespace}.partials.table")

        <div class="text-center">
            {!! $rows->appends(compact('search', 'status', 'categoria_id'))->render() !!}
        </div>
    @else
        <h2>
            Nenhum registro foi encontrado.

            @if(Gate::check("{$namespace}-participante") && $periodoDeInscricao)
            <br/> Clique <a href="{{ route("{$namespace}.create") }}">aqui</a> para adicionar uma nova inscrição.
            @endif
        </h2>
    @endif

@endsection