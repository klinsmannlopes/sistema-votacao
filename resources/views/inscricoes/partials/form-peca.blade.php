<div class="panel peca numeroPeca {{ !empty($peca) && is_object($peca) ? ($peca->is_rejeitada() ? 'rejeitada panel-danger' : 'nao-rejeitada panel-default') : 'panel-default' }}" data-index="{{ $index }}">

    <?php $disabled = !empty($readonly) || (!empty($peca) && is_object($peca) && !$peca->is_rejeitada() && $pendenteDeCorrecao) ? 'disabled' : ''; ?>

    @if(!empty($peca->tipo))
    <input type="hidden" name="pecas[{{ $index }}][id]" value="{{ $peca->id }}">
    @endif

    <div class="panel-heading"><b>Peça {{ $index }}</b> @if(empty($readonly))<button type="button" class="btn btn-xs btn-danger remove-peca pull-right"><i class="fa fa-close"></i></button>@endif</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-lg-2 control-label" for="tipo_{{ $index }}">Tipo <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <select class="form-control tipo selectTipo" name="pecas[{{ $index }}][tipo]" id="tipo_{{ $index }}" {{ $disabled }}>
                    <option value="">Selecione uma opção</option>
                    @foreach($tiposVeiculos as $value => $label)
                        <option value="{{ $value }}" {{ old("pecas.{$index}.tipo", !empty($peca->tipo) ? $peca->tipo : '') == $value ? 'selected' : '' }}>{{ $label  }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="titulo_{{ $index }}">Título <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="pecas[{{ $index }}][titulo]" id="titulo_{{ $index }}" value="{{ old("pecas.{$index}.titulo", !empty($peca->titulo) && $peca->titulo ? $peca->titulo : '') }}" {{ $disabled }}>
            </div>
        </div>
        <div class="form-group inputs-categoria-fixa">
            <label class="col-lg-2 control-label" for="veiculo_{{ $index }}">Veículos <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <select class="form-control veiculo" name="pecas[{{ $index }}][veiculo]" id="veiculo_{{ $index }}" {{ $disabled }}>
                    <option value="">Selecione uma opção</option>
                    @foreach($veiculosPorTipo as $tipo => $veiculos)
                        @foreach($veiculos as $veiculo)
                            <option value="{{ $veiculo }}" data-tipo="{{ $tipo }}" {{ old("pecas.{$index}.veiculo", !empty($peca->veiculo) ? $peca->veiculo : '') == $veiculo ? 'selected' : '' }}>{{ $veiculo }}</option>
                        @endforeach
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="formato_{{ $index }}">Formato <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <select class="form-control formato" name="pecas[{{ $index }}][formato]" id="formato_{{ $index }}" {{ $disabled }}>
                    <option value="">Selecione uma opção</option>
                    @foreach($veiculosFormatos as $tipo => $formatos)
                        @foreach($formatos as $formato)
                            <option value="{{ $formato }}" data-tipo="{{ $tipo }}" {{ old("pecas.{$index}.formato", !empty($peca->formato) ? $peca->formato : '') == $formato  ? 'selected' : '' }}>{{ $formato }}</option>
                        @endforeach
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group wrapper-formato-diferenciado hidden">
            <label class="col-lg-2 control-label" for="formato_diferenciado_{{ $index }}">Formato Diferenciado<b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="pecas[{{ $index }}][formato_diferenciado]" id="formato_diferenciado_{{ $index }}" value="{{ old("pecas.{$index}.formato_diferenciado", !empty($peca->formato_diferenciado) && $peca->formato_diferenciado ? $peca->formato_diferenciado : '') }}" {{ $disabled }}>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="produtora_{{ $index }}">Produtora </label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="pecas[{{ $index }}][produtora]" id="produtora_{{ $index }}" value="{{ old("pecas.{$index}.produtora", !empty($peca->produtora) && $peca->produtora ? $peca->produtora : '') }}" {{ $disabled }}>
                <b class="text-danger">Campo não obrigatório para peças do tipo Jornal</b>
            </div>
        </div>

        @if(empty($viewPdf))
            <div class="form-group">
                <label class="col-lg-2 control-label" for="arquivo_{{ $index }}">Peça <b class="text-danger">*</b></label>
                <div class="col-lg-10">
                    @if(empty($readonly) && !(!empty($peca) && is_object($peca) && !$peca->is_rejeitada() && $pendenteDeCorrecao))
                    <input type="file" class="form-control peca" name="pecas[{{ $index }}][arquivo]" id="arquivo_{{ $index }}">
                    <small>Arquivo com no máximo 20mb de acordo com o tipo da mídia selecionada</small>
                    <div class="progress hidden">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemax="100"></div>
                    </div>
                    @endif

                    <div class="uploaded">
                        @if($arquivo_old = old("pecas.{$index}.arquivo-old", (!empty($peca->arquivo) ? $peca->arquivo : '')))
                        <a class="btn btn-white" target="_blank" href="{{ url_uploads("pecas/{$arquivo_old}") }}"><i class="fa fa-file"></i> <b>Arquivo atual:</b> Clique aqui para visualizá-lo.</a></small>
                        <input type="hidden" name="pecas[{{ $index }}][arquivo-old]" value="{{ $arquivo_old }}">
                        @endif
                    </div>
                </div>
            </div>

            @if(!Gate::check("{$namespace}-participante") && !empty($peca) && is_object($peca) && empty($viewPdf))
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="rejeita_peca_{{ $index }}">Rejeitar Peça?</label>
                    <div class="col-xs-10">
                        <label>
                            <small>
                                <input type="checkbox" class="rejeitar_peca" name="pecas[{{ $index }}][rejeitar-peca]" id="rejeita_peca_{{ $index }}" value="{{ $peca->id }}" {{ $peca->peca_rejeitada ? 'checked' : '' }}>&nbsp;&nbsp;Selecione caso deseje rejeitar essa peça
                            </small>
                        </label>
                    </div>
                </div>
            @endif
        @endif

        <div class="form-group wrapper-descricao_audio hidden">
            <label class="col-lg-2 control-label" for="descricao_audio_{{ $index }}">Descrição de áudio</label>
            <div class="col-lg-10">
                <textarea class="form-control" name="pecas[{{ $index }}][descricao_audio]" id="descricao_audio_{{ $index }}" {{ $disabled }}>{{ old("pecas.{$index}.descricao_audio", !empty($peca->descricao_audio) && $peca->descricao_audio ? $peca->descricao_audio : '') }}</textarea>
                <b class="text-danger">Obrigatório para peças do tipo Rádio</b>
            </div>
        </div>

        @if(empty($viewPdf))
            @include('inscricoes.partials.form-pis-peca', [
                'namespace' =>$namespace,
                'peca'      =>old("pecas.{$index}", isset($peca) ? $peca : null),
                'readonly'  =>isset($readonly) ? $readonly : null,
                'viewPdf'   =>isset($viewPdf) ? $viewPdf : null,
                'peca_index'=>$index,
            ])
        @endif
    </div>
</div>