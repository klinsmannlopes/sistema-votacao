<div class="modal fade" id="modal-associar-opec" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Selecione o usuário OPEC ao qual as inscrições serão associadas</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning text-center" role="alert" id="div_msg_multiplo" style="display:none"><b id="msg_multiplo"></b></div>
                <div class="form-group">
                    <select name="usuarios-opec" id="usuarios-opec" class="form-control unico_opec">
                        <option value="">Selecione uma opção</option>
                        @foreach($usuariosOpec as $usuario)
                            <option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
                        @endforeach
                    </select>
                    <small class="hidden help-block text-danger">Selecione o usuário da OPEC</small>

                    <table class="table" id="multiplo_opec" style="display:none">
                            <thead>
                                <tr><th class="hidden-print"><input type="checkbox" class="check-all-opecs"></th>
                                <th class="text-center">OPEC</th>
                            </tr>
                            </thead>
                            <tbody>
                                <input type="hidden" name="qtd_opecs" value="0">
                                @foreach($usuariosOpec as $usuario)
                                    <tr>
                                        <td class="hidden-print"><input name="usuarios-opec" type="checkbox" value="{{ $usuario->id }}"></td>
                                        <td class="text-center">{{ $usuario->name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Voltar</button>
                <button id="confirmar-associar-opec" type="button" class="btn btn-info" data-loading-text="..."><i class="fa fa-check-square-o"></i> Associar</button>
            </div>
        </div>
    </div>
</div>