<?php $hasPermissionToReject = !Gate::check("{$namespace}-participante") && !empty($peca) && is_object($peca) && empty($viewPdf); ?>

<table class="table inputs-categoria-fixa pis" data-peca-index="{{ $peca_index }}">
    <thead>
        <tr>
            <td colspan="3">PI's</td>
            <td>
                @if(!$readonly)
                    <a href="#" class="btn btn-default" data-action="add-pi-row" data-owner="peca" data-input-name="pecas" data-peca-index="{{ $peca_index }}" >Adicionar PI</a>
                @endif
            </td>
        </tr>
        <tr>
            <td class="form-group">
                Nº da P.I.
            </td>
            <td>
                P.I. da peça <br/>
                <i><small>Arquivo do tipo .PDF com no máximo 5mb</small></i>
            </td>
            @if( $hasPermissionToReject )
            <td>
                Rejeitar P.I. da peça? <br />
                <i><small>Selecione caso deseje rejeitar a P.I. da peça</small></i>
            </td>
            @endif
            <td>&nbsp;</td>
        </tr>
    </thead>
    <?php
    $pis    = extractPis($peca,'pis');
    $piInit = [
        'pis'       => $pis,
        'readonly'  => $readonly,
        'uploadUrl' => url_uploads(),
        'hasPermissionToReject'  => $hasPermissionToReject,
        'peca_index'=> $peca_index,
        'input_name'=> 'pecas'
    ];
    ?>
    <tbody class="table-body pi-table-body" id="table-pis-peca-{{$peca_index}}" data-pi-init='{!! json_encode($piInit) !!}'></tbody>
</table>