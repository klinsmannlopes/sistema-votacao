<div>
    <fieldset>
        <legend>Peças</legend>

        @if(empty($readonly))
            <button type="button" id="add-peca" class="btn btn-primary"><i class="fa fa-plus"></i> Adicionar peça</button>
        @endif

        <div class="row">
            <div class="col-md-12 col-lg-8 col-lg-offset-2">
                <div class="pecas">
                    <?php $index = 1; ?>
                    @foreach(old('pecas', (! empty($row) ? ($row->trashed() ? $row->pecas()->onlyTrashed()->get() : $row->pecas) : []) ) as $peca)
                        @include("{$namespace}.partials.form-peca", compact('index', 'peca'))
                        <?php $index++; ?>
                    @endforeach
                </div>
            </div>
        </div>
    </fieldset>
</div>
