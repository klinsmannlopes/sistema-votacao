<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div class="table-responsive">
    <table class="table inscricoes_table">
        <thead>
        @if(Gate::check($namespace))
            <th class="hidden-print"><input type="checkbox" class="check-all"></th>
        @endif

        <th width="120" class="text-center hidden-print">PDF</th>
        <th class="text-center">N&ordm; da inscrição</th>
        <th class="text-center">Categoria</th>

        @if(Gate::check($namespace))
            <th class="text-center">Agência</th>
            <th class="text-center">Responsável</th>
            <th class="text-center">Telefone</th>
        @endif

        <th class="text-center">Anunciante</th>
        <th class="text-center">Campanha</th>
        <th class="text-center">Criação/Responsável Pela Defesa</th>
        <th class="text-center">Data de Cadastro</th>
        <th class="text-center">Status</th>

        @if(Gate::check($namespace) || Gate::check("{$namespace}-participante"))
            <th class="text-center">OPEC</th>
        @endif

        <th class="actions text-right hidden-print">
            @if(Gate::check("{$namespace}-participante") && $periodoDeInscricao)
                <a class="btn btn-info" href="{{ route("{$namespace}.create") }}"><i class="fa fa-plus"></i> Novo</a>
            @endif
        </th>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr @if($row->trashed()) class="trashed" @endif @if(!Gate::check($namespace)) data-url="{{ route(Gate::check("{$namespace}-opec") ? "{$namespace}.show" : "{$namespace}.edit", [ $row->id ]) }}" @endif>

                @if(Gate::check($namespace))
                    <td class="hidden-print">
                        @if(! $row->trashed())
                        <input type="checkbox" value="{{ $row->id }}" />
                        @endif
                    </td>
                    @endif

                    <td class="text-center hidden-print">
                    @if(! $row->trashed())
                    <a target="_blank" href="{{ route("{$namespace}.pdf", [ $row->id ]) }}" class="btn btn-white"><i class="fa fa-file-pdf-o text-danger"></i></a>
                    @endif
                </td>

                <td class="text-center">{{ $row->present()->numeroInscricao() }}</td>
                <td class="text-center">{{ $row->categoria->nome }}</td>

                @if(Gate::check($namespace))
                    <td class="text-center">{{ !empty($row->user->agency) ? $row->user->agency : '-' }}</td>
                    <td class="text-center">{{ $row->responsavel }}</td>
                    <td class="text-center">{{ $row->telefone }}</td>
                @endif

                <td class="text-center">{{ $row->anunciante }}</td>
                <td class="text-center">{{ $row->campanha }}</td>
                <td class="text-center">{{ $row->criacao }}</td>
                <td class="text-center">{{ date('d/m/Y', strtotime($row->created_at)) }}</td>

                @if(!Gate::check("{$namespace}-opec") )
                        <td class="text-center"> {!! $row->present()->inscricaoStatusColored() !!}</td>
                @else
                        <td class="text-center">
                            <?php  echo $row->present()->coresStatus(!empty($row->user_opec->name) ? $row->user_opec->name : '-') ?>
                        </td>
                @endif


                @if(Gate::check($namespace) || Gate::check("{$namespace}-participante"))
                    <td class="text-center">
                        <?php  echo $row->present()->coresStatus(!empty($row->user_opec->name) ? $row->user_opec->name : '-') ?>
                    </td>
                @endif


                <td class="text-right hidden-print" width="145">
                    @if($row->trashed())
                        <a href="{{ route("{$namespace}.log", [ $row->id ]) }}" class="btn btn-white" data-toggle="tooltip" title="Detalhes"><i class="fa fa-eye"></i></a>
                    @else
                        @if(!Gate::check("{$namespace}-participante"))
                            <a href="{{ route("{$namespace}.show", [ $row->id ]) }}" class="btn btn-white" data-toggle="tooltip" title="Detalhes"><i class="fa fa-eye"></i></a>
                        @endif

                        @if(Gate::check("{$namespace}-participante"))
                            @if($periodoDeInscricao || $row->is_pendente_de_correcao() || $row->is_cancelada() )
                            <a href="{{ route("{$namespace}.edit", [ $row->id ]) }}" class="btn btn-white" data-toggle="tooltip" title="Editar"><i class="fa fa-pencil"></i></a>
                            @endif
                        @endif

                        @if(!Gate::check("{$namespace}-opec"))
                            @include('form-destroy', [ 'route' => "{$namespace}.destroy", 'id' => $row->id ])
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

