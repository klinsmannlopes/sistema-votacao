<?php $hasPermissionToReject = !Gate::check("{$namespace}-participante") && !empty($inscricao) && is_object($inscricao); ?>

<fieldset>
    <legend>PI</legend>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table">
                <thead>
                <tr>
                    <td colspan="3">PI's</td>
                    <td>
                        @if(!$readonly)
                            <a href="#" class="btn btn-default" data-action="add-pi-row" data-owner="inscricao" data-input-name="pis" >Adicionar PI</a>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="form-group">
                        Nº da P.I.
                    </td>
                    <td>
                        P.I. da peça <br/>
                        <i><small>Arquivo do tipo .PDF com no máximo 5mb</small></i>
                    </td>
                    @if( $hasPermissionToReject )
                    <td>
                        Rejeitar P.I. da peça? <br />
                        <i><small>Selecione caso deseje rejeitar a P.I. da peça</small></i>
                    </td>
                    @endif
                    <td>&nbsp;</td>
                </tr>
                </thead>
                <?php
                $pis    = extractPis($inscricao,'pis');
                $piInit = [
                    'readonly'  => $readonly,
                    'uploadUrl' => url_uploads(),
                    'hasPermissionToReject' => $hasPermissionToReject,
                    'pis'       => $pis,
                    'input_name'=> 'pis'
                ];
                ?>

                <tbody class="table-body pi-table-body" id="table-pis" data-pi-init='{!! json_encode($piInit) !!}'></tbody>
            </table>
        </div>
    </div>
</fieldset>