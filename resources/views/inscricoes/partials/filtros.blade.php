<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Filtros</h5>
    </div>
    <div class="ibox-content">
        <form method="get" action="">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="search">N&ordm; da Inscrição @can($namespace)/ Agência @endcan/ Anunciante</label>
                        <input type="text" class="form-control" name="search" id="search" autocomplete="off" value="{{ $search }}">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <label for="categoria_id">Categoria</label>
                        <select name="categoria_id" id="categoria_id" class="form-control">
                            <option value="">Selecione uma opção</option>
                            @foreach($categorias as $categoria)
                            <option value="{{ $categoria->id }}" {{ $categoria->id == $categoria_id ? 'selected' : '' }}>{{ $categoria->nome }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control">
                            <option value="">Selecione uma opção</option>
                            @if(\Auth::user()->role === 'opec')
                            @else
                                @foreach($statusList as $statusRow)
                                    <option value="{{ $statusRow }}" {{ $statusRow == $status ? 'selected' : '' }}>{{ $statusRow }}</option>
                                @endforeach
                            @endif

                            @foreach($statusOpec as $statusOpecRow)
                                <?php
                                    if($statusOpecRow != "") {
                                    ?>
                                    <option value="{{ $statusOpecRow }}" {{ $statusOpecRow == $status ? 'selected' : '' }}>{{ $statusOpecRow }}</option>
                                    <?php
                                    }
                                ?>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <button class="btn btn-white"><i class="fa fa-search"></i> Filtrar</button>
                        <a target="_blank" href="{{ route("{$namespace}.imprimir") }}?{{ http_build_query(compact('search', 'status', 'categoria_id')) }}" class="btn btn-white"><i class="fa fa-print text-center"></i> Imprimir</a>
                        <a target="_blank" href="{{ route("{$namespace}.xls") }}?{{ http_build_query(compact('search', 'status', 'categoria_id')) }}" class="btn btn-white"><i class="fa fa-file-excel-o text-info"></i> Excel</a>
                        <a target="_blank" href="{{ route("{$namespace}.csv") }}?{{ http_build_query(compact('search', 'status', 'categoria_id')) }}" class="btn btn-white"><i class="fa fa-file-o text-success"></i> CSV</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>