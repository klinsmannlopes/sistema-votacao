<div class="modal fade" id="modal-enviar-para-correcao" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-danger">Justificativa de Correção</h3>
            </div>
            <div class="modal-body">
                <h2 class="text-center">Escreva o mais breve possível, que informações o participante deve corrigir na sua inscrição:</h2>
                <form method="post" id="enviar-para-correcao" action="{{ route("{$namespace}.enviar-para-correcao", [ $row->id ]) }}">

                    {!! csrf_field() !!}

                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group">
                        <textarea name="justificativa" id="justificativa" class="form-control"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Voltar</button>
                <button type="button" id="confirmar-enviar-para-correcao" class="btn btn-warning" data-loading-text="..."><i class="fa fa-send"></i> Enviar para Correção</button>
            </div>
        </div>
    </div>
</div>