<b class="text-success">Arquivo enviado com sucesso:</b> clique <a target="_blank" href="{{ $newFileURL }}">aqui</a> para visualizá-lo.
&nbsp;<button type="button" class="btn btn-xs btn-danger remover-arquivo"><i class="fa fa-close"></i> Remover</button>

@if( $owner_type == 'inscricao' )
    <input type="hidden" name="pis[{{ $pi_index }}][{{ $key }}]" value="{{ $newArquivo }}">
@else
    <input type="hidden" name="pecas[{{ $index }}][pis][{{$pi_index}}][{{ $key }}]" value="{{ $newArquivo }}">
@endif
