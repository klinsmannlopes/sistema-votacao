<html>
    <head>
        <meta http-equiv="Content-Type" content="text/csv; charset=utf-8" />
    </head>
    <body>
        <table>
            <tbody>
                <tr>
                    <td align="center"><b>Nº da Inscrição</b></td>
                    <td align="center"><b>Resposável</b></td>
                    <td align="center"><b>Cargo</b></td>
                    <td align="center"><b>E-mail</b></td>
                    <td align="center"><b>Agência</b></td>
                    <td align="center"><b>Endereço</b></td>
                    <td align="center"><b>Telefone</b></td>
                    <td align="center"><b>Categoria</b></td>
                    <td align="center"><b>Anunciante</b></td>
                    <td align="center"><b>Produto/Serviço</b></td>
                    <td align="center"><b>Início da Veiculação</b></td>
                    <td align="center"><b>Fim da Veiculação</b></td>
                    <td align="center"><b>Criação</b></td>
                    <td align="center"><b>Mídia</b></td>
                    <td align="center"><b>Atendimento</b></td>
                    <td align="center"><b>Aprovação</b></td>

                    <td align="center"><b>Campanha</b></td>
                    <td align="center"><b>Status</b></td>

                    @if($qtdPecas)
                        @foreach(range(1, $qtdPecas) as $index)
                            <td align="center"><b>Título da peça {{ ($index) }}</b></td>
                            <td align="center"><b>Veículo da peça {{ ($index) }}</b></td>
                            <td align="center"><b>Formato da peça {{ ($index) }}</b></td>
                            <td align="center"><b>Número da PI da peça {{ ($index) }}</b></td>
                            <td align="center"><b>Produtora da peça {{ ($index) }}</b></td>
                        @endforeach
                    @endif

                </tr>

                @foreach($rows as $row)
                <tr>
                    <td align="center">{{ ($row->present()->numeroInscricao()) }}</td>
                    <td align="center">{{ ($row->responsavel) }}</td>
                    <td align="center">{{ ($row->cargo) }}</td>
                    <td align="center">{{ ($row->email) }}</td>
                    <td align="center">{{ ($row->user->agency) }}</td>
                    <td align="center">{{ ($row->user->address) }}</td>
                    <td align="center">{{ ($row->telefone) }}</td>
                    <td align="center">{{ ($row->categoria->nome) }}</td>
                    <td align="center">{{ ($row->anunciante) }}</td>
                    <td align="center">{{ ($row->produto_ou_servico) }}</td>
                    <td align="center">{{ (date('d/m/Y', strtotime($row->veiculacao_ini))) }}</td>
                    <td align="center">{{ (date('d/m/Y', strtotime($row->veiculacao_fim))) }}</td>

                    <td align="center">
                        @if(!$row->is_midia() && !$row->is_planejamento())
                        {{ ($row->criacao) }}
                        @endif
                    </td>

                    <td align="center">{{ ($row->midia) }}</td>
                    <td align="center">{{ (!$row->is_midia() && !$row->is_planejamento() ? $row->atendimento : '') }}</td>
                    <td align="center">{{ (!$row->is_midia() && !$row->is_planejamento() ? $row->aprovacao : '') }}</td>
                    <td align="center">{{ ($row->campanha) }}</td>

                    <td align="center">
                        {{ ($row->present()->inscricaoStatus()) }}

                        @if($row->is_cancelada() || $row->is_pendente_de_correcao())
                        ({{ ($row->justificativa) }})
                        @endif
                    </td>

                    @if(!$row->is_midia() && !$row->is_planejamento())
                        @foreach($row->pecas as $peca)
                        <td align="center">{{ ($peca->titulo) }}</td>
                        <td align="center">{{ ($peca->veiculo) }}</td>
                        <td align="center">{{ ($peca->formato) }}</td>
                        <td align="center">{{ ($peca->pi_numero) }}</td>
                        <td align="center">{{ ($peca->produtora) }}</td>
                        @endforeach
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>