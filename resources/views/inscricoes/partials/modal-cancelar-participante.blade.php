<div class="modal fade" id="modal-cancelar-participante" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-danger">Confirmação de Cancelamento</h3>
            </div>
            <div class="modal-body">
                <h2 class="text-center">Tem certeza que deseja cancelar esta inscrição?</h2>
                <form method="post" id="cancelar-participante" action="{{ route("{$namespace}.cancelada-pelo-participante", [ $row->id ]) }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="PUT">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Não</button>
                <button type="button" id="confirmar-cancelar-participante" class="btn btn-danger" data-loading-text="...">Sim</button>
            </div>
        </div>
    </div>
</div>