<fieldset>
    <legend>Mídia</legend>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="titulo_do_case">Título do Case <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="metas[titulo_do_case]" id="titulo_do_case" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.titulo_do_case', !empty($row) && ($value = $row->meta('titulo_do_case')) ? $value : '') }}">
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label" for="problema_ou_oportunidade">Problema/Oportunidade <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[problema_ou_oportunidade]" id="problema_ou_oportunidade" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.problema_ou_oportunidade', !empty($row) && ($value = $row->meta('problema_ou_oportunidade')) ? $value : '') }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="veiculos_utilizados">Veículos Usados <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="metas[veiculos_utilizados]" id="veiculos_utilizados" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.veiculos_utilizados', !empty($row) && ($value = $row->meta('veiculos_utilizados')) ? $value : '') }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="mix_de_veiculos">Def. Mix de Veículos <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[mix_de_veiculos]" id="mix_de_veiculos" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.mix_de_veiculos', !empty($row) && ($value = $row->meta('mix_de_veiculos')) ? $value : '') }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="publico_alvo">Público Alvo <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="metas[publico_alvo]" id="publico_alvo" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.publico_alvo', !empty($row) && ($value = $row->meta('publico_alvo')) ? $value : '') }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="resumo_da_midia">Resumo da Mídia <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[resumo_da_midia]" id="resumo_da_midia" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.resumo_da_midia', !empty($row) && ($value = $row->meta('resumo_da_midia')) ? $value : '') }}</textarea>
        </div>
    </div>

    <fieldset>
        <legend>Resultado Domiciliar</legend>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="total_de_insercoes_domiciliar">Total de Inserções <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[total_de_insercoes_domiciliar]" id="total_de_insercoes_domiciliar" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.total_de_insercoes_domiciliar', !empty($row) && ($value = $row->meta('total_de_insercoes_domiciliar')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="total_de_grp">Total de GRP <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[total_de_grp]" id="total_de_grp" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.total_de_grp', !empty($row) && ($value = $row->meta('total_de_grp')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="alcance_domiciliar">Alcance % <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[alcance_domiciliar]" id="alcance_domiciliar" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.alcance_domiciliar', !empty($row) && ($value = $row->meta('alcance_domiciliar')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="frequencia_media_domiciliar">Frequência Média <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[frequencia_media_domiciliar]" id="frequencia_media_domiciliar" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.frequencia_media_domiciliar', !empty($row) && ($value = $row->meta('frequencia_media_domiciliar')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="universo_domiciliar">Universo Domiciliar <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[universo_domiciliar]" id="universo_domiciliar" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.universo_domiciliar', !empty($row) && ($value = $row->meta('universo_domiciliar')) ? $value : '') }}">
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Resultado Público Alvo</legend>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="total_de_insercoes_publico_alvo">Total de Inserções <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[total_de_insercoes_publico_alvo]" id="total_de_insercoes_publico_alvo" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.universo_domiciliar', !empty($row) && ($value = $row->meta('universo_domiciliar')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="total_tarp">Total TARP <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input class="form-control" name="metas[total_tarp]" id="total_tarp" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.total_tarp', !empty($row) && ($value = $row->meta('total_tarp')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="alcance_public_alvo">Alcance % <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[alcance_public_alvo]" id="alcance_public_alvo" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.alcance_public_alvo', !empty($row) && ($value = $row->meta('alcance_public_alvo')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="frequencia_media_publico_alvo">Frequência Média <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[frequencia_media_publico_alvo]" id="frequencia_media_publico_alvo" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.frequencia_media_publico_alvo', !empty($row) && ($value = $row->meta('frequencia_media_publico_alvo')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="total_de_impactos">Total de Impactos <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[total_de_impactos]" id="total_de_impactos" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.total_de_impactos', !empty($row) && ($value = $row->meta('total_de_impactos')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="cmp_impactos">CPM impactos <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[cmp_impactos]" id="cmp_impactos" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.cmp_impactos', !empty($row) && ($value = $row->meta('cmp_impactos')) ? $value : '') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="universo_do_target">Universo do Target <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="metas[universo_do_target]" id="universo_do_target" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.universo_do_target', !empty($row) && ($value = $row->meta('universo_do_target')) ? $value : '') }}">
            </div>
        </div>
    </fieldset>
    <hr>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="defesa_geral">Defesa Geral <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[defesa_geral]" id="defesa_geral" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.defesa_geral', !empty($row) && ($value = $row->meta('defesa_geral')) ? $value : '') }}</textarea>
        </div>
    </div>
</fieldset>
