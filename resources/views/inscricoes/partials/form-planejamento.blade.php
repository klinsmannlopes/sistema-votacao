<fieldset>
    <legend>Planejamento</legend>

    <div class="form-group">
        <label class="col-lg-2 control-label" for="objetivo">Objetivo <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[objetivo]" id="objetivo" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.objetivo', !empty($row) && ($value = $row->meta('objetivo')) ? $value : '') }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="veiculos">Veículos <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="metas[veiculos]" id="veiculos" {{ !empty($readonly) ? 'disabled' : '' }} value="{{ old('metas.veiculos', !empty($row) && ($value = $row->meta('veiculos')) ? $value : '') }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="defesa_mix">Defesa Mix <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[defesa_mix]" id="defesa_mix" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.defesa_mix', !empty($row) && ($value = $row->meta('defesa_mix')) ? $value : '') }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="contribuicao_especifica_do_trabalho">Contribuição Específica do Trabalho <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[contribuicao_especifica_do_trabalho]" id="contribuicao_especifica_do_trabalho" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.contribuicao_especifica_do_trabalho', !empty($row) && ($value = $row->meta('contribuicao_especifica_do_trabalho')) ? $value : '') }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="grau_de_dificuldade_e_interdepencia">Grau de Dificuldade e Interdependência <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[grau_de_dificuldade_e_interdepencia]" id="grau_de_dificuldade_e_interdepencia" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.grau_de_dificuldade_e_interdepencia', !empty($row) && ($value = $row->meta('grau_de_dificuldade_e_interdepencia')) ? $value : '') }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="resultados_obtidos">Resultados Obtidos <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <textarea class="form-control" name="metas[resultados_obtidos]" id="resultados_obtidos" {{ !empty($readonly) ? 'disabled' : '' }}>{{ old('metas.resultados_obtidos', !empty($row) && ($value = $row->meta('resultados_obtidos')) ? $value : '') }}</textarea>
        </div>
    </div>
</fieldset>