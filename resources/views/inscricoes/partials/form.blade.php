@include('errors')

@if(!empty($row) && ! $authUser->is_jurado() && ! $authUser->is_juiz() && ! $authUser->is_opec())
<fieldset class="status">
    <legend>Status</legend>
    <div class="row">
        <div class="col-md-12 col-lg-6 col-lg-offset-2">
            <h2 style="color: {{ $color }};">{{ $row->present()->inscricaoStatus }} @if(! empty($row) && $row->trashed())<span class="text-danger">- DELETADA</span>@endif</h2>

            @if($row->justificativa)
            <div style="color: {{ $color }};" class="well justificativa">{!! nl2br($row->justificativa) !!}</div>
            @endif
        </div>
    </div>
</fieldset>

@if(!empty($row->present()->inscricaoStatusOpec()) and \Auth::user()->role === 'administrador' )
<fieldset class="status">
    <legend>Status OPEC</legend>
    <div class="row">
        <div class="col-md-12 col-lg-6 col-lg-offset-2">
            <h2><?php  echo $row->present()->coresStatus(!empty($row->user_opec->name) ? $row->user_opec->name : '-')?></h2>
        </div>
    </div>
</fieldset>
@endif

@elseif(\Auth::user()->role === 'opec')


    <fieldset class="status">
        <legend>Status</legend>
        <div class="row">
            <div class="col-md-12 col-lg-6 col-lg-offset-2">
                <h2 style="color: {{ $color }};">{{ $row->present()->inscricaoStatus }} @if(! empty($row) && $row->trashed())<span class="text-danger">- DELETADA</span>@endif</h2>

                @if($row->justificativa)
                    <div style="color: {{ $color }};" class="well justificativa">{!! nl2br($row->justificativa) !!}</div>
                @endif
            </div>
        </div>
    </fieldset>

    <fieldset class="status">
        <legend>Status OPEC</legend>
        <div class="row">
            <div class="col-md-12 col-lg-6 col-lg-offset-2">
                <h2><?php  echo $row->present()->coresStatus(!empty($row->user_opec->name) ? $row->user_opec->name : '-')?></h2>
            </div>
        </div>
    </fieldset>
@endif

<fieldset>
    <legend>Dados da Inscrição</legend>

    @if(!empty($row))
    <div class="form-group">
        <label class="col-lg-2 control-label">Nº da Inscrição</label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="numero" value="{{ $row->present()->numeroInscricao() }}" disabled>
        </div>
    </div>
    @endif

    @if( ! $authUser->is_jurado() && ! $authUser->is_juiz() )
    <div class="form-group">
        <label class="col-lg-2 control-label" for="agencia">Agência <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="agencia" id="agencia" value="{{ $agencia }}" disabled>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="endereco">Endereço <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="endereco" id="endereco" autocomplete="off" value="{{ $endereco }}" disabled>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="responsavel">Responsável <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="responsavel" id="responsavel" autocomplete="off" value="{{ old('responsavel', isset($row->responsavel) ? $row->responsavel : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="cargo">Cargo <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="cargo" id="cargo" autocomplete="off" value="{{ old('cargo', isset($row->cargo) ? $row->cargo : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="email">E-mail <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="email" class="form-control" name="email" id="email" autocomplete="off" value="{{ old('email', isset($row->email) ? $row->email : \Auth::user()->email) }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="telefone">Telefone <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control telefone" name="telefone" id="telefone" autocomplete="off" value="{{ old('telefone', isset($row->telefone) ? $row->telefone : \Auth::user()->phone) }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="categoria_id">Categoria <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            {!! getSelectCategoria('categoria_id', isset($row->categoria_id) ? $row->categoria_id : null) !!}
            <input type="hidden" name='categoria_abrev' id="categoria_abreviatura" value="" >
        </div>
    </div>
    @endif

    <div class="categoria-midia {{ $midiaClass }}">
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-2">
                @include("{$namespace}.partials.form-midia")
            </div>
        </div>
    </div>

    <div class="categoria-planejamento {{ $planejamentoClass }}">
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-2">
                @include("{$namespace}.partials.form-planejamento")
            </div>
        </div>
    </div>

    <div class="categoria-nao-fixa {{ $naoFixaClass  }}">
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-2">
                @include("{$namespace}.partials.form-categoria-nao-fixa")
            </div>
        </div>
    </div>

    @if( ! $authUser->is_jurado() && ! $authUser->is_juiz() )
    <div class="pis-inscricao {{ $piClass  }}">
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-2">
                @include('inscricoes.partials.form-pis-inscricao', [
                    'namespace' => $namespace,
                    'inscricao' => isset($row) ? $row : null,
                    'peca'      => isset($peca) ? $peca : null,
                    'readonly'  => isset($readonly) ? $readonly : null,
                    'viewPdf'   => isset($viewPdf) ? $viewPdf : null
                ])
            </div>
        </div>
    </div>

    <div class="inputs-categoria-fixa">
        <div class="form-group">
            <label class="col-lg-2 control-label" for="anunciante">Anunciante <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="anunciante" id="anunciante" autocomplete="off" value="{{ old('anunciante', isset($row->anunciante) ? $row->anunciante : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="produto_ou_servico">Produto/Serviço <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="produto_ou_servico" id="produto_ou_servico" autocomplete="off" value="{{ old('produto_ou_servico', isset($row->produto_ou_servico) ? $row->produto_ou_servico : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="veiculacao_ini">Início da Veiculação <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control datepicker" name="veiculacao_ini" id="veiculacao_ini" autocomplete="off" value="{{ old('veiculacao_ini', isset($row->veiculacao_ini) ? $row->present()->veiculacaoIni() : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label" for="veiculacao_fim">Fim da Veiculação <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control datepicker" name="veiculacao_fim" id="veiculacao_fim" autocomplete="off" value="{{ old('veiculacao_fim', isset($row->veiculacao_fim) ? $row->present()->veiculacaoFim() : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="midia">Mídia <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="midia" id="midia" autocomplete="off" value="{{ old('midia', isset($row->midia) ? $row->midia : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="atendimento">Atendimento <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="atendimento" id="atendimento" autocomplete="off" value="{{ old('atendimento', isset($row->atendimento) ? $row->atendimento : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="aprovacao">Aprovação <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="text" class="form-control" name="aprovacao" id="aprovacao" autocomplete="off" value="{{ old('aprovacao', isset($row->aprovacao) ? $row->aprovacao : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label criacao_label" for="criacao">Criação <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="criacao" id="criacao" autocomplete="off" value="{{ old('criacao', isset($row->criacao) ? $row->criacao : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
        </div>
    </div>

    @endif

    <div class="form-group">
        <label class="col-lg-2 control-label" for="campanha">Campanha <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="campanha" id="campanha" autocomplete="off" value="{{ old('campanha', isset($row->campanha) ? $row->campanha : '') }}" {{ !empty($readonly) || !empty($pendenteDeCorrecao) ? 'disabled' : '' || !empty($pendenteDeRevisao) ? 'disabled' : ''}}>
        </div>
    </div>

    @if(empty($row) || !empty($pendenteDeCorrecao) || $row->is_aguardando_validacao())
        <div class="form-group">
            <label class="col-lg-2 control-label" for="concordo">Concordo com os Termos <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <label><input {{ !empty($pendenteDeCorrecao) || (!empty($row) && $row->is_aguardando_validacao()) ? 'checked disabled' : '' }} name="corcodo" id="concordo" value="1" type="checkbox" required {{ old('corcodo') ? 'checked' : '' }}>&nbsp;&nbsp; <span id="descricao-termo">Autorizo os organizadores do GP Verdes Mares a utilizar os materiais inscritos para divulgação do Prêmio.</span></label>
            </div>
        </div>
    @endif
</fieldset>

@if(empty($noPrint))
    @include('campos-obrigatorios')
@endif
