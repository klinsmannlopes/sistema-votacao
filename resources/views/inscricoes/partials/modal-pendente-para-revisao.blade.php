<div class="modal fade" id="modal-pendente-para-revisao" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-danger">Justificativa de Correção</h3>
            </div>
            <div class="modal-body">
                <h2 class="text-center">Escreva o mais breve possível, que motivo(s) para a solicitação de mudar status de cancelado:</h2>
                <form method="post" id="enviar-para-correcao" action="{{ route("{$namespace}.pendente-para-revisao", [ $row->id ]) }}">

                    {!! csrf_field() !!}

                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group">
                        <textarea name="justificativa" id="justificativa" class="form-control"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Voltar</button>
                <button type="button" id="confirmar-enviar-para-correcao" class="btn button-revisao" data-loading-text="..."><i class="fa fa-send"></i> Enviar para Revisão</button>
            </div>
        </div>
    </div>
</div>