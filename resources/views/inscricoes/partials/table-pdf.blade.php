<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
        <table>
            <tbody>
                <tr>
                    <td style="width: 20%;" align="center"><b>Nº da Inscrição</b></td>
                    <td style="width: 20%;" align="center"><b>Categoria</b></td>
                    <td style="width: 20%;" align="center"><b>Agência</b></td>
                    <td style="width: 20%;" align="center"><b>Campanha</b></td>
                    <td style="width: 20%;" align="center"><b>Status</b></td>
                </tr>

                @foreach($rows as $row)
                <tr>
                    <td align="center">{{ $row->present()->numeroInscricao() }}</td>
                    <td align="center">{{ $row->categoria->nome }}</td>
                    <td align="center">{{ $row->user->agency }}</td>
                    <td align="center">{{ $row->campanha }}</td>
                    <td align="center">{{ $row->present()->inscricaoStatus() }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>