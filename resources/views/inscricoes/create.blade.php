@extends('layouts.panel')

@section('page-title')
    Cadastrar Inscrição
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('content')
    <form method="post" class="form-horizontal" action="{{ route("{$namespace}.store") }}" enctype="multipart/form-data">
        {!! csrf_field() !!}

        @include("{$namespace}.partials.form")

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route("{$namespace}.index") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button class="btn btn-info" type="submit"><i class="fa fa-plus"></i> Cadastrar</button>
            </div>
        </div>
    </form>
@endsection