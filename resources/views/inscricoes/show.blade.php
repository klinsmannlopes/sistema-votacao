@extends('layouts.panel')

@section('page-title')
    @if(\Auth::user()->role === \App\Role::OPEC)
        Aprovação da OPEC
    @else
        Detalhes da Inscrição
    @endif
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('content')

    @include("{$namespace}.partials.modal-enviar-para-correcao")
    @include("{$namespace}.partials.modal-cancelar")

    <div class="form-horizontal">

        @include("{$namespace}.partials.form")

        <form id="{{ $idForm }}" method="post" action="{{ route($route, [ $row->id ]) }}">
            {!! csrf_field() !!}

            <input type="hidden" name="_method" value="PUT">

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <a class="btn btn-white" href="{{ route("{$namespace}.index") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                    @if(Gate::check($namespace))
                    <button {{ $cancelarDisabled }} type="button" class="btn btn-danger show-modal-cancelar"><i class="fa fa-ban"></i> Cancelar Inscrição</button>

                        @if(\Auth::user()->role === 'administrador' && ($status == 'Cancelada pelo Administrador' || $status == 'Pendente Para Revisão' || $status == 'Cancelada pelo Participante') )
                            <button type="button" class="btn btn-warning show-modal-enviar-para-correcao"><i class="fa fa-exclamation-triangle"></i> Enviar para Correção</button>
                        @else
                            <button {{ $enviarParaCorrecaoDisabled }} type="button" class="btn btn-warning show-modal-enviar-para-correcao"><i class="fa fa-exclamation-triangle"></i> Enviar para Correção</button>
                        @endif
                    <button {{ $validarDisabled }} class="btn btn-info"><i class="fa fa-check"></i> Validar Inscrição</button>
                    @endif

                    @if(Gate::check("{$namespace}-opec"))
                    <button {{ $enviarParaCorrecaoDisabled }} type="button" class="btn btn-warning show-modal-enviar-para-correcao"><i class="fa fa-exclamation-triangle"></i> Enviar para Correção</button>
                    <button {{ $aprovarDisabled }} class="btn btn-info"><i class="fa fa-check"></i> Aprovar Inscrição</button>
                    @endif

                </div>
            </div>
        </form>
    </div>

@endsection