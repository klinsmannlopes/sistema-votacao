@extends('layouts.panel')

@section('page-title')
    Editar Inscrição
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('content')

    @include("{$namespace}.partials.modal-cancelar-participante")
    @include("{$namespace}.partials.modal-pendente-para-revisao")

    <form id="edit-form" method="post" class="form-horizontal" action="{{ $buttonsDisabled ? '' : route("{$namespace}.update", [ $row->id ]) }}" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="row_id" value="{{ $row->id }}">

        @include("{$namespace}.partials.form")
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route("{$namespace}.index") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                @if($status_incricao)
                    <button  type="button" class="btn button-revisao show-modal-pendente-para-revisao" ><i class="glyphicon glyphicon-list-alt"></i>  Solicitar revisão</button>
                @endif

                @if($periodoDeInscricao || $row->is_pendente_de_correcao() || $row->is_cancelada() || $row->is_pendente_de_correcao_opec() )

                    @if($periodoDeInscricao)
                        <button {{ $buttonsDisabled }} type="button" class="btn btn-danger" id="show-modal-cancelar-participante"><i class="fa fa-ban"></i> Cancelar Inscrição</button>
                    @else
                        <button disabled type="button" class="btn btn-danger" id="show-modal-cancelar-participante"><i class="fa fa-ban"></i> Cancelar Inscrição</button>
                    @endif
                    <button {{ $buttonsDisabled }} class="btn btn-info" type="submit"><i class="fa fa-save"></i> Salvar</button>
                @else
                    <button disabled type="button" class="btn btn-danger" id="show-modal-cancelar-participante"><i class="fa fa-ban"></i> Cancelar Inscrição</button>
                    <button disabled class="btn btn-info" type="submit"><i class="fa fa-save"></i> Salvar</button>
                @endif
            </div>
        </div>
    </form>
@endsection