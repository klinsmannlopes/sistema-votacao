<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>@yield('page-title'){{ env('PAGE_TITLE_SUFFIX') }}</title>

        <link rel="stylesheet" href="{{ url('assets/vendor/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('assets/vendor/css/font-awesome.min.css') }}">

        <link rel="stylesheet" href="{{ url('assets/layouts/insipia/css/animate.css') }}" >
        <link rel="stylesheet" href="{{ url('assets/layouts/insipia/css/style.min.css') }}">
        <link rel="stylesheet" href="{{ url('assets/layouts/css/auth.css') }}">

        @yield('styles')
    </head>
    <body class="gray-bg" {!! !empty($envelopagem) ? 'style="background: url('. url_uploads("layout/{$envelopagem}") .') center center no-repeat;"' : '' !!}>

        @include('errors')
        @include('auth.partials.success')

        <div class="middle-box text-center loginscreen animated fadeInDown">
            <a class="logo" href="{{ route('getLogin') }}"><img class="center-block" src=""></a>
            @yield('content')
        </div>

        <!-- Mainly scripts -->
        <script src="{{ url('assets/layouts/insipia/js/jquery-2.1.1.js') }}"></script>
        <script src="{{ url('assets/layouts/insipia/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/vendor/js/jquery.min.js') }}"></script>

        <script>
            jQuery(function($)
            {
                $(document).on('blur', '[required]', function()
                {
                    if($.trim($(this).val()) === '')
                        $(this).val('');
                });
            });
        </script>

        @yield('scripts')
    </body>
</html>
