<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs"><strong class="font-bold">{{ \Auth::user()->name }}</strong></span>
                            <span class="text-muted text-xs block">Opções <b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ route('profile') }}">Editar Perfil</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Sair</a></li>
                    </ul>
                </div>
            </li>

            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a></li>

            @can('layout')
            <li><a href="{{ route('layout') }}"><i class="fa fa-picture-o"></i> <span class="nav-label">Layout</span></a></li>
            @endcan

            @can('usuarios')
            <li>
                <a href="{{ route('usuarios') }}"><i class="fa fa-users"></i> <span class="nav-label">Usuários</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{ route('usuario.create') }}"><i class="fa fa-plus"></i> Cadastro de usuários</a></li>
                    <li><a href="{{ route('usuarios') }}"><i class="fa fa-th-list"></i> Listagem de usuários</a></li>
                </ul>
            </li>
            @endcan

            @can('cadastro-de-participantes')
            <li><a href="{{ route('cadastro-de-participantes') }}"><i class="fa fa-upload"></i> <span class="nav-label">Cadastro de Participantes</span></a></li>
            @endcan

            @can('inscricoes')
            <li>
                <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Parametrização</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('categorias')
                    <li><a href="{{ route('categorias.index') }}"><i class="fa fa-th-list"></i> Categorias</a></li>
                    @endcan

                    @can('edicoes')
                    <li><a href="{{ route('edicoes.index') }}"><i class="fa fa-calendar"></i> Edições</a></li>
                    @endcan

                    @can('inscricoes.parametrizacao')
                    <li><a href="{{ route('periodo-de-inscricoes') }}"><i class="fa fa-calendar"></i> Cadastro das Inscrições</a></li>
                    @endcan
                </ul>
            </li>
            @endcan

            @if(Gate::check('inscricoes') || Gate::check('inscricoes-opec') || Gate::check('inscricoes-participante'))
                <li>
                    <a href="#"><i class="fa fa-users"></i></i> <span class="nav-label">Inscrições</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        @if($periodoDeInscricao && Gate::check('inscricoes-participante'))
                            <li><a href="{{ route('inscricao-profissional.create') }}"><i class="fa fa-plus"></i> Nova Inscrição</a></li>
                        @endif
                        <li><a href="{{ route('inscricao-profissional.index') }}"><i class="fa fa-list"></i> Inscrições</a></li>
                    </ul>
                </li>
            @endif

            @if(Gate::check('inscricoes'))
                <li>
                    <a href="{{ route('profissional-resultados.index') }}"><i class="fa fa-gavel"></i> <span class="nav-label">Resultados</span> </a>
                </li>
            @endif


            @if(Gate::check('resultado-edicao'))
                @can('resultado-edicao')
                <li><a href="{{ route('resultado-edicao') }}"><i class="fa fa-download"></i> <span class="nav-label">Resultado por Agência</span></a></li>
                @endcan
            @endif

        </ul>
    </div>
</nav>