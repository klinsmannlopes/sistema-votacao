<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('page-title'){{ env('PAGE_TITLE_SUFFIX') }}</title>

        <link rel="stylesheet" href="{{ url('assets/vendor/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('assets/vendor/css/font-awesome.min.css') }}">

        <link href="{{ url('assets/layouts/insipia/css/style.min.css') }}" rel="stylesheet">

        <link href="{{ url('assets/vendor/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
        <link href="{{ url('assets/vendor/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

        <link href="{{ url('assets/css/panel.css') }}" rel="stylesheet">

        @yield('styles')
    </head>
    <body>
        @include('alert')
        @include('confirm')

        <div id="wrapper">

            @if(is_edicao_profissional())
                @include('layouts.partials.panel.sidebar-profissional')
            @else
                @include('layouts.partials.panel.sidebar')
            @endif

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                @if( isset($edicaoAtual) && !is_null($edicaoAtual) )
                                    <span class="badge badge-info" style="font-family: 'Open Sans', 'Helvetica Neue'">
                                        {!! $edicaoAtual->nome  !!} ({!! $edicaoAtual->ano  !!})
                                    </span>
                                @else
                                    <span class="badge badge-danger" style="font-family: 'Open Sans', 'Helvetica Neue'">
                                        <strong>Você não selecionou uma edição, por favor, deslogue e logue novamente.</strong>
                                    </span>
                                @endif
                            </li>
                            <li><a href="{{ route('profile') }}">Bem-vindo(a), {{ \Auth::user()->name }}.@if(!empty($lastLogin = \Auth::user()->logged_at)) <i class="fa fa-clock-o"></i><small><i>Seu último login foi em {{ date('d/m/Y \à\s H:i', strtotime($lastLogin)) }}</i></small> @endif</a></li>
                            <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i>Sair</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">

                    @include('flash::message')

                    @yield('filtros')

                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>@yield('page-title')</h5>
                        </div>
                        <div class="ibox-content">
                            @yield('content')
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="pull-right">
                    </div>
                    <div>
                        <strong>Copyright</strong> Verdes Mares &copy; {{ date('Y') }}
                    </div>
                </div>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="{{ url('assets/layouts/insipia/js/jquery-2.1.1.js') }}"></script>
        <script src="{{ url('assets/layouts/insipia/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/layouts/insipia/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script src="{{ url('assets/layouts/insipia/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ url('assets/vendor/js/jquery.validate.min.js') }}"></script>
        <script src="{{ url('assets/vendor/js/jquery.mask.min.js') }}"></script>
        <script src="{{ url('assets/vendor/js/handlebars-v4.0.5.js') }}"></script>
        <script src="{{ url('assets/vendor/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ url('assets/vendor/js/bootstrap-datepicker.pt-BR.min.js') }}"></script>
        <script src="{{ url('assets/vendor/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script src="{{ url('assets/vendor/js/bootstrap-datetimepicker.pt-BR.js') }}"></script>

        <!-- Custom and plugin javascript -->
        <script src="{{ url('assets/layouts/insipia/js/inspinia.js') }}"></script>
        <script src="{{ url('assets/layouts/insipia/js/plugins/pace/pace.min.js') }}"></script>

        <script src="{{ url('assets/js/panel.js') }}"></script>
        @yield('scripts')
    </body>
</html>