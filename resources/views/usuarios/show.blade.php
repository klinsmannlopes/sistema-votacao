@extends('layouts.panel')

@section('page-title')
    Visualizar Usuário
@endsection

@section('scripts')
    @include('usuarios.partials.scripts')
@endsection

@section('content')

    <div class="form-horizontal">
        @include('usuarios.partials.form', [ 'readonly' => true ])

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route('usuarios') }}"><i class="fa fa-arrow-left"></i> Voltar</a>
            </div>
        </div>
    </div>
@endsection