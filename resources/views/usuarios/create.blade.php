@extends('layouts.panel')

@section('page-title')
    Cadastro de Usuário
@endsection

@section('scripts')
    @include('usuarios.partials.scripts')
@endsection

@section('content')
    <form method="post" class="form-horizontal" action="{{ route('usuario.store') }}">
        {!! csrf_field() !!}

        @include('usuarios.partials.form')

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route('usuarios') }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button class="btn btn-info" type="submit"><i class="fa fa-plus"></i> Cadastrar</button>
            </div>
        </div>
    </form>
@endsection