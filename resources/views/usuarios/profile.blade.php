@extends('layouts.panel')

@section('page-title')
    Editar Perfil
@endsection

@section('scripts')
    @include('usuarios.partials.scripts')
@endsection

@section('content')
    <form method="post" class="form-horizontal" action="{{ route('profile.save') }}">
        {!! csrf_field() !!}

        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="row_id" value="{{ \Auth::user()->id }}">
        <input type="hidden" name="is_profile" value="1">

        @include('usuarios.partials.form', ['is_profile' => true])

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route('dashboard') }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button class="btn btn-info" type="submit"><i class="fa fa-save"></i> Salvar</button>
            </div>
        </div>
    </form>
@endsection