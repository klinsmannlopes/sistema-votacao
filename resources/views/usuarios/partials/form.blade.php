@include('errors')

<div class="form-group">
    <label class="col-lg-2 control-label" for="name">Nome <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="text" class="form-control" name="name" id="name" autocomplete="off" value="{{ old('name', !empty($row->name) ? $row->name : '') }}" {{ !empty($readonly) ? 'disabled' : '' }}>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label" for="cnpj">CPNJ</label>
    <div class="col-lg-10">
        <input type="text" class="form-control cnpj" name="cnpj" id="cnpj" value="{{ old('cnpj', !empty($row->cnpj) ? $row->cnpj : '') }}" {{ !empty($readonly) ? 'disabled' : '' }}>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label" for="address">Endereço</label>
    <div class="col-lg-10">
        <input type="text" class="form-control" name="address" id="address" value="{{ old('address', !empty($row->address) ? $row->address : '') }}" {{ !empty($readonly) ? 'disabled' : '' }}>
    </div>
</div>

@if(empty($is_profile))
<div class="form-group">
    <label class="col-lg-2 control-label" for="role">Perfil <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <select name="role" id="role" class="form-control" {{ !empty($readonly) ? 'disabled' : '' }}>
            <option value="">Selecione uma opção</option>
            @foreach($roles as $value => $label)
            <option value="{{ $value }}" {{ old('role', !empty($row->role) ? $row->role : '') == $value ? 'selected' : '' }}>{{ $label }}</option>
            @endforeach
        </select>
    </div>
</div>
@endif

<div class="participante hidden">
    <div class="form-group">
        <label class="col-lg-2 control-label" for="email">Agência <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="agency" id="agency" value="{{ old('agency', !empty($row->agency) ? $row->agency : '') }}" {{ !empty($readonly) ? 'disabled' : '' }}>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="director_partner">Diretoria/Sócios <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="director_partner" id="director_partner" value="{{ old('director_partner', !empty($row->director_partner) ? $row->director_partner : '') }}" {{ !empty($readonly) ? 'disabled' : '' }}>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="email">E-mail <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="email" class="form-control" name="email" id="email" value="{{ old('email', !empty($row->email) ? $row->email : '') }}" {{ !empty($readonly) ? 'disabled' : '' }}>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label" for="phone">Telefone</label>
    <div class="col-lg-10">
        <input type="text" class="form-control phone" name="phone" id="phone" value="{{ old('phone', !empty($row->phone) ? $row->phone : '') }}" {{ !empty($readonly) ? 'disabled' : '' }}>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label" for="login">Login <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="text" class="form-control" name="login" id="login" value="{{ old('login', !empty($row->login) ? $row->login : '') }}" {{ !empty($readonly) ? 'disabled' : '' }}>
    </div>
</div>

@if(empty($is_profile))
    <div class="form-group">
        <label class="col-lg-2 control-label" for="activated">Ativo ?</label>
        <div class="col-lg-10">
            <input type="checkbox" name="activated" id="activated" value="1" {{ old('activated', !empty($row->activated) ? $row->activated : '') ? 'checked' : '' }} {{ !empty($readonly) ? 'disabled' : '' }}>
        </div>
    </div>
@endif


@if(empty($is_profile) && isset($edicoes) && !empty($edicoes) )
    <div class="form-group">
        <label class="col-lg-2 control-label" > Habilitar para as edições</label>
        <div class="col-lg-10">
            <?php $current_edicoes = old('edicoes', isset($row->edicoes_id) ? $row->edicoes_id  : array() );  ?>
            @foreach( $edicoes as $edicao )
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="edicoes[{!! $edicao->id !!}]" value="{!! $edicao->id !!}" {{ in_array($edicao->id, $current_edicoes) ? 'checked' : '' }} {{ !empty($readonly) ? 'disabled' : '' }}>
                        {!! $edicao->nome !!} ({!! $edicao->ano !!})
                    </label>
                </div>
            @endforeach
        </div>
    </div>
@endif

@if(!empty($is_profile))
<fieldset>
    <legend>Deseja alterar sua senha?</legend>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="password">Senha atual</label>
        <div class="col-lg-10">
            <input type="password" class="form-control" name="password" id="password" autocomplete="off" value="">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="new_password">Nova senha</label>
        <div class="col-lg-10">
            <input type="password" class="form-control" name="new_password" id="new_password" autocomplete="off" value="">
            <small>Você deve escolher uma senha com no mínimo de 8 caracteres, entre maiúsculas e minúsculas, números e caracteres especiais.</small>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="new_password_confirmation">Confirmação da nova senha</label>
        <div class="col-lg-10">
            <input type="password" class="form-control" name="new_password_confirmation" id="new_password_confirmation" autocomplete="off" value="">
        </div>
    </div>
</fieldset>
@endif

@if(!empty($row) && empty($readonly) && empty($is_profile) && Gate::check('usuarios.edit-password'))
<fieldset>
    <legend>Alterar senha do usuário</legend>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="new_password">Nova senha</label>
        <div class="col-lg-10">
            <input type="password" class="form-control" name="new_password" id="new_password" autocomplete="off" value="{{ old('new_password') }}">
            <small>Você deve escolher uma senha com no mínimo de 8 caracteres, entre maiúsculas e minúsculas, números e caracteres especiais.</small>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="new_password_confirmation">Confirmação da nova senha</label>
        <div class="col-lg-10">
            <input type="password" class="form-control" name="new_password_confirmation" id="new_password_confirmation" autocomplete="off" value="{{ old('new_password_confirmation') }}">
        </div>
    </div>
</fieldset>
@endif

@if(empty($readonly))
    @include('campos-obrigatorios')
@endif