@extends('layouts.panel')

@section('page-title')
    Usuários
@endsection

@section('filtros')
    @include("usuarios.partials.filtros")
@endsection

@section('content')

    @include('modal-confirm-destroy')

    @if(count($rows))
    <div class="table-responsive">
        <table class="table">
            <thead>
                <th>Nome</th>
                <th>Login</th>
                <th>E-mail</th>
                <th>Perfil</th>
                <th class="text-center">Último Login</th>
                <th class="text-center" width="60">Ativo ?</th>
                <th width="130"><a href="{{ route('usuario.create') }}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Novo</a></th>
            </thead>
            <tbody>
                @foreach($rows as $row)
                <tr data-url="{{ route('usuario.show', [ $row->id ]) }}">
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->login }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ \App\Role::getRole($row->role) }}</td>
                    <td class="text-center">{{ $row->logged_at ? date('d/m/Y H:i:s', strtotime($row->logged_at)) : '-' }}</td>
                    <td class="text-center">
                        @if($row->activated)
                        <a href="{{ route('usuario.unactive', [$row->id]) }}"><span class="label label-primary" data-toggle="tooltip" data-placement="top" title="Clique para desativá-lo"><i class="fa fa-check"></i></span></a>
                        @else
                        <a href="{{ route('usuario.active', [$row->id]) }}"><span class="label label-default" data-toggle="tooltip" data-placement="top" title="Clique para ativá-lo"><i class="fa fa-close"></i></span></a>
                        @endif
                    </td>
                    <td class="text-right">
                        <a class="btn btn-white" href="{{ route('usuario.edit', ['id' => $row->id]) }}" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="text-center">
        {!! $rows->appends(compact('search'))->render() !!}
    </div>
    @else
    <h2>Nenhum registro foi encontrado. <br/> Clique <a href="{{ route('usuario.create') }}">aqui</a> para cadastrar um novo usuário.</h2>
    @endif

@endsection