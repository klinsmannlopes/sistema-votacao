<div class="modal fade" id="modal-clear-checkboxes" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-danger">Limpar inscrições selecionadas</h3>
            </div>
            <div class="modal-body">
                <h2 class="text-center">Tem certeza que deseja limpar a seleção?</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Voltar</button>
                <button id="clear-checkboxes" type="button" class="btn btn-danger" data-loading-text="..."><i class="fa fa-eraser"></i> Limpar</button>
            </div>
        </div>
    </div>
</div>