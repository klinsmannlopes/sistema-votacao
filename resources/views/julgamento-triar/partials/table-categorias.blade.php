<div class="table-responsive">
    <table class="table">
        <thead>
            <th>Categoria </th>
            <th>Status</th>
        </thead>
        <tbody>

        @if(\Auth::user()->role === \App\Role::JUIZ_SOLIDARIO)
            @foreach($rows as $row)
                <tr>
                    @if($row->abreviatura === "CAMPA" and \Auth::user()->role === \App\Role::JUIZ_SOLIDARIO)
                        <td>{{ $row->nome }}</td>
                        <td>
                            @if($row->is_triagem)
                                @if($row->triagem(null, \Auth::getUser()->id)->count())
                                    <b style="color: forestgreen;">Triagem Realizada</b>
                                @else
                                    <a href="{{ route('julgamento-triar', ['categoria_id' => $row->id ]) }}">Realizar Triagem</a>
                                @endif
                            @else
                                Categoria levada diretamente para Julgamento
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
        @else
            @foreach($rows as $row)
                <tr>
                    <td>{{ $row->nome }}</td>
                    <td>
                        @if($row->is_triagem)
                            @if($row->triagem(null, \Auth::getUser()->id)->count())
                                <b style="color: forestgreen;">Triagem Realizada</b>
                            @else
                                <a href="{{ route('julgamento-triar', ['categoria_id' => $row->id ]) }}">Realizar Triagem</a>
                            @endif
                        @else
                            Categoria levada diretamente para Julgamento
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif

        </tbody>
    </table>
</div>
