<style>
    .table tbody td { cursor: pointer; } { cursor: auto; }
</style>

<div class="table-responsive">
    <table class="table">
        <thead>
            <th width="30">&nbsp;</th>
            <th class="text-center">Nº da Inscrição</th>
            <th>Campanha</th>
            <th>Anunciante</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td><input type="checkbox" name="inscricoes[]" class="inscricoes check" value="{{ $row->id }}" /></td>
                <td width="130" class="text-center"><a href="{{ route($row->categoria->is_fixa() ? 'inscricoes.detalhes' : 'inscricoes.pecas', [$row->id]) }}" target="_blank">{{ $row->present()->numeroInscricao() }}</a></td>
                <td>{{ $row->campanha }}</td>
                <td>{{ $row->anunciante }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>