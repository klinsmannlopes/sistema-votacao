@extends('layouts.panel')

@section('page-title')
    Triar - {{ $categoria->nome }}
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

    @include("{$namespace}.partials.errors")
    @include("{$namespace}.partials.modal-clear-checkboxes")

    @if(count($rows))

        <h2><i>"Visualize as inscrições através do link e indique para a próxima fase as 10 que considerar melhores. Se você errar e quiser limpar suas escolhas para começar de novo, basta clicar no botão ‘Limpar’, ao final da tabela. Ao terminar, clique no botão ‘Salvar’, também ao final da tabela."</i></h2>

        <form id="triar" method="post" action="{{ route("{$namespace}.save", [ $categoria->id ] ) }}">

            {!! csrf_field() !!}

            @include("{$namespace}.partials.table")

            <div class="text-center">
                <a class="btn btn-white" href="{{ route("{$namespace}.categorias") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button type="button" id="cofirm-clear-checkboxes" class="btn btn-danger"><i class="fa fa-eraser"></i> Limpar</button>
                <button type="button" id="enviar-triagem" class="btn btn-info"><i class="fa fa-save"></i> Salvar</button>
            </div>

        </form>
    @else
        <h2>Nenhuma das inscrições está com o status igual a: Válida.</h2>
    @endif

@endsection