@extends('layouts.panel')

@section('page-title')
    {{ $title }}
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

    @include("{$namespace}.partials.errors")

    @if(count($rows))
        @include("{$namespace}.partials.table")
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection