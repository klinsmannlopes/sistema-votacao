@if(count($jurados = $row->juradosQueNaoTriaram()))
<h5>Jurado(s)</h5>
<ol>
    @foreach($jurados as $jurado)
    <li>{{ $jurado->name }}</li>
    @endforeach
</ol>
@endif

@if(count($juizes = $row->juizesQueNaoTriaram()))
<h5>Juíz(es)</h5>
<ol>
    @foreach($juizes as $juiz)
    <li>{{ $juiz->name }}</li>
    @endforeach
</ol>
@endif

@if(count($juizesolidarios = $row->juizSolidarioQueNaoTriaram()) and $row->abreviatura === "CAMPA")
    <h5>Juíz(es) Solidários</h5>
    <ol>
        @foreach($juizesolidarios as $juiz)
            @if($juiz->role === \App\Role::JUIZ_SOLIDARIO)
                <li>{{ $juiz->name }} </li>
            @endif
        @endforeach
    </ol>
@endif