<div class="form-group">
    <select name="categoria_id" id="categoria_id" class="form-control" data-url="{{ route($namespace, [ 'id' ]) }}">
        <option value="">Selecione uma opção</option>
        @foreach($combobox->lists('nome', 'id') as $value => $label)
            <option value="{{ $value }}" {{ $categoria->id == $value ? 'selected' : '' }}>{{ $label }}</option>
        @endforeach
    </select>
</div>