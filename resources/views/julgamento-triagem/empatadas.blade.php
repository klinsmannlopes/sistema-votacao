@extends('layouts.panel')

@section('page-title')
    Aguardando Desempate da Triagem - {{ $categoria->nome }}
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
    <script src="{{ url('assets/modules/js/combobox-categorias.js') }}"></script>
@endsection

@section('content')

    @include("{$namespace}.partials.errors")

    @include("{$namespace}.partials.combobox-empatadas")

    @if(count($rows))
        <form id="triar" method="post" action="{{ route('julgamento-triar.save', [ $categoria->id ] ) }}">

            {!! csrf_field() !!}

            @include("{$namespace}.partials.table-empatadas")

        </form>
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection