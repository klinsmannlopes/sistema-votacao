@extends('layouts.panel')

@section('page-title')
    Cadastro de Participantes
@endsection

@section('content')

    @include('errors')

    <form method="post" action="{{ route('cadastro-de-participantes.store') }}" class="form-horizontal" enctype="multipart/form-data">

        {!! csrf_field() !!}

        <div class="form-group">
            <label class="col-lg-2 control-label" for="participantes">Lista de participantes <b class="text-danger">*</b></label>
            <div class="col-lg-10">
                <input type="file" class="form-control" name="participantes" id="participantes">
                <small>Selecione uma arquivo do tipo .xls no formato idealizado para funcionalidade.</small>
            </div>
        </div>

        @include('campos-obrigatorios')

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route('dashboard') }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button class="btn btn-info" type="submit"><i class="fa fa-plus"></i> Cadastrar</button>
            </div>
        </div>
    </form>
@endsection