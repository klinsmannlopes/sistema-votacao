<div class="form-group">
    <label class="col-lg-2 control-label" for="is_ativo">Habilitar Categoria ?</label>
    <div class="col-lg-10">
        <ul class="list-unstyled">
            <li><label><input type="radio" name="is_ativo" value="1" {{ old('is_ativo', isset($row) ? $row->is_ativo : 1) ? 'checked' : '' }}> Sim</label></li>
            <li><label><input type="radio" name="is_ativo" value="0" {{ old('is_ativo', isset($row) ? !$row->is_ativo : 0) ? 'checked' : '' }}> Não</label></li>
        </ul>
    </div>
</div>