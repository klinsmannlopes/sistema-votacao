@include('errors')
<fieldset>
    <legend>Dados da Categoria</legend>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="nome">Nome <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="nome" id="nome" autocomplete="off" value="{{ old('nome', isset($row->nome_raw) ? $row->nome_raw : '') }}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="abreviatura">Abreviatura <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="abreviatura" id="abreviatura" autocomplete="off" maxlength="5" value="{{ old('abreviatura', isset($row->abreviatura) ? $row->abreviatura : '') }}">
            <small>Altere a abreivatura sugerida caso julgue necessário.</small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label" for="categoria_id">Categoria Pai</label>
        <div class="col-lg-10">
            {!! getSelectCategoria('parent_id', old('parent_id', isset($row) ? $row->parent_id : null), array(), true) !!}
        </div>
    </div>

    @include("{$namespace}.partials.field-activated")
    @include("{$namespace}.partials.field-has-pis")
</fieldset>

<fieldset>
    <legend>Habilitar para as edições</legend>
    @include("{$namespace}.partials.field-edicoes")
</fieldset>

@if( ! is_edicao_profissional() )
    <fieldset>
        <legend>Peso da categoria (Premio Master)</legend>
        @include("{$namespace}.partials.field-peso")
    </fieldset>
@endif


<fieldset>
    <legend>Parametrização</legend>
    <div class="form-group">
        <label class="col-lg-2 control-label" for="qtd_pecas">Quantidade de peças <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <input type="text" class="form-control" name="qtd_pecas" id="qtd_pecas" autocomplete="off" value="{{ old('qtd_pecas', isset($row->qtd_pecas) ? $row->qtd_pecas : '') }}">
            <div>
                @foreach($qtdPecasTipo as $value => $label) <?php $qtd_pecas_tipo = old('qtd_pecas_tipo', !empty($row->qtd_pecas_tipo) ? $row->qtd_pecas_tipo : null); ?>
                <label style="margin: 5px 10px 0 0;"><input type="radio" name="qtd_pecas_tipo" value="{{ $value }}" {{ $qtd_pecas_tipo == $value || (!$qtd_pecas_tipo && $value == \App\Categoria::QTD_PECAS_TIPO_EXATAMENTE) ? 'checked' : '' }}> {{ $label }}</label>
                @endforeach
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Tipo de arquivo <b class="text-danger">*</b></label>
        <div class="col-lg-10">
            <ul class="list-unstyled">
                @foreach($tiposDeAnexo as $value => $label)
                <li><label><input type="checkbox" name="tipos_de_anexo[]" value="{{ $value }}" {{ in_array($value, old('tipos_de_anexo', !empty($row->tipos_de_anexo) ? unserialize($row->tipos_de_anexo) : [])) ? 'checked' : '' }}> {{ $label }}</label></li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Realizar Triagem?</label>
        <div class="col-lg-10">
            <ul class="list-unstyled">
                <li><label><input type="radio" name="is_triagem" value="1" {{ old('is_triagem', isset($row) ? $row->is_triagem : 1) ? 'checked' : '' }}> Sim</label></li>
                <li><label><input type="radio" name="is_triagem" value="0" {{ old('is_triagem', isset($row) ? !$row->is_triagem : 0) ? 'checked' : '' }}> Não</label></li>
            </ul>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label">Multiplas OPEC?</label>
        <div class="col-lg-10">
            <ul class="list-unstyled">
                <li><label><input type="radio" name="is_multiplo_opec" value="1" {{ old('is_multiplo_opec', isset($row) ? $row->is_multiplo_opec : 1) ? 'checked' : '' }}> Sim</label></li>
                <li><label><input type="radio" name="is_multiplo_opec" value="0" {{ old('is_multiplo_opec', isset($row) ? !$row->is_multiplo_opec : 0) ? 'checked' : '' }}> Não</label></li>
            </ul>
        </div>
        <label id="qtd_opec" class="col-lg-2 control-label" for="qtd_opec">Quantidade de OPEC <b class="text-danger"></b></label>    
        <div class="col-lg-10" id="qtd_opec">
            <input type="number" class="form-control" name="qtd_opecs" min="0" max="5" id="qtd_opec" autocomplete="off" value="{{ old('qtd_opecs', isset($row->qtd_opecs) ? $row->qtd_opecs : '') }}">
        </div>
    </div>

    @include("{$namespace}.partials.field-criterios")
</fieldset>

@include('campos-obrigatorios')
