<div class="form-group">
    <label class="col-lg-2 control-label" for="is_ativo">Obrigatório PI's ?</label>
    <div class="col-lg-10">
        <ul class="list-unstyled">
            <li><label><input type="radio" name="has_pis" value="1" {{ old('has_pis', isset($row) ? $row->has_pis : 1) ? 'checked' : '' }}> Sim</label></li>
            <li><label><input type="radio" name="has_pis" value="0" {{ old('has_pis', isset($row) ? !$row->has_pis : 0) ? 'checked' : '' }}> Não</label></li>
        </ul>
    </div>
</div>