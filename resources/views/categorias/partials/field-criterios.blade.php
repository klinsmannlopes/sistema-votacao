<div class="form-group">
    <label class="col-lg-2 control-label">Critérios <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        @if(count($criterios))
            <ul class="list-unstyled">
                @foreach($criterios as $criterio)
                    <li><label><input type="checkbox" name="criterios[]" value="{{ $criterio->id }}" {{ in_array($criterio->id, old('criterios', !empty($rowCriterios) ? $rowCriterios : [])) ? 'checked' : '' }}> {{ $criterio->nome }}</label></li>
                @endforeach
            </ul>
        @else
            <h3 class="text-danger">Não há critérios disponíveis. Cadastre e habilite critérios em Parametrização > Critérios</h3>
        @endif
    </div>
</div>