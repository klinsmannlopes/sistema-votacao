@if(empty($is_profile) && isset($edicoes) && !empty($edicoes) )
<div class="form-group">
     <label class="col-lg-2 control-label">Edições <b class="text-danger">*</b></label>
    <div class="col-lg-10 check-edicoes">
        <?php  $current_edicoes = isset($row) ? old('edicoes', $row->edicoes()->get()->pluck('id')->toArray()) : array(); ?>
         @foreach( $edicoes as $edicao )
            <?php $selected = in_array($edicao->id, $current_edicoes); ?>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="edicoes[{!! $edicao->id !!}]" value="{!! $edicao->id !!}" {{ $selected ? 'checked' : '' }} {{ !empty($readonly) ? 'disabled' : '' }}>
                    {!! $edicao->nome !!} ({!! $edicao->ano !!})
                </label>
            </div>
        @endforeach
    </div>
</div>
@endif
