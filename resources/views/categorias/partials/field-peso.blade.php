<div class="form-group">
    <label class="col-lg-2 control-label" for="qtd_pecas">Peso <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="number" class="form-control" name="peso" id="peso" autocomplete="off" value="{{ old('peso', isset($row->peso) ? $row->peso : '') }}">
    </div>
</div>