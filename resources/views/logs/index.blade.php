@extends('layouts.panel')

@section('page-title')
    Logs
@endsection

@section('filtros')
    @include("{$namespace}.partials.filtros")
@endsection

@section('content')

    @if(count($rows))

        @include("{$namespace}.partials.table")

        <div class="text-center">
            {!! $rows->appends(compact('search'))->render() !!}
        </div>
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection