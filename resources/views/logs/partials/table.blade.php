<div class="table-responsive">
    <table class="table">
        <thead>
            <th>Ação</th>
            <th>Usuário</th>
            <th>Titulo</th>
            <th>Descrição</th>
            <th>Data/Hora</th>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td>{{ $row->action }}</td>
                <td>{{ ! empty($row->user->name) ? $row->user->name : '-' }}</td>
                <td>{{ $row->title }}</td>
                <td>{{ $row->description }}</td>
                <td>{{ date('d/m/Y H:i', strtotime($row->created_at)) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
