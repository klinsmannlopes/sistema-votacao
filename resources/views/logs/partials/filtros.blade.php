<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Filtros</h5>
    </div>
    <div class="ibox-content">
        <form method="get" action="">
            <div class="form-group">
                <label for="search">Buscar por:</label>
                <input type="text" class="form-control" name="search" id="search" autocomplete="off" value="{{ old('search', $search) }}">
            </div>
            <div class="form-group">
                <button class="btn btn-white"><i class="fa fa-search"></i> Filtrar</button>
            </div>
        </form>
    </div>
</div>