@extends('layouts.panel')

@section('page-title')
    Listagem das inscrições
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('filtros')
    @include("{$namespace}.partials.filtros")
@endsection

@section('content')

    @include('modal-confirm-destroy')

    @if(empty($periodoDeInscricao))
        <div class="alert alert-warning text-center" role="alert"><b>O período de cadastro de novas inscrições está encerrado.</b></div>
    @endif

    @if(count($rows))

        @include("{$namespace}.partials.table")


        <div class="text-center">
            {!! $rows->appends(compact('search', 'status', 'categoria_id'))->render() !!}
        </div>
    @else
        <h2>
            Nenhum registro foi encontrado.

            @if(Gate::check("inscricoes-participante") && $periodoDeInscricao)
                <br/> Clique <a href="{{ route("{$namespace}.create") }}">aqui</a> para adicionar uma nova inscrição.
            @endif
        </h2>
    @endif
@endsection