<div class="table-responsive">
    <table class="table inscricoes_table">
        <thead>
        <th width="120" class="text-center hidden-print">Número Inscricao</th>
        <th width="120" class="text-center hidden-print">Agência</th>
        <th width="120" class="text-center hidden-print">Nome</th>
        <th class="text-center">Categoria</th>
        <th class="text-center">Defesa</th>
        <th class="text-center">Status</th>
        <th class="actions text-right hidden-print">
            @if(Gate::check("inscricoes-participante") && $periodoDeInscricao)
                <a class="btn btn-info" href="{{ route("{$namespace}.create") }}"><i class="fa fa-plus"></i> Novo</a>
            @endif
        </th>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                <td class="text-center">{{$row->present()->numeroInscricao()}}</td>
                <td class="text-center">{{$row->user->agency}}</td>
                <td class="text-center">{{$row->nome}}</td>
                <td class="text-center">{{empty($row->categoria->nome) ? '-' : $row->categoria->nome }}</td>
                <td class="text-center" style="text-align: justify;">{{$row->defesa}}</td>
                <td class="text-center">{!! $row->present()->inscricaoStatusColored() !!}</td>
                <td>
                    <a target="_blank" style="float: right;" href="{{exibirPdf($row->nome, $row->present()->numeroInscricao())}}" class="btn btn-white" data-toggle="tooltip" title="Ficha de Inscrição"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>

                    @if(Gate::check("inscricoes"))
                    <a  style="float: right;" href="{{ route("{$namespace}.show", [ $row->id ]) }}" class="btn btn-white" data-toggle="tooltip" title="Detalhes"><i class="fa fa-eye"></i></a>
                    @endif

                    @if(Gate::check("inscricoes-participante"))
                        @if($periodoDeInscricao)
                        <a class="btn btn-white" style="float: right;" href="{{ route('inscricao-profissional.edit', ['id' => $row->id]) }}" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                        @endif
                    @endif
                    @include('form-destroy', [ 'route' => "{$namespace}.destroy", 'id' => $row->id ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>