<b class="text-success">Arquivo enviado com sucesso:</b> clique <a target="_blank" href="{{ $newFileUrl }}">aqui</a> para visualizá-lo.
&nbsp;<button type="button" class="btn btn-xs btn-danger remover-arquivo"><i class="fa fa-close"></i> Remover</button>

<input type="hidden" name="curriculo_old" value="{{ $newArquivo }}">