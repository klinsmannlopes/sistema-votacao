<html>
    <head>
        <meta http-equiv="Content-Type" content="text/csv; charset=utf-8" />
    </head>
    <body>
        <table>
            <tbody>
                <tr>
                    <td align="center"><b>Nº da Inscrição</b></td>
                    <td align="center"><b>Agência</b></td>
                    <td align="center"><b>Nome</b></td>
                    <td align="center"><b>Categoria</b></td>
                    <td align="center"><b>Defesa</b></td>
                    <td align="center"><b>Status</b></td>
                </tr>

                @foreach($rows as $row)
                <tr>
                    <td align="center">{{ $row->present()->numeroInscricao() }}</td>
                    <td align="center">{{ $row->user->agency }}</td>
                    <td align="center">{{ $row->nome }}</td>
                    <td align="center">{{ $row->categoria->nome }}</td>                   
                    <td align="center">{{ $row->defesa }}</td>
                    <td align="center">
                        {{ $row->present()->inscricaoStatus() }}

                        @if($row->is_cancelada())
                        ({{ $row->justificativa }})
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>