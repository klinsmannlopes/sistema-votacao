@include('errors')

<div class="form-group">
    <label class="col-lg-2 control-label" for="agencia">Agência <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="text" class="form-control" name="agencia" id="agencia" value="{{ $agencia }}" disabled>
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label" for="endereco">Endereço <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="text" class="form-control" name="endereco" id="endereco" autocomplete="off" value="{{ $endereco }}" disabled>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="nome">Nome Completo <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input {{ !empty($readonly) ? 'disabled' : '' }} type="text" class="form-control" name="nome" id="nome" autocomplete="off" value="{{ old('nome', !empty($row->nome) ? $row->nome : '') }}" >
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="categoria_id">Categoria <b class="text-danger">*</b></label>
    <div class="col-lg-10">
            @if(!empty($readonly))
                <input type="text" disabled class="form-control"  value="{{ empty($row->categoria->nome) ? '-' : $row->categoria->nome }}" >
            @else
                {!! getSelectCategoria('categoria_id', isset($row->categoria_id) ? $row->categoria_id : null) !!}
            @endif
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="curriculo">Currículo<b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="file"  {{ !empty($readonly) ? 'disabled' : '' }} class="form-control curriculo" name="curriculo" id="curriculo" autocomplete="off" value="{{ old('curriculo', !empty($row->curriculo) ? $row->curriculo : '') }}" >
        <small>Arquivo com no máximo 20mb de acordo com o tipo da mídia selecionada</small>
        <div class="progress hidden">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemax="100"></div>
        </div>
        <div class="uploaded">
            @if($arquivo_old = old("curriculo_old", (!empty($row->curriculo) ? $row->curriculo : '')))
                <a class="btn btn-white" target="_blank" href="{{url_uploads('curriculos'.DIRECTORY_SEPARATOR.$arquivo_old)}}"><i class="fa fa-file"></i> <b>Arquivo atual:</b> Clique aqui para visualizá-lo.</a></small>
                <input type="hidden" name="curriculo_old" value="{{$arquivo_old}}">
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="foto">Foto 3x4<b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <input type="file"  {{ !empty($readonly) ? 'disabled' : '' }} class="form-control" name="foto" id="foto" autocomplete="off" value="{{ old('foto', !empty($row->foto) ? $row->foto : '') }}" >
        <small>Arquivo com no máximo 20mb de acordo com o tipo da mídia selecionada</small>
        <div class="progress hidden">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemax="100"></div>
        </div>
        <div class="uploaded">
            @if($arquivo_old = old("foto_old", (!empty($row->foto) ? $row->foto : '')))
                    <a class="btn btn-white" target="_blank" href="{{url_uploads('fotos'.DIRECTORY_SEPARATOR.$arquivo_old)}}"><i class="fa fa-file"></i> <b>Arquivo atual:</b> Clique aqui para visualizá-lo.</a></small>
                    <input type="hidden" name="foto_old" value="{{$arquivo_old}}">
            @endif
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="defesa_geral">Defesa  <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <textarea rows="14"  {{ !empty($readonly) ? 'disabled' : '' }} class="form-control"  name="defesa" id="defesa"  >{{ old('defesa', !empty($row->defesa) ? $row->defesa : '') }}</textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label" for="concordo">Concordo com os Termos <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <label><input name="corcodo" id="concordo" value="1" type="checkbox" {{ !empty($readonly) || (!empty($row) && $row->is_aguardando_validacao()) ? 'checked  disabled' : '' }} required >&nbsp;&nbsp; Autorizo os organizadores do GP Verdes Mares a utilizar os materiais inscritos para divulgação do Prêmio.</label>

</div>


@if(!empty($row->justificativa) && isset($row->justificativa))
<div class="form-group">
    <label class="col-lg-2 control-label" for="defesa_geral">Justificativa do cancelamento <b class="text-danger">*</b></label>
    <div class="col-lg-10">
        <textarea style="width: 100%" rows="5" disabled cqlass="form-control"  name="justificativa" id="justificativa"  >{{$row->justificativa}}</textarea>
    </div>
</div>
@endif


@include('campos-obrigatorios')