<div class="modal fade" id="modal-cancelar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-danger">Justificativa de Cancelamento</h3>
            </div>
            <div class="modal-body">
                <h2 class="text-center">Escreva o mais breve possível, o porquê da inscrição está sendo cancelada:</h2>
                <form method="post" id="cancelar" action="{{ route("{$namespace}.cancelar", [ $row->id ]) }}">

                    {!! csrf_field() !!}

                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group">
                        <textarea name="justificativa" id="justificativa-cancelamento" class="form-control"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Voltar</button>
                <button type="button" id="confirmar-cancelar" class="btn btn-danger" data-loading-text="..."><i class="fa fa-ban"></i> Cancelar a Inscrição</button>
            </div>
        </div>
    </div>
</div>