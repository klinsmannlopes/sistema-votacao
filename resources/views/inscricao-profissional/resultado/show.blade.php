@extends('layouts.panel')

@section('page-title')
    Visualizar Aprovados
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('styles')
    @include("{$namespace}.partials.styles")
@endsection

@section('content')

    @include("{$namespace}.resultado.partials.table-inscricoes")

@endsection
