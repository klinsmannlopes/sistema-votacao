@extends('layouts.panel')

@section('page-title')
    Resultados por categoria
@endsection

@section('content')

    @if(count($rows))
        @include("{$namespace}.resultado.partials.table-categoria")
    @else
        <h2>Nenhum registro foi encontrado.</h2>
    @endif

@endsection