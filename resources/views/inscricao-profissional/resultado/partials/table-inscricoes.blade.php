<div class="table-responsive">
    <table class="table inscricoes_table">
        <thead>
        <th width="120" class="text-center hidden-print">Número Inscricao</th>
        <th width="120" class="text-center hidden-print">Nome</th>
        <th class="text-center">Categoria</th>
        <th class="text-center">Defesa</th>
        <th class="text-center">Status</th>

        <th class="actions text-right hidden-print">

        </th>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                <td class="text-center">{{$row->present()->numeroInscricao()}}</td>
                <td class="text-center">{{$row->nome}}</td>
                <td class="text-center">{{empty($row->categoria->nome) ? '-' : $row->categoria->nome }}</td>
                <td class="text-center" style="text-align: justify;">{{$row->defesa}}</td>
                <td class="text-center">{!! $row->present()->inscricaoStatusColored() !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>