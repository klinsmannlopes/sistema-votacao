<div class="table-responsive">
    <table class="table">
        <thead>
        <th>Categoria</th>
        </thead>
        <tbody>
        @foreach($rows as $row)
            <tr>
                <td> <a href="{{  route("profissional-resultados.show", [ $row->id ])  }}"> {{ $row->nome }}</a></td>
                <td> <a href="{{  route("profissional-resultados.zipper", [ $row->id ])  }}"><i class="fa fa-download"></i></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
