@extends('layouts.panel')

@section('page-title')
    Visualizar Inscrição
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

   @include("{$namespace}.partials.modal-cancelar")

    <form class="form-horizontal" method="post" action="{{ route($route, [ $row->id ]) }}">
        {!! csrf_field() !!}
        @include("{$namespace}.partials.form")
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route("{$namespace}.index") }}"><i class="fa fa-arrow-left"></i> Voltar</a>

                @if(Gate::check('inscricoes'))
                    <button {{ $buttonsDisabled }} type="button" class="btn btn-danger show-modal-cancelar"><i class="fa fa-ban"></i> Cancelar Inscrição</button>
                    <button {{ $buttonsDisabled }} class="btn btn-info"><i class="fa fa-check"></i> Aprovar Inscrição</button>
                @endif
            </div>
        </div>
    </form>
@endsection