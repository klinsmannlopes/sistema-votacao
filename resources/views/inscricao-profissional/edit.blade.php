@extends('layouts.panel')

@section('page-title')
    Editar Inscrição
@endsection

@section('scripts')
    @include("{$namespace}.partials.scripts")
@endsection

@section('content')

    @include("{$namespace}.partials.modal-cancelar-participante")

    <form id="edit-form" method="post" class="form-horizontal" action="{{route("{$namespace}.update", [ $row->id ]) }}" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="row_id" value="{{ $row->id }}">

        @include("{$namespace}.partials.form")

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <a class="btn btn-white" href="{{ route("{$namespace}.index") }}"><i class="fa fa-arrow-left"></i> Voltar</a>
                <button {{ $buttonsDisabled }} type="button" class="btn btn-danger" id="show-modal-cancelar-participante"><i class="fa fa-ban"></i> Cancelar Inscrição</button>
                <button {{ $buttonsDisabled }} class="btn btn-info" type="submit"><i class="fa fa-save"></i> Salvar</button>
            </div>
        </div>
    </form>
@endsection