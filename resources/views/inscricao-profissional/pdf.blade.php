<html>
<head>
    <style>
        {!! fileGetContents('assets/vendor/css/bootstrap.min.css') !!}
        {!! fileGetContents('assets/modules/css/inscricao.css') !!}

        .form-control{
            height: auto;
            text-align: justify;
        }
        .form-horizontal {
            float: left;
            width: 70%;
        }
        .title{
            text-align: center;
        }
        .foto {
            float: left;
            width: 3cm;
            height: 4cm;
            border: 1px solid #999;
            margin-right: 20px;
        }
    </style>

</head>
<body>
    <div class="container">
        <div class="title"> <h3> FICHA DE INSCRIÇÃO {{$row->present()->numeroInscricao()}} </h3> </div>

        <hr>

        <div class="foto">
            <img width="100%" height="100%" src="data:image/jpeg;base64, {{ base64_encode(fileGetContents($pathFoto)) }}">
        </div>

        <div class="form-horizontal">

            <div class="form-group">
                <label class="col-lg-2 control-label" for="nome">Nome Completo:</label>
                <div class="col-lg-10">
                    <div class="form-control"> {{$row->nome}}</div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="categoria_id">Categoria:</label>
                <div class="col-lg-10">
                    <div class="form-control">{{$row->categoria->nome}}</div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="agencia">Agência:</label>
                <div class="col-lg-10">
                    <div class="form-control">{{ $row->user->name }}</div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="endereco">Endereço:</label>
                <div class="col-lg-10">
                    <div class="form-control"> {{  $row->user->address }}</div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label" for="defesa_geral">Defesa:</label>
                <div class="col-lg-10">
                    <div  class="form-control">{{ $row->defesa }}</div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

