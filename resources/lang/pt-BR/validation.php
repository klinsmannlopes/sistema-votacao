<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => '<b>":attribute"</b> deve ser aceito.',
    'active_url'           => '<b>":attribute"</b> não é uma URL válida.',
    'after'                => '<b>":attribute"</b> deve ser uma data depois de :date.',
    'alpha'                => '<b>":attribute"</b> deve conter somente letras.',
    'alpha_dash'           => '<b>":attribute"</b> deve conter letras, números e traços.',
    'alpha_num'            => '<b>":attribute"</b> deve conter somente letras e números.',
    'array'                => '<b>":attribute"</b> deve ser um array.',
    'before'               => '<b>":attribute"</b> deve ser uma data antes de <b>":date"</b>.',
    'before_equal'         => '<b>":attribute"</b> deve ser uma data antes ou igual a <b>":date"</b>.',
    'between'              => [
        'numeric' => '<b>":attribute"</b> deve estar entre :min e "<b>:max</b>".',
        'file'    => '<b>":attribute"</b> deve estar entre :min e "<b>:max</b>".',
        'string'  => '<b>":attribute"</b> deve estar entre :min e "<b>:max</b>" caracteres.',
        'array'   => '<b>":attribute"</b> deve ter entre :min e "<b>:max</b>" itens.',
    ],
    'boolean'              => '<b>":attribute"</b> deve ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação de <b>":attribute"</b> não confere.',
    'date'                 => '<b>":attribute"</b> não é uma data válida.',
    'date_format'          => '<b>":attribute"</b> não confere com o formato :format.',
    'different'            => '<b>":attribute"</b> e :other devem ser diferentes.',
    'digits'               => '<b>":attribute"</b> deve ter :digits dígitos.',
    'digits_between'       => '<b>":attribute"</b> deve ter entre :min e "<b>:max</b>" dígitos.',
    'email'                => 'Informe um <b>":attribute"</b> válido.',
    'filled'               => '<b>":attribute"</b> é um campo obrigatório.',
    'exists'               => 'O <b>":attribute"</b> selecionado é inválido.',
    'image'                => 'O campo <b>":attribute"</b> deve ser uma imagem.',
    'in'                   => '<b>":attribute"</b> é inválido.',
    'integer'              => '<b>":attribute"</b> deve ser um inteiro.',
    'ip'                   => '<b>":attribute"</b> deve ser um endereço IP válido.',
    'max'                  => [
        'numeric' => '<b>":attribute"</b> não deve ser maior que "<b>:max</b>".',
        'file'    => '<b>":attribute"</b> não deve ter mais que "<b>:max</b>".',
        'string'  => '<b>":attribute"</b> não deve ter mais que "<b>:max</b>" caracteres.',
        'array'   => '<b>":attribute"</b> não pode ter mais que "<b>:max</b>" itens.',
    ],
    'mimes'                => '<b>":attribute"</b> deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O campo <b>":attribute"</b> deve ser no mínimo <b>:min</b>.',
        'file'    => 'O campo <b>":attribute"</b> deve ter no mínimo <b>:min</b>.',
        'string'  => 'O campo <b>":attribute"</b> deve ter no mínimo <b>:min</b> caracteres.',
        'array'   => 'O campo <b>":attribute"</b> deve ter no mínimo <b>:min</b> itens.',
    ],
    'not_in'               => 'O <b>":attribute"</b> selecionado é inválido.',
    'numeric'              => '<b>":attribute"</b> deve ser um número.',
    'regex'                => 'O formato de <b>":attribute"</b> é inválido.',
    'required'             => 'O campo <b>":attribute"</b> deve ser preenchido.',
    'required_if'          => 'O campo <b>":attribute"</b> deve ser preenchido quando <b>":other"</b> é igual a <b>":value"</b>.',
    'required_with'        => 'O campo <b>":attribute"</b> deve ser preenchido quando <b>":values"</b> está presente.',
    'required_with_all'    => 'O campo <b>":attribute"</b> deve ser preenchido quando <b>":values"</b> estão presentes.',
    'required_without'     => 'O campo <b>":attribute"</b> deve ser preenchido quando <b>":values"</b> não está presente.',
    'required_without_all' => 'O campo <b>":attribute"</b> deve ser preenchido quando nenhum destes estão presentes: :values.',
    'same'                 => '<b>":attribute"</b> e :other devem ser iguais.',
    'size'                 => [
        'numeric' => 'O campo <b>":attribute"</b> deve ser <b>:size</b>.',
        'file'    => 'O campo <b>":attribute"</b> deve ter <b>:size</b>.',
        'string'  => 'O campo <b>":attribute"</b> deve ter <b>:size</b> caracteres.',
        'array'   => 'O campo <b>":attribute"</b> deve conter <b>:size</b> itens.',
    ],
    'timezone'             => '<b>":attribute"</b> deve ser uma timezone válida.',
    'unique'               => 'Este <b>":attribute"</b> não está disponível, tente outro.',
    'url'                  => 'O formato de <b>":attribute"</b> é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

    'extensions'            => '<b>":attribute"</b> deve ser um arquivo do tipo: :values.',
];
