<?php 

return [
    'categoria'=>[
        'created'=>'Categoria :nome criado',
        'deleted'=>'Categoria :nome Deletado',
    ],
    'criterio'=>[
        'created'=>'Criterio  :nome criado',
        'deleted'=>'Criterio  :nome Deletado',
    ],
    'parametrizacao'=>[
        'created'=>'Parametrização :key criado',
        'deleted'=>'Parametrização :key Deletado',
    ],
    'user'=>[
        'created'=>'Usuário :name criado',
        'deleted'=>'Usuário :name Deletado',
    ]
];