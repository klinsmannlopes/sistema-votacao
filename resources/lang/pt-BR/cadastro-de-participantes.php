<?php

return [
    'store.success' => "Importação realizada com sucesso!<br/><b>:qtd</b> participantes foram criados com sucesso!",
    'store.error' => "Falha na criação de <b>:qtd</b>, participantes::html",
];