<?php

return [
    'stored' => 'Registro <b>"cadastrado"</b> com sucesso!',
    'updated' => 'Registro <b>"editado"</b> com sucesso!',
    'destroyed' => 'Registro <b>"removido"</b> com sucesso!',
    'activated' => 'Registro <b>"ativado"</b> com sucesso!',
    'unactivated' => 'Registro <b>"desativado"</b> com sucesso!',
];