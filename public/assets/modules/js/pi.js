Handlebars.registerHelper('inputName', function(root, index, field) {

    var name = root.input_name || "pecas";

    field = field ? "["+field+"]" : "";

    if( ( typeof root.peca_index == "number" || typeof root.peca_index == "string" ) && root.peca_index != ""){
        name = root.peca_index ? name+"["+root.peca_index+"]" : name;
        return name+"[pis]["+index+"]"+field;
    }else{
        return name+"["+index+"]"+field;
    }

});

Handlebars.registerHelper('inputReadonly', function(root) {
    if( typeof root.readonly != "undefined" && root.readonly ){
        return "readonly disabled";
    }

    return "";
});

Handlebars.registerPartial('piInputNumber','<input type="text" style="width: 75px;" required class="form-control" name="{{inputName @root index \'pi_numero\'}}" {{inputReadonly @root}} value="{{ pi_numero }}" {{ disabled }}>');

Handlebars.registerPartial('piHiddenID',
    '{{#if id}}' +
        '<input type="hidden" name="{{inputName @root index \'id\'}}" value="{{ id }}">' +
    '{{/if}}');

Handlebars.registerPartial('piInputFile',
    '{{#if canUpload}}' +
        '<input type="file" class="form-control pi" {{inputReadonly @root}} name="{{inputName @root index \'pdf\'}}" >' +
        '<div class="progress hidden">' +
            '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemax="100"></div>' +
        '</div>' +
    '{{/if}}');

Handlebars.registerPartial('piUploaded',
    '<div class="uploaded">' +
    '{{#if pdf }}' +
        '<small>' +
            '<a class="btn btn-white" target="_blank" href="{{ @root.uploadUrl }}/pecas-pdf/{{ pdf }}">' +
                '<i class="fa fa-file-pdf-o"></i> <b>Arquivo atual:</b> Clique aqui para visualizá-lo.' +
            '</a>' +
        '</small>' +
        '<input type="hidden" {{inputReadonly @root}} name="{{inputName @root index \'pdf-old\'}}" value="{{ pdf }}">' +
    '{{/if}}' +
    '</div>');

Handlebars.registerPartial('piRejectCheckbox',
    '{{#if @root.hasPermissionToReject }}' +
    '<div class="form-group">' +
        '<div class="col-xs-10">' +
            '<label> ' +
                '<small>' +
                    '<input type="checkbox" class="rejeitar_pi" name="{{inputName @root index \'pi_rejeitada\'}}" value="{{ id }}" {{#if pi_rejeitada }} checked {{/if}}> ' +
                'Rejeitar' +
                '</small>' +
            '</label>' +
        '</div>' +
    '</div>' +
    '{{/if}}');

Handlebars.registerPartial('piDeleteButton','{{#unless @root.readonly }}<a href="#" class="btn btn-danger" data-action="delete-pi-row"><i class="fa fa-trash"></i></a>{{/unless}}');

Handlebars.registerPartial('piRowColumns',
    '<td>{{> piInputNumber}}</td>' +
    '<td>' +
        '{{> piInputFile}}' +
        '{{> piUploaded}}' +
        '{{> piHiddenID}}' +
    '</td>' +
    '{{#if @root.hasPermissionToReject }}<td>{{> piRejectCheckbox }}</td>{{/if}}' +
    '<td>{{> piDeleteButton }}</td>');

Handlebars.registerPartial('piRow', '<tr class="pi-row {{#if pi_rejeitada }} alert-danger {{/if}}">{{> piRowColumns}}</tr>');

var defaultObjectPi = {
    id:null,
    index:null,
    canUpload:true,
    owner_id:null,
    owner_type:"",
    pdf:"",
    pi_numero:"",
    pi_rejeitada:false,
    hasPermissionToReject:false
};

/**
 * Compile Template
 *
 * param template
 * param context
 * returns string
 */
function templateCompile(template, context)
{
    var source;

    if( template.startsWith('#') || template.startsWith('.') ){
        source = $(template).html();
    }else{
        source = template;
    }

    var handlebars  = Handlebars.compile(source);

    return handlebars(context);
}

/**
 *
 * @param content
 * @param target
 * @param append
 */
function renderHtml(content,target, append)
{
    append = append || true;

    if( append ) {
        $(target).append(content);
    }else{
        $(target).html(content);
    }
}

/**
 *
 * @param context
 * @param target
 * @param append
 */
function piRowColumnsCompile(context, target, append)
{
    var html = templateCompile('{{> piRowColumns}}', context || {});
    renderHtml(html,target, append);
}

/**
 *
 * @param context
 * @param target
 * @param append
 */
function piRowCompile(context, target, append)
{
    context = Handlebars.Utils.extend(defaultObjectPi, context);
    var html = templateCompile('{{> piRow}}', context || {});
    renderHtml(html,target, append);
}

/**
 *
 * @param context
 * @param target
 * @param append
 */
function piRowsCompile(context, target, append)
{
    target = target || '#pi-table-body';
    Handlebars.Utils.extend(context, {});

    if( context.pis ){
        var index = 1;
        for (var key in context.pis) {
            context.pis[key].index = index++;
        }
    }

    var html = templateCompile('{{#each pis}} {{> piRow}} {{/each}}', context || {});

    renderHtml(html,target, append);
}

jQuery(function($){
    $(document).on('click', '[data-action="add-pi-row"]', function(e) {
        e.preventDefault();

        var $el         = $(this);
        var owner       = $el.data('owner') || 'peca';
        var inputName   = $el.data('input-name') || 'pecas';
        var target      = $($el.data('action-target') || $el.parent().parent().parent().parent().find('.pi-table-body'));
        var pisRows     = target.find('.pi-row');
        var pisCount    = pisRows.length || 0;
        var data        = {};

        if( owner == 'peca' ){
            data.peca_index = $el.data('peca-index') || 0;
        }else{
            data.peca_index = null;
        }

        data.index      = pisCount + 1;
        data.input_name = inputName;

        piRowCompile(data, target);
    });

    $(document).on('click', '[data-action="delete-pi-row"]', function(e) {
        e.preventDefault();

        var $el     = $(this);
        var target  = $( $el.data('action-target') || $el.parent().parent() );

        if( target ){
            target.remove();
        }
    });

    $('[data-pi-init]').each(function(){

        var $el     = $(this);
        var data    = $el.data('pi-init');

        piRowsCompile(data, $el, false);
    });
});