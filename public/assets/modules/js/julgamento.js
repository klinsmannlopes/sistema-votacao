jQuery(function($)
{
    $('#limpar').click(function()
    {
        $('#modal-confirm-limpar').modal('show');
    });

    $('#confirm-limpar').click(function()
    {
        $('.number').val('');
        $('#modal-confirm-limpar').modal('hide');
    })

    $('#salvar').click(function()
    {
        if(validarNotas())
            $('#modal-confirm-finalizar').modal('show');
        else
            window.alert('Você deve avaliar todos os critérios e inscrições');
    });

    $('#confirm-finalizar').click(function()
    {
        if(validarNotas())
            $('#finalizar').submit();
        else
            window.alert('Você deve avaliar todos os critérios e inscrições');
    });

    function validarNotas()
    {
        var valid = true;

        $('input.number').each(function()
        {
            if($(this).val() == '')
            {
                $(this).parent().removeClass('has-success');
                $(this).parent().addClass('has-error');

                valid = false;
            }
            else
            {
                $(this).parent().removeClass('has-error');
                $(this).parent().addClass('has-success');
            }
        });

        return valid;
    }

    //BEFORE UNLOAD
    if( $('form').length )
    {
        window.onbeforeunload = function (e)
        {
            return "Tem certeza que deseja sair desta página?";
        };
    }

    $('form').submit(function()
    {
        window.onbeforeunload = null;
    });

    $('#finalizar .number').on('blur', function()
    {
        var value = $(this).val().replace(',', '.');

        if(value >= NOTA_MINIMA && value <= NOTA_MAXIMA)
            $(this).parents('.form-group').removeClass('has-error');
        else
            $(this).parents('.form-group').addClass('has-error');
    });
});