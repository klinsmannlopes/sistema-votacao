jQuery(function($)
{
    TEXTAREA_MAX = 1200;

    $(document).ready(function(){
        var categoria = $("#categoria_id");
        var cat_abrevi = $('#categoria_abreviatura').val();
        var possui_pis = categoria.find('option:selected').data('has_pis');
        if(possui_pis==0){
            $('.inputs-categoria-fixa.pis').addClass('hidden')
        }else{
            $('.inputs-categoria-fixa.pis').removeClass('hidden')
        }

        var abreviatura = categoria.find('option:selected').attr('data-abreviatura');
        console.log(cat_abrevi);
        if (cat_abrevi == ""){
            $("#categoria_abreviatura").val(abreviatura);
        }


    });

    $('.check-all').click(function()
    {
        $('tbody input[type=checkbox]').prop('checked', $(this).is(':checked'));
    });

    $('.check-all-opecs').click(function()
    {
        $('#multiplo_opec tbody input[type=checkbox]').prop('checked', $(this).is(':checked'));
        mudarBotaoAssociar();
    });

    function pecasPorInscricao(qtd_pecas)
    {
        var contador = 0;
        $(".numeroPeca").each(function( index ){
            contador++;
        });
        if(qtd_pecas >= contador)
            return true;
    }
    $('.remove-peca').click(function(){
        var contador = 0;
        $(".numeroPeca").each(function( index ){
            contador++;
        });
        contador--;
        if(contador <= CATEGORIA.qtd_pecas){
            $('.peca .remove-peca').css('display','none');
        }
    });

    function removendoBotoesDeRemocao()
    {
        if(CATEGORIA.qtd_pecas_tipo === 'exatamente')
        {
            $('#add-peca').addClass('hidden');

            for(var i = 0; i < CATEGORIA.qtd_pecas; i++)
                $('#add-peca').trigger('click');

            $('.peca .remove-peca').remove();
        }

        if(CATEGORIA.qtd_pecas_tipo === 'minimo')
        {
            $('#add-peca').removeClass('hidden');

            for(var i = 0; i < CATEGORIA.qtd_pecas; i++)
                $('#add-peca').trigger('click');

            if(pecasPorInscricao(CATEGORIA.qtd_pecas)){
                $('.peca .remove-peca').remove();
            }
        }

        if(CATEGORIA.qtd_pecas_tipo === 'maximo')
        {
            $('#add-peca').removeClass('hidden').trigger('click');
            $('.peca .remove-peca').remove();
        }
    }

    function hideAndShowAddPeca()
    {
        if(typeof CATEGORIA !== 'undefined')
        {
            if(CATEGORIA.qtd_pecas_tipo === 'maximo')
            {
                if($('.peca').length >= CATEGORIA.qtd_pecas)
                    return $('#add-peca').addClass('hidden');

                $('#add-peca').removeClass('hidden');
            }
        }
    }

    function verifyCountPecas()
    {
        $('.peca .panel-heading b').each(function()
        {
            var index = parseInt($(this).index('.peca .panel-heading b')) + 1;
            $(this).html('Peça '+ index);
        });
    }

    function showHideDescricaoAudio()
    {
        var attr = $("#categoria_id").find('option:selected').attr('data-tipos_de_anexo');

        if(attr)
        {
            var tiposDeAnexo = $.parseJSON(attr);

            if(tiposDeAnexo && tiposDeAnexo.indexOf('audio') === -1)
                $('.wrapper-descricao_audio').addClass('hidden');
            else
                $('.wrapper-descricao_audio').removeClass('hidden');
        }
    }

    function changeCategoria(confirmed)
    {

        var old_value = $("#categoria_id").data('old');

        if(!confirmed)
        {
            categoria.val(old_value);
        }
        else
        {
            categoria.data('old', categoria.val());

            var abreviatura = categoria.find('option:selected').attr('data-abreviatura');
            var possui_pis = categoria.find('option:selected').data('has_pis');

            $("#categoria_abreviatura").val(abreviatura);
            
            $('.pis-inscricao').addClass('hidden');
            $('.categoria-midia').addClass('hidden');
            $('.categoria-planejamento').addClass('hidden');
            $('.categoria-nao-fixa').addClass('hidden');
            $('.inputs-categoria-fixa').addClass('hidden');

            switch(abreviatura)
            {
                case '':
                    $(this).val('');
                    break;
                case 'MID':
                    TEXTAREA_MAX = 1200;

                    $('.categoria-midia').removeClass('hidden');
                    $('.pis-inscricao').removeClass('hidden');
                    $('.pis-inscricao .pi-row').remove();
                    break;
                case 'PLA':
                    TEXTAREA_MAX = 1200;

                    $('.categoria-planejamento').removeClass('hidden');
                    $('.pis-inscricao').removeClass('hidden');
                    $('.pis-inscricao .pi-row').remove();
                    break;
                default:
                    TEXTAREA_MAX = 255;

                    $('.peca').remove();

                    if(CATEGORIA = categorias[categoria.val()])
                        removendoBotoesDeRemocao();

                    removeInputs(abreviatura);
                    $('.categoria-nao-fixa').removeClass('hidden');
            }

            if(possui_pis==0){
                $('.inputs-categoria-fixa.pis').addClass('hidden')
            }else{
                $('.inputs-categoria-fixa.pis').removeClass('hidden')
            }

            showHideDescricaoAudio();

        }
    }

    //ARTIFICIO PARA SALVAR O VALOR ANTES DO CHANGE SER DISPARADO
    var categoria = $("#categoria_id");
    var abreviatura = categoria.find('option:selected').attr('data-abreviatura');

    categoria.data('old', categoria.val());
    categoria.on('change', function()
    {
        if($('.peca').length)
        {
            window.confirm('Ao alterar a categoria todas as peças serão perdidas, deseja continuar?', changeCategoria);
        }
        else //ON CREATE
            changeCategoria(true);
    });

    if(categoria.val() !== '')
    {
        switch($('#categoria_id option:selected').attr('data-abreviatura'))
        {
            case '':
                $(this).val('');
            break;
            case 'MID':
                TEXTAREA_MAX = 1200;
                $('.categoria-midia, .pis-inscricao').removeClass('hidden');
                break;
            case 'PLA':
                TEXTAREA_MAX = 1200;
                $('.categoria-planejamento, .pis-inscricao').removeClass('hidden');
                break;
            default:
                TEXTAREA_MAX = 255;
                removeInputs($('#categoria_id option:selected').attr('data-abreviatura'));
                $('.categoria-nao-fixa').removeClass('hidden');
        }
    }

    showHideDescricaoAudio();

    $(document).on('change', '.peca .tipo', function()
    {
        var tipoSelected = $(this).val();
        var siblings = $(this).parents('.peca').find('.veiculo, .formato');

        if(!tipoSelected)
            return siblings.val('').prop('disabled', true);

        siblings.prop('disabled', false);

        siblings.each(function()
        {
            $(this).find('option:not([value=""])').addClass('hidden').prop('disabled', true);
            $(this).find('option[data-tipo="'+ tipoSelected +'"]').removeClass('hidden').prop('disabled', false);

            $(this).val('');
        });
    });

    //VERIFICA SE O FORM JÁ VEM COM PEÇAS
    //E FAZ AS ALTERAÇÕES NECESSÁRIAS PARA QUE O FORM SEJA UTILIZADO
    if($('.peca').length && (typeof readonly === 'undefined' || !readonly))
    {
        //FAZ COM QUE OS CAMPOS DISABLEDS TORNEM-SE ENABLE
        $('.peca:not(.nao-rejeitada) select:disabled').prop('disabled', false);

        //REMOVE OS BOTÕES DE REMOVER PEÇA DE ACORDO COM A CATEGORIA SELECIONADA
        if(CATEGORIA = categorias[$('#categoria_id').val()])
            removendoBotoesDeRemocao();

        //PERCORRE CADA PEÇA > TIPO, ESCONDENDO AS OPÇÕES INDEVIDAS DE ACORDO COM O TIPO SELECIONADO
        $('.peca .tipo').each(function()
        {
            var tipoSelected = $(this).val();
            var siblings = $(this).parents('.peca').find('.veiculo, .formato');

            siblings.each(function()
            {
                var value = $(this).val();

                $(this).find('option:not([value=""])').addClass('hidden').prop('disabled', true);
                $(this).find('option[data-tipo="'+ tipoSelected +'"]').removeClass('hidden').prop('disabled', false);

                $(this).val(value);
            });
        });
    }

    $(document).on('change', '.formato', function()
    {
        var formato_diferenciado = $(this).parents('.form-group').siblings('.wrapper-formato-diferenciado');

        if($(this).val() === 'Diferenciado')
            formato_diferenciado.removeClass('hidden');
        else
            formato_diferenciado.addClass('hidden');
    });

    $('.formato').each(function()
    {
        if($(this).val() === 'Diferenciado')
            $(this).trigger('change');
    });

    $(document).on('click', '.remove-peca', function()
    {
        $(this).parents('.peca').remove();

        hideAndShowAddPeca();
        verifyCountPecas();
    });

    $(document).on('click', '#add-peca', function()
    {
        var index = 0;

        if($('.peca').length)
        {
            $('.peca').each(function()
            {
                var dataIndex = parseInt( $(this).attr('data-index') );

                if( dataIndex > index )
                    index = dataIndex;
            });
        }

        ++index;

        $('.pecas').append(pecaHTML.replace(/%INDEX%/g, index));

        hideAndShowAddPeca();
        showHideDescricaoAudio();
        verifyCountPecas();
        removeInputs($('#categoria_id option:selected').attr('data-abreviatura'));
    });

    $('.table tbody td').click(function()
    {
        if(!$(this).find('a,button,input[type=checkbox]').length)
        {
            var seletor = $(this).parents('tr').find('input[type=checkbox]');
            seletor.prop('checked', !seletor.is(':checked'));
        }
    });

    $('.show-modal-cofirm-mass-destroy').click(function()
    {
        if($('.table tbody input[type=checkbox]:checked').length)
            $('#modal-mass-destroy').modal('show');
        else
            window.alert('Selecione pelo menos um registro!');
    });

    $('#confirmar-remover-selecionados').click(function()
    {
        var ids;
        if(ids = $('.table tbody input[type=checkbox]:checked').map(function(){ return $(this).val() }).get())
        {
            var form = $('#remover-selecionados');

            form.attr('action', form.attr('action').replace('ids', ids.join()));
            form.submit();
        }
    });

    $('.show-modal-associar-opec').click(function()
    {
        if($('.table tbody input[type=checkbox]:checked').length)
            $('#modal-associar-opec').modal('show');
        else
            window.alert('Selecione pelo menos uma inscrição!');
    });

    $('#confirmar-associar-opec').click(function()
    {

        if( $('#usuarios-opec').css('display') == 'block' ) {
            var ids = $('.inscricoes_table tbody input[type=checkbox]:checked').map(function(){ return $(this).val() }).get();
            var user = $('#usuarios-opec');
            var user_id = user.val();

            if(user_id !== '')
            {
                user.parent().removeClass('has-error');
                user.siblings('.help-block').addClass('hidden');

                if(ids)
                {
                    var form = $('#associar-opec-selecionados');

                    form.attr('action', form.attr('action').replace('ids', ids.join()).replace('user_id', user_id));
                    form.submit();
                }
            }
            else
            {
                user.parent().addClass('has-error');
                user.siblings('.help-block').removeClass('hidden');
            }
        } else if ( $('#usuarios-opec').css('display') == 'none' ) {
            var ids = $('.table tbody input[type=checkbox]:checked').map(function(){ return $(this).val() }).get();
            var itens = $("input[name=usuarios-opec]:checked")
            var user_id = []
            for (var i = 0; i<itens.length; i++) {
                user_id.push(itens[i].value)
            };

            if(ids)
            {
                var form = $('#associar-opec-selecionados');

                form.attr('action', form.attr('action').replace('ids', ids.join()).replace('user_id', user_id));
                form.submit();
            }
        }
        
    });

    $('#habilitar-cancelamento').click(function()
    {
        $('#cancelar').prop('disabled', !$(this).is(':checked'));
    });

    $('.show-modal-enviar-para-correcao').click(function()
    {
        //SE EXISTIR PEÇAS E NENHUM ESTIVER REJEITADA
        if($('.rejeitar_peca, .rejeitar_pi').length && !$('.rejeitar_peca:checked, .rejeitar_pi:checked').length)
            return window.alert('Indique a(s) peça(s) ou P.I.(s) rejeitada(s).');

        //VERIFICA SE O USUÁRIO REJEITOU ALGUMA DAS PEÇAS
        $('#modal-enviar-para-correcao').modal('show');
    });

    $('#modal-enviar-para-correcao').on('show.bs.modal', function (e)
    {
        $('#justificativa').parent().removeClass('has-error');
        $('.justificativa-error').hide();
    });

    $('.show-modal-pendente-para-revisao').click(function()
    {
        $('#modal-pendente-para-revisao').modal('show');
    });

    function showErrorJustificativa(justificativa)
    {
        var message = '<b>*</b> O campo Justificativa deve ser preenchido';
        var errorPlaceholder = justificativa.siblings('small.justificativa-error');

        if(errorPlaceholder.length)
            errorPlaceholder.html(message).show();
        else
            justificativa.after('<small class="justificativa-error text-danger text-center">'+ message +'</small>');

        return justificativa.parents('.form-group').addClass('has-error');
    }

    $(document).on('click', '#confirmar-enviar-para-correcao', function()
    {
        var pis_rejeitadas = $('.rejeitar_pi:checked').map(function(){ return $(this).val(); }).get();
        var pecas_rejeitadas = $('.rejeitar_peca:checked').map(function(){ return $(this).val(); }).get();

        if($('.rejeitar_peca, .rejeitar_pi').length && (!pis_rejeitadas.length && !pecas_rejeitadas.length))
            return window.alert('Indique a(s) peça(s) ou P.I.(s) rejeitada(s).');

        var justificativa = $('#justificativa');
        if(justificativa.val() == '')
            return showErrorJustificativa(justificativa);

        justificativa.addClass('has-success');
        $('.justificativa-error').hide();

        if(pis_rejeitadas.length || pecas_rejeitadas.length)
        {
            var html = '';

            if(pis_rejeitadas.length)
                html += '<input type="hidden" name="pis_rejeitadas" id="pis_rejeitadas" value="">';

            if(pecas_rejeitadas.length)
                html += '<input type="hidden" name="pecas_rejeitadas" id="pecas_rejeitadas" value="">';

            
            $('#enviar-para-correcao').append(html);
            
            $('#pis_rejeitadas').val(pis_rejeitadas.join());
            
            $('#pecas_rejeitadas').val(pecas_rejeitadas.join());
        }

        $('#enviar-para-correcao').submit();
    });

    $('.show-modal-cancelar').click(function()
    {

        if($('.rejeitar_pi, .rejeitar_peca').length)
            if(! $('.rejeitar_pi:checked, .rejeitar_peca:checked').length)
                return window.alert('Indique a(s) peça(s) ou P.I.(s) rejeitada(s).');

        $('#modal-cancelar').modal('show');
    });

    $('#modal-cancelar').on('hidden.bs.modal', function(e)
    {
        $('#justificativa-cancelamento').parent().removeClass('has-error');
        $('.justificativa-error').hide();
    });

    $(document).on('click', '#confirmar-cancelar', function()
    {
        var justificativa = $('#justificativa-cancelamento');

        if(justificativa.val() === '')
            return showErrorJustificativa(justificativa);

        $('.justificativa-error').hide();
        justificativa.parents('.form-group').removeClass('has-error').addClass('has-success');

        $('#cancelar').submit();
    });

    $('#show-modal-cancelar-participante').click(function()
    {
        $('#modal-cancelar-participante').modal('show');
    });

    $(document).on('click', '#confirmar-cancelar-participante', function()
    {
        $('#cancelar-participante').submit();
    });

    $('#edit-form').submit(function()
    {
        $('[disabled]').prop('disabled', false)
    });


    $(document).on('keydown', 'textarea', function(e)
    {
        var qtd = $(this).val().length;

        var faltante = 1000 - qtd;

        if(faltante <= 0 && (e.keyCode !== 8 && e.keyCode !== 46))
            e.preventDefault();

        if($(this).siblings('.counter-letters').length)
            $(this).siblings('.counter-letters').html(faltante);
        else
            $(this).after('<span class="counter-letters pull-right label label-primary">'+ (faltante) +'</span>');
    });

    $(document).on('blur change', 'textarea', function()
    {
        if($(this).val().length > TEXTAREA_MAX)
            $(this).val($(this).val().substr(0, TEXTAREA_MAX));

        $(this).trigger('keydown');
    });

    $('#aprovar').submit(function()
    {
        if($('.rejeitar_peca:checked, .rejeitar_pi:checked').length)
        {
            window.alert('Para aprovar uma inscrição não pode existir peça ou P.I. rejeitada.');
            return false;
        }
    });

    $('#validar').submit(function()
    {
        if($('.rejeitar_peca:checked, .rejeitar_pi:checked').length)
        {
            window.alert('Para validar uma inscrição não pode existir peça ou P.I. rejeitada.');
            return false;
        }
    });

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });



    $(document).on('change', 'input[type=file]', function(e)
    {
        var field = $(this);
        var progress = $(this).siblings('.progress');
        var fieldName = $(this).attr('name');

        var formData = new FormData();
        formData.append(fieldName, e.target.files[0]);
        formData.append('categoria_id', $('#categoria_id').val());

        var route = $(this).hasClass('peca') ? uploadPecaRoute : uploadPIRoute;
        var is_peca = $(this).hasClass('peca');

        $.ajax({
            url: route,
            type: 'POST',
            data: formData,
            success: function(data)
            {
                window.setTimeout(function()
                {
                    field.val('');

                    progress.addClass('hidden');

                    if(typeof data === 'object')
                        return window.alert(is_peca ? data.arquivo.join('<br/>') : data.pdf.join('<br/>'));

                    if(typeof data === 'string')
                    {
                        field.siblings('.uploaded').html('');
                        return field.siblings('.uploaded').html(data);
                    }
                }, 1000);
            },
            cache: false,
            contentType: false,
            processData: false,
            xhr: function()
            {
                var myXhr = $.ajaxSettings.xhr();

                if(myXhr.upload)
                {
                    myXhr.upload.addEventListener("progress", function(evt)
                    {
                        if(evt.lengthComputable)
                        {
                            progress.removeClass('hidden');

                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            progress.find('.progress-bar').css('width', percentComplete + '%').html(percentComplete + '%');
                        }
                    }, false);
                }

                return myXhr;
            }
        });
    });

    $(document).on('click', '.remover-arquivo', function(){
        $(this).parents('.uploaded').html('');
    });

    var selecionado =  $('#categoria_id').find(":selected").attr('data-abreviatura');
  
    if(typeof selecionado !== 'undefined')
    {
        var select = removerAcentos(selecionado);

        if(select == 'mid' || select == 'pla' ){
            $(".criacao_label").html('Responsável Pela Defesa <b class="text-danger">*</b>');
        }
    }
  
    $(document).on('change', '.cartegoria_select', function(){
           var selecionado =  $('#categoria_id option:selected').attr('data-abreviatura');
           
           if(selecionado !== 'undefined')
           {
               var select = removerAcentos(selecionado);
               
               if( select == 'mid' || select == 'pla' ){
                    $(".criacao_label").html('Responsável Pela Defesa <b class="text-danger">*</b>');
               }
               else{
                   $(".criacao_label").html('Criação <b class="text-danger">*</b>');
               }
           }  
     });

    function removeInputs(category)
    {
        if(category == 'CSOL') {
            $('.inputs-categoria-fixa').addClass('hidden');
            $('#descricao-termo').html("Autorizo os organizadores do GP Verdes Mares a utilizar os materiais inscritos para veiculação, caso premiado.")
        }
        else {
            $('.inputs-categoria-fixa').removeClass('hidden');
            $('#descricao-termo').html("Autorizo os organizadores do GP Verdes Mares a utilizar os materiais inscritos para divulgação do Prêmio.")
        }
    }


     // SELEÇÃO DE MULTIPLAS OPECS

     function getInscricoesSelected(){
        var checkeds = $(".hidden-print input[type=checkbox]:checked")
        inscricoes = [];
        for(var i=0;i<checkeds.length;i++){
            inscricoes.push({inscricao_id:checkeds[i].value})
        }   
        return inscricoes
    }

    function verifyAuthInscricoesSelected(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        return new $.ajax({
            url: document.URL,
            type: 'GET',
            data: {_token: CSRF_TOKEN, inscricoes:inscricoes},
            dataType: 'JSON'
        }); 
    }

    function showMultiploOPEC(qtd_opecs){
        $("input[name=qtd_opecs]").val(qtd_opecs)
        $('#modal-associar-opec .modal-body #multiplo_opec').show()
        $('.unico_opec').hide()
    }

    function showUnicoOPEC(){
        $('#modal-associar-opec .modal-body #multiplo_opec').hide()
        $('.unico_opec').show()
    }

    function setarAlerta(mensagem){
        $("#msg_multiplo").text(mensagem)
        $("#div_msg_multiplo").show()
        /*setTimeout(function(){
            $("#div_msg_multiplo").hide()    
        }, 5000)*/
    }

    function setErros(erros){
        if(erros>0){
            $("input[name=qtd_opecs]").attr('qtd_erro', erros)
            $('#modal-associar-opec .modal-body .form-group').hide()
            $("#div_msg_multiplo").removeClass('alert-warning')
            $("#div_msg_multiplo").addClass('alert-danger')    
            $('#confirmar-associar-opec').prop('disabled', true)  
        }else{
            $('#modal-associar-opec .modal-body .form-group').show()
            $("#div_msg_multiplo").removeClass('alert-danger')    
            $("#div_msg_multiplo").addClass('alert-warning')
            $('#confirmar-associar-opec').prop('disabled', false)  
        }
    }

    function isMultiploOPEC(){
        var inscricoes = getInscricoesSelected()
        verifyAuthInscricoesSelected(inscricoes).done(function(response){
            if(response.action=='success'){
                setarAlerta(response.message)
                showMultiploOPEC(response.qtd_opecs)
                setErros(response.erro)
            }else if(response.action=='error'){
                setErros(response.erro)
                setarAlerta(response.message)
                showUnicoOPEC()
            }
        })
    }

    function mudarBotaoAssociar(){
        var items_selected  = $("input[name=usuarios-opec]:checked")
        var qtd_opecs       = $("input[name=qtd_opecs]").val()
        if( items_selected.length <= qtd_opecs ) {
            $('#confirmar-associar-opec').prop('disabled', false) 
        } else if( items_selected > qtd_opecs ) {
            $('#confirmar-associar-opec').prop('disabled', true)  
            setarAlerta("Regra de parametrização: você só pode selecionar no máximo "+qtd_opecs+" OPEC.")  
        }
    }

    $("input[name=usuarios-opec]").on('change', function(r){
        mudarBotaoAssociar();
    })

    $('.show-modal-associar-opec').on('click', function(){
        isMultiploOPEC();
    })

    /**
     * Remove acentos de caracteres
     * @param  {String} stringComAcento [string que contem os acentos]
     * @return {String}                 [string sem acentos]
     */
    function removerAcentos( newStringComAcento ) {
      var string = newStringComAcento.toLowerCase();
        var mapaAcentosHex  = {
            a : /[\xE0-\xE6]/g,
            e : /[\xE8-\xEB]/g,
            i : /[\xEC-\xEF]/g,
            o : /[\xF2-\xF6]/g,
            u : /[\xF9-\xFC]/g,
            c : /\xE7/g,
            n : /\xF1/g
        };
        for ( var letra in mapaAcentosHex ) {
            var expressaoRegular = mapaAcentosHex[letra];
            string = string.replace( expressaoRegular, letra );
        }
        return string;
    }

});