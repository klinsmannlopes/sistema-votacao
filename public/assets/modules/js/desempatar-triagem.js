jQuery(function($)
{
    $('#limpar-ordenacao').click(function()
    {
        $('#modal-confirm-limpar').modal('show');
    });

    $('#confirm-limpar-ordenacao').click(function()
    {

        $('select.form-control.text-center.ordenacao').val('');
        $('.ordenacao').attr('checked',false);
        $('#modal-confirm-limpar').modal('hide');
    });

    $('#show-modal-confirm-desempatar').click(function()
    {
        $('#modal-confirm-desempatar').modal('show');
    });

    $('#confirm-desempatar').click(function()
    {
        $('#desempatar').submit();
    });

    //BEFORE UNLOAD
    if( $('form').length )
    {
        window.onbeforeunload = function (e)
        {
            return "Tem certeza que deseja sair desta página?";
        };
    }

    $('form').submit(function()
    {
        window.onbeforeunload = null;
    });
});