jQuery(function($)
{
    $('#categoria_id').change(function()
    {
        var id = $(this).val();

        if(id !== '')
        {
            var url = $(this).attr('data-url');
            return window.location.href = url.replace('id', id);
        }
    });
});