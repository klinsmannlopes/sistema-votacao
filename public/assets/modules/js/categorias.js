jQuery(function($)
{
    $(document).on('keyup', '#nome', function()
    {
        var value = $(this).val();

        if($('#abreviatura').val().length < 5)
            $('#abreviatura').val(value.substr(0,5));
    });

    $('#qtd_pecas').mask('00');

    if( $("input[name='qtd_opecs']").val() == 0 && $("input[name='is_multiplo_opec']:checked").val() == 0 ) {
    	$('label#qtd_opec').hide();
    	$('div#qtd_opec').hide();
    }else if( $("input[name='qtd_opecs']").val() == 0 && $("input[name='is_multiplo_opec']:checked").val() == 1 ){
    	$('label#qtd_opec').show();
    	$('div#qtd_opec').show();
    }

    $("input[name='is_multiplo_opec']").on('change', function(){
    	if( $("input[name='is_multiplo_opec']:checked").val() == 1 ) {
    		$('label#qtd_opec').show();
    		$('div#qtd_opec').show();
    	}else{
    		$("input[name='qtd_opecs']").val(0);
    		$('label#qtd_opec').hide();
    		$('div#qtd_opec').hide();
    	}
    })
    
});