jQuery(function($)
{
    $('#role').change(function()
    {
        if($(this).val() === 'participante')
            $('.participante').removeClass('hidden');
        else
            $('.participante').addClass('hidden');
    })
    .trigger('change');

    //MASKS
    $('.cnpj').mask('00.000.000/0000-00');
    $('.phone').mask('(00) 000000009');
});