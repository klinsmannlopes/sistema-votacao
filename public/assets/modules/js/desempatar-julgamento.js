jQuery(function($)
{
    $('#categoria_id').change(function()
    {
        var id = $(this).val();

        if(id !== '')
        {
            var attrName = $(this).find('option:selected').attr('data-attr-url');

            var url = $(this).attr(attrName);

            return window.location.href = url.replace('id', id);
        }
    });
});