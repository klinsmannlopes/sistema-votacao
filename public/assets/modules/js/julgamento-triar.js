jQuery(function($)
{
    $('#cofirm-clear-checkboxes').click(function()
    {
        if($('.table input[type=checkbox]').is(':checked'))
            $('#modal-clear-checkboxes').modal('show');
    });

    $('#clear-checkboxes').click(function()
    {
        $('.table input[type=checkbox]').prop('checked', false);
        $('#modal-clear-checkboxes').modal('hide');
    });

    $('.table td').click(function()
    {
        if(! $(this).find('a,button,input[type=checkbox]').length)
        {
            var seletor = $(this).parents('tr').find('input[type=checkbox]');
            var isChecked = seletor.is(':checked');

            seletor.prop('checked', ! isChecked);
        }
    });

    $(document).on('click', '#enviar-triagem', function()
    {
        var errorMessage;
        var qtdInscricoesSelecionadas = $('.inscricoes:checked').length;

        if(qtdInscricoesSelecionadas < 10)
            errorMessage = 'Você deve escolher 10 inscrições para a próxima fase.';

        if(qtdInscricoesSelecionadas > 10)
            errorMessage = 'Você não pode escolher mais de 10 inscrições para a próxima fase.';

        if(errorMessage)
        {
            window.alert(errorMessage);
            return false;
        }
        else
        {
            window.confirm('Tem certeza que deseja finalizar a triagem ?', function(confirmed)
            {
                if(confirmed)
                    $('#triar').submit();
            });
        }
    });

    $(window).on('beforeunload', function()
    {
        var qtdSelecionados = $('.inscricoes:checked').length;

        if(qtdSelecionados && qtdSelecionados !== 10)
            return 'Tem certeza que deseja sair desta página ?';
    });

    // ALERTA PARA 10 CHECKBOX MARCADOS
    $('.check').click(function () {
       
        var checkbox = $('input:checkbox[name^=inscricoes]:checked');
      
        if(checkbox.length > 0){
            var val = [];
            checkbox.each(function(){
                val.push($(this).val());
            });

            if(val.length == 11){
                window.alert('Você não pode escolher mais de 10 inscrições para a próxima fase.');
            }
        } 
    });
});