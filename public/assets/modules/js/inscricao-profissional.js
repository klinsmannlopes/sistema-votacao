jQuery(function($) {

    $(document).ready(function(){
        var possui_pis = categoria.find('option:selected').data('has_pis');
        if(possui_pis==0){
            $('.inputs-categoria-fixa.pis').addClass('hidden')
        }else{
            $('.inputs-categoria-fixa.pis').removeClass('hidden')
        }
    })

    $(document).on('keydown', 'textarea', function (e) {
        TEXTAREA_MAX = 1200;

        var qtd = $(this).val().length;

        var faltante = TEXTAREA_MAX - qtd;

        if (faltante <= 0 && (e.keyCode !== 8 && e.keyCode !== 46))
            e.preventDefault();

        if ($(this).siblings('.counter-letters').length)
            $(this).siblings('.counter-letters').html(faltante);
        else
            $(this).after('<span class="counter-letters pull-right label label-primary">' + (faltante) + '</span>');
    });


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

    $(document).on('change', 'input[type=file]', function (e) {
        var field = $(this);
        var progress = $(this).siblings('.progress');
        var fieldName = $(this).attr('name');

        var formData = new FormData();
        formData.append(fieldName, e.target.files[0]);
        formData.append('categoria_id', $('#categoria_id').val());

        var route = $(this).hasClass('curriculo') ? uploadCurriculo : uploadFoto;
        var arquivo = $(this).hasClass('curriculo');

        $.ajax({
            url: route,
            type: 'POST',
            data: formData,
            success: function (data) {
                window.setTimeout(function () {
                    progress.addClass('hidden');

                    if (typeof data === 'object')
                        return window.alert(arquivo ? data.curriculo.join('<br/>') : data.foto.join('<br/>'));

                    if (typeof data === 'string') {
                        field.siblings('.uploaded').html('');
                        return field.siblings('.uploaded').html(data);
                    }

                }, 1000);
            },
            cache: false,
            contentType: false,
            processData: false,
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();

                if (myXhr.upload) {
                    myXhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            progress.removeClass('hidden');

                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            progress.find('.progress-bar').css('width', percentComplete + '%').html(percentComplete + '%');

                        }
                    }, false);
                }

                return myXhr;
            }
        });
    });

    $(document).on('click', '.remover-arquivo', function () {
        $(this).parents('.uploaded').html('');
    });

    $('.show-modal-cancelar').click(function(){

        $('#modal-cancelar').modal('show');
    });

    function showErrorJustificativa(justificativa)
    {
        var message = '<b>*</b> O campo Justificativa deve ser preenchido';
        var errorPlaceholder = justificativa.siblings('small.justificativa-error');

        if(errorPlaceholder.length)
            errorPlaceholder.html(message).show();
        else
            justificativa.after('<small class="justificativa-error text-danger text-center">'+ message +'</small>');

        return justificativa.parents('.form-group').addClass('has-error');
    }

    $(document).on('click', '#confirmar-cancelar', function()
    {
        var justificativa = $('#justificativa-cancelamento');

        if(justificativa.val() === '')
            return showErrorJustificativa(justificativa);

        $('.justificativa-error').hide();
        justificativa.parents('.form-group').removeClass('has-error').addClass('has-success');

        $('#cancelar').submit();
    });


    $('#show-modal-cancelar-participante').click(function()
    {
        $('div#modal-cancelar-participante').modal('show');
    });


    $(document).on('click', '#confirmar-cancelar-participante', function()
    {
        $('#cancelar-participante').submit();
    });


    $('#aprovar').submit(function()
    {
        if($('.rejeitar_peca:checked, .rejeitar_pi:checked').length)
        {
            window.alert('Para aprovar uma inscri��o n�o pode existir pe�a ou P.I. rejeitada.');
            return false;
        }
    });


});

