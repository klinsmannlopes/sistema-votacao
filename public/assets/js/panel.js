jQuery(function($)
{
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $('tbody tr[data-url] td').click(function()
    {
        if(!$(this).has('button,a').length)
            return window.location.href = $(this).parents('tr').attr('data-url');
    });

    $('button.show-modal-destroy').click(function()
    {
        DESTROY_ROW_ID = $(this).parents('form').attr('data-row-id');
        $('#modal-destroy').modal('show');
    });

    $('button.confirm-row-destroy').click(function()
    {
        $(this).button('loading');
        $('form[data-row-id='+ DESTROY_ROW_ID +']').submit();
    });

    $('.datepicker').datepicker({
        language: "pt-BR",
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom'
    });

    $(".datetimepicker").datetimepicker({
        language: 'pt-BR',
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 0,
        fontAwesome: 1,
    });

    window.alert = function(message, title)
    {
        var modal = $('#alert');

        if(typeof message !== 'undefined')
            modal.find('.msg').html(message);

        if(typeof title !== 'undefined')
            modal.find('.modal-title span').html(title);

        modal.modal('show');
    };

    window.confirm = function(message, callback)
    {
        var modal = $('#confirm');

        modal.find('.modal-body .msg').html(message);

        modal.modal('show');

        modal.find('.no').click(function()
        {
            if(typeof callback === 'function')
                callback(false);

            modal.modal('hide');
        });

        modal.find('.yes').click(function()
        {
            if(typeof callback === 'function')
                callback(true);

            modal.modal('hide');
        });
    };

    $(window).resize(function()
    {
        if($(this).width() <= 1366)
            $('body').addClass('mini-navbar');
        else
            $('body').removeClass('mini-navbar');
    })
    .trigger('resize');

    //MASKS
    $('.cnpj').mask('00.000.000/0000-00');
    $('.phone, .telefone, .celular').mask('(00) 000000009');
    $('.number').mask("00,00", { reverse: true });

    $('.peca_swf').mousedown(function(e){ e.preventDefault(); });
});